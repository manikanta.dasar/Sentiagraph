CREATE TABLE tbl_registeredwomen(user_Id TEXT, women_Id TEXT, Women_Image BLOB, women_name TEXT, date_of_adm TEXT, time_of_adm TEXT, w_age Integer, address TEXT, phone_num TEXT, del_status Integer, doc_name TEXT, nurse_name TEXT, attendant_name TEXT, gravida Integer, para Integer, hosp_no TEXT, facility TEXT, special_instructions TEXT, trans_Id INTEGER, comments TEXT, cond_while_admn Integer, del_comments TEXT, del_Time TEXT, del_date TEXT, del_result1 Integer, no_of_children INTEGER ,babywt1 INTEGER,babywt2 INTEGER, babysex1 Integer, babysex2 Integer , gestation_age INTEGER, del_result2 Integer, MothersDeath INTEGER, del_Time2 TEXT, admitted_with TEXT,memb_pres_absent Integer, mothers_death_reason TEXT, dateinserted TEXT, state TEXT, district TEXT, taluk TEXT, facility_name TEXT, wmonth INTEGER, wyear INTEGER, datelastupdated TEXT,gestation_age_days INTEGER, thaicardno TEXT, deltype_reason TEXT, deltype_otherreasons TEXT, LMP TEXT, riskoptions TEXT, tears TEXT, episiotomy INT, suturematerial INT, bloodgroup INT,EDD TEXT,Height TEXT,Weight TEXT,AdmittedWithOther TEXT, height_unit INTEGER, extra_comments TEXT, regtype INTEGER, regtypereason TEXT, date_of_reg_entry TEXT,normaltype INTEGER, regtypeOtherReason TEXT);
CREATE TABLE tbl_ObjectTypeMaster (object_Id NUMERIC, object_Type TEXT);
INSERT INTO tbl_ObjectTypeMaster VALUES(1,'FetalHeartRate');
INSERT INTO tbl_ObjectTypeMaster VALUES(2,'AmnioticFluidMoulding');
INSERT INTO tbl_ObjectTypeMaster VALUES(3,'Dilatation');
INSERT INTO tbl_ObjectTypeMaster VALUES(4,'Contractions');
INSERT INTO tbl_ObjectTypeMaster VALUES(5,'Oxytocin');
INSERT INTO tbl_ObjectTypeMaster VALUES(6,'Drugsgiven');
INSERT INTO tbl_ObjectTypeMaster VALUES(7,'PulseandBP');
INSERT INTO tbl_ObjectTypeMaster VALUES(8,'Temperature');
INSERT INTO tbl_ObjectTypeMaster VALUES(9,'Urinetest');
INSERT INTO tbl_ObjectTypeMaster VALUES(10,'ApgarScore');
INSERT INTO tbl_ObjectTypeMaster VALUES(11,'ThirdStage');
INSERT INTO tbl_ObjectTypeMaster VALUES(12,'FourthStage');
CREATE TABLE tbl_PropertyMaster (object_Id NUMERIC, prop_Id TEXT, prop_Name TEXT);
INSERT INTO tbl_PropertyMaster VALUES(1,'1','FHR');
Insert into tbl_PropertyMaster VALUES(1,'1.2','FHRComment');
INSERT INTO tbl_PropertyMaster VALUES(2,'2.1','Amniotic_Fluid');
INSERT INTO tbl_PropertyMaster VALUES(2,'2.2','Moulding');
Insert into tbl_PropertyMaster VALUES(2,'2.3','AFMComment');
INSERT INTO tbl_PropertyMaster VALUES(3,'3.1','cervix');
INSERT INTO tbl_PropertyMaster VALUES(3,'3.2','descent_of_head');
INSERT INTO tbl_PropertyMaster VALUES(3,'3.3','hours');
Insert into tbl_PropertyMaster VALUES(3,'3.4','DilationComment');
INSERT INTO tbl_PropertyMaster VALUES(4,'4.1','no_of_contractions');
INSERT INTO tbl_PropertyMaster VALUES(4,'4.2','duration');
Insert into tbl_PropertyMaster VALUES(4,'4.3','ContractionComment');
INSERT INTO tbl_PropertyMaster VALUES(5,'5.1','Oxytocin');
INSERT INTO tbl_PropertyMaster VALUES(5,'5.2','Units');
Insert into tbl_PropertyMaster VALUES(5,'5.3','OxytocinComment');
INSERT INTO tbl_PropertyMaster VALUES(6,'6.1','Drugs_given');
Insert into tbl_PropertyMaster VALUES(6,'6.2','DrugComment');
INSERT INTO tbl_PropertyMaster VALUES(7,'7.1','pulse_rate');
INSERT INTO tbl_PropertyMaster VALUES(7,'7.2','bp_systolic');
INSERT INTO tbl_PropertyMaster VALUES(7,'7.3','bp_diastolic');
Insert into tbl_PropertyMaster VALUES(7,'7.4','PulseComment');
INSERT INTO tbl_PropertyMaster VALUES(8,'8.1','temperature');
Insert into tbl_PropertyMaster VALUES(8,'8.2','TemperatureComment');
INSERT INTO tbl_PropertyMaster VALUES(9,'9.1','protein');
INSERT INTO tbl_PropertyMaster VALUES(9,'9.2','acetone');
INSERT INTO tbl_PropertyMaster VALUES(9,'9.3','volume');
Insert into tbl_PropertyMaster VALUES(9,'9.4','UrinetestComment');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.1','appearance');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.2','pulse');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.3','grimace');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.4','activity');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.5','respiration');
Insert into tbl_PropertyMaster VALUES(10,'10.6','comment');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.1','Duration');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.2','Oxytocin');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.3','Ergometrine');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.4','PGF2alpha');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.5','Misoprost');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.6','Placenta');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.7','UterineMassage');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.8','EBL');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.9','PPH');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.10','PPH_Properties');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.11','RetainedPlacenta');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.12','Inversion');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.13','Comments');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.14','DateOfThirdStage');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.15','TimeOfThirdStage');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.16','createddate');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.1','Uterus');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.2','UrinePassed');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.3','Bleeding');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.4','Pulse_Rate');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.5','BP_Systolic');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.6','BP_Diastolic');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.7','Comments');
CREATE TABLE tbl_propertyValues (trans_id INTEGER, user_Id TEXT, women_Id TEXT,child_Id NUMERIC, object_Id NUMERIC, prop_Id TEXT, prop_Name TEXT, prop_value TEXT, date_of_entry TEXT, time_of_entry TEXT, isDanger INTEGER, dateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));
CREATE TABLE tbltransHeader ("TransDate"  TEXT,"TransStatus"  TEXT,"RequestId"  TEXT,"trans_Id"  INTEGER);
CREATE TABLE tbltransdetail ("Action"  TEXT,"TableName"  TEXT,"SQLStatement"  TEXT,"UserId"  TEXT,"trans_Id"  INTEGER);
CREATE TABLE tbl_SyncMaster (user_Id TEXT, RequestId TEXT, RequestDate TEXT, RequestStatusDate TEXT, RequestStatus TEXT);
CREATE TABLE tbl_user (user_id TEXT, password TEXT, LastWomenNumber INTEGER, LastTransNumber INTEGER, DeviceID TEXT, LastRequestNumber INTEGER, RunningId INTEGER, state TEXT, district TEXT, taluk TEXT, facility TEXT, facility_name TEXT, dateinserted TEXT,emailId TEXT, lastmodifieddate TEXT);
CREATE TABLE tbl_comment(user_Id TEXT, women_Id TEXT, object_Id NUMERIC, comments TEXT, date_of_entry TEXT, time_of_entry TEXT, dateinserted TEXT, isviewed INTEGER,child_id INTEGER);
CREATE TABLE tbl_Month (Month_Id INTEGER PRIMARY KEY, MnthName TEXT);
INSERT INTO tbl_Month VALUES(1,'Jan');
INSERT INTO tbl_Month VALUES(2,'Feb');
INSERT INTO tbl_Month VALUES(3,'Mar'); 
INSERT INTO tbl_Month VALUES(4,'Apr');
INSERT INTO tbl_Month VALUES(5,'May');
INSERT INTO tbl_Month VALUES(6,'Jun');
INSERT INTO tbl_Month VALUES(7,'Jul');
INSERT INTO tbl_Month VALUES(8,'Aug');
INSERT INTO tbl_Month VALUES(9,'Sep');
INSERT INTO tbl_Month VALUES(10,'Oct');
INSERT INTO tbl_Month VALUES(11,'Nov');
INSERT INTO tbl_Month VALUES(12,'Dec');
CREATE TABLE tbl_settings(emailid TEXT,user_Id TEXT, ipAddress TEXT, serverdb TEXT, createddate DATETIME DEFAULT CURRENT_TIMESTAMP , lastmodifieddate TEXT,customfield1 TEXT, customfield2 TEXT, evalEndDate DATETIME);
create table tbl_referral(user_Id TEXT, women_Id TEXT, womenname TEXT, facility TEXT, facilityname TEXT, referredto_facility INTEGER, placeofreferral TEXT, reasonforreferral TEXT, descriptionforreferral TEXT, conditionofmother INT, BP TEXT, pulse INT , conditionofbaby INT, dateofreferral TEXT, timeofreferral TEXT, trans_id INTEGER, created_date TEXT, lastdateupdated TEXT,HeartRate INTEGER,SPO2 INTEGER );
create table tbl_phonenumbers(user_Id TEXT, selectedoption  INTEGER, phno_no TEXT, dateinserted DATETIME default(datetime('now','localtime')) , datelastupdated TEXT);
CREATE TABLE tblAuditTrail (AuditId INTEGER PRIMARY KEY, User_Id TEXT, Woman_Id TEXT, TblName TEXT, ColumnName TEXT, DateChangedOn TEXT, Old_Value TEXT, New_Value TEXT, trans_Id INTEGER, RecordCreatedDate DATETIME DEFAULT (datetime('now','localtime')));
CREATE TABLE tblMessageLog (MsgId INTEGER PRIMARY KEY, UserId TEXT, WomanId TEXT, Risk TEXT, RiskObserved TEXT, PhoneNo TEXT, MsgBody TEXT, MsgSentDate TEXT,  Priority INTEGER, NoOfFailures INTEGER, MsgSent Integer, RecordCreatedDate DATETIME DEFAULT (datetime('now','localtime')), trans_Id INTEGER);
create table tbl_generalcomments (Comment_Id INTEGER PRIMARY KEY, user_id TEXT, trans_id INTEGER, comment TEXT, date_of_insertion TEXT, time_of_insertion, Givenby TEXT, date_created DATETIME DEFAULT (datetime ( 'now','localtime')));
CREATE TABLE tbl_notification(user_id TEXT, women_id TEXT, fhr INTEGER, dilatation INTEGER, contraction INTEGER, pulse_bp INTEGER);
INSERT INTO tbl_ObjectTypeMaster VALUES(13,'AdditionalInfo');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.1','breastFeeding');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.2','resonForNotFeeding');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.3','ambulanceRequired');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.4','ambulanceAddress');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.8','breastFeeding');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.9','resonForNotFeeding');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.10','breastFeedingDateTime');
Delete from tbl_propertyMaster where object_id='13' and prop_id='13.1';
Delete from tbl_propertyMaster where object_id='13' and prop_id='13.2';
CREATE TABLE tbl_ColorCodedValues(ID INTEGER, object_Id TEXT, Prop_Id TEXT, prop_Name TEXT, redValues TEXT, amberValues TEXT, greenValues TEXT, suggestedBy TEXT, isDefault TEXT, institution_id TEXT, servercreateddate TEXT, lastmodifieddate TEXT, dateinserted DATETIME DEFAULT (datetime('now','localtime') ));
Create table tbldischargedetails (discUserId TEXT, discWomanId TEXT, discWomanName TEXT, discDateTimeOfDischarge DATETIME, 
 discWomanInvestigations TEXT, discChildInvestigations TEXT, discChild2Investigations TEXT,discStatusOfWoman Integer, 
discReferred Integer, discReasonForDischarge TEXT, discDischargeDiagnosis TEXT, discPatientDisposition TEXT, 
discFindingsHistory TEXT, discActionTaken TEXT, discMotherCondition TEXT, discChildCondition TEXT, discChild2Condition TEXT , discAdvice TEXT, 
discComplicationObserved TEXT, discProcedures TEXT, discDateTimeOfNextFollowUp TEXT, discPlaceOfNextFollowUp TEXT, 
discAdmissionReasons TEXT,discNameOfHCProvider TEXT, discDesignationOfHCProvider TEXT, discComments TEXT, 
trans_Id TEXT, discDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')), discDateModified DATETIME);

Create table tbllatentphase (latUserId TEXT, latWomanId TEXT, latWomanName TEXT, latDateOfEntry DATETIME, 
latPulse Integer, latBp TEXT, latContractions Integer,  latFhs Integer, latPv TEXT, latAdvice TEXT, trans_Id Integer, 
latDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));

Create table tblpostpartumcare (postUserId TEXT, postWomanId TEXT, postWomanName TEXT, 
postPresentingComplaints TEXT, postOtherPresentingComplaints TEXT, postExamination TEXT, postPallor TEXT, postPulse Integer, postBp TEXT, postBreastExamination TEXT, 
postInvolutionOfUterus TEXT, postLochia TEXT, postPerinealCare TEXT, postAdvice TEXT,  
trans_id Integer, postDateTimeOfEntry datetime, postDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));
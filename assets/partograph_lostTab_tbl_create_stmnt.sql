CREATE TABLE tbltransHeader ("TransDate"  TEXT,"TransStatus"  TEXT,"RequestId"  TEXT,"trans_Id"  INTEGER);
CREATE TABLE tbltransdetail ("Action"  TEXT,"TableName"  TEXT,"SQLStatement"  TEXT,"UserId"  TEXT,"trans_Id"  INTEGER);
CREATE TABLE tbl_SyncMaster (user_Id TEXT, RequestId TEXT, RequestDate TEXT, RequestStatusDate TEXT, RequestStatus TEXT);
CREATE TABLE tbl_notification(user_id TEXT, women_id TEXT, fhr INTEGER, dilatation INTEGER, contraction INTEGER, pulse_bp INTEGER);
CREATE TABLE tbl_Month (Month_Id INTEGER PRIMARY KEY, MnthName TEXT);
INSERT INTO tbl_Month VALUES(1,'Jan');
INSERT INTO tbl_Month VALUES(2,'Feb');
INSERT INTO tbl_Month VALUES(3,'Mar'); 
INSERT INTO tbl_Month VALUES(4,'Apr');
INSERT INTO tbl_Month VALUES(5,'May');
INSERT INTO tbl_Month VALUES(6,'Jun');
INSERT INTO tbl_Month VALUES(7,'Jul');
INSERT INTO tbl_Month VALUES(8,'Aug');
INSERT INTO tbl_Month VALUES(9,'Sep');
INSERT INTO tbl_Month VALUES(10,'Oct');
INSERT INTO tbl_Month VALUES(11,'Nov');
INSERT INTO tbl_Month VALUES(12,'Dec');
create table tbl_phonenumbers(user_Id TEXT, selectedoption  INTEGER, phno_no TEXT, dateinserted DATETIME default(datetime('now','localtime')) , datelastupdated TEXT);
CREATE TABLE tbl_commenttemp(user_Id TEXT, women_Id TEXT, object_Id NUMERIC, comments TEXT, date_of_entry TEXT, time_of_entry TEXT, dateinserted TEXT, isviewed INTEGER,child_id INTEGER);
INSERT INTO tbl_commenttemp Select * from tbl_comment;
DROP TABLE tbl_comment;
CREATE TABLE tbl_comment(user_Id TEXT, women_Id TEXT, object_Id NUMERIC, comments TEXT, date_of_entry TEXT, time_of_entry TEXT, dateinserted TEXT, isviewed INTEGER,child_id INTEGER);
INSERT INTO tbl_comment Select * from tbl_commenttemp;
DROP TABLE tbl_commenttemp;
CREATE TABLE tbl_settingstemp(emailid TEXT,user_Id TEXT, ipAddress TEXT, serverdb TEXT, createddate DATETIME DEFAULT CURRENT_TIMESTAMP , lastmodifieddate TEXT,customfield1 TEXT, customfield2 TEXT);
INSERT INTO tbl_settingstemp (user_id, ipaddress, serverdb)  Select user_id, ipAddress , serverdbname  from tbl_settings;
DROP TABLE tbl_settings; 
CREATE TABLE tbl_settings(emailid TEXT,user_Id TEXT, ipAddress TEXT, serverdb TEXT, createddate DATETIME DEFAULT CURRENT_TIMESTAMP , lastmodifieddate TEXT,customfield1 TEXT, customfield2 TEXT);
INSERT INTO tbl_settings Select * from tbl_settingstemp;
DROP TABLE tbl_settingstemp;
CREATE TABLE tbl_registeredwomentemp(user_Id TEXT, women_Id TEXT, Women_Image BLOB, women_name TEXT, date_of_adm TEXT, time_of_adm TEXT, w_age Integer, address TEXT, phone_num TEXT, del_status Integer, doc_name TEXT, nurse_name TEXT, attendant_name TEXT, gravida Integer, para Integer, hosp_no TEXT, facility TEXT, special_instructions TEXT, trans_Id INTEGER, comments TEXT, cond_while_admn Integer, del_comments TEXT, del_Time TEXT, del_date TEXT, del_result1 Integer, no_of_children INTEGER ,babywt1 INTEGER,babywt2 INTEGER, babysex1 Integer, babysex2 Integer , gestation_age INTEGER, del_result2 Integer, MothersDeath INTEGER, del_Time2 TEXT, admitted_with TEXT,memb_pres_absent Integer, mothers_death_reason TEXT, dateinserted TEXT, state TEXT, district TEXT, taluk TEXT, facility_name TEXT, wmonth INTEGER, wyear INTEGER, datelastupdated TEXT,gestation_age_days INTEGER, thaicardno TEXT, deltype_reason TEXT, deltype_otherreasons TEXT, LMP TEXT, riskoptions TEXT, tears TEXT, episiotomy INT, suturematerial INT, bloodgroup INT,EDD TEXT,Height TEXT,Weight TEXT,AdmittedWithOther TEXT, height_unit INTEGER, extra_comments TEXT, regtype INTEGER, regtypereason TEXT, date_of_reg_entry TEXT,normaltype INTEGER, regtypeOtherReason TEXT);
INSERT INTO tbl_registeredwomentemp SELECT user_Id, women_Id, Women_Image, women_name, date_of_adm , time_of_adm, w_age , address , phone_num , del_status , doc_name , nurse_name , attendant_name , gravida , para , hosp_no , facility , special_instructions , trans_Id , comments , cond_while_admn , del_comments , del_Time , del_date , del_result1 , no_of_children  ,babywt1 ,babywt2 , babysex1 , babysex2  , gestation_age , del_result2 , MothersDeath , del_Time2 , admitted_with ,memb_pres_absent , mothers_death_reason , dateinserted , state , district , taluk , facility_name , wmonth , wyear , datelastupdated ,gestation_age_days , thaicardno , deltype_reason , deltype_otherreasons , LMP , riskoptions , tears , episiotomy , suturematerial , bloodgroup ,EDD ,Height ,Weight ,AdmittedWithOther , height_unit , extra_comments , regtype , regtypereason , date_of_reg_entry ,normaltype , regtypeOtherReason from tbl_registeredwomen;
DROP TABLE tbl_registeredwomen; 
CREATE TABLE tbl_registeredwomen(user_Id TEXT, women_Id TEXT, Women_Image BLOB, women_name TEXT, date_of_adm TEXT, time_of_adm TEXT, w_age Integer, address TEXT, phone_num TEXT, del_status Integer, doc_name TEXT, nurse_name TEXT, attendant_name TEXT, gravida Integer, para Integer, hosp_no TEXT, facility TEXT, special_instructions TEXT, trans_Id INTEGER, comments TEXT, cond_while_admn Integer, del_comments TEXT, del_Time TEXT, del_date TEXT, del_result1 Integer, no_of_children INTEGER ,babywt1 INTEGER,babywt2 INTEGER, babysex1 Integer, babysex2 Integer , gestation_age INTEGER, del_result2 Integer, MothersDeath INTEGER, del_Time2 TEXT, admitted_with TEXT,memb_pres_absent Integer, mothers_death_reason TEXT, dateinserted TEXT, state TEXT, district TEXT, taluk TEXT, facility_name TEXT, wmonth INTEGER, wyear INTEGER, datelastupdated TEXT,gestation_age_days INTEGER, thaicardno TEXT, deltype_reason TEXT, deltype_otherreasons TEXT, LMP TEXT, riskoptions TEXT, tears TEXT, episiotomy INT, suturematerial INT, bloodgroup INT,EDD TEXT,Height TEXT,Weight TEXT,AdmittedWithOther TEXT, height_unit INTEGER, extra_comments TEXT, regtype INTEGER, regtypereason TEXT, date_of_reg_entry TEXT,normaltype INTEGER, regtypeOtherReason TEXT);
INSERT INTO tbl_registeredwomen SELECT * from tbl_registeredwomentemp;
DROP TABLE tbl_registeredwomentemp;
create table tbl_referraltemp(user_Id TEXT, women_Id TEXT, womenname TEXT, facility TEXT, facilityname TEXT, referredto_facility INTEGER, placeofreferral TEXT, reasonforreferral TEXT, descriptionforreferral TEXT, conditionofmother INT, BP TEXT, pulse INT , conditionofbaby INT, dateofreferral TEXT, timeofreferral TEXT, trans_id INTEGER, created_date TEXT, lastdateupdated TEXT,HeartRate INTEGER,SPO2 INTEGER );
INSERT INTO tbl_referraltemp SELECT user_Id , women_Id , womenname , facility , facilityname , referredto_facility , placeofreferral , reasonforreferral , descriptionforreferral , conditionofmother , BP , pulse  , conditionofbaby , dateofreferral , timeofreferral , trans_id , created_date , lastdateupdated ,HeartRate ,SPO2   from tbl_referral;
DROP table tbl_referral;
create table tbl_referral(user_Id TEXT, women_Id TEXT, womenname TEXT, facility TEXT, facilityname TEXT, referredto_facility INTEGER, placeofreferral TEXT, reasonforreferral TEXT, descriptionforreferral TEXT, conditionofmother INT, BP TEXT, pulse INT , conditionofbaby INT, dateofreferral TEXT, timeofreferral TEXT, trans_id INTEGER, created_date TEXT, lastdateupdated TEXT,HeartRate INTEGER,SPO2 INTEGER );
INSERT INTO tbl_referral SELECT * from tbl_referraltemp;
DROP TABLE tbl_referraltemp;
CREATE TABLE tbl_propertyValuestemp (trans_id INTEGER, user_Id TEXT, women_Id TEXT,child_Id NUMERIC, object_Id NUMERIC, prop_Id TEXT, prop_Name TEXT, prop_value TEXT, date_of_entry TEXT, time_of_entry TEXT, isDanger INTEGER, dateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));
INSERT INTO  tbl_propertyValuestemp SELECT trans_id , user_Id , women_Id ,child_Id , object_Id , prop_Id , prop_Name , prop_value , date_of_entry , time_of_entry , isDanger , dateOfInsertion from tbl_propertyvalues;
DROP TABLE tbl_propertyValues;
CREATE TABLE tbl_propertyValues (trans_id INTEGER, user_Id TEXT, women_Id TEXT,child_Id NUMERIC, object_Id NUMERIC, prop_Id TEXT, prop_Name TEXT, prop_value TEXT, date_of_entry TEXT, time_of_entry TEXT, isDanger INTEGER, dateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));
INSERT INTO tbl_propertyValues SELECT * from tbl_propertyValuestemp;
DROP TABLE tbl_propertyValuestemp;
CREATE TABLE tblAuditTrailtemp (AuditId INTEGER PRIMARY KEY, User_Id TEXT, Woman_Id TEXT, TblName TEXT, ColumnName TEXT, DateChangedOn TEXT, Old_Value TEXT, New_Value TEXT, trans_Id INTEGER, RecordCreatedDate DATETIME DEFAULT (datetime('now','localtime')));
INSERT INTO tblAuditTrailtemp SELECT AuditId  , User_Id , Woman_Id , TblName , ColumnName , DateChangedOn , Old_Value , New_Value , trans_Id , RecordCreatedDate   from tblAuditTrail;
DROP table tblAuditTrail;
CREATE TABLE tblAuditTrail (AuditId INTEGER PRIMARY KEY, User_Id TEXT, Woman_Id TEXT, TblName TEXT, ColumnName TEXT, DateChangedOn TEXT, Old_Value TEXT, New_Value TEXT, trans_Id INTEGER, RecordCreatedDate DATETIME DEFAULT (datetime('now','localtime')));
INSERT INTO tblAuditTrail SELECT * from tblAuditTrailtemp;
DROP table tblAuditTrailtemp;
CREATE TABLE tblMessageLogtemp (MsgId INTEGER PRIMARY KEY, UserId TEXT, WomanId TEXT, Risk TEXT, RiskObserved TEXT, PhoneNo TEXT, MsgBody TEXT, MsgSentDate TEXT,  Priority INTEGER, NoOfFailures INTEGER, MsgSent Integer, RecordCreatedDate DATETIME DEFAULT (datetime('now','localtime')), trans_Id INTEGER);
INSERT INTO tblMessageLogtemp SELECT  MsgId , UserId , WomanId , Risk , RiskObserved , PhoneNo , MsgBody , MsgSentDate ,  Priority , NoOfFailures , MsgSent , RecordCreatedDate , trans_Id from tblMessageLog;
DROP table tblMessageLog;
CREATE TABLE tblMessageLog (MsgId INTEGER PRIMARY KEY, UserId TEXT, WomanId TEXT, Risk TEXT, RiskObserved TEXT, PhoneNo TEXT, MsgBody TEXT, MsgSentDate TEXT,  Priority INTEGER, NoOfFailures INTEGER, MsgSent Integer, RecordCreatedDate DATETIME DEFAULT (datetime('now','localtime')), trans_Id INTEGER);
INSERT INTO tblMessageLog SELECT * from tblMessageLogtemp;
DROP TABLE tblMessageLogtemp;
create table tbl_generalcommentstemp (Comment_Id INTEGER PRIMARY KEY, user_id TEXT, trans_id INTEGER, comment TEXT, date_of_insertion TEXT, time_of_insertion TEXT, Givenby TEXT, date_created DATETIME DEFAULT (datetime ( 'now','localtime')));
INSERT INTO tbl_generalcommentstemp SELECT Comment_Id  , user_id , trans_id , comment , date_of_insertion , time_of_insertion, Givenby , date_created from tbl_generalcomments;
DROP table tbl_generalcomments;
create table tbl_generalcomments (Comment_Id INTEGER PRIMARY KEY, user_id TEXT, trans_id INTEGER, comment TEXT, date_of_insertion TEXT, time_of_insertion TEXT, Givenby TEXT, date_created DATETIME DEFAULT (datetime ( 'now','localtime')));
INSERT INTO tbl_generalcomments SELECT * from tbl_generalcommentstemp;
DROP TABLE tbl_generalcommentstemp;
DROP TABLE trantable;
DROP TABLE tblvillages;
DROP TABLE tbl_usertypemaster;
DROP TABLE tbl_usermaster;
DROP TABLE tbl_userfacility;
DROP TABLE tbl_tempuserloggedin;
DROP TABLE tbl_registrationproperties;
DROP TABLE tbl_propertiesdisplaymaster;
DROP TABLE tbl_facilitydetails;
DROP TABLE tbl_facility;
DROP TABLE tbl_auditrail_userlogin;
DELETE FROM tbl_objectTypeMaster;
INSERT INTO tbl_ObjectTypeMaster VALUES(1,'FetalHeartRate');
INSERT INTO tbl_ObjectTypeMaster VALUES(2,'AmnioticFluidMoulding');
INSERT INTO tbl_ObjectTypeMaster VALUES(3,'Dilatation');
INSERT INTO tbl_ObjectTypeMaster VALUES(4,'Contractions');
INSERT INTO tbl_ObjectTypeMaster VALUES(5,'Oxytocin');
INSERT INTO tbl_ObjectTypeMaster VALUES(6,'Drugsgiven');
INSERT INTO tbl_ObjectTypeMaster VALUES(7,'PulseandBP');
INSERT INTO tbl_ObjectTypeMaster VALUES(8,'Temperature');
INSERT INTO tbl_ObjectTypeMaster VALUES(9,'Urinetest');
INSERT INTO tbl_ObjectTypeMaster VALUES(10,'ApgarScore');
INSERT INTO tbl_ObjectTypeMaster VALUES(11,'ThirdStage');
INSERT INTO tbl_ObjectTypeMaster VALUES(12,'FourthStage');
DELETE FROM tbl_propertyMaster;
INSERT INTO tbl_PropertyMaster VALUES(1,'1','FHR');
Insert into tbl_PropertyMaster VALUES(1,'1.2','FHRComment');
INSERT INTO tbl_PropertyMaster VALUES(2,'2.1','Amniotic_Fluid');
INSERT INTO tbl_PropertyMaster VALUES(2,'2.2','Moulding');
Insert into tbl_PropertyMaster VALUES(2,'2.3','AFMComment');
INSERT INTO tbl_PropertyMaster VALUES(3,'3.1','cervix');
INSERT INTO tbl_PropertyMaster VALUES(3,'3.2','descent_of_head');
INSERT INTO tbl_PropertyMaster VALUES(3,'3.3','hours');
Insert into tbl_PropertyMaster VALUES(3,'3.4','DilationComment');
INSERT INTO tbl_PropertyMaster VALUES(4,'4.1','no_of_contractions');
INSERT INTO tbl_PropertyMaster VALUES(4,'4.2','duration');
Insert into tbl_PropertyMaster VALUES(4,'4.3','ContractionComment');
INSERT INTO tbl_PropertyMaster VALUES(5,'5.1','Oxytocin');
INSERT INTO tbl_PropertyMaster VALUES(5,'5.2','Units');
Insert into tbl_PropertyMaster VALUES(5,'5.3','OxytocinComment');
INSERT INTO tbl_PropertyMaster VALUES(6,'6.1','Drugs_given');
Insert into tbl_PropertyMaster VALUES(6,'6.2','DrugComment');
INSERT INTO tbl_PropertyMaster VALUES(7,'7.1','pulse_rate');
INSERT INTO tbl_PropertyMaster VALUES(7,'7.2','bp_systolic');
INSERT INTO tbl_PropertyMaster VALUES(7,'7.3','bp_diastolic');
Insert into tbl_PropertyMaster VALUES(7,'7.4','PulseComment');
INSERT INTO tbl_PropertyMaster VALUES(8,'8.1','temperature');
Insert into tbl_PropertyMaster VALUES(8,'8.2','TemperatureComment');
INSERT INTO tbl_PropertyMaster VALUES(9,'9.1','protein');
INSERT INTO tbl_PropertyMaster VALUES(9,'9.2','acetone');
INSERT INTO tbl_PropertyMaster VALUES(9,'9.3','volume');
Insert into tbl_PropertyMaster VALUES(9,'9.4','UrinetestComment');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.1','appearance');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.2','pulse');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.3','grimace');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.4','activity');
INSERT INTO tbl_PropertyMaster VALUES(10,'10.5','respiration');
INSERT into tbl_PropertyMaster VALUES(10,'10.6','comment');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.1','Duration');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.2','Oxytocin');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.3','Ergometrine');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.4','PGF2alpha');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.5','Misoprost');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.6','Placenta');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.7','UterineMassage');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.8','EBL');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.9','PPH');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.10','PPH_Properties');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.11','RetainedPlacenta');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.12','Inversion');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.13','Comments');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.14','DateOfThirdStage');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.15','TimeOfThirdStage');
INSERT INTO tbl_PropertyMaster VALUES(11,'11.16','createddate');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.1','Uterus');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.2','UrinePassed');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.3','Bleeding');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.4','Pulse_Rate');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.5','BP_Systolic');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.6','BP_Diastolic');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.7','Comments');
INSERT INTO tbl_ObjectTypeMaster VALUES(13,'AdditionalInfo');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.1','breastFeeding');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.2','resonForNotFeeding');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.3','ambulanceRequired');
INSERT INTO tbl_PropertyMaster VALUES(13,'13.4','ambulanceAddress');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.8','breastFeeding');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.9','resonForNotFeeding');
INSERT INTO tbl_PropertyMaster VALUES(12,'12.10','breastFeedingDateTime');
CREATE TABLE tbl_usertemp (user_id TEXT, password TEXT, LastWomenNumber INTEGER, LastTransNumber INTEGER, DeviceID TEXT, LastRequestNumber INTEGER, RunningId INTEGER, state TEXT, district TEXT, taluk TEXT, facility TEXT, facility_name TEXT,dateinserted TEXT,emailId TEXT, lastmodifieddate TEXT);
INSERT INTO tbl_usertemp (user_id, password,LastWomenNumber , LastTransNumber ,   state, district, taluk, facility , facility_name )  SELECT user_id, password,LastWomenNumber , LastTransNumber,    state, district, taluk, facility , facility_name from tbl_user;
DROP TABLE tbl_user;
CREATE TABLE tbl_user (user_id TEXT, password TEXT, LastWomenNumber INTEGER, LastTransNumber INTEGER, DeviceID TEXT, LastRequestNumber INTEGER, RunningId INTEGER, state TEXT, district TEXT, taluk TEXT, facility TEXT, facility_name TEXT, dateinserted TEXT,emailId TEXT, lastmodifieddate TEXT);
INSERT INTO tbl_user SELECT * from tbl_usertemp;
DROP TABLE tbl_usertemp;
CREATE TABLE tbl_ColorCodedValues(ID INTEGER, object_Id TEXT, Prop_Id TEXT, prop_Name TEXT, redValues TEXT, amberValues TEXT, greenValues TEXT, suggestedBy TEXT, isDefault TEXT, institution_id TEXT, servercreateddate TEXT, lastmodifieddate TEXT, dateinserted DATETIME DEFAULT (datetime('now','localtime') ));
Create table tbldischargedetailstemp (discUserId TEXT, discWomanId TEXT, discWomanName TEXT, discDateTimeOfDischarge DATETIME, 
 discWomanInvestigations TEXT, discChildInvestigations TEXT, discChild2Investigations TEXT,discStatusOfWoman Integer, 
discReferred Integer, discReasonForDischarge TEXT, discDischargeDiagnosis TEXT, discPatientDisposition TEXT, 
discFindingsHistory TEXT, discActionTaken TEXT, discMotherCondition TEXT, discChildCondition TEXT, discChild2Condition TEXT , discAdvice TEXT, 
discComplicationObserved TEXT, discProcedures TEXT, discDateTimeOfNextFollowUp TEXT, discPlaceOfNextFollowUp TEXT, 
discAdmissionReasons TEXT,discNameOfHCProvider TEXT, discDesignationOfHCProvider TEXT, discComments TEXT, 
trans_Id TEXT, discDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')), discDateModified DATETIME);

Insert into tbldischargedetailstemp Select discUserId , discWomanId , discWomanName , discDateTimeOfDischarge , 
 discWomanInvestigations , discChildInvestigations , discChild2Investigations ,discStatusOfWoman , 
discReferred , discReasonForDischarge , discDischargeDiagnosis , discPatientDisposition , 
discFindingsHistory , discActionTaken , discMotherCondition TEXT, discChildCondition TEXT, discChild2Condition  , discAdvice , 
discComplicationObserved , discProcedures , discDateTimeOfNextFollowUp , discPlaceOfNextFollowUp , 
discAdmissionReasons ,discNameOfHCProvider , discDesignationOfHCProvider , discComments , 
trans_Id , discDateOfInsertion , discDateModified  from tbldischargedetails;

DROP table tbldischargedetails;

Create table tbldischargedetails(discUserId TEXT, discWomanId TEXT, discWomanName TEXT, discDateTimeOfDischarge DATETIME, 
 discWomanInvestigations TEXT, discChildInvestigations TEXT, discChild2Investigations TEXT,discStatusOfWoman Integer, 
discReferred Integer, discReasonForDischarge TEXT, discDischargeDiagnosis TEXT, discPatientDisposition TEXT, 
discFindingsHistory TEXT, discActionTaken TEXT, discMotherCondition TEXT, discChildCondition TEXT, discChild2Condition TEXT , discAdvice TEXT, 
discComplicationObserved TEXT, discProcedures TEXT, discDateTimeOfNextFollowUp TEXT, discPlaceOfNextFollowUp TEXT, 
discAdmissionReasons TEXT,discNameOfHCProvider TEXT, discDesignationOfHCProvider TEXT, discComments TEXT, 
trans_Id TEXT, discDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')), discDateModified DATETIME);

Insert into tbldischargedetails Select * from tbldischargedetailstemp;
DROP table tbldischargedetailstemp;


Create table tbllatentphasetemp (latUserId TEXT, latWomanId TEXT, latWomanName TEXT, latDateOfEntry DATETIME, 
latPulse Integer, latBp TEXT, latContractions Integer,  latFhs Integer, latPv TEXT, latAdvice TEXT, trans_Id Integer, 
latDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));

Insert into  tbllatentphasetemp Select latUserId , latWomanId , latWomanName , latDateOfEntry , 
latPulse , latBp , latContractions ,  latFhs , latPv , latAdvice , trans_Id , 
latDateOfInsertion  from tbllatentphase;
DROP table tbllatentphase;
Create table tbllatentphase (latUserId TEXT, latWomanId TEXT, latWomanName TEXT, latDateOfEntry DATETIME, 
latPulse Integer, latBp TEXT, latContractions Integer,  latFhs Integer, latPv TEXT, latAdvice TEXT, trans_Id Integer, 
latDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));
Insert into tbllatentphase Select * from tbllatentphasetemp;
DROP table tbllatentphasetemp;

Create table tblpostpartumcaretemp (postUserId TEXT, postWomanId TEXT, postWomanName TEXT, 
postPresentingComplaints TEXT, postOtherPresentingComplaints TEXT, postExamination TEXT, postPallor TEXT, postPulse Integer, postBp TEXT, postBreastExamination TEXT, 
postInvolutionOfUterus TEXT, postLochia TEXT, postPerinealCare TEXT, postAdvice TEXT,  
trans_id Integer, postDateTimeOfEntry datetime, postDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));

Insert into tblpostpartumcaretemp Select postUserId , postWomanId , postWomanName , 
postPresentingComplaints , postOtherPresentingComplaints , postExamination , postPallor , postPulse , postBp , 
postBreastExamination , 
postInvolutionOfUterus , postLochia , postPerinealCare , postAdvice ,  
trans_id , postDateTimeOfEntry , postDateOfInsertion   from tblpostpartumcare;

DROP table tblpostpartumcare;
Create table tblpostpartumcare (postUserId TEXT, postWomanId TEXT, postWomanName TEXT, 
postPresentingComplaints TEXT, postOtherPresentingComplaints TEXT, postExamination TEXT, postPallor TEXT, postPulse Integer, postBp TEXT, postBreastExamination TEXT, 
postInvolutionOfUterus TEXT, postLochia TEXT, postPerinealCare TEXT, postAdvice TEXT,  
trans_id Integer, postDateTimeOfEntry datetime, postDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));
Insert into tblpostpartumcare Select * from tblpostpartumcaretemp;
DROP table tblpostpartumcaretemp;

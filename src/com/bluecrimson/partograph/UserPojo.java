package com.bluecrimson.partograph;

public class UserPojo {
	String userId;
	String pwd;
	int LastWomennumber;
	
	String state;
	String district;
	String taluk;
	String facility;
	String facility_name;
	
	
	int lastTransNumber;//03April2017 Arpitha
	int lastRequestNumber;//03April2017 Arpitha
	
	//06April2017 Arpitha
	String emailid;
	String password;
	
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}//06April2017 Arpitha
	
	
	public int getLastTransNumber() {
		return lastTransNumber;
	}
	public void setLastTransNumber(int lastTransNumber) {
		this.lastTransNumber = lastTransNumber;
	}
	public int getLastRequestNumber() {
		return lastRequestNumber;
	}
	public void setLastRequestNumber(int lastRequestNumber) {
		this.lastRequestNumber = lastRequestNumber;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getLastWomennumber() {
		return LastWomennumber;
	}
	public void setLastWomennumber(int lastWomennumber) {
		LastWomennumber = lastWomennumber;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getTaluk() {
		return taluk;
	}
	public void setTaluk(String taluk) {
		this.taluk = taluk;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getFacility_name() {
		return facility_name;
	}
	public void setFacility_name(String facility_name) {
		this.facility_name = facility_name;
	}

	
}

// updated by Arpitha on 25Feb2016
package com.bluecrimson.partograph;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;

import android.app.Activity;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class Settings_Adapter extends ArrayAdapter<PhoneNumber_Pojo> {
	Context context;
	private SparseBooleanArray mSelectedItemsIds;

	public Settings_Adapter(Context context, int textViewResourceId, ArrayList<PhoneNumber_Pojo> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		mSelectedItemsIds = new SparseBooleanArray();
		// TODO Auto-generated constructor stub
	}

	private class ecomments {
		TextView txtobjname, txtcomment;

	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return getCount();
	}

	// updated on 21july2016 by Arpitha
	public void toggleSelection(int position) {
		selectView(position, !mSelectedItemsIds.get(position));
	}

	public void selectView(int position, boolean value) {
		if (value)
			mSelectedItemsIds.put(position, value);
		else
			mSelectedItemsIds.delete(position);
		notifyDataSetChanged();
	}

	public SparseBooleanArray getSelectedIds() {
		return mSelectedItemsIds;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		try {
			ecomments holder = null;
			PhoneNumber_Pojo rowItem = getItem(position);
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.settings_adapter, null);
				holder = new ecomments();
				holder.txtobjname = (TextView) convertView.findViewById(R.id.textView1);
				holder.txtcomment = (TextView) convertView.findViewById(R.id.txtComment);

				convertView.setTag(holder);
			} else
				holder = (ecomments) convertView.getTag();


			int selecteitem = rowItem.getSelectedoption();
			String item = context.getResources().getStringArray(R.array.phn_no)[selecteitem];

			holder.txtobjname.setText(item);
			holder.txtcomment.setText(rowItem.getPhn_no());

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

}

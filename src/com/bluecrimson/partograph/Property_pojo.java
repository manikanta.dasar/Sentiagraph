package com.bluecrimson.partograph;

import java.util.ArrayList;

public class Property_pojo {
	String strTime;
	String strdate;
	int obj_Id;
	String obj_type;
	ArrayList<String> prop_id;
	ArrayList<String> prop_name;
	ArrayList<String> prop_value;
	int isDangerval1;
	int isDangerval2;
	
	int cervixDanger;//12Jun2017 Arpitha
	int descentofHeadDanger;//12Jun2017 Arpitha
	
	//ArrayList<Integer> danger;
	
	 
public int getCervixDanger() {
		return cervixDanger;
	}
	public void setCervixDanger(int cervixDanger) {
		this.cervixDanger = cervixDanger;
	}
	public int getDescentofHeadDanger() {
		return descentofHeadDanger;
	}
	public void setDescentofHeadDanger(int descentofHeadDanger) {
		this.descentofHeadDanger = descentofHeadDanger;
	}
	//	public ArrayList<Integer> getDanger() {
//		return danger;
//	}
//	public void setDanger(ArrayList<Integer> danger) {
//		this.danger = danger;
//	}
	public int getAmioticfluidDanger() {
		return AmioticfluidDanger;
	}
	public void setAmioticfluidDanger(int amioticfluidDanger) {
		AmioticfluidDanger = amioticfluidDanger;
	}
	int AmioticfluidDanger;//12Jun2017 Arpitha
	
	public ArrayList<String> getProp_value() {
		return prop_value;
	}
	public void setProp_value(ArrayList<String> prop_value) {
		this.prop_value = prop_value;
	}
	public String getStrTime() {
		return strTime;
	}
	public void setStrTime(String strTime) {
		this.strTime = strTime;
	}
	public String getStrdate() {
		return strdate;
	}
	public void setStrdate(String strdate) {
		this.strdate = strdate;
	}
	public int getObj_Id() {
		return obj_Id;
	}
	public void setObj_Id(int obj_Id) {
		this.obj_Id = obj_Id;
	}
	public String getObj_type() {
		return obj_type;
	}
	public void setObj_type(String obj_type) {
		this.obj_type = obj_type;
	}
	public ArrayList<String> getProp_id() {
		return prop_id;
	}
	public void setProp_id(ArrayList<String> prop_id) {
		this.prop_id = prop_id;
	}
	public ArrayList<String> getProp_name() {
		return prop_name;
	}
	public void setProp_name(ArrayList<String> prop_name) {
		this.prop_name = prop_name;
	}
	public int getIsDangerval1() {
		return isDangerval1;
	}
	public void setIsDangerval1(int isDangerval1) {
		this.isDangerval1 = isDangerval1;
	}
	public int getIsDangerval2() {
		return isDangerval2;
	}
	public void setIsDangerval2(int isDangerval2) {
		this.isDangerval2 = isDangerval2;
	}
	
	
}

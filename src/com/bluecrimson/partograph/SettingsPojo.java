package com.bluecrimson.partograph;

public class SettingsPojo {

	String ipAddress;
	String serverDb;
	
	// updated on 19july2016 by Arpitha
	String regphnno;
	String ref1phnno;
	String ref2phnno;
	String ref3phnno;
	String ambulancephnno;
	
	int defregsms;
	int defrefno;
	
	String expire_date;//11Sep2017 Arpitha
	
	
	public String getExpire_date() {
		return expire_date;
	}
	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}
	public int getDefregsms() {
		return defregsms;
	}
	public void setDefregsms(int defregsms) {
		this.defregsms = defregsms;
	}
	public int getDefrefno() {
		return defrefno;
	}
	public void setDefrefno(int defrefno) {
		this.defrefno = defrefno;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getServerDb() {
		return serverDb;
	}
	public void setServerDb(String serverDb) {
		this.serverDb = serverDb;
	}
	public String getRegphnno() {
		return regphnno;
	}
	public void setRegphnno(String regphnno) {
		this.regphnno = regphnno;
	}
	public String getRef1phnno() {
		return ref1phnno;
	}
	public void setRef1phnno(String ref1phnno) {
		this.ref1phnno = ref1phnno;
	}
	public String getRef2phnno() {
		return ref2phnno;
	}
	public void setRef2phnno(String ref2phnno) {
		this.ref2phnno = ref2phnno;
	}
	public String getRef3phnno() {
		return ref3phnno;
	}
	public void setRef3phnno(String ref3phnno) {
		this.ref3phnno = ref3phnno;
	}
	public String getAmbulancephnno() {
		return ambulancephnno;
	}
	public void setAmbulancephnno(String ambulancephnno) {
		this.ambulancephnno = ambulancephnno;
	}
	
	
}

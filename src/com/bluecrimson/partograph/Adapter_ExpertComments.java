//Arpitha on 25Feb2016
package com.bluecrimson.partograph;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_DB;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class Adapter_ExpertComments extends ArrayAdapter<Add_value_pojo> {
	Context context;
	// Add_value_pojo data;
	Partograph_DB dbh;
	// Cursor res;
	// String strobject_name;
	// String comment;

	public Adapter_ExpertComments(Context context, int textViewResourceId, ArrayList<Add_value_pojo> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	private class ecomments {
		TextView txtobjname, txtcomment, txtdate, txttime;

	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		try {
			ecomments holder = null;
			Add_value_pojo rowItem = getItem(position);
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.activity_expertcomments, null);
				holder = new ecomments();
				holder.txtobjname = (TextView) convertView.findViewById(R.id.textView1);
				holder.txtcomment = (TextView) convertView.findViewById(R.id.txtComment);
				holder.txtdate = (TextView) convertView.findViewById(R.id.txtdateComments);
				holder.txttime = (TextView) convertView.findViewById(R.id.txttimecomments);

				convertView.setTag(holder);
			} else
				holder = (ecomments) convertView.getTag();

			dbh = Partograph_DB.getInstance(context);

			String str = "";
			Cursor res = dbh.getobjectname(rowItem.getObj_Id());
			if (res.getCount() > 0) {
				res.moveToFirst();
				str = res.getString(0);
			}
			holder.txtobjname.setText(str);

			holder.txtcomment.setText(rowItem.getComments());
			holder.txtdate.setText(rowItem.getStrdate() + ",");
			holder.txttime.setText(rowItem.getStrTime());

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

}

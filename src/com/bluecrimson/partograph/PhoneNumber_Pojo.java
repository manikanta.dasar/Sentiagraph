package com.bluecrimson.partograph;

public class PhoneNumber_Pojo {

	int selectedoption;
	String phn_no;
	String userid;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getSelectedoption() {
		return selectedoption;
	}

	public void setSelectedoption(int selectedoption) {
		this.selectedoption = selectedoption;
	}

	public String getPhn_no() {
		return phn_no;
	}

	public void setPhn_no(String phn_no) {
		this.phn_no = phn_no;
	}

}

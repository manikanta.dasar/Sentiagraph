package com.bluecrimson.partograph;

import java.io.Serializable;

public class Comments_Pojo implements Serializable{
	
	
	String userid;
	String givenby;
	String comment;
	String date_of_insertion;
	String time_of_insertion;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getGivenby() {
		return givenby;
	}
	public void setGivenby(String givenby) {
		this.givenby = givenby;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getDate_of_insertion() {
		return date_of_insertion;
	}
	public void setDate_of_insertion(String date_of_insertion) {
		this.date_of_insertion = date_of_insertion;
	}
	public String getTime_of_insertion() {
		return time_of_insertion;
	}
	public void setTime_of_insertion(String time_of_insertion) {
		this.time_of_insertion = time_of_insertion;
	}
	
	
}

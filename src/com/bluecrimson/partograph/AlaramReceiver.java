/*package com.bluecrimson.partograph;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bluecrimson.notification.Notification_Pojo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class AlaramReceiver extends BroadcastReceiver {

	private int numMessagesOne = 0;
	boolean is = false;
	boolean isvalidduration = false;
	ArrayList<String> womenid;
	Partograph_DB dbh;
	String strlasttime;
	String strlastdate;
	public static String content_text;
	String notification_title;
	Notification notification;
	public static int pendingNotificationsCount_fhr;
	private static int pendingNotificationsCount_Dilatation = 0;
	private static int pendingNotificationsCount_contraction = 0;
	private static int pendingNotificationsCount_pulse = 0;
	Date date;
	String currdatetime;
	public static int notification_count;
	public static boolean isalarmreceiver;
	int count = 0;
	Notification_Pojo npojo;
	ArrayList<Notification_Pojo> nitems;
	int pos = 0;
	public static boolean fhr_notification = false;
	public static boolean dilatation_notification = false;
	public static boolean contraction_notification = false;
	public static boolean pulse_notification = false;
	// updated on 17August2016 by Arpitha
	String strlastnotificationtime;
	String strcurrenttime;
	Date d2 = null;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {

			Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

			npojo = new Notification_Pojo();

		//	SharedPreferences.Editor editor = LoginActivity.sharedpreferences.edit();

			womenid = getwomenid();
			String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String time = Partograph_CommonClass.getCurrentTime();
			currdatetime = date + "_" + time;

			for (String s : womenid) {

				for (int i = 1; i <= 7; i++)

				{
					nitems = new ArrayList<Notification_Pojo>();

					npojo.setWomenid(s);
					npojo.setObjectid(i);

					nitems.add(npojo);
					Intent notificationIntent = new Intent(context, LoginActivity.class);
					notificationIntent.putExtra("nitems", nitems);
					notificationIntent.putExtra("pos", pos);
					isalarmreceiver = true;
					TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
					stackBuilder.addParentStack(LoginActivity.class);
					stackBuilder.addNextIntent(notificationIntent);

					PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT // can
																														// only
																														// be
																														// used
																														// once
					);

					PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

					NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

					int objectid = i;
					strlastdate = lastvaldate(s, objectid);
					strlasttime = lastvaltime(s, objectid, strlastdate);

					if (strlastdate != null && strlasttime != null) {
						String strlastduration = strlastdate + "_" + strlasttime;
						if (objectid == 1) {

							isvalidduration = Partograph_CommonClass.getisValidTime(strlastduration, currdatetime, 30);
							if (isvalidduration) {
								strcurrenttime = Partograph_CommonClass.getTodaysDate() + " "
										+ Partograph_CommonClass.getCurrentTime();
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
								Date d1 = sdf.parse(strcurrenttime);

								// updated on 17August2016 by Arpitha
								if (strlastnotificationtime != null) {
									d2 = sdf.parse(strlastnotificationtime);

								}

								fhr_notification = true;
								content_text = "Please Enter FHR VAlue!!" + s;
								notification_title = "FHR";

								pendingNotificationsCount_fhr = AppContext.getPendingNotificationsCount_fhr() + 1;
								AppContext.setPendingNotificationsCount_fhr(pendingNotificationsCount_fhr);

								PendingIntent pendingIntent1 = PendingIntent.getActivity(context, 1, notificationIntent,
										0);
								
								

								notification = builder.setContentTitle(notification_title).setContentText(content_text)
										.setTicker("New Notification Alert!").setSmallIcon(R.drawable.notifi_icon)
										.setNumber(pendingNotificationsCount_fhr).setSound(soundUri)
										.setContentIntent(pendingIntent).build();

								notification.contentIntent = pendingIntent;
								notification.flags |= Notification.FLAG_AUTO_CANCEL;

								NotificationManager notificationManager = (NotificationManager) context
										.getSystemService(Context.NOTIFICATION_SERVICE);
								notificationManager.notify(1, notification);
								count = 1;
								strlastnotificationtime = Partograph_CommonClass.getTodaysDate() + " "
										+ Partograph_CommonClass.getCurrentTime();

								//editor.putString(s, strlastnotificationtime);
								//editor.commit();
							}
						} else if (objectid == 3) {
							isvalidduration = Partograph_CommonClass.getisValidTime(strlastduration, currdatetime, 60);
							if (isvalidduration) {
								dilatation_notification = true;
								content_text = "Please Enter Dilatation VAlue!!" + s;
								notification_title = "Dilatation";

								pendingNotificationsCount_Dilatation = AppContext
										.getPendingNotificationsCount_Dilatation() + 1;
								AppContext
										.setPendingNotificationsCount_Dilatation(pendingNotificationsCount_Dilatation);

								PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 2, notificationIntent,
										0);

								Notification notification = builder.setContentTitle(notification_title)
										.setContentText(content_text).setTicker("New Notification Alert!")
										.setSmallIcon(R.drawable.notifi_icon)
										.setNumber(pendingNotificationsCount_Dilatation).setSound(soundUri)
										.setContentIntent(pendingIntent).build();

								notification.contentIntent = pendingIntent;
								notification.flags |= Notification.FLAG_AUTO_CANCEL;

								NotificationManager notificationManager = (NotificationManager) context
										.getSystemService(Context.NOTIFICATION_SERVICE);
								notificationManager.notify(2, notification);
								count = 1;
							}
						} else if (objectid == 4) {
							isvalidduration = Partograph_CommonClass.getisValidTime(strlastduration, currdatetime, 30);
							if (isvalidduration) {
								content_text = "Please Enter COntraction VAlue!!" + s;
								notification_title = "Contraction";
								contraction_notification = true;

								pendingNotificationsCount_contraction = AppContext
										.getPendingNotificationsCount_contraction() + 1;
								AppContext.setPendingNotificationsCount_contraction(
										pendingNotificationsCount_contraction);

								PendingIntent pendingIntent3 = PendingIntent.getActivity(context, 3, notificationIntent,
										0);

								Notification notification = builder.setContentTitle(notification_title)
										.setContentText(content_text).setTicker("New Notification Alert!")
										.setSmallIcon(R.drawable.notifi_icon)
										.setNumber(pendingNotificationsCount_contraction).setSound(soundUri)
										.setContentIntent(pendingIntent).build();

								notification.contentIntent = pendingIntent;
								notification.flags |= Notification.FLAG_AUTO_CANCEL;

								NotificationManager notificationManager = (NotificationManager) context
										.getSystemService(Context.NOTIFICATION_SERVICE);
								notificationManager.notify(3, notification);
								count = 1;
							}
						} else if (objectid == 7) {
							isvalidduration = Partograph_CommonClass.getisValidTime(strlastduration, currdatetime, 30);
							if (isvalidduration) {
								content_text = "Please Enter Pulse and BP Value";
								notification_title = "Pulse & BP";
								pulse_notification = true;

								//pendingNotificationsCount_pulse = AppContext.getPendingNotificationsCount_pulse() + 1;
								//AppContext.setPendingNotificationsCount_pulse(pendingNotificationsCount_pulse);

								PendingIntent pendingIntent4 = PendingIntent.getActivity(context, 4, notificationIntent,
										0);

								Notification notification = builder.setContentTitle(notification_title)
										.setContentText(content_text).setTicker("New Notification Alert!")
										.setSmallIcon(R.drawable.notifi_icon).setNumber(pendingNotificationsCount_pulse)
										.setSound(soundUri).setContentIntent(pendingIntent).build();

								notification.contentIntent = pendingIntent;
								notification.flags |= Notification.FLAG_AUTO_CANCEL;

								NotificationManager notificationManager = (NotificationManager) context
										.getSystemService(Context.NOTIFICATION_SERVICE);
								notificationManager.notify(4, notification);
								count = 1;
							}

						}
					} else {
						isvalidduration = false;
					}

					if (isvalidduration) {

					}

				}
			}
			notification_count = pendingNotificationsCount_fhr + pendingNotificationsCount_Dilatation
					+ pendingNotificationsCount_contraction + pendingNotificationsCount_pulse;
			setBadge(context, notification_count);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> getwomenid() {

		ArrayList<String> womenid = new ArrayList<String>();
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		Cursor cur = s.rawQuery(
				"Select women_id from tbl_registeredwomen where del_status is null and women_id NOT IN(Select women_id from tbl_referral)",
				null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				String id = cur.getString(0);
				womenid.add(id);
			} while (cur.moveToNext());
		}
		return womenid;
	}

	public String lastvaldate(String womenid, int object_id) {
		String str = null;
		String sql = "Select max(date_of_entry)  from tbl_propertyvalues where object_id = '" + object_id
				+ "' and women_id='" + womenid + "' order by trans_id desc LIMIT 1";
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		Cursor res = s.rawQuery(sql, null);
		res.moveToFirst();
		if (res.getCount() > 0) {
			
			str = res.getString(0);
		}
		return str;
	}

	public String lastvaltime(String womenid, int object_id, String strlastdate2) {
		String str = null;
		String sql = "Select max(time_of_entry) from tbl_propertyvalues where object_id = '" + object_id
				+ "' and women_id='" + womenid + "' and date_of_entry='" + strlastdate2
				+ "' order by trans_id desc LIMIT 1";
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		Cursor res = s.rawQuery(sql, null);
		res.moveToFirst();
		if (res.getCount() > 0) {
			
			str = res.getString(0);
		}
		return str;
	}

	public static void setBadge(Context context, int count) {
		String launcherClassName = getLauncherClassName(context);
		if (launcherClassName == null) {
			return;
		}
		Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
		intent.putExtra("badge_count", count);
		intent.putExtra("badge_count_package_name", context.getPackageName());
		intent.putExtra("badge_count_class_name", launcherClassName);
		context.sendBroadcast(intent);
	}

	public static String getLauncherClassName(Context context) {

		PackageManager pm = context.getPackageManager();

		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);

		List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
		for (ResolveInfo resolveInfo : resolveInfos) {
			String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
			if (pkgName.equalsIgnoreCase(context.getPackageName())) {
				String className = resolveInfo.activityInfo.name;
				return className;
			}
		}
		return null;
	}

}
*/
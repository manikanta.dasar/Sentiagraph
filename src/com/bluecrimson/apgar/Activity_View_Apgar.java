package com.bluecrimson.apgar;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.Helper;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.usermanual.UseManual;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_View_Apgar extends FragmentActivity implements OnClickListener {

	AQuery aq;
	/*
	 * ArrayList<Women_Profile_Pojo> rowItems; int pos = -1;
	 */
	Partograph_DB dbh;
	ActionBar actionBar;
	Map<String, String> insertApgar = new LinkedHashMap<String, String>();
	Thread myThread;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	public static final Map<String, String> propertynamesMap = new LinkedHashMap<String, String>();
	Cursor cursor, cursor2;
	Apgar_Pojo apgardata;
	int child_Id;
	int totalScoreFirst, totalScoreSecond;
	int no_of_children;
	public static int apgarviewtabpos = 0;
	public static boolean isApgarview;
	ArrayList<Add_value_pojo> arrComments;
	ArrayList<Add_value_pojo> arrCommentschild2;

	int object_Id; // 04jan2016
	public static MenuItem menuItem;
	Comments_Adapter adapter;
	ImageButton imginfo;
	int i;
	// 07Oct2016 Arpitha
	TextView txtdialogtitle;

	Women_Profile_Pojo woman;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apgar);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(this);

			apgarviewtabpos = Partograph_CommonClass.curr_tabpos;
			isApgarview = true;

			/*
			 * rowItems = (ArrayList<Women_Profile_Pojo>)
			 * getIntent().getSerializableExtra("rowitems"); pos =
			 * getIntent().getIntExtra("position", 0);
			 */
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha
			dbh = Partograph_DB.getInstance(getApplicationContext());
			actionBar = getActionBar();
			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);
			// actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
			actionBar.setTitle(woman.getWomen_name());

			initializeScreen(aq.id(R.id.scrapgar).getView());

			// 23Aug2016 - bindu
			getwomanbasicdata();

			initialView();
			// updated on 9july2016 by Arpitha
			imginfo = (ImageButton) findViewById(R.id.fhrinfo);
			imginfo.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					try {
						if (MotionEvent.ACTION_UP == event.getAction()) {
							apgar_info();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return true;
				}
			});
			displayComments();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		// KillAllActivitesAndGoToLogin.activity_stack.add(this);
		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	// updated on 23Aug2016 by bindu
	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		String doa = "", toa = "", risk = "", dod = "", tod = "";
		if (woman != null) {

			aq.id(R.id.wname).text(woman.getWomen_name());
			aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				toa = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
				aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {
				aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + woman.getDate_of_reg_entry());
			}
			aq.id(R.id.wgest)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016
			// -
			// bindu
			// -
			// gest
			// age
			// not
			// known
			// chk
			aq.id(R.id.wgravida).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);// 07Oct2016
				// Arpitha
			} else
				risk = getResources().getString(R.string.low);// 07Oct2016
			// Arpitha
			aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatus)
						.text(getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);
				tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				aq.id(R.id.wdeldate).text(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

			}

		}

	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		TableLayout tblbaby1_details = (TableLayout) findViewById(R.id.tblapgar);// 10Nov2016
		// Arpitha
		TableLayout tblbaby2_details = (TableLayout) findViewById(R.id.tblapgar2);
		// 10Nov2016 Arpitha
		if (woman.getDel_result1() == 2) {
			tblbaby1_details.setVisibility(View.GONE);
			aq.id(R.id.txt1).visible();
		} else {
			tblbaby1_details.setVisibility(View.VISIBLE);
			aq.id(R.id.txt1).gone();
		}
		if (woman.getDel_result2() == 2) {
			tblbaby2_details.setVisibility(View.GONE);
			aq.id(R.id.txt2).visible();
		} else {
			tblbaby2_details.setVisibility(View.VISIBLE);
			aq.id(R.id.txt2).gone();
		} // 10Nov2016 Arpitha

		aq.id(R.id.btnsaveapgar1).visible();
		aq.id(R.id.btnsaveapgar).text(getResources().getString(R.string.previous));
		aq.id(R.id.btnsaveapgar1).text(getResources().getString(R.string.next));
		aq.id(R.id.btnviewapgar).text(getResources().getString(R.string.add));

		no_of_children = woman.getNo_of_child();

		// TableLayout tblbaby2_details = (TableLayout)
		// findViewById(R.id.tblapgar2);

		if (no_of_children == 2 && !(woman.getDel_result2() == 2)) {

			tblbaby2_details.setVisibility(View.VISIBLE);
		} else {
			tblbaby2_details.setVisibility(View.GONE);
		}

		object_Id = 10;
		child_Id = 1;
		cursor = dbh.GetApgarScoreDetails(strWomenid, strwuserId, object_Id, child_Id);
		if (cursor != null && cursor.getCount() > 0 && woman.getDel_result1() != 2) {// 10Nov2016
																						// Arpitha
			cursor.moveToFirst();
			prepareApgarPojo();

		}

		if (no_of_children == 2 && !(woman.getDel_result2() == 2)) {
			child_Id = 2;
			cursor2 = dbh.GetApgarScoreDetails(strWomenid, strwuserId, object_Id, child_Id);
			if (cursor2 != null && cursor2.getCount() > 0 && woman.getDel_result2() != 2) {// 10Nov2016
																							// Arpitha
				cursor2.moveToFirst();
				prepareApgarPojoSecond();
			}
		}

		disableApgarFields();

		// 10Aug2017 Atpitha
		// if (cursor != null && cursor.getCount() <=0) {
		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (arrVal.size() > 0) {
			aq.id(R.id.btnviewapgar).enabled(false);
			aq.id(R.id.btnviewapgar).backgroundColor(getResources().getColor(R.color.ashgray));
			// aq.id(R.id.txtdisable).visible();
			// // txtdisabled.setVisibility(View.VISIBLE);
			// // aq.id(R.id.txtdisable).gone();
			// // aq.id(R.id.tbldetails).gone();
			// aq.id(R.id.scrapgarscore).gone();
			// aq.id(R.id.btnsaveapgar).enabled(false);
			// aq.id(R.id.btnsaveapgar).backgroundColor(getResources().getColor(R.color.ashgray));
			// aq.id(R.id.llparentapgar).gone();
			// }

		} // 10Aug2017 Atpitha

	}

	private void prepareApgarPojoSecond() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		apgardata = new Apgar_Pojo();

		apgardata.setDateOfEntry2(cursor2.getString(0));
		apgardata.setTimeOfEntry2(cursor2.getString(1));
		apgardata.setAppearance2(cursor2.getString(2));
		apgardata.setPulse2(cursor2.getString(3));
		apgardata.setGrimace2(cursor2.getString(4));
		apgardata.setActivity2(cursor2.getString(5));
		apgardata.setRespiration2(cursor2.getString(6));
		// updated on 28feb16 by Arpitha
		apgardata.setApgarcomments2(cursor2.getString(7));

		/*
		 * totalScoreSecond = Integer.parseInt(cursor2.getString(2)) +
		 * Integer.parseInt(cursor2.getString(3)) +
		 * Integer.parseInt(cursor2.getString(3)) +
		 * Integer.parseInt(cursor2.getString(4)) +
		 * Integer.parseInt(cursor2.getString(5));
		 */// commented on 18Feb2017 Arpitha

		// 18Feb2017 Arpitha
		totalScoreSecond = Integer.parseInt(cursor2.getString(2)) + Integer.parseInt(cursor2.getString(3))
				+ Integer.parseInt(cursor2.getString(4)) + Integer.parseInt(cursor2.getString(5))
				+ Integer.parseInt(cursor2.getString(6));

		setApgarDataSecond(apgardata);

	}

	private void setApgarDataSecond(Apgar_Pojo apgardata2) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		String apgardate2 = apgardata.getDateOfEntry2();// 09Jan2017 Arpitha
		aq.id(R.id.etapgardatebaby2)
				.text(Partograph_CommonClass.getConvertedDateFormat(apgardate2, Partograph_CommonClass.defdateformat));
		aq.id(R.id.etapgartimebaby2).text(apgardata.getTimeOfEntry2());
		aq.id(R.id.etappearancebaby2).text(apgardata.getAppearance2());
		aq.id(R.id.etpulsebaby2).text(apgardata.getPulse2());
		aq.id(R.id.etgrimacebaby2).text(apgardata.getGrimace2());
		aq.id(R.id.etactivitybaby2).text(apgardata.getActivity2());
		aq.id(R.id.etrespirationbaby2).text(apgardata.getRespiration2());
		aq.id(R.id.ettotalscoresecond).text("" + totalScoreSecond);

		// updated on 28feb16 by Arpitha
		aq.id(R.id.etcomments2).text(apgardata.getApgarcomments2());

		setApgarColor(apgardata.getAppearance2(), R.id.etappearancebaby2);
		setApgarColor(apgardata.getPulse2(), R.id.etpulsebaby2);
		setApgarColor(apgardata.getGrimace2(), R.id.etgrimacebaby2);
		setApgarColor(apgardata.getActivity2(), R.id.etactivitybaby2);
		setApgarColor(apgardata.getRespiration2(), R.id.etrespirationbaby2);

		if (totalScoreSecond <= 3)
			aq.id(R.id.ettotalscoresecond).textColor(getResources().getColor(R.color.red));
		else if (totalScoreSecond > 3 && totalScoreSecond <= 6)
			aq.id(R.id.ettotalscoresecond).textColor(getResources().getColor(R.color.amber));
		else
			aq.id(R.id.ettotalscoresecond).textColor(getResources().getColor(R.color.green));
	}

	// Disable the fields
	private void disableApgarFields() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		aq.id(R.id.etappearance).enabled(false);
		aq.id(R.id.etpulse).enabled(false);
		aq.id(R.id.etgrimace).enabled(false);
		aq.id(R.id.etactivity).enabled(false);
		aq.id(R.id.etrespiration).enabled(false);
		aq.id(R.id.etapgardate).enabled(false);
		aq.id(R.id.etapgartime).enabled(false);

		// updated on 28feb16 by Arpitha
		aq.id(R.id.etcomments).enabled(false);

		aq.id(R.id.etappearancebaby2).enabled(false);
		aq.id(R.id.etpulsebaby2).enabled(false);
		aq.id(R.id.etgrimacebaby2).enabled(false);
		aq.id(R.id.etactivitybaby2).enabled(false);
		aq.id(R.id.etrespirationbaby2).enabled(false);
		aq.id(R.id.etapgardatebaby2).enabled(false);
		aq.id(R.id.etapgartimebaby2).enabled(false);
		// updated on 28feb16 by Arpitha
		aq.id(R.id.etcomments2).enabled(false);

		aq.id(R.id.etappearance).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etpulse).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etgrimace).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etactivity).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etrespiration).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etapgardate).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etapgartime).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etcomments).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etappearancebaby2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etgrimacebaby2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etactivitybaby2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etrespirationbaby2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etapgardatebaby2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etapgartimebaby2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etcomments2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etpulsebaby2).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));

	}

	// Create Apgar Pojo data
	private void prepareApgarPojo() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		apgardata = new Apgar_Pojo();

		apgardata.setDateOfEntry(cursor.getString(0));
		apgardata.setTimeOfEntry(cursor.getString(1));
		apgardata.setAppearance(cursor.getString(2));
		apgardata.setPulse(cursor.getString(3));
		apgardata.setGrimace(cursor.getString(4));
		apgardata.setActivity(cursor.getString(5));
		apgardata.setRespiration(cursor.getString(6));

		// updated on 28feb16 by Arpitha
		apgardata.setApgarcomments(cursor.getString(7));

		/*
		 * totalScoreFirst = Integer.parseInt(cursor.getString(2)) +
		 * Integer.parseInt(cursor.getString(3)) +
		 * Integer.parseInt(cursor.getString(3)) +
		 * Integer.parseInt(cursor.getString(4)) +
		 * Integer.parseInt(cursor.getString(5));
		 */// 18Feb2017 Arpitha
			// 18Feb2017 Arpitha
		totalScoreFirst = Integer.parseInt(cursor.getString(2)) + Integer.parseInt(cursor.getString(3))
				+ Integer.parseInt(cursor.getString(4)) + Integer.parseInt(cursor.getString(5))
				+ Integer.parseInt(cursor.getString(6));

		setApgarData(apgardata);
		int i = cursor.getPosition();
		aq.id(R.id.number).text(getResources().getString(R.string.apgar_score) + ":" + "" + (i + 1));
	}

	// Set Apgar data
	private void setApgarData(Apgar_Pojo apgardata) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		String apgardate = apgardata.getDateOfEntry();// 09Jan2017 Arpitha
		aq.id(R.id.etapgardate)
				.text(Partograph_CommonClass.getConvertedDateFormat(apgardate, Partograph_CommonClass.defdateformat));
		aq.id(R.id.etapgartime).text(apgardata.getTimeOfEntry());
		aq.id(R.id.etappearance).text(apgardata.getAppearance());

		aq.id(R.id.etpulse).text(apgardata.getPulse());
		aq.id(R.id.etgrimace).text(apgardata.getGrimace());
		aq.id(R.id.etactivity).text(apgardata.getActivity());
		aq.id(R.id.etrespiration).text(apgardata.getRespiration());
		aq.id(R.id.ettotalscorefirst).text("" + totalScoreFirst);

		// updated on 28feb16 by Arpitha
		aq.id(R.id.etcomments).text(apgardata.getApgarcomments());

		setApgarColor(apgardata.getAppearance(), R.id.etappearance);
		setApgarColor(apgardata.getPulse(), R.id.etpulse);
		setApgarColor(apgardata.getGrimace(), R.id.etgrimace);
		setApgarColor(apgardata.getActivity(), R.id.etactivity);
		setApgarColor(apgardata.getRespiration(), R.id.etrespiration);

		if (totalScoreFirst <= 3)
			aq.id(R.id.ettotalscorefirst).textColor(getResources().getColor(R.color.red));
		else if (totalScoreFirst > 3 && totalScoreFirst <= 6)
			aq.id(R.id.ettotalscorefirst).textColor(getResources().getColor(R.color.amber));
		else
			aq.id(R.id.ettotalscorefirst).textColor(getResources().getColor(R.color.green));

	}

	// Set the apgar score value colors
	private void setApgarColor(String val, int etID) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		if (Integer.parseInt(val) == 1)
			aq.id(etID).textColor(getResources().getColor(R.color.amber));
		else if (Integer.parseInt(val) == 2)
			aq.id(etID).textColor(getResources().getColor(R.color.green));
		else
			aq.id(etID).textColor(getResources().getColor(R.color.red));
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {

			// Save apgar
			case R.id.btnsaveapgar:
				try {

					boolean isPre = false;

					if (cursor != null && (!cursor.isFirst())) {
						isPre = true;
						cursor.moveToPrevious();
						prepareApgarPojo();
						setApgarData(apgardata);
					}

					if (no_of_children == 2 && !(woman.getDel_result2() == 2)) {
						if (cursor2 != null && (!cursor2.isFirst())) {
							isPre = true;
							cursor2.moveToPrevious();
							prepareApgarPojoSecond();
							setApgarDataSecond(apgardata);
						}
					}

					if (!isPre) {
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_records),
								Toast.LENGTH_LONG).show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				break;

			case R.id.btnviewapgar:
				try {
					Intent viewApgaract = new Intent(this, Activity_Apgar.class);
					viewApgaract.putExtra("woman", woman);
					// viewApgaract.putExtra("position", pos);
					startActivity(viewApgaract);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;

			case R.id.btnsaveapgar1:
				try {

					boolean isNext = false;
					if (cursor != null && (!cursor.isLast())) {
						isNext = true;
						cursor.moveToNext();
						prepareApgarPojo();
						setApgarData(apgardata);
						/* i++; */
					}

					if (no_of_children == 2 && !(woman.getDel_result2() == 2)) {
						if (cursor2 != null && (!cursor2.isLast())) {
							isNext = true;
							cursor2.moveToNext();
							prepareApgarPojoSecond();
							setApgarDataSecond(apgardata);
						}
					}
					if (!isNext) {
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_records),
								Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {

					doWork();
					Thread.sleep(1000); // Pause of 1 Second

				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					Thread.currentThread().interrupt();
					myThread.stop();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					myThread.stop();
				}
			}
		}
	}

	public void doWork() throws Exception {

		if (getApplicationContext() != null) {
			runOnUiThread(new Runnable() {
				public void run() {
					try {

						// displayComments();
						if (adapter != null) {
							adapter.notifyDataSetChanged();
						}

						updatetblComments(strwuserId, strWomenid, object_Id);
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	// Update tblcomments set isviewed=true 04jan2016
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.apgar_cnt = 0;

		SlidingActivity.isMsgApg = false; // 04jan2016
	}

	// Display Comments
	private void displayComments() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		int objectid = 10;
		int childid = 1;
		Cursor cursor = dbh.getCommentsApgar(strWomenid, strwuserId, objectid, childid);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());
		}
		if (arrComments.size() > 0) {
			aq.id(R.id.txtCommentsapgarchild1).visible();
			aq.id(R.id.listCommentschild1).visible();
			Parcelable state = aq.id(R.id.listCommentschild1).getListView().onSaveInstanceState();
			aq.id(R.id.listCommentschild1).visible();
			adapter = new Comments_Adapter(getApplicationContext(), R.layout.activity_apgar, arrComments);
			aq.id(R.id.listCommentschild1).adapter(adapter);
			// updated on 6/6/16
			aq.id(R.id.listCommentschild1).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentschild1).getListView());
		} else {
			aq.id(R.id.listCommentschild1).gone();
			aq.id(R.id.txtCommentsapgarchild1).gone();
		}

		// child2
		if (no_of_children == 2 && !(woman.getDel_result2() == 2)) {
			childid = 2;
			Cursor cursor2 = dbh.getCommentsApgar(strWomenid, strwuserId, objectid, childid);

			arrCommentschild2 = new ArrayList<Add_value_pojo>();
			if (cursor2.getCount() > 0) {
				cursor2.moveToFirst();

				do {
					Add_value_pojo commentsData = new Add_value_pojo();
					commentsData.setComments(cursor2.getString(0));
					commentsData.setStrdate(cursor2.getString(1));
					commentsData.setStrTime(cursor2.getString(2));

					arrCommentschild2.add(commentsData);
				} while (cursor2.moveToNext());
			}
			if (arrCommentschild2.size() > 0) {
				aq.id(R.id.txtCommentsapgarchild2).visible();
				aq.id(R.id.listCommentschild2).visible();
				// updated on 6/6/16 by Arpitha
				Parcelable state = aq.id(R.id.listCommentschild2).getListView().onSaveInstanceState();
				aq.id(R.id.listCommentschild2).visible();
				adapter = new Comments_Adapter(getApplicationContext(), R.layout.activity_apgar, arrCommentschild2);
				// updated on 6/6/16
				aq.id(R.id.listCommentschild2).getListView().onRestoreInstanceState(state);

				Helper.getListViewSize(aq.id(R.id.listCommentschild2).getListView());
				aq.id(R.id.listCommentschild2).adapter(adapter);
				aq.id(R.id.listCommentschild2).getListView().onRestoreInstanceState(state);
				Helper.getListViewSize(aq.id(R.id.listCommentschild2).getListView());
			} else {
				aq.id(R.id.listCommentschild2).gone();
				aq.id(R.id.txtCommentsapgarchild2).gone();
			}
		}

		else {
			aq.id(R.id.listCommentschild2).gone();
			aq.id(R.id.txtCommentsapgarchild2).gone();
		}
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		// KillAllActivitesAndGoToLogin
		// .delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);
		menu.findItem(R.id.delete).setVisible(false);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.home:
				Intent home = new Intent(this, Activity_WomenView.class);
				startActivity(home);
				return true;

			case R.id.about:
				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			case R.id.disch:
				Intent disch = new Intent(Activity_View_Apgar.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			case R.id.info:

				Intent info = new Intent(Activity_View_Apgar.this, GraphInformation.class);
				startActivity(info);
				return true;

			case R.id.usermanual:

				Intent manual = new Intent(Activity_View_Apgar.this, UseManual.class);
				startActivity(manual);
				return true;

			case R.id.settings:

				Intent sett = new Intent(Activity_View_Apgar.this, Settings_parto.class);
				startActivity(sett);
				return true;

			case R.id.summary:

				Intent sum = new Intent(Activity_View_Apgar.this, Summary_Activity.class);
				startActivity(sum);
				return true;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// menu.findItem(R.id.help).setVisible(false);
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		menu.findItem(R.id.ic_msg).setVisible(false);// 17Aug2017
		menu.findItem(R.id.sms).setVisible(false);// 17Aug2017
		return super.onPrepareOptionsMenu(menu);
	}

	// Sync
	private void calSyncMtd() {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	void apgar_info() throws Exception

	{
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.apgar_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.apgar_score) + "</font>"));// 08Feb2017
																				// Arpitha
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017
																				// Arpitha

		dialog.show();
		dialog.setCancelable(false);// 31Oct2016 Arpitha
	}

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		Intent i = new Intent(Activity_View_Apgar.this, Activity_WomenView.class);
		startActivity(i);
	}
}

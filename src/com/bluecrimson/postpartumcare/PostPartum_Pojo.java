package com.bluecrimson.postpartumcare;

public class PostPartum_Pojo {

	String userId;
	String womanId;
	String womanName;
	String examination;
	String complaints;
	String pallor;
	int pulse;
	String bp;
	String involution;
	String lochia;
	String perineal;
	String breastExamination;
	String dateTimeOfEntry;
	String advice;
	String other;

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWomanId() {
		return womanId;
	}

	public void setWomanId(String womanId) {
		this.womanId = womanId;
	}

	public String getWomanName() {
		return womanName;
	}

	public void setWomanName(String womanName) {
		this.womanName = womanName;
	}

	public String getExamination() {
		return examination;
	}

	public void setExamination(String examination) {
		this.examination = examination;
	}

	public String getComplaints() {
		return complaints;
	}

	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}

	public String getPallor() {
		return pallor;
	}

	public void setPallor(String pallor) {
		this.pallor = pallor;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public String getBp() {
		return bp;
	}

	public void setBp(String bp) {
		this.bp = bp;
	}

	public String getInvolution() {
		return involution;
	}

	public void setInvolution(String involution) {
		this.involution = involution;
	}

	public String getLochia() {
		return lochia;
	}

	public void setLochia(String lochia) {
		this.lochia = lochia;
	}

	public String getPerineal() {
		return perineal;
	}

	public void setPerineal(String perineal) {
		this.perineal = perineal;
	}

	public String getBreastExamination() {
		return breastExamination;
	}

	public void setBreastExamination(String breastExamination) {
		this.breastExamination = breastExamination;
	}

	public String getDateTimeOfEntry() {
		return dateTimeOfEntry;
	}

	public void setDateTimeOfEntry(String dateTimeOfEntry) {
		this.dateTimeOfEntry = dateTimeOfEntry;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

}

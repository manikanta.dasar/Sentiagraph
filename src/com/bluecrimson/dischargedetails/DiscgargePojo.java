//24July2017 Arpitha - v3.0
package com.bluecrimson.dischargedetails;

public class DiscgargePojo {

	String womanId;
	String userId;
	String womanName;
	int womanDeliveryStatus;
	int referred;
	String dateOfNextFollowUp;

	String placeOfNextFOllowUp;
	String dateTimeOfDischarge;
	String womanInvestigation;
	String childInvestigation;
	String conditionOfWoman;
	String conditionOfChild;
	String adviceGiven;
	String dischargeDiagnosis;
	String procedures;
	String complicationObserved;
	String findingHistorys;
	String actionTaken;
	String patientDisposition;
	String admissionReasons;
	String remarks;
	String HCProviderName;
	String designation;
	String dischargeReasons;
	String discDateTimeOfInsertion;

	String conditionOfChild2;
	String child2Investigation;

	String datelastupdated;

	public String getDatelastupdated() {
		return datelastupdated;
	}

	public void setDatelastupdated(String datelastupdated) {
		this.datelastupdated = datelastupdated;
	}

	public String getConditionOfChild2() {
		return conditionOfChild2;
	}

	public void setConditionOfChild2(String conditionOfChild2) {
		this.conditionOfChild2 = conditionOfChild2;
	}

	public String getChild2Investigation() {
		return child2Investigation;
	}

	public void setChild2Investigation(String child2Investigation) {
		this.child2Investigation = child2Investigation;
	}

	public String getDiscDateTimeOfInsertion() {
		return discDateTimeOfInsertion;
	}

	public void setDiscDateTimeOfInsertion(String discDateTimeOfInsertion) {
		this.discDateTimeOfInsertion = discDateTimeOfInsertion;
	}

	public String getDischargeReasons() {
		return dischargeReasons;
	}

	public void setDischargeReasons(String dischargeReasons) {
		this.dischargeReasons = dischargeReasons;
	}

	public String getWomanId() {
		return womanId;
	}

	public void setWomanId(String womanId) {
		this.womanId = womanId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWomanName() {
		return womanName;
	}

	public void setWomanName(String womanName) {
		this.womanName = womanName;
	}

	public int getWomanDeliveryStatus() {
		return womanDeliveryStatus;
	}

	public void setWomanDeliveryStatus(int womanDeliveryStatus) {
		this.womanDeliveryStatus = womanDeliveryStatus;
	}

	public int getReferred() {
		return referred;
	}

	public void setReferred(int referred) {
		this.referred = referred;
	}

	public String getDateOfNextFollowUp() {
		return dateOfNextFollowUp;
	}

	public void setDateOfNextFollowUp(String dateOfNextFollowUp) {
		this.dateOfNextFollowUp = dateOfNextFollowUp;
	}

	public String getPlaceOfNextFOllowUp() {
		return placeOfNextFOllowUp;
	}

	public void setPlaceOfNextFOllowUp(String placeOfNextFOllowUp) {
		this.placeOfNextFOllowUp = placeOfNextFOllowUp;
	}

	public String getDateTimeOfDischarge() {
		return dateTimeOfDischarge;
	}

	public void setDateTimeOfDischarge(String dateTimeOfDischarge) {
		this.dateTimeOfDischarge = dateTimeOfDischarge;
	}

	public String getWomanInvestigation() {
		return womanInvestigation;
	}

	public void setWomanInvestigation(String womanInvestigation) {
		this.womanInvestigation = womanInvestigation;
	}

	public String getChildInvestigation() {
		return childInvestigation;
	}

	public void setChildInvestigation(String childInvestigation) {
		this.childInvestigation = childInvestigation;
	}

	public String getConditionOfWoman() {
		return conditionOfWoman;
	}

	public void setConditionOfWoman(String conditionOfWoman) {
		this.conditionOfWoman = conditionOfWoman;
	}

	public String getConditionOfChild() {
		return conditionOfChild;
	}

	public void setConditionOfChild(String conditionOfChild) {
		this.conditionOfChild = conditionOfChild;
	}

	public String getAdviceGiven() {
		return adviceGiven;
	}

	public void setAdviceGiven(String adviceGiven) {
		this.adviceGiven = adviceGiven;
	}

	public String getDischargeDiagnosis() {
		return dischargeDiagnosis;
	}

	public void setDischargeDiagnosis(String dischargeDiagnosis) {
		this.dischargeDiagnosis = dischargeDiagnosis;
	}

	public String getProcedures() {
		return procedures;
	}

	public void setProcedures(String procedures) {
		this.procedures = procedures;
	}

	public String getComplicationObserved() {
		return complicationObserved;
	}

	public void setComplicationObserved(String complicationObserved) {
		this.complicationObserved = complicationObserved;
	}

	public String getFindingHistorys() {
		return findingHistorys;
	}

	public void setFindingHistorys(String findingHistorys) {
		this.findingHistorys = findingHistorys;
	}

	public String getActionTaken() {
		return actionTaken;
	}

	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}

	public String getPatientDisposition() {
		return patientDisposition;
	}

	public void setPatientDisposition(String patientDisposition) {
		this.patientDisposition = patientDisposition;
	}

	public String getAdmissionReasons() {
		return admissionReasons;
	}

	public void setAdmissionReasons(String admissionReasons) {
		this.admissionReasons = admissionReasons;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getHCProviderName() {
		return HCProviderName;
	}

	public void setHCProviderName(String hCProviderName) {
		HCProviderName = hCProviderName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

}

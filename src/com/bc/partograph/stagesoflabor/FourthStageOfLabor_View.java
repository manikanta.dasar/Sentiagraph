package com.bc.partograph.stagesoflabor;

import java.util.ArrayList;
import java.util.Locale;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.StagesofLabor_Pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class FourthStageOfLabor_View extends Activity implements OnClickListener {

	Partograph_DB dbh;
	AQuery aq;
	Cursor cursor;
	String strWomenid, strwuserId;
	StagesofLabor_Pojo sData;
	public static MenuItem menuItem;
	public static int viewfourthstagepos = 0;
	public static boolean isFourthStageOfLaborView;
	ArrayList<StagesofLabor_Pojo> listItems;
	TableLayout tbl;
	GraphicalView mChartView = null;
	Women_Profile_Pojo woman;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Locale locale = null;

		if (AppContext.prefs.getBoolean("isEnglish", false)) {
			locale = new Locale("en");
			Locale.setDefault(locale);
		} else if (AppContext.prefs.getBoolean("isHindi", false)) {
			locale = new Locale("hi");
			Locale.setDefault(locale);
		}

		Configuration config = new Configuration();
		config.locale = locale;
		getResources().updateConfiguration(config, getResources().getDisplayMetrics());

		AppContext.setNamesAccordingToRes(getResources());
		setContentView(R.layout.view_fourthstage);
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			viewfourthstagepos = Partograph_CommonClass.curr_tabpos;
			isFourthStageOfLaborView = true;
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			dbh = Partograph_DB.getInstance(getApplicationContext());
			aq = new AQuery(this);

			// get the action bar
			ActionBar actionBar = getActionBar();

			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);

			initializeScreen(aq.id(R.id.rlfourthstageview).getView());

			tbl = (TableLayout) findViewById(R.id.tblfourthstageview);

			initialView();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
		}

		// 13Sep2016- bindu
		KillAllActivitesAndGoToLogin.addToStack(this);
		// KillAllActivitesAndGoToLogin.activity_stack.add(this);
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		strwuserId = woman.getUserId();
		strWomenid = woman.getWomenId();

		aq.id(R.id.txtwname).text(getResources().getString(R.string.name) + " : " + woman.getWomen_name());

		aq.id(R.id.btnviewfourthstage).text(getResources().getString(R.string.add));
		aq.id(R.id.btnsavefourthstage).text(getResources().getString(R.string.previous));

		listItems = new ArrayList<StagesofLabor_Pojo>();

		cursor = dbh.GetFourthStageDetails(strWomenid, strwuserId, 12);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				sData = new StagesofLabor_Pojo();
				sData.setDateofentry(cursor.getString(0));
				sData.setTimeofentry(cursor.getString(1));
				sData.setUterus(cursor.getString(2));
				sData.setUrinepassed(cursor.getString(3));
				sData.setBleeding(cursor.getString(4));
				sData.setPulse(cursor.getString(5));
				sData.setBpsystolic(cursor.getString(6));
				sData.setBpdiastolic(cursor.getString(7));
				sData.setFourthstagecomments(cursor.getString(8));
				// 13Feb2017 Arpitha
				sData.setBreastFeeding(cursor.getString(9));
				sData.setReasonForNotFeeding(cursor.getString(10));
				sData.setBreastFeedingDateTime(cursor.getString(11));// 13Feb2017
																		// Arpitha

				listItems.add(sData);
			} while (cursor.moveToNext());
		}

		displayValues();
	}

	// Display values
	private void displayValues() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		if (listItems.size() > 0) {

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(getApplicationContext());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView texttimelbl = new TextView(getApplicationContext());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getApplicationContext());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getApplicationContext());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			textval1lbl.setText(getResources().getString(R.string.uterus));
			trlabel.addView(textval1lbl);

			TextView textval2lbl = new TextView(getApplicationContext());
			textval2lbl.setTextSize(18);
			textval2lbl.setPadding(10, 10, 10, 10);
			textval2lbl.setTextColor(getResources().getColor(R.color.black));
			textval2lbl.setTextSize(18);
			textval2lbl.setGravity(Gravity.CENTER);
			textval2lbl.setText(getResources().getString(R.string.urinepassed));
			trlabel.addView(textval2lbl);

			TextView txtbleeding = new TextView(getApplicationContext());
			txtbleeding.setTextSize(18);
			txtbleeding.setPadding(10, 10, 10, 10);
			txtbleeding.setTextColor(getResources().getColor(R.color.black));
			txtbleeding.setTextSize(18);
			txtbleeding.setGravity(Gravity.CENTER);
			txtbleeding.setText(getResources().getString(R.string.ableeding));
			trlabel.addView(txtbleeding);

			TextView txtpulse = new TextView(getApplicationContext());
			txtpulse.setTextSize(18);
			txtpulse.setPadding(10, 10, 10, 10);
			txtpulse.setTextColor(getResources().getColor(R.color.black));
			txtpulse.setTextSize(18);
			txtpulse.setGravity(Gravity.CENTER);
			txtpulse.setText(getResources().getString(R.string.pulse));
			trlabel.addView(txtpulse);

			TextView txtbp = new TextView(getApplicationContext());
			txtbp.setTextSize(18);
			txtbp.setPadding(10, 10, 10, 10);
			txtbp.setTextColor(getResources().getColor(R.color.black));
			txtbp.setTextSize(18);
			txtbp.setGravity(Gravity.CENTER);
			txtbp.setText(getResources().getString(R.string.txtbpvalue));
			trlabel.addView(txtbp);

			TextView txtcomments = new TextView(getApplicationContext());
			txtcomments.setTextSize(18);
			txtcomments.setPadding(10, 10, 10, 10);
			txtcomments.setTextColor(getResources().getColor(R.color.black));
			txtcomments.setTextSize(18);
			txtcomments.setGravity(Gravity.CENTER);
			txtcomments.setText(getResources().getString(R.string.comments));
			trlabel.addView(txtcomments);

			// 13Feb2017 Arpitha
			TextView txtbreastFeeding = new TextView(getApplicationContext());
			txtbreastFeeding.setTextColor(getResources().getColor(R.color.black));
			txtbreastFeeding.setTextSize(18);
			txtbreastFeeding.setPadding(10, 10, 10, 10);
			txtbreastFeeding.setTextSize(18);
			txtbreastFeeding.setText(getResources().getString(R.string.breast_feeding));
			txtbreastFeeding.setGravity(Gravity.CENTER);
			trlabel.addView(txtbreastFeeding);

			// 13Feb2017 Arpitha
			TextView txtnotFeedingReason = new TextView(getApplicationContext());
			txtnotFeedingReason.setTextColor(getResources().getColor(R.color.black));
			txtnotFeedingReason.setTextSize(18);
			txtnotFeedingReason.setPadding(10, 10, 10, 10);
			txtnotFeedingReason.setTextSize(18);
			txtnotFeedingReason.setGravity(Gravity.CENTER);
			txtnotFeedingReason.setText(getResources().getString(R.string.reasons));
			trlabel.addView(txtnotFeedingReason);

			// 13Feb2017 Arpitha
			TextView txtbreastFeedingDate = new TextView(getApplicationContext());
			txtbreastFeedingDate.setTextColor(getResources().getColor(R.color.black));
			txtbreastFeedingDate.setTextSize(18);
			txtbreastFeedingDate.setPadding(10, 10, 10, 10);
			txtbreastFeedingDate.setTextSize(18);
			txtbreastFeedingDate.setGravity(Gravity.CENTER);
			txtbreastFeedingDate.setText(getResources().getString(R.string.breast_feeding) + " "
					+ getResources().getString(R.string.date_time));
			trlabel.addView(txtbreastFeedingDate);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listItems.size(); i++) {
				TableRow tr = new TableRow(getApplicationContext());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getApplicationContext());
				txttime.setText(listItems.get(i).getTimeofentry());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getApplicationContext());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listItems.get(i).getDateofentry(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				TextView txtvalue1 = new TextView(getApplicationContext());
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);
				tr.addView(txtvalue1);

				TextView txtvalue2 = new TextView(getApplicationContext());
				txtvalue2.setTextColor(getResources().getColor(R.color.black));
				txtvalue2.setTextSize(18);
				txtvalue2.setPadding(10, 10, 10, 10);
				txtvalue2.setTextSize(18);
				txtvalue2.setGravity(Gravity.CENTER);
				tr.addView(txtvalue2);

				TextView txtbleedingval = new TextView(getApplicationContext());
				txtbleedingval.setTextColor(getResources().getColor(R.color.black));
				txtbleedingval.setTextSize(18);
				txtbleedingval.setPadding(10, 10, 10, 10);
				txtbleedingval.setTextSize(18);
				txtbleedingval.setGravity(Gravity.CENTER);
				tr.addView(txtbleedingval);

				TextView txtpulseval = new TextView(getApplicationContext());
				txtpulseval.setTextColor(getResources().getColor(R.color.black));
				txtpulseval.setTextSize(18);
				txtpulseval.setPadding(10, 10, 10, 10);
				txtpulseval.setTextSize(18);
				txtpulseval.setGravity(Gravity.CENTER);
				tr.addView(txtpulseval);

				TextView txtbpval = new TextView(getApplicationContext());
				txtbpval.setTextColor(getResources().getColor(R.color.black));
				txtbpval.setTextSize(18);
				txtbpval.setPadding(10, 10, 10, 10);
				txtbpval.setTextSize(18);
				txtbpval.setGravity(Gravity.CENTER);
				tr.addView(txtbpval);

				TextView txtcommentsval = new TextView(getApplicationContext());
				txtcommentsval.setTextColor(getResources().getColor(R.color.black));
				txtcommentsval.setTextSize(18);
				txtcommentsval.setPadding(10, 10, 10, 10);
				txtcommentsval.setTextSize(18);
				txtcommentsval.setGravity(Gravity.CENTER);
				tr.addView(txtcommentsval);

				// if (i == 0)
				{// 02March2017 Arpitha
					String breastFeeding = "";
					if (listItems.get(i).getBreastFeeding() != null
							&& listItems.get(i).getBreastFeeding().trim().length() > 0) {
						if (listItems.get(i).getBreastFeeding().equalsIgnoreCase("1")) {
							breastFeeding = getResources().getString(R.string.not_done);
						} else if (listItems.get(i).getBreastFeeding().equalsIgnoreCase("2")) {
							breastFeeding = getResources().getString(R.string.done);
						}
					}

					// 13Feb2017 Arpitha
					TextView txtbreastFeedingVal = new TextView(getApplicationContext());
					txtbreastFeedingVal.setTextColor(getResources().getColor(R.color.black));
					txtbreastFeedingVal.setTextSize(18);
					txtbreastFeedingVal.setPadding(10, 10, 10, 10);
					txtbreastFeedingVal.setTextSize(18);
					txtbreastFeedingVal.setText(breastFeeding);
					txtbreastFeedingVal.setGravity(Gravity.CENTER);
					tr.addView(txtbreastFeedingVal);

					// 13Feb2017 Arpitha
					TextView txtnotFeedingReasonVal = new TextView(getApplicationContext());
					txtnotFeedingReasonVal.setTextColor(getResources().getColor(R.color.black));
					txtnotFeedingReasonVal.setTextSize(18);
					txtnotFeedingReasonVal.setPadding(10, 10, 10, 10);
					txtnotFeedingReasonVal.setTextSize(18);
					txtnotFeedingReasonVal.setGravity(Gravity.CENTER);
					txtnotFeedingReasonVal.setText(listItems.get(i).getReasonForNotFeeding());
					tr.addView(txtnotFeedingReasonVal);

					// 13Feb2017 Arpitha
					TextView txtbreastFeedingDateVal = new TextView(getApplicationContext());
					txtbreastFeedingDateVal.setTextColor(getResources().getColor(R.color.black));
					txtbreastFeedingDateVal.setTextSize(18);
					txtbreastFeedingDateVal.setPadding(10, 10, 10, 10);
					txtbreastFeedingDateVal.setTextSize(18);
					txtbreastFeedingDateVal.setGravity(Gravity.CENTER);
					if (listItems.get(i).getBreastFeeding() != null
							&& listItems.get(i).getBreastFeeding().trim().length() > 0
							&& listItems.get(i).getBreastFeeding().equalsIgnoreCase("2"))// 19May2017
																							// Arpitha
																							// -
																							// v2.6
					{
						String bfDate = Partograph_CommonClass.getConvertedDateFormat(
								(listItems.get(i).getBreastFeedingDateTime()), Partograph_CommonClass.defdateformat);
						txtbreastFeedingDateVal
								.setText(bfDate + "/" + listItems.get(i).getBreastFeedingDateTime().split("\\s+")[1]);
					}
					tr.addView(txtbreastFeedingDateVal);
				} // 02March2017 Arpitha

				String val1 = "", val2 = "", strbleeding = "", strpulse = "", strbp = "", strcomments = "";

				val1 = listItems.get(i).getUterus();
				val2 = listItems.get(i).getUrinepassed();
				strbleeding = listItems.get(i).getBleeding();
				strpulse = listItems.get(i).getPulse();
				strcomments = listItems.get(i).getFourthstagecomments();

				// uterus
				if (val1.equals("0")) {
					val1 = getResources().getString(R.string.contracted);
					// txtvalue1.setTextColor(getResources().getColor(R.color.red));
					txtvalue1.setTextColor(getResources().getColor(R.color.black));
				} else if (val1.equals("1")) {
					val1 = getResources().getString(R.string.relaxed);
					// txtvalue1.setTextColor(getResources().getColor(R.color.black));
					txtvalue1.setTextColor(getResources().getColor(R.color.red));
				}

				txtvalue1.setText(val1);

				// urine passed
				if (val2.equals("0")) {
					val2 = getResources().getString(R.string.yes);
				} else if (val2.equals("1")) {
					val2 = getResources().getString(R.string.no);
				}

				txtvalue2.setText(val2);

				// bleeding
				if (strbleeding.equals("0")) {
					strbleeding = getResources().getString(R.string.normal);
					txtbleedingval.setTextColor(getResources().getColor(R.color.black));
				} else if (strbleeding.equals("1")) {
					strbleeding = getResources().getString(R.string.excess);
					txtbleedingval.setTextColor(getResources().getColor(R.color.red));
				}

				txtbleedingval.setText(strbleeding);

				// Pulse
				int value = Integer.parseInt(strpulse);
				if (value >= 50 && value <= 60 || (value >= 100 && value <= 120)) {
					txtpulseval.setTextColor(getResources().getColor(R.color.amber));
				} else if (value > 120 || value < 50) {
					txtpulseval.setTextColor(getResources().getColor(R.color.red));
				} else {
					txtpulseval.setTextColor(getResources().getColor(R.color.green));
				}

				txtpulseval.setText(strpulse);

				// bp

				String str1 = listItems.get(i).getBpsystolic() == null ? " " : listItems.get(i).getBpsystolic();
				String str2 = listItems.get(i).getBpdiastolic() == null ? " " : listItems.get(i).getBpdiastolic();

				if (str1.length() > 0 && str2.length() > 0)
					strbp = str1 + "/" + str2;

				int valueSystolic = Integer.parseInt(str1);
				int valueDiastolic = Integer.parseInt(str2);
				if ((valueSystolic < 80 || valueSystolic > 160) || (valueDiastolic < 50 || valueDiastolic >= 110)) {
					txtbpval.setTextColor(getResources().getColor(R.color.red));
				} else if (((valueSystolic >= 80 && valueSystolic < 100)
						|| (valueSystolic >= 140 && valueSystolic <= 160))
						|| ((valueDiastolic >= 50 && valueDiastolic < 60)
								|| (valueDiastolic > 100 && valueDiastolic < 110))) {
					txtbpval.setTextColor(getResources().getColor(R.color.amber));
				} else if ((valueSystolic >= 90 && valueSystolic <= 140)
						|| (valueDiastolic >= 50 && valueDiastolic <= 90)) {
					txtbpval.setTextColor(getResources().getColor(R.color.green));
				}

				txtbpval.setText(strbp);

				// Comments
				txtcommentsval.setText(strcomments);

				View view3 = new View(getApplicationContext());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getApplicationContext());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == TextView.class) {
				aq.id(v.getId()).getTextView().setOnClickListener(this);
			}
			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {

			case R.id.btnviewfourthstage: {
				Intent fourthstageadd = new Intent(getApplicationContext(), StageofLabor.class);
				fourthstageadd.putExtra("woman", woman);
				// fourthstageadd.putExtra("position", pos);
				startActivity(fourthstageadd);
				break;
			}
			case R.id.btnviewgraphpulseandbp: {

				if (listItems.size() > 0)
					showGraphDialog();
				else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.nodata),
							Toast.LENGTH_LONG).show();
				break;
			}

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				Activity_WomenView.actionBar.setSelectedNavigationItem(3);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.home:
				Intent home = new Intent(this, Activity_WomenView.class);
				startActivity(home);
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			case R.id.settings:
				Intent settings = new Intent(this, Settings_parto.class);
				startActivity(settings);
				return true;

			case R.id.usermanual:// 28April2017 Arpitha

				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);
				return true;
			// 17JUly2017 Arpitha - bug fixing 2.6.1
			case R.id.summary:
				Intent summ = new Intent(this, Summary_Activity.class);
				startActivity(summ);
				return true;

			case R.id.disch:
				Intent disch = new Intent(FourthStageOfLabor_View.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;
			// 03Oct2017 Arpitha
			case R.id.del:
				// Intent disc = new Intent(FourthStageOfLabor_View.this,
				// DeliveryInfo_Activity.class);
				// disc.putExtra("woman", woman);
				// startActivity(disc);

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(FourthStageOfLabor_View.this, woman);
					// loadWomendata(displayListCountddel);
					// if(adapter!=null)
					// adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(FourthStageOfLabor_View.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpithas
				return true;
			case R.id.viewprofile:
				Intent view = new Intent(FourthStageOfLabor_View.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				// Intent ref = new Intent(FourthStageOfLabor_View.this,
				// ReferralInfo_Activity.class);
				// ref.putExtra("woman", woman);
				// startActivity(ref);
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(FourthStageOfLabor_View.this, woman);
				} else {
					Intent ref = new Intent(FourthStageOfLabor_View.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(FourthStageOfLabor_View.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(FourthStageOfLabor_View.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(FourthStageOfLabor_View.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(FourthStageOfLabor_View.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(FourthStageOfLabor_View.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;
				
			case R.id.lat:
				Intent lat = new Intent(FourthStageOfLabor_View.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(FourthStageOfLabor_View.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		// KillAllActivitesAndGoToLogin
		// .delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));
		menu.findItem(R.id.info).setVisible(false);

		// 16Nov2016 Arpitha
		menu.findItem(R.id.registration).setVisible(false);
		menu.findItem(R.id.sms).setVisible(false);
		menu.findItem(R.id.comments).setVisible(false);
		menu.findItem(R.id.about).setVisible(false); // 16Nov2016 Arpitha
		menu.findItem(R.id.changepass).setVisible(false);// 25April2017 Arpitha

		menu.findItem(R.id.sync).setVisible(false);// 31oct2016 Arpitha

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);

		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) // 22oct2016
			menu.findItem(R.id.sms).setVisible(false);

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	private void createChartPulseandBP(Dialog dialog) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		// loadListDataToDisplayValues();
		if (listItems.size() > 0) {
			// Creating an XYSeries for values

			XYSeries bpsystolicseries = new XYSeries(getResources().getString(R.string.systolic));
			XYSeries bpdiastolicseries = new XYSeries(getResources().getString(R.string.diastolic));
			XYSeries pulseseries = new XYSeries(getResources().getString(R.string.pulse));
			for (int i = 0; i < listItems.size(); i++) {

				if (listItems.get(i).getPulse().length() > 0 && !listItems.get(i).getPulse().isEmpty()) {
					pulseseries.add((i + 1), Double.parseDouble(listItems.get(i).getPulse()));
				} else {
					pulseseries.add((i + 1), 0);
				}

				if (listItems.get(i).getBpsystolic().length() > 0 && listItems.get(i).getBpsystolic().length() > 0) {
					bpsystolicseries.add((i + 1), Double.parseDouble(listItems.get(i).getBpsystolic()));
					bpdiastolicseries.add((i + 1), Double.parseDouble(listItems.get(i).getBpdiastolic()));
				} else {
					bpsystolicseries.add((i + 1), 0);
					bpdiastolicseries.add((i + 1), 0);
				}

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset

			dataset.addSeries(bpsystolicseries);
			dataset.addSeries(bpdiastolicseries);
			dataset.addSeries(pulseseries);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer bpsystolicRenderer = new XYSeriesRenderer();
			bpsystolicRenderer.setColor(getResources().getColor(R.color.blue));
			bpsystolicRenderer.setPointStyle(PointStyle.CIRCLE);
			bpsystolicRenderer.setFillPoints(true);
			bpsystolicRenderer.setLineWidth(5);
			bpsystolicRenderer.setDisplayChartValues(true);
			bpsystolicRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer bpdiastolicRenderer = new XYSeriesRenderer();
			bpdiastolicRenderer.setColor(getResources().getColor(R.color.yellow));
			bpdiastolicRenderer.setPointStyle(PointStyle.CIRCLE);
			bpdiastolicRenderer.setFillPoints(true);
			bpdiastolicRenderer.setLineWidth(5);
			bpdiastolicRenderer.setDisplayChartValues(true);
			bpdiastolicRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize pulseSeries
			XYSeriesRenderer pulseRenderer = new XYSeriesRenderer();
			pulseRenderer.setColor(getResources().getColor(R.color.green));
			pulseRenderer.setPointStyle(PointStyle.CIRCLE);
			pulseRenderer.setFillPoints(true);
			pulseRenderer.setLineWidth(3);
			pulseRenderer.setDisplayChartValues(true);
			pulseRenderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle(getResources().getString(R.string.partograph));
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(getResources().getString(R.string.pulse_bp));
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 60, 80, 60, 60 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(25);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			multiRenderer.setPanEnabled(true, true);
			multiRenderer.setYAxisMax(200);
			multiRenderer.setYAxisMin(40);
			multiRenderer.setYLabels(15);
			// multiRenderer.setYAxisMin(40);
			multiRenderer.setBarSpacing(10);
			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);

			multiRenderer.setZoomButtonsVisible(true);
			for (int i = 0; i < listItems.size(); i++) {
				multiRenderer.addXTextLabel((i + 1), listItems.get(i).getTimeofentry());
				multiRenderer.setYAxisMax(300);
				multiRenderer.setYLabels(15);
			}

			multiRenderer.addSeriesRenderer(bpsystolicRenderer);
			multiRenderer.addSeriesRenderer(bpdiastolicRenderer);
			multiRenderer.addSeriesRenderer(pulseRenderer);

			mChartView = null;

			String[] types = new String[] { BarChart.TYPE, BarChart.TYPE, LineChart.TYPE };

			mChartView = ChartFactory.getCombinedXYChartView(getApplicationContext(), dataset, multiRenderer, types);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 400);
			mChartView.setLayoutParams(params);

			RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.rlpulseandbpchart);
			layout.addView(mChartView);

			if (mChartView != null) {
				mChartView.repaint();
			}
		}
	}

	private void showGraphDialog() {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			dialog.setContentView(R.layout.dialog_pulseandbp);

			dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			dialog.getWindow().setBackgroundDrawableResource(R.color.white);

			TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setText(getResources().getString(R.string.pulse_bp));

			ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						dialog.cancel();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			createChartPulseandBP(dialog);
			TableLayout tbl = (TableLayout) dialog.findViewById(R.id.tblvaluespulsebpdialog);

			// Dialog is dismissed
			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
				}
			});

			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// 15Sep2016 - bindu
	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert(getResources().getString(R.string.exit_msg));
		} catch (NotFoundException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Display Confirmation to exit the screen
	private void displayConfirmationAlert(String exit_msg) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(FourthStageOfLabor_View.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);

		// updated on 19/6/16 by
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				Intent main = new Intent(getApplicationContext(), Activity_WomenView.class);
				startActivity(main);

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}
}

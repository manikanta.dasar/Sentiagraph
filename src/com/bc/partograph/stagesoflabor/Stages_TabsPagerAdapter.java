package com.bc.partograph.stagesoflabor;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class Stages_TabsPagerAdapter extends FragmentStatePagerAdapter {

	public Stages_TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	
	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Today fragment activity
			return new ThirdStageOfLabor();
		case 1:
			// Weekly fragment activity
			return new FourthStageOfLabor();
		
		
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}

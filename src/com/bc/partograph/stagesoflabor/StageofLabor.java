package com.bc.partograph.stagesoflabor;

import java.util.Locale;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.FragmentState;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class StageofLabor extends FragmentActivity implements ActionBar.TabListener {

	private ViewPager viewPager;
	private Stages_TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private static final String TAB_KEY_INDEX = "tab_key";
	private String[] tabs;
	FragmentState fragment = null;
	public static MenuItem menuItem;
	Partograph_DB dbh;
	UserPojo user;
	Cursor cursor;
	public static int stagesoflaborpos = 0;
	public static boolean isStagesOfLabor;

	public static boolean isThirdStageDetails = false;

	@Override
	protected void onCreate(Bundle saved) {
		super.onCreate(saved);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			setContentView(R.layout.stagesoflabour);
			dbh = Partograph_DB.getInstance(getApplicationContext());
			user = Partograph_CommonClass.user;

			stagesoflaborpos = Partograph_CommonClass.curr_tabpos;
			isStagesOfLabor = true;

			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());

			tabs = new String[] { getResources().getString(R.string.thirdstageoflabor),
					getResources().getString(R.string.fourthstageoflabor) };

			// Initilization
			viewPager = (ViewPager) findViewById(R.id.pagerstageoflabor);
			viewPager.setOffscreenPageLimit(1);
			actionBar = getActionBar();

			mAdapter = new Stages_TabsPagerAdapter(getSupportFragmentManager());
			viewPager.setAdapter(mAdapter);

			// updated on 17Nov2015
			Partograph_CommonClass.isRecordExistsForSync = dbh.GetSyncCountRecords();

			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			actionBar.setHomeButtonEnabled(true);
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setTitle(getResources().getString(R.string.stagesoflabor));

			// Adding Tabs
			for (String tab_name : tabs) {
				actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
			}

			// restore to navigation
			if (saved != null) {
				actionBar.setSelectedNavigationItem(saved.getInt(TAB_KEY_INDEX, 0));
			}

			// View Third Stage of Labor
			if (ThirdStageOfLabor_View.isThirdStageOfLaborView) {
				actionBar.setSelectedNavigationItem(ThirdStageOfLabor_View.viewthirdstagepos);
				ThirdStageOfLabor_View.isThirdStageOfLaborView = false;
			}

			// View Fourth Stage of Labor
			if (FourthStageOfLabor_View.isFourthStageOfLaborView) {
				actionBar.setSelectedNavigationItem(FourthStageOfLabor_View.viewfourthstagepos);
				FourthStageOfLabor_View.isFourthStageOfLaborView = false;
			}

			/**
			 * on swiping the viewpager make respective tab selected
			 */
			viewPager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int position) {

					try {
						// on changing the page
						// make respected tab selected
						Partograph_CommonClass.curr_tabpos = position;

						actionBar.setSelectedNavigationItem(Partograph_CommonClass.curr_tabpos);
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});

			actionBar.setSelectedNavigationItem(Partograph_CommonClass.curr_tabpos);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		KillAllActivitesAndGoToLogin.addToStack(this);
		// KillAllActivitesAndGoToLogin.activity_stack.add(this);
	}

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		Partograph_CommonClass.curr_tabpos = tab.getPosition();
		viewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.home:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.main));
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			case R.id.settings:
				Intent settings = new Intent(this, Settings_parto.class);
				startActivity(settings);
				return true;

			// case R.id.viewprofile:
			//
			// displayConfirmationAlert(getResources().getString(R.string.exit_msg),
			// getResources().getString(R.string.view_profile));
			// return true;

			case R.id.info:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.info));
				return true;

			case R.id.sms:
				// if (AppContext.checkSimState() ==
				// TelephonyManager.SIM_STATE_READY) {
				Partograph_CommonClass.display_messagedialog(StageofLabor.this, user.getUserId());
				// }

				return true;

			case R.id.usermanual:// 25April2017 Arpitha

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.user_manual));
				return true;

			case R.id.summary:// 25April2017 Arpitha

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.summary));
				return true;

			case R.id.comments:// 25April2017 Arpitha

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.comments));
				return true;
				
			case R.id.about:// 25April2017 Arpitha

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.about));
				return true;
				
			case R.id.changepass:// 25April2017 Arpitha

				Partograph_CommonClass.dispalyChangePasswordDialog(StageofLabor.this);
				return true;
				
			case R.id.disch:
				Intent disch = new Intent(StageofLabor.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;
				
				/*// 03Oct2017 Arpitha
							case R.id.del:
								Intent disc = new Intent(LatentPhase_Activity.this, DeliveryInfo_Activity.class);
								disc.putExtra("woman", woman);
								startActivity(disc);
								return true;
							case R.id.viewprofile:
								Intent view = new Intent(LatentPhase_Activity.this, ViewProfile_Activity.class);
								view.putExtra("woman", woman);
								startActivity(view);
								return true;

							case R.id.ref:
								Intent ref = new Intent(LatentPhase_Activity.this, ReferralInfo_Activity.class);
								ref.putExtra("woman", woman);
								startActivity(ref);
								return true;

							case R.id.pdf:
								Partograph_CommonClass.exportPDFs(LatentPhase_Activity.this, woman);
								return true;

							case R.id.disc:
								Intent discharge = new Intent(LatentPhase_Activity.this, DiscargeDetails_Activity.class);
								discharge.putExtra("woman", woman);
								startActivity(discharge);
								return true;
								
							case R.id.apgar:
								if(woman.getDel_type()!=0)
								{
								Intent apgar = new Intent(LatentPhase_Activity.this, Activity_Apgar.class);
								apgar.putExtra("woman", woman);
								startActivity(apgar);
								}else
									Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
								return true;
								
							case R.id.stages:
								if(woman.getDel_type()!=0)
								{
								Intent stages = new Intent(LatentPhase_Activity.this, StageofLabor.class);
								stages.putExtra("woman", woman);
								startActivity(stages);
								}else
									Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
								return true;
								
							case R.id.post:
								if(woman.getDel_type()!=0)
								{
								Intent post = new Intent(LatentPhase_Activity.this, PostPartumCare_Activity.class);
								post.putExtra("woman", woman);
								startActivity(post);
								}else
									Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
								return true;

							case R.id.parto:
								Intent parto = new Intent(LatentPhase_Activity.this, SlidingActivity.class);
								parto.putExtra("woman", woman);
								startActivity(parto);
								return true;// 03Oct2017 Arpitha
*/
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		// KillAllActivitesAndGoToLogin
		// .delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		menu.findItem(R.id.addcomments).setVisible(false);
		// if (!(AppContext.checkSimState() ==
		// TelephonyManager.SIM_STATE_READY)) {// 22oct2016
		// Arpitha
		// menu.findItem(R.id.sms).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);// 31oct2016 Arpitha

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		// menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 22oct2016
			// Arpitha
			menu.findItem(R.id.sms).setVisible(false);
		}
		
		menu.findItem(R.id.save).setVisible(false);//08Aug2017 Arpitha
		
		menu.findItem(R.id.search).setVisible(false);//14Jun2017 Arpitha
		
		// 03Oct2017 Arpitha
//				menu.findItem(R.id.del).setVisible(true);
//				menu.findItem(R.id.ref).setVisible(true);
//				menu.findItem(R.id.pdf).setVisible(true);
////				menu.findItem(R.id.stages).setVisible(true);
//				menu.findItem(R.id.apgar).setVisible(true);
//				menu.findItem(R.id.post).setVisible(true);
//				menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		// } // 22oct2016 Arpitha
		return super.onPrepareOptionsMenu(menu);

	}

	// Display Confirmation to exit the screen
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(StageofLabor.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);

		// updated on 19/6/16 by
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.login))) {
					Intent login = new Intent(getApplicationContext(), LoginActivity.class);
					startActivity(login);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.settings))) {
					Intent settings = new Intent(getApplicationContext(), Settings_parto.class);
					startActivity(settings);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.info))) {
					Intent info = new Intent(getApplicationContext(), GraphInformation.class);
					startActivity(info);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.user_manual)))// 25April2017
																										// Arpitha
				{
					Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
					startActivity(usermanual);
				} // 25April2017 Arpitha
				else if (classname.equalsIgnoreCase(getResources().getString(R.string.summary)))// 25April2017
																								// Arpitha
				{
					Intent usermanual = new Intent(getApplicationContext(), Summary_Activity.class);
					startActivity(usermanual);
				} // 25April2017 Arpitha
				else if (classname.equalsIgnoreCase(getResources().getString(R.string.comments)))// 25April2017
																									// Arpitha
				{
					Intent usermanual = new Intent(getApplicationContext(), Comments_Activity.class);
					startActivity(usermanual);
				} // 25April2017 Arpitha
				
				else if(classname.equalsIgnoreCase(getResources().getString(R.string.about)))
				{
					Intent usermanual = new Intent(getApplicationContext(), About.class);
					startActivity(usermanual);
					
				}
				

				else {
					Intent main = new Intent(getApplicationContext(), Activity_WomenView.class);
					startActivity(main);
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.main));
		} catch (NotFoundException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		// super.onBackPressed();
	}

}

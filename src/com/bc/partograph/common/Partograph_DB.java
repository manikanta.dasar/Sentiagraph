package com.bc.partograph.common;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.NoRouteToHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import com.bc.partograph.latentphase.LatentPhasePojo;
import com.bc.partograph.sync.PrepareSyncXML;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Pojo;
import com.bluecrimson.partograph.PhoneNumber_Pojo;
import com.bluecrimson.partograph.Property_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.SettingsPojo;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartum_Pojo;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Base64;

public class Partograph_DB extends SQLiteOpenHelper {

	public static Partograph_DB dbhInstance = null;

	private static final String DATABASE_NAME = Partograph_CommonClass.DATABASE_NAME;
	private static final int DATABASE_VERSION = Partograph_CommonClass.DATABASE_VERSION;

	public SQLiteDatabase db;
	Context context;

	// tbl_registeredwomen
	public static final String TBL_REGISTEREDWOMEN = "tbl_registeredwomen";

	// tbl_user
	public static final String TBL_USER = "tbl_user";

	// tbl_transheader
	public static final String TBL_TRANSHEADER = "tbltransHeader";

	// tbl_transdetail
	public static final String TBL_TRANSDETAIL = "tbltransdetail";

	// tbl_settings
	public static final String TBL_SETTINGS = "tbl_Settings";

	public static final String TRANS_ID = "trans_Id";
	// COLUMN names tbl_user
	public static final String PASSWORD = "password";
	public static final String LAST_WOMENNUM = "LastWomenNumber";
	public static final String LAST_TRANSNUM = "LastTransNumber";
	public static final String LAST_REQUESTNUM = "LastRequestNumber";
	public static final String RUNNIND_ID = "RunningId";
	public static final String DEVICE_ID = "DeviceID";
	public static final String STATE = "state";
	public static final String DISTRICT = "district";
	public static final String TALUK = "taluk";
	public static final String FACILITY_NAME = "facility_name";
	public static final String DATEINSERTED = "dateinserted";

	// 20Aug2015
	public static final String USERID = "user_id";
	// COLUMN NAMES tbl_registeredwomen
	public static final String USER_ID = "user_Id";
	public static final String WOMEN_ID = "women_Id";
	public static final String WOMEN_IMAGE = "women_Image";
	public static final String WOMEN_NAME = "women_Name";
	public static final String DATE_OF_ADM = "date_of_adm";
	public static final String TIME_OF_ADM = "time_of_adm";
	public static final String W_AGE = "w_age";
	public static final String ADDRESS = "address";
	public static final String PHONE_NUM = "phone_num";
	public static final String DEL_STATUS = "del_status";
	public static final String DOC_NAME = "doc_name";
	public static final String NURSE_NAME = "nurse_name";
	public static final String ATTENDANT_NAME = "attendant_name";
	public static final String GRAVIDA = "gravida";
	public static final String PARA = "para";
	public static final String HOSP_NO = "hosp_no";
	public static final String FACILITY = "facility";
	// public static final String RUPTURED_MEMB = "ruptured_membranes";
	public static final String SPECIAL_INSTRUCTIONS = "special_instructions";
	public static final String COMMENTS = "comments";
	public static final String COND_WHILE_ADMN = "cond_while_admn";
	public static final String DEL_COMMENTS = "del_comments";
	public static final String DEL_TIME = "del_Time";
	public static final String DEL_DATE = "del_date";
	public static final String DEL_RESULT1 = "del_result1";
	public static final String NO_OF_CHILDREN = "no_of_children";
	public static final String BABYWT1 = "babywt1";
	public static final String BABYWT2 = "babywt2";
	public static final String BABYSEX1 = "babysex1";
	public static final String BABYSEX2 = "babysex2";
	public static final String GESTATION_AGE = "gestation_age";
	public static final String MOTHERSDEATH = "MothersDeath";
	public static final String DEL_RESULT2 = "del_result2";
	public static final String DEL_TIME2 = "del_Time2";

	public static final String RISK_CATEGORY = "risk_category";
	public static final String ADMITTED_WITH = "admitted_with";
	public static final String MEMBRANES_PRES_ABS = "memb_pres_absent";
	public static final String MOTHERS_DEATH_REASON = "mothers_death_reason";

	public static final String W_MONTH = "wmonth";
	public static final String W_YEAR = "wyear";
	public static final String DATE_LASTUPDATED = "datelastupdated";
	public static final String GESTATION_AGE_DAYS = "gestation_age_days";

	// Column names
	public static final String GRAPH_VAlUES = "graph_values";
	public static final String TIME_OF_ENTRY = "time_of_entry";
	public static final String DATE_OF_ENTRY = "date_of_entry";

	// table tbl_ObjectTypeMaster
	public static final String TBL_OBJECTTYPEMASTER = "tbl_ObjectTypeMaster";

	// col names tbl_ObjectTypeMaster
	public static final String C_OBJECTID = "object_Id";
	public static final String C_OBJECTTYPE = "object_Type";

	// tbl_PropertyValues
	public static final String TBL_PROPERTYVALUES = "tbl_propertyValues";

	// Col tbl_propertyvalues
	public static final String C_TRANSID = "trans_id";
	public static final String C_PROP_ID = "prop_Id";
	public static final String C_PROP_NAME = "prop_Name";
	public static final String C_PROP_VALUE = "prop_value";
	public static final String C_CHILDID = "child_Id";
	public static final String C_ISDANGER = "isDanger";

	// tbl_propertyMaster
	public static final String TBL_PROPERTYMASTER = "tbl_PropertyMaster";

	// tbl_syncmaster
	public static final String TBL_SYNCMASTER = "tbl_SyncMaster";

	// Col names of tbl_SyncMaster
	public static final String C_REQUESTID = "RequestId";
	public static final String C_REQUESTDATE = "RequestDate";
	public static final String C_REQUESTSTATUSDATE = "RequestStatusDate";
	public static final String C_REQUESTSTATUS = "RequestStatus";

	// Col names of tbltransHeader
	public static final String C_TRANSDATE = "TransDate";
	public static final String C_TRANSSTATUS = "TransStatus";

	// Col names of tbltransdetail
	public static final String C_ACTION = "Action";
	public static final String C_TABLENAME = "TableName";
	public static final String C_SQLSTATEMENT = "SQLStatement";

	// TBL COMMENT
	public static final String TBL_COMMENT = "tbl_Comment";

	// COLS TBL_COMMENT
	public static final String C_COMMENT = "Comments";
	public static final String C_ISVIEWED = "isviewed";

	// TBL REFERRAL
	public static final String TBL_REFERRAL = "tbl_referral";

	// COLS TBL_REFERRAL
	public static final String C_WNAME = "womenname";
	public static final String C_FNAME = "facilityname";
	public static final String C_REFERREDTOFACILITY = "referredto_facility";
	public static final String C_PLACEOFREFERRAL = "placeofreferral";
	public static final String C_REASONFORREFERRAL = "reasonforreferral";
	public static final String C_DATEOFREFERRAL = "dateofreferral";
	public static final String C_TIMEOFREFERRAL = "timeofreferral";
	public static final String C_CREATEDDATE = "created_date";
	public static final String C_LASTDATEUPDATED = "lastdateupdated";

	// 04jan2016
	public static final String C_DESCFORREFERRAL = "descriptionforreferral";
	public static final String C_CONDITIONOFMOTHER = "conditionofmother";
	public static final String C_BP = "BP";
	public static final String C_PULSE = "pulse";
	public static final String C_CONDITIONOFBABY = "conditionofbaby";

	// 30Nov2015
	public static final String C_THAYICARDNUM = "thaicardno";
	public static final String C_DELTYPEREASON = "deltype_reason";
	public static final String C_DELTYPEOTHERREASONS = "deltype_otherreasons";

	public static final String C_ISREFERRED = "isReferred";

	// 25dec2015
	public static final String C_LMP = "LMP";
	public static final String C_RISKOPTIONS = "riskoptions";

	// 04jan2016
	public static final String C_TEARS = "tears";
	public static final String C_EPISIOTOMY = "episiotomy";
	public static final String C_SUTUREMATERIAL = "suturematerial";

	// 19jan2016
	public static final String C_BLOODGROUP = "bloodgroup";

	// updated on27Feb2016 by Arpitha
	public static final String C_EDD = "EDD";
	public static final String C_Height = "Height";
	public static final String C_Weight = "Weight";
	public static final String C_AdmittedWithOther = "AdmittedWithOther";

	public static final String C_HeartRate = "HeartRate";
	public static final String C_SPO2 = "SPO2";

	// updated on 20May16
	public static final String C_heightunit = "height_unit";

	// updated on 20july2016 by Arpitha
	public static final String TBL_PHONENUMBER = "tbl_phonenumbers";
	public static final String C_SELECTEDOPTION = "selectedoption";
	public static final String C_PHONENUMBER = "phno_no";

	public static final String C_EXTRACOMMENTS = "extra_comments";// 13Oct2016
																	// Arpitha

	// 14Oct2016 Arpitha
	public static final String C_GENERALCOMMENT = "comment";
	public static final String C_date_of_insertion = "date_of_insertion";
	public static final String C_time_of_insertion = "time_of_insertion";
	public static final String C_Givenby = "Givenby";
	public static final String TBL_GENERALCOMMENTS = "tbl_generalcomments";

	public static final String TBL_MESSAGELOG = "tblMessageLog";

	public static final String C_REGTYPE = "regtype";// 16Oct2016 Arpitha

	public static final String C_REGTYPEREASON = "regtypereason";// 18Oct2016
																	// Arpitha

	public static final String DATE_OF_REG_ENTRY = "date_of_reg_entry";// 10Nov2016
																		// Arpitha

	public static final String C_NORMALTYPE = "normaltype";// 20Nov2016 Arpitha

	public static final String C_REGTYPEOTHERREASON = "regtypeOtherReason";// 21Nov2016
																			// Arpitha

	public static final String C_EMAILID = "emailid";// 06April2017 Arpitha

	public static String Lock = "dblock";
	// 24July2017 Arpitha - v3.0
	public static final String TBLdischargedetails = "tbldischargedetails";
	public static final String DISC_discUserId = "discUserId";
	public static final String DISC_discWomanId = "discWomanId";
	public static final String DISC_discWomanName = "discWomanName";
	public static final String DISC_discDateOfDischarge = "discDateTimeOfDischarge";
	// public static final String DISC_discTimeOfDischarge =
	// "discTimeOfDischarge";
	public static final String DISC_discMotherInvestigations = "discWomanInvestigations";
	public static final String DISC_discChildInvestigations = "discChildInvestigations";
	public static final String DISC_discChild2Investigations = "discChild2Investigations";
	public static final String DISC_discStatusOfWoman = "discStatusOfWoman";
	public static final String DISC_discReferred = "discReferred";
	public static final String DISC_discReasonForDischarge = "discReasonForDischarge";
	public static final String DISC_discDischargeDiagonsis = "discDischargeDiagnosis";
	public static final String DISC_discPatientDisposition = "discPatientDisposition";
	public static final String DISC_discFindingsHistory = "discFindingsHistory";
	public static final String DISC_discActionTaken = "discActionTaken";
	public static final String DISC_discMotherCondition = "discMotherCondition";
	public static final String DISC_discChildCondition = "discChildCondition";
	public static final String DISC_discChild2Condition = "discChild2Condition";
	public static final String DISC_discAdvice = "discAdvice";
	public static final String DISC_discComplicationObserved = "discComplicationObserved";
	public static final String DISC_discProcedures = "discProcedures";
	public static final String DISC_discDateTimeOfNextFollowUp = "discDateTimeOfNextFollowUp";
	public static final String DISC_discPlaceOfNextFollowUp = "discPlaceOfNextFollowUp";
	public static final String DISC_discAdmissionReasons = "discAdmissionReasons";
	public static final String DISC_discNameOfHCProvider = "discNameOfHCProvider";
	public static final String DISC_discDesignationOfHealthCareProvider = "discDesignationOfHCProvider";
	public static final String DISC_discComments = "discComments";
	public static final String DISC_discTransId = "trans_id";
	public static final String DISC_discDateOfInsertion = "discDateOfInsertion";
	public static final String DISC_discDateModified = "discDateModified";

	// 10Aug2017 Arpitha - latent phase
	public static final String TBL_LATENTPHASE = "tbllatentphase";
	public static final String LAT_USERID = "latUserId";
	public static final String LAT_WOMANID = "latWomanId";
	public static final String LAT_PULSE = "latPulse";
	public static final String LAT_BP = "latBp";
	public static final String LAT_CONTRACTIONS = "latContractions";
	// public static final String LAT_DURATION = "latDuration";
	public static final String LAT_FHS = "latFhs";
	public static final String LAT_PV = "latPv";
	public static final String LAT_ADVICE = "latAdvice";
	public static final String LAT_DATETIME = "latdateOfEntry";
	public static final String LAT_WOMANAME = "latWomanName";
	public static final String LAT_TRANSID = "trans_id";

	// 21Aug2017 Arpitha
	public static final String TBL_POSTPARTUM = "tblpostpartumcare";
	public static final String POST_USERID = "postuserid";
	public static final String POST_WOMANID = "postwomanid";
	public static final String POST_WOMAN_NAME = "postwomanname";
	public static final String POST_EXAMINATION = "postexamination";
	public static final String POST_COMPLAINTS = "postpresentingcomplaints";
	public static final String POST_PULSE = "postpulse";
	public static final String POST_BP = "postbp";
	public static final String POST_PALLOR = "postpallor";
	public static final String POST_BREASTEXAMINATION = "postbreastexamination";
	public static final String POST_INVOLUTION = "postinvolutionOfUterus";
	public static final String POST_LOCHIA = "postlochia";
	public static final String POST_PERINEALCARE = "postperinealcare";
	public static final String POST_ADVICE = "postadvice";
	public static final String POST_DATETIMEOFENTRY = "postdatetimeofentry";
	public static final String POST_OTHER = "postOtherpresentingcomplaints";

	/** To Create one instance of sqliteopenhelper object */
	public static Partograph_DB getInstance(Context context) {
		if (dbhInstance == null) {
			dbhInstance = new Partograph_DB(context.getApplicationContext());
		}
		return dbhInstance;
	}

	public Partograph_DB(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		this.db = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// 06Jan2015 - backup the existing db file before upgrading
		Partograph_CommonClass.copyDbToExternalStorage(context, Partograph_CommonClass.DATABASE_NAME, oldVersion);// 21Nov2016
																													// -
																													// pass
																													// version
																													// number
		int upgradeTo = oldVersion + 1;
		try {
			while (upgradeTo <= newVersion) {
				switch (upgradeTo) {
				case 2:
					updateTablesandData("partodbversion1_to_2.sql", db);
					break;
				// updated on 27Feb16 by Arpitha
				case 3:
					updateTablesandData("partodbversion2_to3.sql", db);
					break;

				case 4:
					updateTablesandData("partographdbversion3_to4.sql", db);
					break;

				case 5:
					updateTablesandData("partographdbversion4_to_5.sql", db);
					break;

				case 6:
					updateTablesandData("partographdbversion5_to_6.sql", db);
					break;
				// 16May2017 Arpitha - v2.6 color coded values
				case 7:
					updateTablesandData("partographdbversion6_to_7.sql", db);
					break;

				// 17Aug2017 Arpitha - v2.6 color coded values
				case 8:
					updateTablesandData("partographdbversion_7_to_8.sql", db);
					break;

				case 9:
					updateTablesandData("partographdbversion_8_to_9.sql", db);
					break;

				default:
					break;
				}
				upgradeTo++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Register the user - add one row in TBL_REGISTEREDWOMEN
	 * 
	 * @param wpojo
	 * @param transId
	 * @return true if record is inserted else false
	 * @throws Exception
	 */
	public boolean addUser(Women_Profile_Pojo wpojo, int transId) throws Exception {
		boolean isRegistered = false;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(USER_ID, wpojo.getUserId());
		byte[] b = wpojo.getWomen_Image();
		String encodedImage = (b != null) ? Base64.encodeToString(b, Base64.DEFAULT) : "";
		values.put(WOMEN_IMAGE, encodedImage);
		values.put(WOMEN_NAME, wpojo.getWomen_name());
		values.put(DATE_OF_ADM, wpojo.getDate_of_admission());
		values.put(TIME_OF_ADM, wpojo.getTime_of_admission());
		values.put(W_AGE, wpojo.getAge());
		values.put(ADDRESS, wpojo.getAddress());
		values.put(PHONE_NUM, wpojo.getPhone_No());
		values.put(DOC_NAME, wpojo.getDoc_name());
		values.put(NURSE_NAME, wpojo.getNurse_name());
		values.put(ATTENDANT_NAME, wpojo.getW_attendant());
		values.put(GRAVIDA, wpojo.getGravida());
		values.put(PARA, wpojo.getPara());
		values.put(HOSP_NO, wpojo.getHosp_no());
		values.put(FACILITY, wpojo.getFacility());
		values.put(SPECIAL_INSTRUCTIONS, wpojo.getSpecial_inst());
		values.put(TRANS_ID, transId);
		values.put(WOMEN_ID, wpojo.getWomenId());
		values.put(COMMENTS, wpojo.getComments());
		values.put(GESTATION_AGE, wpojo.getGestationage());
		values.put(COND_WHILE_ADMN, wpojo.getRisk_category());
		values.put(ADMITTED_WITH, wpojo.getAdmitted_with());
		values.put(MEMBRANES_PRES_ABS, wpojo.getMemb_pres_abs());
		values.put(STATE, wpojo.getState());
		values.put(DISTRICT, wpojo.getDistrict());
		values.put(TALUK, wpojo.getTaluk());
		values.put(FACILITY_NAME, wpojo.getFacility_name());
		values.put(DATEINSERTED, wpojo.getDateinserted());
		values.put(W_MONTH, wpojo.getWmonth());
		values.put(W_YEAR, wpojo.getWyear());
		values.put(GESTATION_AGE_DAYS, wpojo.getGest_age_days());
		// 30Nov2015
		values.put(C_THAYICARDNUM, wpojo.getThayicardnumber());

		// 25dec2015
		values.put(C_LMP, wpojo.getLmp());
		values.put(C_RISKOPTIONS, wpojo.getRiskoptions());
		values.put(C_BLOODGROUP, wpojo.getBloodgroup());

		// updated on 27Feb2016 by Arpitha

		values.put(C_EDD, wpojo.getEdd());
		values.put(C_AdmittedWithOther, wpojo.getOther());
		values.put(C_Height, wpojo.getHeight());
		/* values.put(C_Weight, wpojo.getWeight()); */
		values.put(C_Weight, wpojo.getW_weight());
		// updated on 20May16 by Arpitha
		values.put(C_heightunit, wpojo.getHeight_unit());

		values.put(C_EXTRACOMMENTS, wpojo.getExtracomments());// 13Oct2016
																// Arpitha

		values.put(C_REGTYPE, wpojo.getregtype());// 16Oct2016 ARpitha

		values.put(C_REGTYPEREASON, wpojo.getRegtypereason());// 18Oct2016
																// ARpitha
		values.put(DATE_OF_REG_ENTRY, wpojo.getDate_of_reg_entry());// 10Nov2016

		values.put(C_REGTYPEOTHERREASON, wpojo.getRegtypeOtherReason());// 21Feb2017
																		// Arpitha
		values.put(C_NORMALTYPE, wpojo.getNormaltype());// 05May2017 Arpitha -
														// v2.6 mantis
														// id-0000231

		long id = db.insert(TBL_REGISTEREDWOMEN, null, values);
		if (id > 0) {
			isRegistered = true;
		}
		return isRegistered;
	}

	/**
	 * Get date from input string
	 * 
	 * @param inputDate
	 * @return date in yyyy-MM-dd
	 */
	public Date getDateS(String inputDate) {
		// SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date sendDate = null;
		try {
			sendDate = sdf.parse(inputDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendDate;
	}

	/**
	 * Get time from input string
	 * 
	 * @param inputDate
	 * @return time in HH:mm
	 */
	public Date getTime(String inputTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date sendDate = null;
		try {
			sendDate = sdf.parse(inputTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendDate;
	}

	/**
	 * Get user details from tbl_user
	 * 
	 * @param inputDate
	 * @return user
	 */
	public UserPojo getUserProfile() throws Exception {
		db = this.getWritableDatabase();
		UserPojo user = null;

		String strselect = "SELECT * FROM " + TBL_USER;
		Cursor cursor = db.rawQuery(strselect, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				user = new UserPojo();
				user.setUserId(cursor.getString(0));
				user.setPwd(cursor.getString(1));
				user.setLastWomennumber(cursor.getInt(2));
				user.setState(cursor.getString(7));
				user.setDistrict(cursor.getString(8));
				user.setTaluk(cursor.getString(9));
				user.setFacility(cursor.getString(10));
				user.setFacility_name(cursor.getString(11));
				user.setLastTransNumber(cursor.getInt(3));
				user.setLastRequestNumber(cursor.getInt(5));

				user.setEmailid(cursor.getString(13));// 06April2017 Arpitha

			} while (cursor.moveToNext());
		}

		if (cursor != null)
			cursor.close();

		return user;
	}

	/**
	 * Create tables and insert data to specified tables - read file from assets
	 * folder
	 */
	public void createTablesAndData() {
		db = this.getWritableDatabase();
		int count = 0;
		try {
			InputStream in = context.getResources().getAssets().open("partograph.sql");
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			while (true) {
				int read = in.read(buffer);
				if (read == -1) {
					break;
				}
				out.write(buffer, 0, read);
			}

			byte[] bytes = out.toByteArray();
			String sql = new String(bytes);
			String[] lines = sql.split(";(\\s)*[\n\r]");
			if (true) {
				db.beginTransaction();
				int cnt = 0;
				for (String line : lines) {
					line = line.trim();
					if (line.length() > 0) {
						cnt++;
						db.execSQL(line);
						count++;
					}
				}
				db.setTransactionSuccessful();
			}
			db.endTransaction();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	/**
	 * Get LastWomenNumber, Add 1 and Generate the New WomenId
	 * 
	 * @param userId
	 * @return womenid
	 * @throws Exception
	 */
	public String getWomenId(String userId) throws Exception {
		String womenID = null;
		db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT LastWomenNumber FROM " + TBL_USER + " WHERE " + USER_ID + " = '" + userId + "'",
				null);
		while (c.moveToNext()) {
			int lastwomenno = c.getInt(0) + 1;
			int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
			if (lastwomenno <= 9) {
				womenID = year + userId + "00" + lastwomenno;
			} else if (lastwomenno <= 99) {
				womenID = year + userId + "0" + lastwomenno;
			} else {
				womenID = year + userId + lastwomenno;
			}
		}
		c.close();
		return womenID;
	}

	/**
	 * Create new trans Id
	 * 
	 * @param userId
	 * @return transid
	 */
	public int iCreateNewTrans(String userId) {
		db = this.getWritableDatabase();
		int LastTransNumber = 0;
		int NewTransNumber = 0;
		String format = "dd-MM-yyyy";
		String TodaysDateString = Partograph_CommonClass.getTodaysDate(format);

		LastTransNumber = getIntUsersValue(userId, "LastTransNumber");
		NewTransNumber = LastTransNumber + 1;

		try {
			Object[] arr1 = { TodaysDateString, "N", "0", NewTransNumber };
			db.execSQL(Partograph_CommonClass.SQL3, arr1);

			String strUpdate = "Update " + TBL_USER + " SET " + LAST_TRANSNUM + " = '" + NewTransNumber + "'"
					+ " WHERE " + USER_ID + " = '" + userId + "'";
			db.execSQL(strUpdate);

			return NewTransNumber;
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return 0;
		}

	}

	/**
	 * Get value from tbl_user
	 * 
	 * @param UserId
	 * @param ColumnName
	 * @return specified column val
	 */
	private int getIntUsersValue(String UserId, String ColumnName) {
		Cursor c;
		int returnValue = 0;
		db = this.getWritableDatabase();
		String[] arr = { UserId };
		c = db.rawQuery("SELECT " + ColumnName + " FROM tbl_User Where user_Id = ? ", arr);
		if (c.getCount() > 0) {
			if (c.moveToFirst()) {
				returnValue = c.getInt(0);
			}
		}
		c.close();
		return returnValue;
	}

	/**
	 * Add new record to tbltransdetail
	 * 
	 * @param userId
	 * @param transId
	 * @param tableName
	 * @return
	 */
	public boolean iNewRecordTrans(String userId, int transId, String tableName) {
		db = this.getWritableDatabase();
		Object[] arr = { "I", tableName, "", userId, transId };
		try {
			db.execSQL(Partograph_CommonClass.SQL2, arr);
			return true;

		} catch (Exception E) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					E);
			return false;
		}
	}

	/**
	 * update last women number by incrementing womennumber by 1
	 * 
	 * @param userId
	 * @param transId
	 * @throws Exception
	 */
	public String UpdateLastWomenNumber(String userId, int transId) throws Exception {
		db = this.getWritableDatabase();

		try {
			db.execSQL(Partograph_CommonClass.SQL5, new Object[] { userId });
			iUpdateRecordTrans(userId, transId, TBL_USER, Partograph_CommonClass.SQL5, new String[] {},
					new Object[] { userId });

			return Partograph_CommonClass.SQL5;
		} catch (Exception e) {
			e.printStackTrace();
			return new String();
		}
	}

	// Update transaction table

	/**
	 * update transaction table
	 * 
	 * @param userId
	 * @param transId
	 * @param tableName
	 * @param SQLStatement
	 * @param arr2
	 * @param arr3
	 * @return
	 */
	@SuppressLint("DefaultLocale")
	public boolean iUpdateRecordTrans(String userId, int transId, String tableName, String SQLStatement, String[] arr2,
			Object[] arr3) {
		try {
			String WHERECondition;
			String SQLStr = null;
			Cursor c;

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			int idx = SQLStatement.toLowerCase().indexOf("WHERE".toLowerCase());
			if (idx < 0) {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				return false;
			} else
				WHERECondition = SQLStatement.substring(idx);

			// Check if an Insert is already Present
			SQLStr = "SELECT * FROM " + tableName + " " + WHERECondition;
			c = db.rawQuery(SQLStr, arr2);
			int NoofRecords = c.getCount();
			c.close();

			if (NoofRecords > 0) {
				String str = SQLStatement;
				String stemp = "";
				int i = 0;

				if (str.contains("?")) {
					while (str.length() > 2) {
						if (arr3 != null && arr3.length > 0) {
							stemp = stemp + str.substring(0, str.indexOf("?") + 1);
							Object obj = arr3[i++];
							if (obj instanceof String)
								stemp = stemp.replace("?", "'" + obj.toString() + "'");
							else
								stemp = stemp.replace("?", " " + obj.toString() + " ");
							str = str.substring(str.indexOf("?") + 1);
						} else {
							return false;
						}
					}
				} else {
					stemp = SQLStatement;
				}

				Object[] arr = { "U", tableName, stemp, userId, transId };
				db.execSQL(Partograph_CommonClass.SQL2, arr);
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			return false;
		}
		return true;
	}

	/**
	 * Update women registration details in tbl_registeredwomen
	 * 
	 * @param wpojo
	 * @param transId
	 * @param oldWomanObj
	 * @return
	 * @throws Exception
	 */
	public String updateWomenRegData(Women_Profile_Pojo wpojo, int transId, Women_Profile_Pojo oldWObj)
			throws Exception {
		db = this.getWritableDatabase();
		try {

			ArrayList<AuditTrailPojo> auditArr = new ArrayList<AuditTrailPojo>();
			String appendStr = "";

			byte[] oldB = oldWObj.getWomen_Image();
			String oldEncodedImage = (oldB != null) ? Base64.encodeToString(oldB, Base64.DEFAULT) : "";

			byte[] b1 = wpojo.getWomen_Image();
			String encodedImage1 = (b1 != null) ? Base64.encodeToString(b1, Base64.DEFAULT) : "";

			// image- 21oct2016 changed woman image to string
			if (!oldEncodedImage.equals(encodedImage1)) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + WOMEN_IMAGE + " = " + (char) 39 + encodedImage1 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, WOMEN_IMAGE,
						Partograph_CommonClass.getCurrentDateandTime(), "oldwomanimage", "womanimageupdated", transId));
			}

			// woman name
			if (isEligibleEdit(oldWObj.getWomen_name(), wpojo.getWomen_name())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + WOMEN_NAME + " = " + (char) 39 + wpojo.getWomen_name() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, WOMEN_NAME,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getWomen_name(), wpojo.getWomen_name(),
						transId));
			}

			// Age
			if (oldWObj.getAge() != wpojo.getAge()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + W_AGE + " = " + (char) 39 + wpojo.getAge() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, W_AGE,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getAge() + "", wpojo.getAge() + "",
						transId));
			}

			// 19Aug2016 - risk category , risk options, other risk observed
			// Risk category
			if (oldWObj.getRisk_category() != wpojo.getRisk_category()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + COND_WHILE_ADMN + " = " + (char) 39 + wpojo.getRisk_category() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						COND_WHILE_ADMN, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getRisk_category() + "", wpojo.getRisk_category() + "", transId));
			}

			// Risk Options
			if (isEligibleEdit(oldWObj.getRiskoptions(), wpojo.getRiskoptions())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_RISKOPTIONS + " = " + (char) 39 + wpojo.getRiskoptions() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_RISKOPTIONS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getRiskoptions(),
						wpojo.getRiskoptions(), transId));
			}

			// Other Risk observed
			if (isEligibleEdit(oldWObj.getComments(), wpojo.getComments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + COMMENTS + " = " + (char) 39 + wpojo.getComments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, COMMENTS,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getComments(), wpojo.getComments(),
						transId));
			}

			// Delstatus
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_STATUS + " = " + (char) 39 + wpojo.getDel_type() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_STATUS,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_type() + "", transId));

			// No of children
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + NO_OF_CHILDREN + " = " + (char) 39 + wpojo.getNo_of_child() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, NO_OF_CHILDREN,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getNo_of_child() + "", transId));

			// Del Time
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_TIME + " = " + (char) 39 + wpojo.getDel_Time() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_TIME,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_Time() + "", transId));

			// Del Date
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_DATE + " = " + (char) 39 + wpojo.getDel_Date() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_DATE,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_Date(), transId));

			// Del Result
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_RESULT1 + " = " + (char) 39 + wpojo.getDel_result1() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_RESULT1,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_result1() + "", transId));

			int noofchild = wpojo.getNo_of_child();
			String deltime2 = "";
			int del_result2 = 99, babysex2 = 99;
			if (noofchild == 2) {
				deltime2 = wpojo.getDel_time2();
				del_result2 = wpojo.getDel_result2();
				babysex2 = wpojo.getBabysex2();

				if (appendStr != "")
					appendStr = appendStr + ", ";

				appendStr = appendStr + DEL_TIME2 + " = " + (char) 39 + deltime2 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_TIME2,
						Partograph_CommonClass.getCurrentDateandTime(), "", deltime2 + "", transId));

				// Del Result2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DEL_RESULT2 + " = " + (char) 39 + del_result2 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_RESULT2,
						Partograph_CommonClass.getCurrentDateandTime(), "", del_result2 + "", transId));

				// Baby Sex2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYSEX2 + " = " + (char) 39 + babysex2 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYSEX2,
						Partograph_CommonClass.getCurrentDateandTime(), "", babysex2 + "", transId));

				// Baby wt 2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYWT2 + " = " + (char) 39 + wpojo.getBabywt2() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT2,
						Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getBabywt2() + "", transId));

			}
			// Address
			if (isEligibleEdit(oldWObj.getAddress(), wpojo.getAddress())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + ADDRESS + " = " + (char) 39 + wpojo.getAddress() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, ADDRESS,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getAddress(), wpojo.getAddress(),
						transId));
			}

			// Phone num
			if (isEligibleEdit(oldWObj.getPhone_No(), wpojo.getPhone_No())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + PHONE_NUM + " = " + (char) 39 + wpojo.getPhone_No() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, PHONE_NUM,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getPhone_No(), wpojo.getPhone_No(),
						transId));
			}

			// Doc Name
			if (isEligibleEdit(oldWObj.getDoc_name(), wpojo.getDoc_name())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DOC_NAME + " = " + (char) 39 + wpojo.getDoc_name() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DOC_NAME,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDoc_name(), wpojo.getDoc_name(),
						transId));
			}

			// Nurse name
			if (isEligibleEdit(oldWObj.getNurse_name(), wpojo.getNurse_name())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + NURSE_NAME + " = " + (char) 39 + wpojo.getNurse_name() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, NURSE_NAME,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getNurse_name(), wpojo.getNurse_name(),
						transId));
			}

			// Attendant name
			if (isEligibleEdit(oldWObj.getW_attendant(), wpojo.getW_attendant())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + ATTENDANT_NAME + " = " + (char) 39 + wpojo.getW_attendant() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						ATTENDANT_NAME, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getW_attendant(),
						wpojo.getW_attendant(), transId));
			}

			// gravida
			if (oldWObj.getGravida() != wpojo.getGravida()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + GRAVIDA + " = " + (char) 39 + wpojo.getGravida() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, GRAVIDA,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getGravida() + "",
						wpojo.getGravida() + "", transId));
			}

			// para
			if (oldWObj.getPara() != wpojo.getPara()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + PARA + " = " + (char) 39 + wpojo.getPara() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, PARA,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getPara() + "", wpojo.getPara() + "",
						transId));
			}

			// Hosp
			if (isEligibleEdit(oldWObj.getHosp_no(), wpojo.getHosp_no())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + HOSP_NO + " = " + (char) 39 + wpojo.getHosp_no() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, HOSP_NO,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getHosp_no(), wpojo.getHosp_no(),
						transId));
			}

			// Special Instructions
			if (isEligibleEdit(oldWObj.getSpecial_inst(), wpojo.getSpecial_inst())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + SPECIAL_INSTRUCTIONS + " = " + (char) 39 + wpojo.getSpecial_inst() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						SPECIAL_INSTRUCTIONS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getSpecial_inst(),
						wpojo.getSpecial_inst(), transId));
			}

			// Comments - risk observed
			if (isEligibleEdit(oldWObj.getComments(), wpojo.getComments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + COMMENTS + " = " + (char) 39 + wpojo.getComments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, COMMENTS,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getComments(), wpojo.getComments(),
						transId));
			}

			// Delivery Comments
			if (isEligibleEdit(oldWObj.getDel_Comments(), wpojo.getDel_Comments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DEL_COMMENTS + " = " + (char) 39 + wpojo.getDel_Comments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						DEL_COMMENTS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDel_Comments(),
						wpojo.getDel_Comments(), transId));
			}

			// Baby wt 1
			if (oldWObj.getBabywt1() != wpojo.getBabywt1()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYWT1 + " = " + (char) 39 + wpojo.getBabywt1() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT1,
						Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getBabywt1() + "", transId));
			}

			// Baby Sex1
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + BABYSEX1 + " = " + (char) 39 + wpojo.getBabysex1() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYSEX1,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getBabysex1() + "", transId));

			// Gestation age
			if (oldWObj.getGestationage() != wpojo.getGestationage()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + GESTATION_AGE + " = " + (char) 39 + wpojo.getGestationage() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						GESTATION_AGE, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getGestationage() + "",
						wpojo.getGestationage() + "", transId));
			}

			// Mothers death
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + MOTHERSDEATH + " = " + (char) 39 + wpojo.getMothersdeath() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, MOTHERSDEATH,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getMothersdeath() + "", transId));

			// Mothers death reason
			if (isEligibleEdit(oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + MOTHERS_DEATH_REASON + " = " + (char) 39 + wpojo.getMothers_death_reason()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						MOTHERS_DEATH_REASON, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason(), transId));
			}

			// Gestation age days
			if (oldWObj.getGest_age_days() != wpojo.getGest_age_days()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + GESTATION_AGE_DAYS + " = " + (char) 39 + wpojo.getGest_age_days() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						GESTATION_AGE_DAYS, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getGest_age_days() + "", wpojo.getGest_age_days() + "", transId));
			}

			// thai card no
			if (isEligibleEdit(oldWObj.getThayicardnumber(), wpojo.getThayicardnumber())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_THAYICARDNUM + " = " + (char) 39 + wpojo.getThayicardnumber() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_THAYICARDNUM, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getThayicardnumber(),
						wpojo.getThayicardnumber(), transId));
			}

			// LMP
			if (isEligibleEdit(oldWObj.getLmp(), wpojo.getLmp())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_LMP + " = " + (char) 39 + wpojo.getLmp() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_LMP,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getLmp(), wpojo.getLmp(), transId));
			}

			// Blood group
			if (oldWObj.getBloodgroup() != wpojo.getBloodgroup()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_BLOODGROUP + " = " + (char) 39 + wpojo.getBloodgroup() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_BLOODGROUP, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBloodgroup() + "",
						wpojo.getBloodgroup() + "", transId));
			}

			// EDD
			if (isEligibleEdit(oldWObj.getEdd(), wpojo.getEdd())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_EDD + " = " + (char) 39 + wpojo.getEdd() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_EDD,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getEdd(), wpojo.getEdd(), transId));
			}

			// Height
			if (isEligibleEdit(oldWObj.getHeight(), wpojo.getHeight())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_Height + " = " + (char) 39 + wpojo.getHeight() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_Height,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getHeight(), wpojo.getHeight(),
						transId));
			}

			// Weight
			if (isEligibleEdit(oldWObj.getW_weight(), wpojo.getW_weight())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_Weight + " = " + (char) 39 + wpojo.getW_weight() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_Weight,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getW_weight(), wpojo.getW_weight(),
						transId));
			}

			// Height unit
			if (oldWObj.getHeight_unit() != wpojo.getHeight_unit()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_heightunit + " = " + (char) 39 + wpojo.getHeight_unit() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_heightunit, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getHeight_unit() + "",
						wpojo.getHeight_unit() + "", transId));
			}

			// Tears
			if (isEligibleEdit(oldWObj.getTears(), wpojo.getTears())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_TEARS + " = " + (char) 39 + wpojo.getTears() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_TEARS,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getTears(), wpojo.getTears(), transId));
			}

			// Episiotomy
			
			//if (oldWObj.getEpisiotomy() != wpojo.getEpisiotomy())
			{
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_EPISIOTOMY + " = " + (char) 39 + wpojo.getEpisiotomy() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_EPISIOTOMY, Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getEpisiotomy() + "",
						transId));
			}
			//}

			// Suture
			if (oldWObj.getSuturematerial() != wpojo.getSuturematerial()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_SUTUREMATERIAL + " = " + (char) 39 + wpojo.getSuturematerial() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_SUTUREMATERIAL, Partograph_CommonClass.getCurrentDateandTime(), "",
						wpojo.getSuturematerial() + "", transId));
			}

			// Del Type Reason
			if (isEligibleEdit(oldWObj.getDelTypeReason(), wpojo.getDelTypeReason())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_DELTYPEREASON + " = " + (char) 39 + wpojo.getDelTypeReason() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_DELTYPEREASON, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDelTypeReason(),
						wpojo.getDelTypeReason(), transId));
			}

			// Del type Other Reason
			if (isEligibleEdit(oldWObj.getDeltype_otherreasons(), wpojo.getDeltype_otherreasons())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_DELTYPEOTHERREASONS + " = " + (char) 39 + wpojo.getDeltype_otherreasons()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_DELTYPEOTHERREASONS, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getDeltype_otherreasons(), wpojo.getDeltype_otherreasons(), transId));
			}

			// Date last updated
			if (isEligibleEdit(oldWObj.getDatelastupdated(), wpojo.getDatelastupdated())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DATE_LASTUPDATED + " = " + (char) 39 + wpojo.getDatelastupdated() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						DATE_LASTUPDATED, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDatelastupdated(),
						wpojo.getDatelastupdated(), transId));
			}

			// Extra Comments - 13Oct2016 Arpitha
			if (isEligibleEdit(oldWObj.getExtracomments(), wpojo.getExtracomments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_EXTRACOMMENTS + " = " + (char) 39 + wpojo.getExtracomments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_EXTRACOMMENTS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getExtracomments(),
						wpojo.getExtracomments(), transId));
			}

			// // Reg type - 13Oct2016 Arpitha
			// if (oldWObj.getregtype() != wpojo.getregtype()) {
			// if (appendStr != "")
			// appendStr = appendStr + ", ";
			// appendStr = appendStr + C_REGTYPE + " = " + (char) 39 +
			// wpojo.getregtype() + (char) 39 + "";
			// auditArr.add(new AuditTrailPojo(wpojo.getUserId(),
			// wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_REGTYPE,
			// Partograph_CommonClass.getCurrentDateandTime(), "",
			// wpojo.getregtype() + "", transId));
			// }
			//
			// // date of adm - 10Nov2016 Arpitha
			// if (oldWObj.getDate_of_admission() !=
			// wpojo.getDate_of_admission()) {
			// if (appendStr != "")
			// appendStr = appendStr + ", ";
			// appendStr = appendStr + DATE_OF_ADM + " = " + (char) 39 +
			// wpojo.getDate_of_admission() + (char) 39 + "";
			// auditArr.add(new AuditTrailPojo(wpojo.getUserId(),
			// wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DATE_OF_ADM,
			// Partograph_CommonClass.getCurrentDateandTime(), "",
			// wpojo.getDate_of_admission() + "",
			// transId));
			// }
			//
			// // time of adm - 10Nov2016 Arpitha
			// if (oldWObj.getTime_of_admission() !=
			// wpojo.getTime_of_admission()) {
			// if (appendStr != "")
			// appendStr = appendStr + ", ";
			// appendStr = appendStr + TIME_OF_ADM + " = " + (char) 39 +
			// wpojo.getTime_of_admission() + (char) 39 + "";
			// auditArr.add(new AuditTrailPojo(wpojo.getUserId(),
			// wpojo.getWomenId(), TBL_REGISTEREDWOMEN, TIME_OF_ADM,
			// Partograph_CommonClass.getCurrentDateandTime(), "",
			// wpojo.getTime_of_admission() + "",
			// transId));
			// }

			// Normal type - 20Nov2016 Arpitha
			if (oldWObj.getNormaltype() != wpojo.getNormaltype()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_NORMALTYPE + " = " + (char) 39 + wpojo.getNormaltype() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_NORMALTYPE, Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getNormaltype() + "",
						transId));
			}

			String strUpdate = "";
			if (appendStr.length() > 0) {
				strUpdate = "Update " + TBL_REGISTEREDWOMEN + "  Set " + appendStr + " where  user_Id = '"
						+ wpojo.getUserId() + "' and women_id = '" + wpojo.getWomenId() + "' ";

				db.execSQL(strUpdate);

				iUpdateRecordTrans(wpojo.getUserId(), transId, TBL_REGISTEREDWOMEN, strUpdate, null, null);

				for (AuditTrailPojo ap : auditArr) {
					if (!insertTotblAuditTrail(ap))
						return null;
				}

				iNewRecordTrans(wpojo.getUserId(), transId, "tblAuditTrail");
			}

			return strUpdate;

		} catch (Exception e) {
			e.printStackTrace();
			return new String();
		}
	}

	/**
	 * Get the object id based on input string
	 * 
	 * @param obj_Type
	 * @return
	 * @throws Exception
	 */
	public int getObjectID(String obj_Type) throws Exception {
		db = this.getWritableDatabase();
		int obj_id = 0;
		try {
			String strSelect = "Select " + C_OBJECTID + " FROM " + TBL_OBJECTTYPEMASTER + " WHERE " + C_OBJECTTYPE
					+ " = '" + obj_Type + "'";
			Cursor cursor = db.rawQuery(strSelect, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				obj_id = cursor.getInt(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
		return obj_id;
	}

	/**
	 * add rows in tbl_propertyvalues
	 * 
	 * @param valpojo
	 * @param tblPropertyvalues
	 * @param strWomenid
	 * @param strwuserId
	 * @param transId
	 * @return
	 * @throws Exception
	 */
	public boolean insertpropertyvalues(Add_value_pojo valpojo, String tblPropertyvalues, String strWomenid,
			String strwuserId, int transId) throws Exception {
		db = this.getWritableDatabase();
		String strVal1 = "";
		boolean isAdded = false;
		String strInsert;

		try {

			String strSelect = "SELECT * FROM " + TBL_PROPERTYMASTER + " WHERE " + C_OBJECTID + " = '"
					+ valpojo.getObj_Id() + "'";
			Cursor cursor = db.rawQuery(strSelect, null);

			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					int danger = 0;// 12Jun2017 Arpitha

					// updated on 27feb16 by Arpitha
					if (cursor.getString(1).equalsIgnoreCase("1")) {
						strVal1 = "" + valpojo.getStrVal();
						danger = valpojo.getFhrDanger();// 12Jun2017 Arpitha
					}
					if (cursor.getString(1).equalsIgnoreCase("1.2"))
						strVal1 = valpojo.getFhrcomments();

					if (cursor.getString(1).equalsIgnoreCase("6.1"))
						strVal1 = valpojo.getDrug();
					if (cursor.getString(1).equalsIgnoreCase("6.2"))
						strVal1 = valpojo.getDrugcomments();

					if (cursor.getString(1).equalsIgnoreCase("8.1")) {
						strVal1 = valpojo.getStrVal();
						danger = valpojo.getTempDanger();// 12Jun2017 Arpitha
					}
					if (cursor.getString(1).equalsIgnoreCase("8.2"))
						strVal1 = valpojo.getTempcomments();

					ContentValues values = new ContentValues();
					values.put(C_TRANSID, transId);
					values.put(USER_ID, strwuserId);
					values.put(WOMEN_ID, strWomenid);
					values.put(C_OBJECTID, valpojo.getObj_Id());
					values.put(C_PROP_ID, cursor.getString(1));
					values.put(C_PROP_NAME, cursor.getString(2));
					values.put(C_PROP_VALUE, strVal1);
					values.put(DATE_OF_ENTRY, valpojo.getStrdate());
					values.put(TIME_OF_ENTRY, valpojo.getStrTime());
					values.put(C_ISDANGER, danger);

					long id = db.insert(TBL_PROPERTYVALUES, null, values);
					if (id > 0)
						isAdded = true;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return isAdded;
	}

	/**
	 * Retrieve data from tbl_propertyvalues to display
	 * 
	 * @param strWomenid
	 * @param strwuserId
	 * @param obj_Id
	 * @return
	 * @throws Exception
	 */
	public Cursor getDataToDisplay(String strWomenid, String strwuserId, int obj_Id) throws Exception {
		Cursor cursor = null;
		db = this.getWritableDatabase();
		try {
			String strSelect = "SELECT " + C_PROP_VALUE + "," + DATE_OF_ENTRY + " , " + TIME_OF_ENTRY + " FROM "
					+ TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = " + obj_Id + " and " + WOMEN_ID + " = '"
					+ strWomenid + "'" + "and " + USER_ID + "= '" + strwuserId + "' order by " + C_TRANSID + " desc";
			cursor = db.rawQuery(strSelect, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cursor;
	}

	/**
	 * Get Data from tbl_propertyvalues for input objectid,women id and user id
	 * 
	 * @param obj_Id
	 * @param strWomenid
	 * @param strwuserId
	 * @return
	 */
	public ArrayList<Add_value_pojo> getAFMData(int obj_Id, String strWomenid, String strwuserId) {

		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
					+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
					+ strwuserId + "' order by " + C_TRANSID + " desc";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {
					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							// updated by Arpitha
							if (c2.getString(0).equalsIgnoreCase("2.1")) {
								avp.setAmnioticfluid(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("2.2")) {
								avp.setMoulding(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("2.3")) {
								avp.setAfmcomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}
						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// update on 28feb16 by Arpitha

	public ArrayList<Add_value_pojo> getTempdata(int obj_Id, String strWomenid, String strwuserId) {

		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
					+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
					+ strwuserId + "' order by " + C_TRANSID + " desc";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {

					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							if (c2.getString(0).equalsIgnoreCase("8.1")) {
								avp.setStrVal(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("8.2")) {
								avp.setTempcomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}

						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// update on 28feb16 by Arpitha
	public ArrayList<Add_value_pojo> getDrugdata(int obj_Id, String strWomenid, String strwuserId) {

		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
					+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
					+ strwuserId + "' order by " + C_TRANSID + " desc";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {

					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							if (c2.getString(0).equalsIgnoreCase("6.1")) {
								avp.setDrug(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("6.2")) {
								avp.setDrugcomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}

						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// update on 28feb16 by Arpitha
	public ArrayList<Add_value_pojo> getFHRData1(int obj_Id, String strWomenid, String strwuserId) {

		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
					+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
					+ strwuserId + "' order by " + C_TRANSID + " desc";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {

					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							if (c2.getString(0).equalsIgnoreCase("1")) {
								avp.setStrVal(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("1.2")) {
								avp.setFhrcomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}

						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	/**
	 * Get Data from tblpropertyvalues for objectid, womenid, userid and boolean
	 * graph clicked
	 * 
	 * @param obj_Id
	 * @param strWomenid
	 * @param strwuserId
	 * @param isGraphClicked
	 * @return
	 */
	public ArrayList<Add_value_pojo> getDilatationData(int obj_Id, String strWomenid, String strwuserId,
			boolean isGraphClicked) {
		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();
			String mainSql;

			if (!isGraphClicked)
				mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
						+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
						+ strwuserId + "' order by " + C_TRANSID + " desc";

			else
				mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
						+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
						+ strwuserId + "'";

			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {
					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							// updated By Arpitha
							if (c2.getString(0).equalsIgnoreCase("3.1")) {
								avp.setCervix(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("3.2")) {
								avp.setDesc_of_head(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("3.3")) {
								avp.setHours(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("3.4")) {
								avp.setDilatationcomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}
						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// Insert contraction data

	/**
	 * insert contraction data in tblpropertyvalues
	 * 
	 * @param valpojo
	 * @param strWomenid
	 * @param strwuserId
	 * @param transId
	 * @return
	 */
	public boolean insertContractiondata(Add_value_pojo valpojo, String strWomenid, String strwuserId, int transId) {
		boolean isAdded = false;
		try {
			db = this.getWritableDatabase();
			String strVal = null;
			String strSelect = "SELECT " + C_PROP_ID + "," + C_PROP_NAME + " FROM " + TBL_PROPERTYMASTER + " WHERE "
					+ C_OBJECTID + " = " + valpojo.getObj_Id();
			Cursor cursor = db.rawQuery(strSelect, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					int danger = 0;// 12Jun2017 Arpitha
					// updated by Arpitha
					if (cursor.getString(0).equalsIgnoreCase("4.1")) {
						strVal = valpojo.getContractions();
						danger = valpojo.getContractionDanger();// 12Jun2017
																// Arpitha
					}
					if (cursor.getString(0).equalsIgnoreCase("4.2"))
						strVal = "" + valpojo.getDuration();
					if (cursor.getString(0).equalsIgnoreCase("4.3"))
						strVal = "" + valpojo.getContactioncomments();

					ContentValues values = new ContentValues();
					values.put(C_TRANSID, transId);
					values.put(USER_ID, strwuserId);
					values.put(WOMEN_ID, strWomenid);
					values.put(C_OBJECTID, valpojo.getObj_Id());
					values.put(C_PROP_ID, cursor.getString(0));
					values.put(C_PROP_NAME, cursor.getString(1));
					values.put(C_PROP_VALUE, strVal);
					values.put(DATE_OF_ENTRY, valpojo.getStrdate());
					values.put(TIME_OF_ENTRY, valpojo.getStrTime());
					values.put(C_ISDANGER, danger);// 12Jun2017 Arpitha

					long id = db.insert(TBL_PROPERTYVALUES, null, values);
					if (id > 0)
						isAdded = true;

				} while (cursor.moveToNext());
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return isAdded;
	}

	// Get contraction data
	public ArrayList<Add_value_pojo> getContractionData(int obj_Id, String strWomenid, String strwuserId,
			boolean isGraphClicked) {
		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();
			String mainSql;

			if (!isGraphClicked)
				mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
						+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
						+ strwuserId + "' order by " + C_TRANSID + " desc";
			else
				mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
						+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
						+ strwuserId + "'";

			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {
				// arrAvp = new ArrayList<Add_value_pojo>();
				do {
					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							// updated By Arpitha
							if (c2.getString(0).equalsIgnoreCase("4.1")) {
								avp.setContractions(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("4.2")) {
								avp.setDuration(Integer.parseInt(c2.getString(1)));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("4.3")) {
								avp.setContactioncomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}
						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// Insert pulse and Bp values
	public boolean insertPulseBPVal(Add_value_pojo valpojo, String strWomenid, String strwuserId, int transId) {
		boolean isAdded = false;
		try {
			db = this.getWritableDatabase();
			String strVal = null;
			int strdanger = 0;

			String strSelect = "SELECT " + C_PROP_ID + "," + C_PROP_NAME + " FROM " + TBL_PROPERTYMASTER + " WHERE "
					+ C_OBJECTID + " = " + valpojo.getObj_Id();
			Cursor cursor = db.rawQuery(strSelect, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					// updated by Arpitha
					if (cursor.getString(0).equalsIgnoreCase("7.1")) {
						strVal = valpojo.getStrPulseval();
						strdanger = valpojo.getIsDanger();
					}
					if (cursor.getString(0).equalsIgnoreCase("7.2")) {
						strVal = valpojo.getStrBPsystolicval();
						strdanger = valpojo.getIsDangerval2();
					}
					if (cursor.getString(0).equalsIgnoreCase("7.3")) {
						strVal = valpojo.getStrBPdiastolicval();
						strdanger = valpojo.getIsDangerval2();
					}
					if (cursor.getString(0).equalsIgnoreCase("7.4")) {
						strVal = valpojo.getPulsecomments();
						strdanger = valpojo.getIsDangerval2();
					}

					ContentValues values = new ContentValues();
					values.put(C_TRANSID, transId);
					values.put(USER_ID, strwuserId);
					values.put(WOMEN_ID, strWomenid);
					values.put(C_OBJECTID, valpojo.getObj_Id());
					values.put(C_PROP_ID, cursor.getString(0));
					values.put(C_PROP_NAME, cursor.getString(1));
					values.put(C_PROP_VALUE, strVal);
					values.put(DATE_OF_ENTRY, valpojo.getStrdate());
					values.put(TIME_OF_ENTRY, valpojo.getStrTime());
					values.put(C_ISDANGER, strdanger);

					long id = db.insert(TBL_PROPERTYVALUES, null, values);
					if (id > 0)
						isAdded = true;

				} while (cursor.moveToNext());
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return isAdded;
	}

	// Get the pulse/BP Data
	public ArrayList<Add_value_pojo> getPulseBPData(int obj_Id, String strWomenid, String strwuserId,
			boolean isGraphCliked) {
		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();
			String mainSql;

			if (!isGraphCliked)
				mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
						+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
						+ strwuserId + "' order by " + C_TRANSID + " desc";

			else
				mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
						+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
						+ strwuserId + "'";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {
				// arrAvp = new ArrayList<Add_value_pojo>();
				do {
					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							// updated By Arpitha
							if (c2.getString(0).equalsIgnoreCase("7.1")) {
								avp.setStrPulseval(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("7.2")) {
								avp.setStrBPsystolicval(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("7.3")) {
								avp.setStrBPdiastolicval(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("7.4")) {
								avp.setPulsecomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}
						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// Insert urine test data
	public boolean insertUrinetestData(Add_value_pojo valpojo, String strWomenid, String strwuserId, int transId) {
		boolean isAdded = false;
		try {
			db = this.getWritableDatabase();
			String strVal = null;
			String strSelect = "SELECT " + C_PROP_ID + "," + C_PROP_NAME + " FROM " + TBL_PROPERTYMASTER + " WHERE "
					+ C_OBJECTID + " = " + valpojo.getObj_Id();
			Cursor cursor = db.rawQuery(strSelect, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					// updated by Arpitha
					if (cursor.getString(0).equalsIgnoreCase("9.1"))
						strVal = valpojo.getStrProteinval();
					if (cursor.getString(0).equalsIgnoreCase("9.2"))
						strVal = valpojo.getStrAcetoneval();
					if (cursor.getString(0).equalsIgnoreCase("9.3"))
						strVal = valpojo.getStrVolumeval();
					if (cursor.getString(0).equalsIgnoreCase("9.4"))
						strVal = valpojo.getUrinecomments();

					ContentValues values = new ContentValues();
					values.put(C_TRANSID, transId);
					values.put(USER_ID, strwuserId);
					values.put(WOMEN_ID, strWomenid);
					values.put(C_OBJECTID, valpojo.getObj_Id());
					values.put(C_PROP_ID, cursor.getString(0));
					values.put(C_PROP_NAME, cursor.getString(1));
					values.put(C_PROP_VALUE, strVal);
					values.put(DATE_OF_ENTRY, valpojo.getStrdate());
					values.put(TIME_OF_ENTRY, valpojo.getStrTime());

					long id = db.insert(TBL_PROPERTYVALUES, null, values);
					if (id > 0)
						isAdded = true;

				} while (cursor.moveToNext());
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return isAdded;
	}

	// Get the urine test data
	public ArrayList<Add_value_pojo> getUrineTestData(int obj_Id, String strWomenid, String strwuserId) {
		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
					+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
					+ strwuserId + "' order by " + C_TRANSID + " desc";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {
					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							// updated by Arpitha
							if (c2.getString(0).equalsIgnoreCase("9.1")) {
								avp.setStrProteinval(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("9.2")) {
								avp.setStrAcetoneval(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("9.3")) {
								avp.setStrVolumeval(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("9.4")) {
								avp.setUrinecomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}
						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// Get the property ids from tbl property master
	public Cursor getPropertyIds(int obj_Id) {

		Cursor cursor = null;
		try {
			db = this.getWritableDatabase();
			String strSelect = " SELECT " + C_PROP_ID + "," + C_PROP_NAME + " FROM " + TBL_PROPERTYMASTER + " WHERE "
					+ C_OBJECTID + " = " + obj_Id;
			cursor = db.rawQuery(strSelect, null);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return cursor;
	}

	// Insert data to tbl_propertyvalues
	public boolean insertData(Property_pojo pdata, String strWomenid, String strwuserId, int transId) {

		boolean isInserted = false;
		try {
			db = this.getWritableDatabase();

			ArrayList<String> adata = pdata.getProp_value();

			for (int i = 0; i < adata.size(); i++) {
				ContentValues values = new ContentValues();
				// 12Jun2017 Arpitha
				int danger = 0;

				if (pdata.getProp_id().get(i).equals("2.1"))
					danger = pdata.getAmioticfluidDanger();
				if (pdata.getProp_id().get(i).equals("3.1"))
					danger = pdata.getCervixDanger();
				if (pdata.getProp_id().get(i).equals("3.2"))
					danger = pdata.getDescentofHeadDanger();// 12Jun2017 Arpitha

				values.put(C_TRANSID, transId);
				values.put(USER_ID, strwuserId);
				values.put(WOMEN_ID, strWomenid);
				values.put(C_OBJECTID, pdata.getObj_Id());
				values.put(C_PROP_ID, pdata.getProp_id().get(i));
				values.put(C_PROP_NAME, pdata.getProp_name().get(i));
				values.put(C_PROP_VALUE, adata.get(i));
				values.put(DATE_OF_ENTRY, pdata.getStrdate());
				values.put(TIME_OF_ENTRY, pdata.getStrTime());
				values.put(C_ISDANGER, danger);// 12Jun2017 Arpitha

				long id = db.insert(TBL_PROPERTYVALUES, null, values);
				if (id > 0)
					isInserted = true;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return isInserted;
	}

	// Sync mthd
	public String callResponseXML() throws Exception {
		String returnValue = null;

		try {
			db = this.getReadableDatabase();
			PrepareSyncXML xExportXML;
			xExportXML = new PrepareSyncXML(db, context);
			returnValue = xExportXML.WriteResponseXML();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			throw e;
			// e.printStackTrace();
		}
		return returnValue;
	}

	public String callXMLExport(String macAddress, String UserId) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String returnValue = null;
		try {
			db = this.getWritableDatabase();

			PrepareSyncXML xExportXML;
			xExportXML = new PrepareSyncXML(db, context);
			returnValue = xExportXML.WriteSyncXML(macAddress, UserId);
			xExportXML.writeTxtFile(returnValue);
		} catch (Exception e) {
			throw e;
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return returnValue;
	}

	// This method is related to syncronisation
	public void UpdateTransStatus(String RequestId, String Status) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		db = this.getWritableDatabase();

		String strUpdate = " UPDATE " + TBL_SYNCMASTER + " SET " + C_REQUESTSTATUS + " = '" + Status + "' WHERE "
				+ C_REQUESTID + " = '" + RequestId + "'";
		db.execSQL(strUpdate);

		// If Request is rolled back in Server, change the status to 'N' in
		// tblTransHeader
		if (Status.equals("R"))
			Status = "N";

		strUpdate = "UPDATE " + TBL_TRANSHEADER + " SET " + C_TRANSSTATUS + " = '" + Status + "' WHERE " + C_REQUESTID
				+ " = '" + RequestId + "'";
		db.execSQL(strUpdate);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
	}

	// Insert record into tbluser
	public boolean insertUser(String user_id, String pwd) throws Exception {
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(USER_ID, "");
		values.put(PASSWORD, pwd);
		values.put(LAST_WOMENNUM, 0);
		values.put(LAST_TRANSNUM, 0);
		values.put(LAST_REQUESTNUM, 0);
		values.put(STATE, "");
		values.put(DISTRICT, "");
		values.put(TALUK, "");
		values.put(FACILITY, "");
		values.put(FACILITY_NAME, "");
		SimpleDateFormat sdf = new SimpleDateFormat("");
		String currentDateandTime = sdf.format(new Date());
		values.put(DATEINSERTED, currentDateandTime);

		values.put(C_EMAILID, user_id);// 06April2017 Arpitha

		long result = db.insertOrThrow(TBL_USER, null, values);
		if (result > 0) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			return true;
		}
		return false;
	}

	// Insert Apgar Score values to tbl_propertyvalues
	public boolean insertApgarDetails(String strWomenid, String strwuserId, Map<String, String> insertApgar,
			String strTime, String strDate, int transId, int object_ID, int child_Id) {

		boolean isAdded = false;

		try {
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();

			for (Map.Entry<String, String> val : insertApgar.entrySet()) {
				values.put(TRANS_ID, transId);
				values.put(USER_ID, strwuserId);
				values.put(WOMEN_ID, strWomenid);
				values.put(C_CHILDID, child_Id);
				values.put(C_OBJECTID, object_ID);
				values.put(C_PROP_ID, val.getKey());
				values.put(C_PROP_NAME, Activity_Apgar.propertynamesMap.get(val.getKey()));
				values.put(C_PROP_VALUE, val.getValue());
				values.put(DATE_OF_ENTRY, strDate);
				values.put(TIME_OF_ENTRY, strTime);
				// .put(, value);

				long id = db.insert(TBL_PROPERTYVALUES, null, values);
				if (id > 0)
					isAdded = true;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return isAdded;
	}

	public Cursor getPropertyMap(int object_ID) {
		Cursor cursor = null;
		try {
			db = this.getWritableDatabase();

			String strSql = "Select " + Partograph_DB.C_PROP_ID + "," + Partograph_DB.C_PROP_NAME + " from "
					+ Partograph_DB.TBL_PROPERTYMASTER + " where " + Partograph_DB.C_OBJECTID + " = " + object_ID;
			cursor = db.rawQuery(strSql, null);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return cursor;
	}

	// Get Apgar Score Details
	public Cursor GetApgarScoreDetails(String strWomenid, String strwuserId, int object_Id, int child_Id) {
		Cursor cursor = null;
		Cursor cursor2 = null;
		try {
			db = this.getWritableDatabase();

			String strSql = "Select date_of_entry, time_of_entry, Max(case when prop_Id =  '10.1' Then prop_value END) Appearance, Max(case when prop_Id =  '10.2' Then prop_value END) Pulse, Max(case when prop_Id =  '10.3' Then prop_value END) Grimace,Max(case when prop_Id =  '10.4' Then prop_value END) Activity,Max(case when prop_Id =  '10.5' Then prop_value END) Respiration, Max(case when prop_Id =  '10.6' Then prop_value END) Comments from tbl_propertyvalues where women_Id = '"
					+ strWomenid + "' and user_Id = '" + strwuserId + "' and object_Id = '10' and child_Id = '"
					+ child_Id + "' group by trans_Id ";

			cursor = db.rawQuery(strSql, null);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return cursor;
	}

	// Get max(date_inserted) from tbl_comment
	public String getCommentMaxdate(String wUserId) {
		String maxDate = " ";
		db = this.getWritableDatabase();
		try {
			String strSql = "Select max(dateinserted) from tbl_comment where user_Id= '" + wUserId + "'";
			Cursor cursor = db.rawQuery(strSql, null);

			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				maxDate = cursor.getString(0);
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return maxDate;
	}

	public void insertComments(ContentValues values) {
		db = this.getWritableDatabase();
		try {

			db.insert("tbl_comment", null, values);
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Get Comments from tbl_comment
	public Cursor getComments(String strWomenid, String strwuserId, int obj_Id) throws Exception {
		db = this.getWritableDatabase();

		String strSql = "SELECT " + C_COMMENT + "," + DATE_OF_ENTRY + "," + TIME_OF_ENTRY + " FROM " + TBL_COMMENT
				+ " WHERE " + WOMEN_ID + " = '" + strWomenid + "' and " + USER_ID + " = '" + strwuserId + "' and "
				+ C_OBJECTID + " = '" + obj_Id + "' order by " + DATEINSERTED + " desc";
		Cursor cursor = db.rawQuery(strSql, null);

		return cursor;
	}

	// Get Comments unread from tbl_comments
	public Cursor getCommentsUnread(String womenId, String userId) throws Exception {
		db = this.getWritableDatabase();

		Cursor cursor = null;

		String strSelect = "SELECT count(*) , " + C_OBJECTID + " FROM " + TBL_COMMENT + " where " + WOMEN_ID + " = '"
				+ womenId + "' and " + USER_ID + " = '" + userId + "' and " + C_ISVIEWED + " = '0' group by "
				+ C_OBJECTID;
		cursor = db.rawQuery(strSelect, null);

		return cursor;
	}

	// Update tbl comments set isviewd to true
	public void updatetblComments(String userid, String womenid, int objid) throws Exception {

		db = this.getWritableDatabase();

		String strUpdate = "Update " + TBL_COMMENT + " set " + C_ISVIEWED + " = 1" + " where " + WOMEN_ID + " = '"
				+ womenid + "' and " + USER_ID + " = '" + userid + "' and " + C_OBJECTID + " = '" + objid + "'";
		db.execSQL(strUpdate);

	}

	// Check whether the women is in danger or not from tblpropertyvalues
	public boolean chkIsDanger(String womenid, String userId) throws Exception {
		db = this.getWritableDatabase();
		boolean isDanger = false;

		String strSelect = "Select distinct " + WOMEN_ID + " from " + TBL_PROPERTYVALUES + " where " + WOMEN_ID + " = '"
				+ womenid + "' and " + USER_ID + " = '" + userId + "' and " + C_ISDANGER + " = 1";
		Cursor cursor = db.rawQuery(strSelect, null);
		if (cursor.getCount() > 0) {
			isDanger = true;
		}

		return isDanger;
	}

	// Get overall comments count for a particular woman
	public int getComentsCount(String womenId, String userId) throws Exception {
		db = this.getWritableDatabase();

		int count = 0;

		String strSelect = "SELECT count(*) FROM " + TBL_COMMENT + " WHERE " + WOMEN_ID + " = '" + womenId + "' and "
				+ USER_ID + " = '" + userId + "' and " + C_ISVIEWED + " = '0'";
		Cursor cursor = db.rawQuery(strSelect, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			count = cursor.getInt(0);
		}
		return count;
	}

	// Update Women Details
	public String updateProfileData(Women_Profile_Pojo wpojo, int transId, boolean isupdatedel,
			Women_Profile_Pojo oldWObj) throws Exception {
		db = this.getWritableDatabase();
		try {

			// 08aug2016 - Bindu - audittrail
			ArrayList<AuditTrailPojo> auditArr = new ArrayList<AuditTrailPojo>();
			String appendStr = "";

			byte[] oldB = oldWObj.getWomen_Image();
			String oldEncodedImage = (oldB != null) ? Base64.encodeToString(oldB, Base64.DEFAULT) : "";

			byte[] b1 = wpojo.getWomen_Image();
			String encodedImage1 = (b1 != null) ? Base64.encodeToString(b1, Base64.DEFAULT) : "";

			// image - 18Oct2016 Bindu - oldwomanimage & womanimageupdated in
			// audit trail for woman photo updation
			// image- 21oct2016 changed woman image to string
			if (!oldEncodedImage.equals(encodedImage1)) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + WOMEN_IMAGE + " = " + (char) 39 + encodedImage1 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, WOMEN_IMAGE,
						Partograph_CommonClass.getCurrentDateandTime(), "oldwomanimage", "womanimageupdated", transId));
			}

			// woman name
			if (isEligibleEdit(oldWObj.getWomen_name(), wpojo.getWomen_name())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + WOMEN_NAME + " = " + (char) 39 + wpojo.getWomen_name() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, WOMEN_NAME,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getWomen_name(), wpojo.getWomen_name(),
						transId));
			}

			// Age
			if (oldWObj.getAge() != wpojo.getAge()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + W_AGE + " = " + (char) 39 + wpojo.getAge() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, W_AGE,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getAge() + "", wpojo.getAge() + "",
						transId));
			}

			// 19Aug2016 - risk category, riskoptions, other risk observed
			// Risk category
			if (oldWObj.getRisk_category() != wpojo.getRisk_category()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + COND_WHILE_ADMN + " = " + (char) 39 + wpojo.getRisk_category() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						COND_WHILE_ADMN, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getRisk_category() + "", wpojo.getRisk_category() + "", transId));
			}

			// Risk Options
			if (isEligibleEdit(oldWObj.getRiskoptions(), wpojo.getRiskoptions())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_RISKOPTIONS + " = " + (char) 39 + wpojo.getRiskoptions() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_RISKOPTIONS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getRiskoptions(),
						wpojo.getRiskoptions(), transId));
			}

			// Other Risk observed
			if (isEligibleEdit(oldWObj.getComments(), wpojo.getComments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + COMMENTS + " = " + (char) 39 + wpojo.getComments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, COMMENTS,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getComments(), wpojo.getComments(),
						transId));
			}

			// Address
			if (isEligibleEdit(oldWObj.getAddress(), wpojo.getAddress())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + ADDRESS + " = " + (char) 39 + wpojo.getAddress() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, ADDRESS,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getAddress(), wpojo.getAddress(),
						transId));
			}

			// Phone num
			if (isEligibleEdit(oldWObj.getPhone_No(), wpojo.getPhone_No())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + PHONE_NUM + " = " + (char) 39 + wpojo.getPhone_No() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, PHONE_NUM,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getPhone_No(), wpojo.getPhone_No(),
						transId));
			}

			// Doc Name
			if (isEligibleEdit(oldWObj.getDoc_name(), wpojo.getDoc_name())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DOC_NAME + " = " + (char) 39 + wpojo.getDoc_name() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DOC_NAME,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDoc_name(), wpojo.getDoc_name(),
						transId));
			}

			// Nurse name
			if (isEligibleEdit(oldWObj.getNurse_name(), wpojo.getNurse_name())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + NURSE_NAME + " = " + (char) 39 + wpojo.getNurse_name() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, NURSE_NAME,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getNurse_name(), wpojo.getNurse_name(),
						transId));
			}

			// Attendant name
			if (isEligibleEdit(oldWObj.getW_attendant(), wpojo.getW_attendant())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + ATTENDANT_NAME + " = " + (char) 39 + wpojo.getW_attendant() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						ATTENDANT_NAME, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getW_attendant(),
						wpojo.getW_attendant(), transId));
			}

			// gravida
			if (oldWObj.getGravida() != wpojo.getGravida()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + GRAVIDA + " = " + (char) 39 + wpojo.getGravida() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, GRAVIDA,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getGravida() + "",
						wpojo.getGravida() + "", transId));
			}

			// para
			if (oldWObj.getPara() != wpojo.getPara()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + PARA + " = " + (char) 39 + wpojo.getPara() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, PARA,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getPara() + "", wpojo.getPara() + "",
						transId));
			}

			// Hosp
			if (isEligibleEdit(oldWObj.getHosp_no(), wpojo.getHosp_no())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + HOSP_NO + " = " + (char) 39 + wpojo.getHosp_no() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, HOSP_NO,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getHosp_no(), wpojo.getHosp_no(),
						transId));
			}

			// Special Instructions
			if (isEligibleEdit(oldWObj.getSpecial_inst(), wpojo.getSpecial_inst())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + SPECIAL_INSTRUCTIONS + " = " + (char) 39 + wpojo.getSpecial_inst() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						SPECIAL_INSTRUCTIONS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getSpecial_inst(),
						wpojo.getSpecial_inst(), transId));
			}

			// Gestation age
			if (oldWObj.getGestationage() != wpojo.getGestationage()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + GESTATION_AGE + " = " + (char) 39 + wpojo.getGestationage() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						GESTATION_AGE, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getGestationage() + "",
						wpojo.getGestationage() + "", transId));
			}

			// Gestation age days
			if (oldWObj.getGest_age_days() != wpojo.getGest_age_days()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + GESTATION_AGE_DAYS + " = " + (char) 39 + wpojo.getGest_age_days() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						GESTATION_AGE_DAYS, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getGest_age_days() + "", wpojo.getGest_age_days() + "", transId));
			}

			// thai card no
			if (isEligibleEdit(oldWObj.getThayicardnumber(), wpojo.getThayicardnumber())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_THAYICARDNUM + " = " + (char) 39 + wpojo.getThayicardnumber() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_THAYICARDNUM, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getThayicardnumber(),
						wpojo.getThayicardnumber(), transId));
			}

			// LMP
			if (isEligibleEdit(oldWObj.getLmp(), wpojo.getLmp())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_LMP + " = " + (char) 39 + wpojo.getLmp() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_LMP,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getLmp(), wpojo.getLmp(), transId));
			}

			// Blood group
			if (oldWObj.getBloodgroup() != wpojo.getBloodgroup()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_BLOODGROUP + " = " + (char) 39 + wpojo.getBloodgroup() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_BLOODGROUP, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBloodgroup() + "",
						wpojo.getBloodgroup() + "", transId));
			}

			// EDD
			if (isEligibleEdit(oldWObj.getEdd(), wpojo.getEdd())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_EDD + " = " + (char) 39 + wpojo.getEdd() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_EDD,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getEdd(), wpojo.getEdd(), transId));
			}

			// Height
			if (isEligibleEdit(oldWObj.getHeight(), wpojo.getHeight())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_Height + " = " + (char) 39 + wpojo.getHeight() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_Height,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getHeight(), wpojo.getHeight(),
						transId));
			}

			// Weight
			if (isEligibleEdit(oldWObj.getW_weight(), wpojo.getW_weight())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_Weight + " = " + (char) 39 + wpojo.getW_weight() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_Weight,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getW_weight(), wpojo.getW_weight(),
						transId));
			}

			// Height unit
			if (oldWObj.getHeight_unit() != wpojo.getHeight_unit()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_heightunit + " = " + (char) 39 + wpojo.getHeight_unit() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_heightunit, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getHeight_unit() + "",
						wpojo.getHeight_unit() + "", transId));
			}

			if (isupdatedel) {
				int noofchild = oldWObj.getNo_of_child();
				int babysex2 = 99;
				// Delivery Comments
				if (isEligibleEdit(oldWObj.getDel_Comments(), wpojo.getDel_Comments())) {
					if (appendStr != "")
						appendStr = appendStr + ", ";
					appendStr = appendStr + DEL_COMMENTS + " = " + (char) 39 + wpojo.getDel_Comments() + (char) 39 + "";
					auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
							DEL_COMMENTS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDel_Comments(),
							wpojo.getDel_Comments(), transId));
				}

				// Baby wt 1
				if (oldWObj.getBabywt1() != wpojo.getBabywt1()) {
					if (appendStr != "")
						appendStr = appendStr + ", ";
					appendStr = appendStr + BABYWT1 + " = " + (char) 39 + wpojo.getBabywt1() + (char) 39 + "";
					auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT1,
							Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabywt1() + "",
							wpojo.getBabywt1() + "", transId));
				}
				// Baby Sex1
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYSEX1 + " = " + (char) 39 + wpojo.getBabysex1() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYSEX1,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabysex1() + "",
						wpojo.getBabysex1() + "", transId));

				if (noofchild == 2) {
					babysex2 = wpojo.getBabysex2();

					// Baby Sex2
					if (appendStr != "")
						appendStr = appendStr + ", ";
					appendStr = appendStr + BABYSEX2 + " = " + (char) 39 + babysex2 + (char) 39 + "";
					auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
							BABYSEX2, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabysex2() + "",
							babysex2 + "", transId));

					// Baby wt 2
					if (appendStr != "")
						appendStr = appendStr + ", ";
					appendStr = appendStr + BABYWT2 + " = " + (char) 39 + wpojo.getBabywt2() + (char) 39 + "";
					auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT2,
							Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabywt2() + "",
							wpojo.getBabywt2() + "", transId));

				}

				// Mothers death
				if (oldWObj.getMothersdeath() != wpojo.getMothersdeath()) {
					if (appendStr != "")
						appendStr = appendStr + ", ";
					appendStr = appendStr + MOTHERSDEATH + " = " + (char) 39 + wpojo.getMothersdeath() + (char) 39 + "";
					auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
							MOTHERSDEATH, Partograph_CommonClass.getCurrentDateandTime(),
							oldWObj.getMothersdeath() + "", wpojo.getMothersdeath() + "", transId));
				}

				// Mothers death reason
				if (isEligibleEdit(oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason())) {
					if (appendStr != "")
						appendStr = appendStr + ", ";
					appendStr = appendStr + MOTHERS_DEATH_REASON + " = " + (char) 39 + wpojo.getMothers_death_reason()
							+ (char) 39 + "";
					auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
							MOTHERS_DEATH_REASON, Partograph_CommonClass.getCurrentDateandTime(),
							oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason(), transId));
				}

				/*
				 * // Normal type - 20Nov2016 Arpitha if
				 * (oldWObj.getNormaltype() != wpojo.getNormaltype()) { if
				 * (appendStr != "") appendStr = appendStr + ", "; appendStr =
				 * appendStr + C_NORMALTYPE + " = " + (char) 39 +
				 * wpojo.getNormaltype() + (char) 39 + ""; auditArr.add(new
				 * AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(),
				 * TBL_REGISTEREDWOMEN, C_NORMALTYPE,
				 * Partograph_CommonClass.getCurrentDateandTime(), "",
				 * wpojo.getNormaltype() + "", transId)); }
				 */// commented on 25April2017 Arpitha - cannot change the type
					// once it is saved
			}

			// //05May2017 Arpitha - v2.6 mantis id-0000231
			if (oldWObj.getNormaltype() != wpojo.getNormaltype()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_NORMALTYPE + " = " + (char) 39 + wpojo.getNormaltype() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_NORMALTYPE, Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getNormaltype() + "",
						transId));
			}

			// Date last updated
			if (isEligibleEdit(oldWObj.getDatelastupdated(), wpojo.getDatelastupdated())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DATE_LASTUPDATED + " = " + (char) 39 + wpojo.getDatelastupdated() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						DATE_LASTUPDATED, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDatelastupdated(),
						wpojo.getDatelastupdated(), transId));
			}

			// Extra Comments - 13Oct2016 Arpitha
			if (isEligibleEdit(oldWObj.getExtracomments(), wpojo.getExtracomments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_EXTRACOMMENTS + " = " + (char) 39 + wpojo.getExtracomments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_EXTRACOMMENTS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getExtracomments(),
						wpojo.getExtracomments(), transId));
			}

			// // Reg type - 13Oct2016 Arpitha
			// if (oldWObj.getregtype() != wpojo.getregtype()) {
			// if (appendStr != "")
			// appendStr = appendStr + ", ";
			// appendStr = appendStr + C_REGTYPE + " = " + (char) 39 +
			// wpojo.getregtype() + (char) 39 + "";
			// auditArr.add(new AuditTrailPojo(wpojo.getUserId(),
			// wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_REGTYPE,
			// Partograph_CommonClass.getCurrentDateandTime(), "",
			// wpojo.getregtype() + "", transId));
			// }

			/*
			 * // date of adm - 10Nov2016 Arpitha if
			 * (oldWObj.getDate_of_admission() != wpojo.getDate_of_admission())
			 * { if (appendStr != "") appendStr = appendStr + ", "; appendStr =
			 * appendStr + DATE_OF_ADM + " = " + (char) 39 +
			 * wpojo.getDate_of_admission() + (char) 39 + ""; auditArr.add(new
			 * AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(),
			 * TBL_REGISTEREDWOMEN, DATE_OF_ADM,
			 * Partograph_CommonClass.getCurrentDateandTime(), "",
			 * wpojo.getDate_of_admission() + "", transId)); }
			 * 
			 * // time of adm - 10Nov2016 Arpitha if
			 * (oldWObj.getTime_of_admission() != wpojo.getTime_of_admission())
			 * { if (appendStr != "") appendStr = appendStr + ", "; appendStr =
			 * appendStr + TIME_OF_ADM + " = " + (char) 39 +
			 * wpojo.getTime_of_admission() + (char) 39 + ""; auditArr.add(new
			 * AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(),
			 * TBL_REGISTEREDWOMEN, TIME_OF_ADM,
			 * Partograph_CommonClass.getCurrentDateandTime(), "",
			 * wpojo.getTime_of_admission() + "", transId)); }
			 */
			String strUpdate = "";
			if (appendStr.length() > 0) {
				strUpdate = "Update " + TBL_REGISTEREDWOMEN + "  Set " + appendStr + " where  user_Id = '"
						+ wpojo.getUserId() + "' and women_id = '" + wpojo.getWomenId() + "' ";

				db.execSQL(strUpdate);

				iUpdateRecordTrans(wpojo.getUserId(), transId, TBL_REGISTEREDWOMEN, strUpdate, null, null);

				for (AuditTrailPojo ap : auditArr) {
					if (!insertTotblAuditTrail(ap))
						return null;
				}

				iNewRecordTrans(wpojo.getUserId(), transId, "tblAuditTrail");
			}

			return strUpdate;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return new String();
		}
	}

	// Update Delivery Info
	// added 2 fields - deltypereason and deltypereasonothers
	public String updateDelData(Women_Profile_Pojo wpojo, int transId, Women_Profile_Pojo oldWObj) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		try {

			// Bindu - Aug082016 - audit trail
			ArrayList<AuditTrailPojo> auditArr = new ArrayList<AuditTrailPojo>();
			String appendStr = "";

			// Delstatus
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_STATUS + " = " + (char) 39 + wpojo.getDel_type() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_STATUS,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_type() + "", transId));

			// Del Type Reason
			if (isEligibleEdit(oldWObj.getDelTypeReason(), wpojo.getDelTypeReason())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_DELTYPEREASON + " = " + (char) 39 + wpojo.getDelTypeReason() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_DELTYPEREASON, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDelTypeReason(),
						wpojo.getDelTypeReason(), transId));
			}

			// Del type Other Reason
			if (isEligibleEdit(oldWObj.getDeltype_otherreasons(), wpojo.getDeltype_otherreasons())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_DELTYPEOTHERREASONS + " = " + (char) 39 + wpojo.getDeltype_otherreasons()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_DELTYPEOTHERREASONS, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getDeltype_otherreasons(), wpojo.getDeltype_otherreasons(), transId));
			}

			// No of children
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + NO_OF_CHILDREN + " = " + (char) 39 + wpojo.getNo_of_child() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, NO_OF_CHILDREN,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getNo_of_child() + "", transId));

			// Del Time
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_TIME + " = " + (char) 39 + wpojo.getDel_Time() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_TIME,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_Time() + "", transId));

			// Del Date
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_DATE + " = " + (char) 39 + wpojo.getDel_Date() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_DATE,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_Date(), transId));

			// Del Result1
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + DEL_RESULT1 + " = " + (char) 39 + wpojo.getDel_result1() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_RESULT1,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getDel_result1() + "", transId));

			// Baby wt 1
			if (oldWObj.getBabywt1() != wpojo.getBabywt1()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYWT1 + " = " + (char) 39 + wpojo.getBabywt1() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT1,
						Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getBabywt1() + "", transId));
			}

			// Baby Sex1
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + BABYSEX1 + " = " + (char) 39 + wpojo.getBabysex1() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYSEX1,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getBabysex1() + "", transId));

			int noofchild = wpojo.getNo_of_child();
			String deltime2 = "";
			int del_result2 = 99, babysex2 = 99;
			if (noofchild == 2) {
				deltime2 = wpojo.getDel_time2();
				del_result2 = wpojo.getDel_result2();
				babysex2 = wpojo.getBabysex2();

				if (appendStr != "")
					appendStr = appendStr + ", ";

				appendStr = appendStr + DEL_TIME2 + " = " + (char) 39 + deltime2 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_TIME2,
						Partograph_CommonClass.getCurrentDateandTime(), "", deltime2 + "", transId));

				// Del Result2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DEL_RESULT2 + " = " + (char) 39 + del_result2 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, DEL_RESULT2,
						Partograph_CommonClass.getCurrentDateandTime(), "", del_result2 + "", transId));

				// Baby Sex2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYSEX2 + " = " + (char) 39 + babysex2 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYSEX2,
						Partograph_CommonClass.getCurrentDateandTime(), "", babysex2 + "", transId));

				// Baby wt 2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYWT2 + " = " + (char) 39 + wpojo.getBabywt2() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT2,
						Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getBabywt2() + "", transId));

			}

			// Delivery Comments
			if (isEligibleEdit(oldWObj.getDel_Comments(), wpojo.getDel_Comments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DEL_COMMENTS + " = " + (char) 39 + wpojo.getDel_Comments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						DEL_COMMENTS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDel_Comments(),
						wpojo.getDel_Comments(), transId));
			}

			// Mothers death
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + MOTHERSDEATH + " = " + (char) 39 + wpojo.getMothersdeath() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, MOTHERSDEATH,
					Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getMothersdeath() + "", transId));

			// Mothers death reason
			if (isEligibleEdit(oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + MOTHERS_DEATH_REASON + " = " + (char) 39 + wpojo.getMothers_death_reason()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						MOTHERS_DEATH_REASON, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason(), transId));
			}

			// Tears
			if (isEligibleEdit(oldWObj.getTears(), wpojo.getTears())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_TEARS + " = " + (char) 39 + wpojo.getTears() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_TEARS,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getTears(), wpojo.getTears(), transId));
			}

			// Episiotomy
//			if (oldWObj.getEpisiotomy() != wpojo.getEpisiotomy() && !(oldWObj.getDel_type()!=0)) 
			{
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_EPISIOTOMY + " = " + (char) 39 + wpojo.getEpisiotomy() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_EPISIOTOMY, Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getEpisiotomy() + "",
						transId));
			}

			// Suture
			if (oldWObj.getSuturematerial() != wpojo.getSuturematerial()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_SUTUREMATERIAL + " = " + (char) 39 + wpojo.getSuturematerial() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_SUTUREMATERIAL, Partograph_CommonClass.getCurrentDateandTime(), "",
						wpojo.getSuturematerial() + "", transId));
			}

			// Date last updated
			if (isEligibleEdit(oldWObj.getDatelastupdated(), wpojo.getDatelastupdated())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DATE_LASTUPDATED + " = " + (char) 39 + wpojo.getDatelastupdated() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						DATE_LASTUPDATED, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDatelastupdated(),
						wpojo.getDatelastupdated(), transId));
			}

			/*
			 * // date_of_reg_entry - 10Nov2016 Arpitha if
			 * (oldWObj.getDate_of_reg_entry() != wpojo.getDate_of_reg_entry())
			 * { if (appendStr != "") appendStr = appendStr + ", "; appendStr =
			 * appendStr + DATE_OF_REG_ENTRY + " = " + (char) 39 +
			 * wpojo.getDate_of_reg_entry() + (char) 39 + ""; auditArr.add(new
			 * AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(),
			 * TBL_REGISTEREDWOMEN, DATE_OF_REG_ENTRY,
			 * Partograph_CommonClass.getCurrentDateandTime(), "",
			 * wpojo.getDate_of_reg_entry() + "", transId)); }
			 */

			// Normal type - 20Nov2016 Arpitha
			if (oldWObj.getNormaltype() != wpojo.getNormaltype()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + C_NORMALTYPE + " = " + (char) 39 + wpojo.getNormaltype() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						C_NORMALTYPE, Partograph_CommonClass.getCurrentDateandTime(), "", wpojo.getNormaltype() + "",
						transId));
			}

			String strUpdate = "";
			if (appendStr.length() > 0) {
				strUpdate = "Update " + TBL_REGISTEREDWOMEN + "  Set " + appendStr + " where  user_Id = '"
						+ wpojo.getUserId() + "' and women_id = '" + wpojo.getWomenId() + "' ";

				db.execSQL(strUpdate);

				iUpdateRecordTrans(wpojo.getUserId(), transId, TBL_REGISTEREDWOMEN, strUpdate, null, null);

				for (AuditTrailPojo ap : auditArr) {
					if (!insertTotblAuditTrail(ap))
						return null;
				}

				iNewRecordTrans(wpojo.getUserId(), transId, "tblAuditTrail");
			}

			return strUpdate;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return new String();
		}
	}

	// Get Comments Apgar
	public Cursor getCommentsApgar(String strWomenid, String strwuserId, int objectid, int childid) throws Exception {
		// TODO Auto-generated method stub

		// 09Aug2016 - Bindu - include order by
		String strSql = "SELECT " + C_COMMENT + "," + DATE_OF_ENTRY + "," + TIME_OF_ENTRY + " FROM " + TBL_COMMENT
				+ " WHERE " + WOMEN_ID + " = '" + strWomenid + "' and " + USER_ID + " = '" + strwuserId + "' and "
				+ C_OBJECTID + " = '" + objectid + "' and " + C_CHILDID + "='" + childid
				+ "' order by dateinserted desc ";
		Cursor cursor = db.rawQuery(strSql, null);

		return cursor;
	}

	public void updateUserData(ContentValues values, String userid) throws Exception {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		try {

			db.update("tbl_user", values, "user_id" + "= '" + userid + "'", null);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 04Feb2015 Mtd to get oxytocin data
	public ArrayList<Add_value_pojo> getOxytocinData(int obj_Id, String strWomenid, String strwuserId) {
		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
					+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
					+ strwuserId + "' order by " + C_TRANSID + " desc";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {
					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "'";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							// updated By Arpitha
							if (c2.getString(0).equalsIgnoreCase("5.1")) {
								avp.setOxytocindrops(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("5.2")) {
								avp.setOxytocinunits(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("5.3")) {
								avp.setOxytocincomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}
						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// Get the danger sign list for the women
	public ArrayList<Integer> getDangerList(String womenid, String userId) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		ArrayList<Integer> dangerlist = null;

		String strSelect = "Select distinct " + WOMEN_ID + "," + C_OBJECTID + " from " + TBL_PROPERTYVALUES + " where "
				+ WOMEN_ID + " = '" + womenid + "' and " + USER_ID + " = '" + userId + "' and " + C_ISDANGER + " = 1";
		Cursor cursor = db.rawQuery(strSelect, null);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			dangerlist = new ArrayList<Integer>();
			do {
				dangerlist.add(cursor.getInt(1));
			} while (cursor.moveToNext());

		}

		return dangerlist;
	}

	// Get FHR value
	public ArrayList<Add_value_pojo> getFHRGraphData(int obj_Id, String strWomenid, String strwuserId) {
		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT " + C_PROP_VALUE + "," + DATE_OF_ENTRY + " , " + TIME_OF_ENTRY + " FROM "
					+ TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = " + obj_Id + " and " + WOMEN_ID + " = '"
					+ strWomenid + "'" + "and " + USER_ID + "= '" + strwuserId + "'" + "and " + C_PROP_ID + "= '1'";

			Cursor cursor = db.rawQuery(mainSql, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				Add_value_pojo data;
				do {
					data = new Add_value_pojo();

					data.setStrVal(cursor.getString(0));
					data.setStrTime(cursor.getString(2));
					data.setStrdate(cursor.getString(1));

					arrAvp.add(data);
				} while (cursor.moveToNext());

				cursor.close();
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// Get Settins last modified date
	public String getSettingsLastmodifieddate(String wUserId) {
		// TODO Auto-generated method stub
		String maxDate = "";
		db = this.getWritableDatabase();
		try {
			String strSql = "Select max(lastmodifieddate) from tbl_settings where user_Id= '" + wUserId + "'";
			Cursor cursor = db.rawQuery(strSql, null);

			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				maxDate = cursor.getString(0);
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return maxDate;
	}

	public boolean insertSettings(String userID) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			// values.put(USER_ID, userID);
			values.put(C_EMAILID, userID);// 06April2017 Arpitha

			db.insert(TBL_SETTINGS, null, values);
			return true;
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return false;
		}
	}

	public SettingsPojo getSettingsDetails(String userId2) throws Exception {
		db = this.getWritableDatabase();
		SettingsPojo spojo = null;

		String strselect = "SELECT * FROM " + TBL_SETTINGS;
		Cursor cursor = db.rawQuery(strselect, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				spojo = new SettingsPojo();
				spojo.setIpAddress(cursor.getString(2));
				spojo.setServerDb(cursor.getString(3));
				spojo.setExpire_date(cursor.getString(8));// 11Sep2017 Arpitha

			} while (cursor.moveToNext());
		}

		if (cursor != null)
			cursor.close();
		return spojo;
	}

	// updated on 17Nov2015 - Check if there are records to sync and return true
	// if count is >0
	public boolean GetSyncCountRecords() throws Exception {
		int cnt = 0;
		String strSql = "SELECT count(*) from " + TBL_TRANSHEADER + " where (TransStatus = 'N' or TransStatus = 'S') ";
		Cursor cursor = db.rawQuery(strSql, null);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			cnt = cursor.getInt(0);
			if (cnt > 0)
				Partograph_CommonClass.isRecordExistsForSync = true;
			else
				Partograph_CommonClass.isRecordExistsForSync = false;
		}

		return Partograph_CommonClass.isRecordExistsForSync;
	}

	// Creating tables and inserting data
	public void updateTablesandData(String filename, SQLiteDatabase db) {
		int count = 0;
		try {
			InputStream in = context.getResources().getAssets().open(filename);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			while (true) {
				int read = in.read(buffer);
				if (read == -1) {
					break;
				}
				out.write(buffer, 0, read);
			}

			byte[] bytes = out.toByteArray();
			String sql = new String(bytes);
			String[] lines = sql.split(";(\\s)*[\n\r]");
			if (true) {
				db.beginTransaction();
				int cnt = 0;
				for (String line : lines) {
					line = line.trim();
					if (line.length() > 0) {
						cnt++;
						db.execSQL(line);
						count++;
					}
				}
				db.setTransactionSuccessful();
			}
			db.endTransaction();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Insert Referral data into tbl_Referral
	public boolean addReferral(WomenReferral_pojo wrefpojo, int transId) throws Exception {
		boolean isReferredAdded = false;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(USER_ID, wrefpojo.getUserid());
		values.put(WOMEN_ID, wrefpojo.getWomenid());
		values.put(C_WNAME, wrefpojo.getWomenname());
		values.put(FACILITY, wrefpojo.getFacility());
		values.put(C_FNAME, wrefpojo.getFacilityname());
		values.put(C_REFERREDTOFACILITY, wrefpojo.getReferredtofacilityid());
		values.put(C_PLACEOFREFERRAL, wrefpojo.getPlaceofreferral());
		values.put(C_REASONFORREFERRAL, wrefpojo.getReasonforreferral());
		values.put(C_DATEOFREFERRAL, wrefpojo.getDateofreferral());
		values.put(C_TIMEOFREFERRAL, wrefpojo.getTimeofreferral());
		values.put(TRANS_ID, transId);
		values.put(C_CREATEDDATE, wrefpojo.getCreated_date());
		values.put(C_LASTDATEUPDATED, wrefpojo.getLastupdateddate());
		// 04jan2016
		values.put(C_DESCFORREFERRAL, wrefpojo.getDescriptionofreferral());
		values.put(C_CONDITIONOFMOTHER, wrefpojo.getConditionofmother());
		values.put(C_BP, wrefpojo.getBp());
		values.put(C_PULSE, wrefpojo.getPulse());
		values.put(C_CONDITIONOFBABY, wrefpojo.getConditionofbaby());

		// updated on 27feb2016 by Arpitha
		values.put(C_HeartRate, wrefpojo.getHeartrate());
		values.put(C_SPO2, wrefpojo.getSpo2());

		long id = db.insert(TBL_REFERRAL, null, values);
		if (id > 0) {
			isReferredAdded = true;
		}
		return isReferredAdded;
	}

	// Get Details of Women Referral from tbl_referral
	public Cursor getReferralDetails(String wUserId, String womenId) throws Exception {
		db = this.getWritableDatabase();
		Cursor cursor = null;
		String strselect = " SELECT * FROM " + TBL_REFERRAL + " where " + WOMEN_ID + " = '" + womenId + "' and "
				+ USER_ID + " = '" + wUserId + "'";

		cursor = db.rawQuery(strselect, null);

		return cursor;
	}

	// Update Women Details
	public String updateWomenReferralData(WomenReferral_pojo wpojo, int transId) throws Exception {
		db = this.getWritableDatabase();
		try {
			// updated on 28feb16 by Arpitha - spo2 and heartrate
			String strUpdate = " UPDATE " + TBL_REFERRAL + " SET " + C_WNAME + " = '" + wpojo.getWomenname() + "'" + ","
					+ C_REFERREDTOFACILITY + " = '" + wpojo.getReferredtofacilityid() + "' " + " ," + C_PLACEOFREFERRAL
					+ " = '" + wpojo.getPlaceofreferral() + "'" + "," + C_REASONFORREFERRAL + " = '"
					+ wpojo.getReasonforreferral() + "' " + " , " + C_DATEOFREFERRAL + " = '"
					+ wpojo.getDateofreferral() + "'" + " , " + C_TIMEOFREFERRAL + " = '" + wpojo.getTimeofreferral()
					+ "'" + " , " + C_LASTDATEUPDATED + " = '" + wpojo.getLastupdateddate() + "'" + ", " + C_HeartRate
					+ " = '" + wpojo.getHeartrate() + "'" + "," + C_SPO2 + " = ' " + wpojo.getSpo2() + "'" + " WHERE "
					+ WOMEN_ID + " = '" + wpojo.getWomenid() + "' and " + USER_ID + " = '" + wpojo.getUserid() + "'";

			db.execSQL(strUpdate);

			iUpdateRecordTrans(wpojo.getUserid(), transId, TBL_REFERRAL, strUpdate, null, null);

			return strUpdate;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return new String();
		}
	}

	// Get Third Stage Details
	public Cursor GetThirdStageDetails(String strWomenid, String strwuserId, int object_Id) {
		Cursor cursor = null;
		try {
			db = this.getWritableDatabase();

			String strSql = "Select date_of_entry, time_of_entry, "
					+ "Max(case when prop_Id =  '11.1' Then prop_value END) Duration, "
					+ "Max(case when prop_Id =  '11.2' Then prop_value END) Oxytocin, "
					+ "Max(case when prop_Id =  '11.3' Then prop_value END) Ergometrine,"
					+ "Max(case when prop_Id =  '11.4' Then prop_value END) PGF2alpha,"
					+ "Max(case when prop_Id =  '11.5' Then prop_value END) Misoprost, "
					+ "Max(case when prop_Id =  '11.6' Then prop_value END) placenta, "
					+ "Max(case when prop_Id =  '11.7' Then prop_value END) uterine_massage, "
					+ "Max(case when prop_Id =  '11.8' Then prop_value END) ebl, "
					+ "Max(case when prop_Id =  '11.9' Then prop_value END) pph, "
					+ "Max(case when prop_Id =  '11.10' Then prop_value END) pph_prop, "
					+ "Max(case when prop_Id =  '11.11' Then prop_value END) retainedplacenta, "
					+ "Max(case when prop_Id =  '11.12' Then prop_value END) inversion, "
					+ "Max(case when prop_Id =  '11.13' Then prop_value END) comments "
					+ " from tbl_propertyvalues where women_Id = '" + strWomenid + "' and user_Id = '" + strwuserId
					+ "' and object_Id = " + object_Id + "  group by trans_Id ";

			cursor = db.rawQuery(strSql, null);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return cursor;
	}

	// Get Third Stage Details
	public Cursor GetFourthStageDetails(String strWomenid, String strwuserId, int object_Id) {
		Cursor cursor = null;
		try {
			db = this.getWritableDatabase();

			String strSql = "Select date_of_entry, time_of_entry, "
					+ "Max(case when prop_Id =  '12.1' Then prop_value END) uterus, "
					+ "Max(case when prop_Id =  '12.2' Then prop_value END) urinepassed, "
					+ "Max(case when prop_Id =  '12.3' Then prop_value END) bleeding,"
					+ "Max(case when prop_Id =  '12.4' Then prop_value END) pulserate,"
					+ "Max(case when prop_Id =  '12.5' Then prop_value END) bpsystolic, "
					+ "Max(case when prop_Id =  '12.6' Then prop_value END) diastolic, "
					+ "Max(case when prop_Id =  '12.7' Then prop_value END) comments, "
					+ "Max(case when prop_Id =  '12.8' Then prop_value END) breastFeeding, "
					+ "Max(case when prop_Id =  '12.9' Then prop_value END) reasonForNotFeeding, "
					+ "Max(case when prop_Id =  '12.10' Then prop_value END) bfDateTime "
					+ " from tbl_propertyvalues where women_Id = '" + strWomenid + "' and user_Id = '" + strwuserId
					+ "' and object_Id = " + object_Id + "  group by trans_Id ";

			cursor = db.rawQuery(strSql, null);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return cursor;
	}

	public boolean getisWomenReferred(String womenId, String userId) throws Exception {
		db = this.getWritableDatabase();

		boolean isReferred = false;

		String strSelect = "SELECT count(*) FROM " + TBL_REFERRAL + " WHERE " + WOMEN_ID + " = '" + womenId + "' and "
				+ USER_ID + " = '" + userId + "'";
		Cursor cursor = db.rawQuery(strSelect, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			int cnt = cursor.getInt(0);
			if (cnt > 0)
				isReferred = true;
		}

		return isReferred;
	}

	// Update some of the fields in delivery details - 06jan2016
	public String updateDelDetailsData(Women_Profile_Pojo wpojo, int transId, Women_Profile_Pojo oldWObj) {
		db = this.getWritableDatabase();
		try {

			// 08aug2016 - Bindu - audittrail
			ArrayList<AuditTrailPojo> auditArr = new ArrayList<AuditTrailPojo>();
			String appendStr = "";

			int noofchild = oldWObj.getNo_of_child();
			int babysex2 = 99;
			// Delivery Comments
			if (isEligibleEdit(oldWObj.getDel_Comments(), wpojo.getDel_Comments())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DEL_COMMENTS + " = " + (char) 39 + wpojo.getDel_Comments() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						DEL_COMMENTS, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDel_Comments(),
						wpojo.getDel_Comments(), transId));
			}

			// Baby wt 1
			if (oldWObj.getBabywt1() != wpojo.getBabywt1()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYWT1 + " = " + (char) 39 + wpojo.getBabywt1() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT1,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabywt1() + "",
						wpojo.getBabywt1() + "", transId));
			}
			// Baby Sex1
			if (appendStr != "")
				appendStr = appendStr + ", ";
			appendStr = appendStr + BABYSEX1 + " = " + (char) 39 + wpojo.getBabysex1() + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYSEX1,
					Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabysex1() + "",
					wpojo.getBabysex1() + "", transId));

			if (noofchild == 2) {
				babysex2 = wpojo.getBabysex2();

				// Baby Sex2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYSEX2 + " = " + (char) 39 + babysex2 + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYSEX2,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabysex2() + "", babysex2 + "",
						transId));

				// Baby wt 2
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + BABYWT2 + " = " + (char) 39 + wpojo.getBabywt2() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN, BABYWT2,
						Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getBabywt2() + "",
						wpojo.getBabywt2() + "", transId));

			}

			// Mothers death
			if (oldWObj.getMothersdeath() != wpojo.getMothersdeath()) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + MOTHERSDEATH + " = " + (char) 39 + wpojo.getMothersdeath() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						MOTHERSDEATH, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getMothersdeath() + "",
						wpojo.getMothersdeath() + "", transId));
			}

			// Mothers death reason
			if (isEligibleEdit(oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + MOTHERS_DEATH_REASON + " = " + (char) 39 + wpojo.getMothers_death_reason()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						MOTHERS_DEATH_REASON, Partograph_CommonClass.getCurrentDateandTime(),
						oldWObj.getMothers_death_reason(), wpojo.getMothers_death_reason(), transId));
			}

			// Date last updated
			if (isEligibleEdit(oldWObj.getDatelastupdated(), wpojo.getDatelastupdated())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DATE_LASTUPDATED + " = " + (char) 39 + wpojo.getDatelastupdated() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(), TBL_REGISTEREDWOMEN,
						DATE_LASTUPDATED, Partograph_CommonClass.getCurrentDateandTime(), oldWObj.getDatelastupdated(),
						wpojo.getDatelastupdated(), transId));
			}

			// Extra Comments - 13Oct2016 Arpitha
			/*
			 * if (isEligibleEdit(oldWObj.getExtracomments(),
			 * wpojo.getExtracomments())) { if (appendStr != "") appendStr =
			 * appendStr + ", "; appendStr = appendStr + C_EXTRACOMMENTS + " = "
			 * + (char) 39 + wpojo.getExtracomments() + (char) 39 + "";
			 * auditArr.add(new AuditTrailPojo(wpojo.getUserId(),
			 * wpojo.getWomenId(), TBL_REGISTEREDWOMEN, C_EXTRACOMMENTS,
			 * Partograph_CommonClass.getCurrentDateandTime(),
			 * oldWObj.getExtracomments(), wpojo.getExtracomments(), transId));
			 * }
			 */

			/*
			 * // Reg type - 13Oct2016 Arpitha if (oldWObj.getregtype() !=
			 * wpojo.getregtype()) { if (appendStr != "") appendStr = appendStr
			 * + ", "; appendStr = appendStr + C_REGTYPE + " = " + (char) 39 +
			 * wpojo.getregtype() + (char) 39 + ""; auditArr.add(new
			 * AuditTrailPojo(wpojo.getUserId(), wpojo.getWomenId(),
			 * TBL_REGISTEREDWOMEN, C_REGTYPE,
			 * Partograph_CommonClass.getCurrentDateandTime(), "",
			 * wpojo.getregtype() + "", transId)); }
			 */

			String strUpdate = "";
			if (appendStr.length() > 0) {
				strUpdate = "Update " + TBL_REGISTEREDWOMEN + "  Set " + appendStr + " where  user_Id = '"
						+ wpojo.getUserId() + "' and women_id = '" + wpojo.getWomenId() + "' ";

				db.execSQL(strUpdate);

				iUpdateRecordTrans(wpojo.getUserId(), transId, TBL_REGISTEREDWOMEN, strUpdate, null, null);

				for (AuditTrailPojo ap : auditArr) {
					if (!insertTotblAuditTrail(ap))
						return null;
				}

				iNewRecordTrans(wpojo.getUserId(), transId, "tblAuditTrail");
			}

			return strUpdate;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return new String();
		}
	}

	// updated on 28feb16 by Arpitha
	public Cursor getExpertComments(String strWomenid, String strwuserId) throws Exception {
		db = this.getWritableDatabase();

		String strSql = "SELECT " + C_OBJECTID + "," + C_COMMENT + "," + DATE_OF_ENTRY + "," + TIME_OF_ENTRY + " FROM "
				+ TBL_COMMENT + " WHERE " + WOMEN_ID + " = '" + strWomenid + "' and " + USER_ID + " = '" + strwuserId
				+ "'  order by " + DATEINSERTED + " desc";
		Cursor cursor = db.rawQuery(strSql, null);

		return cursor;
	}

	// updated on 28feb16 by Arpitha
	public Cursor getobjectname(int object_id) {
		db = this.getWritableDatabase();
		String str = "Select distinct object_Type from tbl_ObjectTypeMaster where object_Id = '" + object_id + "'";
		Cursor res = db.rawQuery(str, null);
		return res;
	}

	public Cursor getdipwomen(String userid, int displaylistcount) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " + DEL_STATUS
				+ " is null  and regtype!=2 and user_Id = '" + userid
				+ "' and women_id IN(Select women_id from tbl_propertyValues where object_id<=9) and women_id NOT IN(Select women_id from tbl_referral) and women_id NOT IN (Select discWomanId from tbldischargedetails) order by dateinserted desc limit "
				+ displaylistcount;

		cur = db.rawQuery(sql, null);
		return cur;
	}

	public Cursor getreggt24hrs(String userid, int displaylistcountdip24hrs) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " + DEL_STATUS
				+ " is null and regtype!=2 and user_Id = '" + userid
				+ "' and ((strftime('%s', datetime('now', 'localtime')) - strftime('%s', date_of_adm  || ' '  || time_of_adm)) / (60) )>=1440 and women_id NOT IN(Select women_id from tbl_referral) and women_id NOT IN(Select women_id from tbl_propertyvalues) order by dateinserted desc limit "
				+ displaylistcountdip24hrs;

		cur = db.rawQuery(sql, null);
		return cur;
	}

	public Cursor getdelivered(String todaysDate, String userid, int mon, int year, String fromDate, String toDate,
			int displaylistcount) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		// Bindu - 10Aug2016 - concatenate del time to get desc order
		String sql = "SELECT * FROM " + TBL_REGISTEREDWOMEN + " WHERE " + USER_ID + "= '" + userid
				+ "'  and del_status is not null and  " + DEL_DATE + " between " + " '" + fromDate + "' and '" + toDate
				+ "' and women_id NOT IN (Select discWomanId from tbldischargedetails) order by " + DEL_DATE + " || "
				+ DEL_TIME + " desc limit " + displaylistcount;
		cur = db.rawQuery(sql, null);
		return cur;
	}

	public Cursor getRetrospectiveData(String userid, int displaylistcount) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		// Bindu - 10Aug2016 - concatenate del time to get desc order
		String sql = "SELECT * FROM " + TBL_REGISTEREDWOMEN + " WHERE " + USER_ID + "= '" + userid
				+ "'  and regtype==2 and women_id NOT IN (Select discWomanId from tbldischargedetails) order by "
				+ DEL_DATE + " || " + DEL_TIME + " desc limit " + displaylistcount;
		cur = db.rawQuery(sql, null);
		return cur;
	}

	// 07Jun2017 Arpitha - display delivered & referred woman in referred tab
	public Cursor getreferredwomenData(String userid, int displaylistcount) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		// ArrayList<String> womenId = new ArrayList<String>();
		/*
		 * String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " +
		 * DEL_STATUS + " is null and user_Id = '" + userid +
		 * "' and women_Id IN(Select women_id from tbl_referral order by dateofreferral || timeofreferral desc)"
		 * ;
		 */// order
			// by
			// dateinserted
			// desc";
			// 01Nov2016 Arpitha // //19Aug2016
			// String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " +
			// DEL_STATUS + " is null and user_Id = '"
			// + userid + "' and women_Id IN(Select women_id from tbl_referral)
			// order by trans_id desc";

		String sql = "Select * from TBL_REGISTEREDWOMEN where user_id= '" + userid
				+ "' and women_id IN (Select women_id from tbl_referral) limit " + displaylistcount;
		cur = db.rawQuery(sql, null);
		return cur;
	}

	public ArrayList<String> getreferredwomen(String userid) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		ArrayList<String> womenId = new ArrayList<String>();
		/*
		 * String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " +
		 * DEL_STATUS + " is null and user_Id = '" + userid +
		 * "' and women_Id IN(Select women_id from tbl_referral order by dateofreferral || timeofreferral desc)"
		 * ;
		 */// order
			// by
			// dateinserted
			// desc";
			// 01Nov2016 Arpitha // //19Aug2016
			// String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " +
			// DEL_STATUS + " is null and user_Id = '"
			// + userid + "' and women_Id IN(Select women_id from tbl_referral)
			// order by trans_id desc";

		String sql = "Select women_id from tbl_referral where user_id= '" + userid + "'";
		cur = db.rawQuery(sql, null);
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				womenId.add(cur.getString(0));
			} while (cur.moveToNext());
		}
		return womenId;
	}

	public String lastvaldate(String womenid, int object_id) {
		db = this.getWritableDatabase();
		String s = null;
		String sql = "Select max(date_of_entry)  from tbl_propertyvalues where object_id = '" + object_id
				+ "' and women_id='" + womenid + "'";
		Cursor res = db.rawQuery(sql, null);
		res.moveToFirst();
		if (res.getCount() > 0) {

			s = res.getString(0);

		}
		return s;
	}

	public String lastvaltime(String womenid, int object_id, String date_of_entry) {
		db = this.getWritableDatabase();
		String s = null;
		String sql = "Select max(time_of_entry) from tbl_propertyvalues where object_id = '" + object_id
				+ "' and women_id='" + womenid + "' and date_of_entry='" + date_of_entry + "'";
		Cursor res = db.rawQuery(sql, null);
		res.moveToFirst();
		if (res.getCount() > 0) {

			s = res.getString(0);
		}
		return s;
	}

	// update on 28jun16 by Arpitha
	public ArrayList<Add_value_pojo> getFHRData_display(int obj_Id, String strWomenid, String strwuserId) {
		db = this.getWritableDatabase();

		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID
					+ " = " + obj_Id + " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID + "= '"
					+ strwuserId + "' order by " + C_TRANSID + " desc";
			Cursor c1 = db.rawQuery(mainSql, null);
			if (c1.moveToFirst() && c1.getCount() > 0) {

				do {

					String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + ","
							+ TIME_OF_ENTRY + " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id
							+ "' and " + C_TRANSID + " = '" + c1.getString(0) + "' order by " + C_TRANSID + " desc";
					Cursor c2 = db.rawQuery(propStr, null);
					if (c2.moveToFirst() && c2.getCount() > 0) {

						Add_value_pojo avp = new Add_value_pojo();
						do {
							if (c2.getString(0).equalsIgnoreCase("1")) {
								avp.setStrVal(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							} else if (c2.getString(0).equalsIgnoreCase("1.2")) {
								avp.setFhrcomments(c2.getString(1));
								avp.setStrdate(c2.getString(2));
								avp.setStrTime(c2.getString(3));
							}

						} while (c2.moveToNext());
						arrAvp.add(avp);
					}

				} while (c1.moveToNext());

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	public ArrayList<WomenReferral_pojo> getreferred_data(String womenid) {
		db = this.getWritableDatabase();
		Cursor cur;
		ArrayList<WomenReferral_pojo> wpojo = new ArrayList<WomenReferral_pojo>();
		String sql = "select womenname, reasonforreferral, descriptionforreferral, placeofreferral, dateofreferral from tbl_referral where women_id ='"
				+ womenid + "'";
		cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				WomenReferral_pojo wrpojo = new WomenReferral_pojo();
				wrpojo.setWomenname(cur.getString(0));
				wrpojo.setReasonforreferral(cur.getString(1));
				wrpojo.setDescriptionofreferral(cur.getString(2));
				wrpojo.setPlaceofreferral(cur.getString(3));
				wrpojo.setDateofreferral(cur.getString(4));
				wpojo.add(wrpojo);
			} while (cur.moveToNext());
		}
		return wpojo;
	}

	public ArrayList<String> getobjectid_danger(String women_id) {
		db = this.getWritableDatabase();
		ArrayList<String> obj_name = new ArrayList<String>();
		String objname = null;
		String s = "Select distinct object_id from tbl_propertyValues where isdanger=1 and women_id ='" + women_id
				+ "'";

		Cursor cur = db.rawQuery(s, null);
		cur.moveToFirst();
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				objname = cur.getString(0);
				obj_name.add(objname);
			} while (cur.moveToNext());
		}

		return obj_name;
	}

	// updated on19july2016 by Arpitha
	public boolean addphno(PhoneNumber_Pojo ppojo) {
		boolean isphnoadded = false;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(C_SELECTEDOPTION, ppojo.getSelectedoption());
		values.put(C_PHONENUMBER, ppojo.getPhn_no());
		values.put(USER_ID, ppojo.getUserid());

		long id = db.insert(TBL_PHONENUMBER, null, values);
		if (id > 0) {
			isphnoadded = true;
		}
		return isphnoadded;
	}

	public ArrayList<PhoneNumber_Pojo> getphn_numbers(String userid) {
		ArrayList<PhoneNumber_Pojo> values = new ArrayList<PhoneNumber_Pojo>();
		PhoneNumber_Pojo ppojo;
		String sql = "Select * from tbl_phonenumbers where user_id ='" + userid + "' order by dateinserted desc";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				ppojo = new PhoneNumber_Pojo();
				ppojo.setSelectedoption(cur.getInt(1));
				ppojo.setPhn_no(cur.getString(2));
				values.add(ppojo);
			} while (cur.moveToNext());
		}
		return values;
	}

	public ArrayList<String> phn_number(String userid, int option) {
		ArrayList<String> values = new ArrayList<String>();
		String sql = "Select phno_no from tbl_phonenumbers where user_id ='" + userid + "' and selectedoption ="
				+ option + " order by dateinserted desc";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0 && cur != null) {
			do {
				String no = cur.getString(0);
				values.add(no);
			} while (cur.moveToNext());
		}
		return values;
	}

	public void delete_phnno(String phn_no, int option, String userid) {
		db = this.getWritableDatabase();
		boolean isdeleted = false;
		String sql = "Delete from tbl_phonenumbers where phno_no='" + phn_no + "' and selectedoption =" + option
				+ " and user_id ='" + userid + "'";
		db.execSQL(sql);
	}

	// Bindu 26July2016
	public boolean getPartoData(String womenId, String wUserId) {
		try {
			db = this.getWritableDatabase();

			// update bindu - 24Aug2016 to check for obj id <=9
			String strSql = "Select * from " + TBL_PROPERTYVALUES + " where " + WOMEN_ID + " = '" + womenId + "' and "
					+ USER_ID + " = '" + wUserId + "' and " + C_OBJECTID + " <= 9 ";
			Cursor cur = db.rawQuery(strSql, null);
			cur.moveToFirst();// 23oct2016 Arpitha
			int i = cur.getCount();
			if (cur != null && i > 0) {
				return true;
			} else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			return false;
		}

	}

	// Check whether user must be alerted if partograph value is not entered in
	// specified time duration
	public boolean getisNotifyUser(String womenId, int objectid) {
		boolean isNotify = false;
		try {
			db = this.getWritableDatabase();
			int[] objarr = { 1, 3, 4, 7 };
			String sql = " Select prop_id from tbl_propertyvalues where object_id in (1,3,4,7) and women_Id = '"
					+ womenId + "' ";
			Cursor c = db.rawQuery(sql, null);
			if (c != null && c.getCount() > 0) {

				for (int i = 0; i < objarr.length; i++) {
					int oid = objarr[i];
					String strSql = "select object_id, date_of_entry || '_' || time_of_entry from tbl_propertyvalues where object_id  = '"
							+ oid + "' and women_id = '" + womenId + "' order by trans_id desc limit 1";
					Cursor cur = db.rawQuery(strSql, null);
					if (cur != null && cur.getCount() > 0) {
						String strCurrentTime = Partograph_CommonClass.getCurrentDateTime();
						cur.moveToFirst();
						do {
							if (cur.getInt(0) != 3)
								isNotify = Partograph_CommonClass.getisValidTime(cur.getString(1), strCurrentTime, 35);
							else
								isNotify = Partograph_CommonClass.getisValidTime(cur.getString(1), strCurrentTime, 65);

							if (isNotify) {
								return true;
							}
						} while (cur.moveToNext());
					}
				}
			} else
				isNotify = false;
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			return false;
		}
		return isNotify;
	}

	// 29July2016 - bindu - getWomandetails
	public Women_Profile_Pojo getWomanDetails(String userId, String womenId) {
		Women_Profile_Pojo wdata = null;
		try {

			db = this.getWritableDatabase();
			String strSql = " Select * from " + TBL_REGISTEREDWOMEN + " where " + USER_ID + " = '" + userId + "' and "
					+ WOMEN_ID + " = '" + womenId + "'";
			Cursor cursor = db.rawQuery(strSql, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();

				do {
					wdata = new Women_Profile_Pojo();
					wdata.setWomenId(cursor.getString(1));

					if (cursor.getType(2) > 0) {
						String b = (cursor.getString(2).length() > 1) ? cursor.getString(2) : null;
						byte[] decoded = (b != null) ? Base64.decode(b, Base64.DEFAULT) : null;
						wdata.setWomen_Image(decoded);
					}

					wdata.setWomen_name(cursor.getString(3));
					wdata.setDate_of_admission(cursor.getString(4));
					wdata.setTime_of_admission(cursor.getString(5));
					wdata.setAge(cursor.getInt(6));
					wdata.setAddress(cursor.getString(7));
					wdata.setPhone_No(cursor.getString(8));
					wdata.setDel_type(cursor.getInt(9));
					wdata.setDoc_name(cursor.getString(10));
					wdata.setNurse_name(cursor.getString(11));
					wdata.setW_attendant(cursor.getString(12));
					wdata.setGravida(cursor.getInt(13));
					wdata.setPara(cursor.getInt(14));
					wdata.setHosp_no(cursor.getString(15));
					wdata.setFacility(cursor.getString(16));
					wdata.setWomenId(cursor.getString(1));
					wdata.setSpecial_inst(cursor.getString(17));
					wdata.setUserId(cursor.getString(0));
					wdata.setComments(cursor.getString(19));
					wdata.setRisk_category(cursor.getInt(20));
					wdata.setDel_Comments(cursor.getString(21));
					wdata.setDel_Time(cursor.getString(22));
					wdata.setDel_Date(cursor.getString(23));
					wdata.setDel_result1(cursor.getInt(24));
					wdata.setNo_of_child(cursor.getInt(25));
					wdata.setBabywt1(cursor.getInt(26));
					wdata.setBabywt2(cursor.getInt(27));
					wdata.setBabysex1(cursor.getInt(28));
					wdata.setBabysex2(cursor.getInt(29));
					wdata.setGestationage(cursor.getInt(30));
					wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
					wdata.setDel_result2(cursor.getInt(31));
					wdata.setAdmitted_with(cursor.getString(34));
					wdata.setMemb_pres_abs(cursor.getInt(35));
					wdata.setMothers_death_reason(cursor.getString(36));
					wdata.setState(cursor.getString(38));
					wdata.setDistrict(cursor.getString(39));
					wdata.setTaluk(cursor.getString(40));
					wdata.setFacility_name(cursor.getString(41));
					wdata.setGest_age_days(cursor.getInt(45));
					wdata.setDel_time2(cursor.getString(33));
					wdata.setThayicardnumber(cursor.getString(46));
					wdata.setDelTypeReason(cursor.getString(47));
					wdata.setDeltype_otherreasons(cursor.getString(48));
					wdata.setLmp(cursor.getString(49));
					wdata.setRiskoptions(cursor.getString(50));
					wdata.setTears(cursor.getString(51));
					wdata.setEpisiotomy(cursor.getInt(52));
					wdata.setSuturematerial(cursor.getInt(53));
					wdata.setBloodgroup(cursor.getInt(54));
					wdata.setEdd(cursor.getString(55));
					wdata.setHeight(cursor.getString(56));
					wdata.setW_weight(cursor.getString(57));
					wdata.setOther(cursor.getString(58));
					wdata.setHeight_unit(cursor.getInt(59));
					wdata.setDatelastupdated(cursor.getString(44));

					wdata.setExtracomments(cursor.getString(60));// 13oct2016
																	// Arpitha

					wdata.setregtype(cursor.getInt(61));// 16Oct2016 Arpitha

					wdata.setRegtypereason(cursor.getString(62));// 18Oct2016
																	// Arpitha
					wdata.setDate_of_reg_entry(cursor.getString(63));// 10Nov2016
																		// Arpitha

					wdata.setNormaltype(cursor.getInt(64));// 20Nov2016 Arpitha

					wdata.setRegtypeOtherReason(cursor.getString(65));// 21Nov2016
																		// Arpitha

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return wdata;
	}

	private boolean isEligibleEdit(String oldStr, String newStr) throws Exception {
		if (oldStr == null && newStr == null) {
			return false;
		} else if (oldStr == null && newStr.length() <= 0) {
			return false;
		} else {
			if (oldStr == null)
				return true;
			else if (newStr == null)
				return false;
			else if (!oldStr.equals(newStr))// 17may2017 Arpitha - v2.6 :
											// equalsIgnorecase to equals
				return true;
			else
				return false;
		}
	}

	public boolean insertTotblAuditTrail(AuditTrailPojo AJO) throws Exception {

		try {
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("User_Id", AJO.getUser_Id());
			values.put("Woman_Id", AJO.getWoman_Id());
			values.put("TblName", AJO.getTable_Name());
			values.put("ColumnName", AJO.getCol_Name());
			values.put("DateChangedOn", AJO.getDateChanged());
			values.put("Old_Value", AJO.getOld_value());
			values.put("New_Value", AJO.getNew_value());
			values.put("trans_Id", AJO.getTransId());

			long res = db.insert("tblAuditTrail", null, values);

			if (res > 0)
				return true;
			else
				return false;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return false;
		}
	}

	// Arpitha - 11Aug2016
	public ArrayList<Integer> getoption2(String user_id, String phn) {
		db = this.getWritableDatabase();
		int option = 0;
		ArrayList<Integer> options = new ArrayList<Integer>();
		String sql = "Select selectedoption from tbl_phonenumbers where phno_no='" + phn + "' and user_id='" + user_id
				+ "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				option = cur.getInt(0);
				options.add(option);

			} while (cur.moveToNext());
		}
		return options;

	}

	/**
	 * Get Failed Sms OR which is not Sent
	 * 
	 * @throws Exception
	 */
	public MessageLogPojo getFailedSmsOrNotSent() throws Exception {

		MessageLogPojo mlp = null;

		db = this.getWritableDatabase();
		Cursor cMsgs = db.rawQuery(Partograph_CommonClass.SQL6, null);

		if (cMsgs.moveToFirst() && cMsgs.getCount() > 0) {
			do {
				if (cMsgs.getString(11) != null && cMsgs.getString(11).length() > 0) {
					Date msgDt = Partograph_CommonClass.getDateSYYYYMMDD(cMsgs.getString(11));

					if (!msgDt.before(Partograph_CommonClass.getDateDDMMYYYY(Partograph_CommonClass.getTodaysDate())))
						mlp = new MessageLogPojo(cMsgs.getInt(0), cMsgs.getString(1), cMsgs.getString(2),
								cMsgs.getString(3), cMsgs.getString(4), cMsgs.getString(5), cMsgs.getString(6),
								cMsgs.getString(7), cMsgs.getInt(8), cMsgs.getInt(9));
				}

			} while (cMsgs.moveToNext());
		}

		cMsgs.close();
		return mlp;
	}

	// Bindu - 13Aug2016
	public void updateMessageLog(MessageLogPojo mlp, boolean isSuccess) throws Exception {
		db = this.getWritableDatabase();
		if (isSuccess) {
			Object[] arr = new Object[] { Partograph_CommonClass.getCurrentDateTime(), mlp.getUserId(),
					mlp.getWomanId(), mlp.getMsgId() };
			db.execSQL(Partograph_CommonClass.SQL7, arr);
		} else {
			Object[] arr = new Object[] { mlp.getUserId(), mlp.getWomanId(), mlp.getMsgId() };
			db.execSQL(Partograph_CommonClass.SQL8, arr);
		}

	}

	public boolean getNotifyForObject(String womenId, int i) {
		boolean isNotify = false;
		try {
			db = this.getWritableDatabase();

			String sql = " Select prop_id from tbl_propertyvalues where object_id = '" + i + "' and women_Id = '"
					+ womenId + "' ";
			Cursor c = db.rawQuery(sql, null);
			if (c != null && c.getCount() > 0) {

				String strSql = "select object_id, date_of_entry || '_' || time_of_entry from tbl_propertyvalues where object_id  = '"
						+ i + "' and women_id = '" + womenId + "' order by trans_id desc limit 1";
				Cursor cur = db.rawQuery(strSql, null);
				if (cur != null && cur.getCount() > 0) {
					String strCurrentTime = Partograph_CommonClass.getCurrentDateTime();
					cur.moveToFirst();
					do {
						if (cur.getInt(0) != 3)
							isNotify = Partograph_CommonClass.getisValidTime(cur.getString(1), strCurrentTime, 35);
						else
							isNotify = Partograph_CommonClass.getisValidTime(cur.getString(1), strCurrentTime, 65);

						if (isNotify) {
							return true;
						}
					} while (cur.moveToNext());
				} else
					return false;
			} else
				isNotify = false;
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			return false;
		}
		return isNotify;
	}

	// Bindu - 23Aug2016
	// insert to message log table
	public boolean insertToMessageLog(MessageLogPojo mlp, int transId) throws Exception {

		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("userId", mlp.getUserId());
		values.put("womanId", mlp.getWomanId());
		values.put("Risk", mlp.getRisk());
		values.put("RiskObserved", mlp.getRiskObserved());
		values.put("PhoneNo", mlp.getPhoneNo());
		values.put("MsgBody", mlp.getMsgBody());
		values.put("MsgSentDate", Partograph_CommonClass.getCurrentDateTime());
		values.put("Priority", mlp.getPriority());
		values.put("NoOfFailures", mlp.getNoOfFailures());
		values.put("MsgSent", 0);
		values.put("trans_id", transId);// 15Oct2016 Arpitha

		long res = db.insert("tblMessageLog", null, values);

		if (res > 0)
			return true;
		else
			return false;
	}

	// updated arpitha 24Aug2016
	public String getrefdate(String womenId, String userId2) throws Exception {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		String refdate = null;
		String sql = "Select dateofreferral || ' ' || timeofreferral from tbl_referral where women_id='" + womenId
				+ "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			refdate = cur.getString(0);
		}
		return refdate;
	}

	// updated on 29july2016 by Arpitha
	public String getlastentrytime(String womenid, int status) throws Exception {
		db = this.getWritableDatabase();
		String lasttime = null;

		String parto = "";
		if (status == 3) {
			parto = "object_id >=10";
		} else {
			parto = "object_id <=9";
		}
		String strSql = "Select * from tbl_propertyvalues where  women_id = '" + womenid + "'";
		Cursor cursor = db.rawQuery(strSql, null);
		if (cursor != null && cursor.getCount() > 0) {
			/*
			 * String sql =
			 * "select max(date_of_entry) || ' ' || time_of_entry from tbl_propertyvalues where "
			 * + parto + " and women_id = '" + womenid +
			 * "' Group by trans_id order by trans_id desc LIMIT 1"; commented
			 * on 22Nov2016 Arpitha
			 */
			String sql = "select max(date_of_entry) || ' ' || time_of_entry from tbl_propertyvalues where "
					+ " women_id = '" + womenid + "' Group by trans_id order by trans_id desc LIMIT 1";
			Cursor cur = db.rawQuery(sql, null);
			cur.moveToFirst();
			if (cur.getCount() > 0)

				lasttime = cur.getString(0);
		}
		return lasttime;

	}

	// 31Aug2016 - bindu
	public boolean getPartoDilData(String womenid, String userid) {
		try {
			db = this.getWritableDatabase();
			// to check for obj id =3 Dilatation
			String strSql = "Select * from " + TBL_PROPERTYVALUES + " where " + WOMEN_ID + " = '" + womenid + "' and "
					+ USER_ID + " = '" + userid + "' and " + C_OBJECTID + " = 3 ";
			Cursor cur = db.rawQuery(strSql, null);
			if (cur != null && cur.getCount() > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			return false;
		}
	}

	// 27Sep2016 Arpitha - to fetch last reg date/time
	public String getlastmaxdate(String userid, String classname) throws Exception {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		String lastdate = null;
		String sql = null;

		if (classname.equalsIgnoreCase(context.getResources().getString(R.string.registration))) {
			sql = "Select max(dateinserted) from " + TBL_REGISTEREDWOMEN + " where " + USER_ID + "='" + userid + "'";
		}
		if (classname.equalsIgnoreCase(context.getResources().getString(R.string.viewprofile))) {
			sql = "Select max(datelastupdated) from " + TBL_REGISTEREDWOMEN + " where " + USER_ID + "='" + userid + "'";
		}
		if (classname.equalsIgnoreCase(context.getResources().getString(R.string.partograph))) {
			sql = "Select max(date_of_entry) from tbl_propertyValues where user_id='" + userid + "'";
		}
		if (classname.equalsIgnoreCase(context.getResources().getString(R.string.settings))) {
			sql = "Select max(dateinserted) from tbl_phonenumbers where user_id='" + userid + "'";
		}
		if (classname.equalsIgnoreCase(context.getResources().getString(R.string.referral))) {
			sql = "Select max(created_date) from tbl_referral where user_id='" + userid + "'";
		}
		Cursor cur = db.rawQuery(sql, null);
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			lastdate = cur.getString(0);
		}
		return lastdate;
	}

	// 14Oct2016 Arpitha
	public boolean addcomment(Comments_Pojo cpojo, int transId) {
		boolean isadded = false;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(USER_ID, cpojo.getUserid());
		values.put(C_GENERALCOMMENT, cpojo.getComment());
		values.put(C_date_of_insertion, cpojo.getDate_of_insertion());
		values.put(C_time_of_insertion, cpojo.getTime_of_insertion());
		values.put(C_TRANSID, transId);
		values.put(C_Givenby, cpojo.getGivenby());

		long id = db.insert(TBL_GENERALCOMMENTS, null, values);
		if (id > 0) {
			isadded = true;
		}

		return isadded;

	}

	// 14Oct2016 Arpitha
	public ArrayList<Comments_Pojo> getCommentsToDisplay(String userId) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		ArrayList<Comments_Pojo> arrval = new ArrayList<Comments_Pojo>();
		Comments_Pojo cpojo;
		String sql = "Select * from " + TBL_GENERALCOMMENTS + " where " + USER_ID + "='" + userId
				+ "' order by date_created desc";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				cpojo = new Comments_Pojo();
				cpojo.setComment(cur.getString(3));
				cpojo.setDate_of_insertion(cur.getString(4));
				cpojo.setTime_of_insertion(cur.getString(5));
				cpojo.setGivenby(cur.getString(6));
				arrval.add(cpojo);

			} while (cur.moveToNext());
		}
		return arrval;

	}

	// 17Oct2016 Arpitha
	public boolean isdataentered(String wid, String userid, int objid) {
		db = this.getWritableDatabase();
		boolean isentered = false;
		String columnname = null;
		if (objid == 10)
			columnname = "object_id= 10";
		if (objid == 11)
			columnname = "object_id= 11 ";
		if (objid == 12)
			columnname = "object_id= 12 ";
		String sql = "Select * from tbl_propertyValues where women_id ='" + wid + "' and user_id='" + userid + "' and "
				+ columnname;
		Cursor cur = db.rawQuery(sql, null);

		if (cur.getCount() > 0) {
			cur.moveToFirst();
			isentered = true;
		}
		return isentered;

	}

	// 17Oct2016 Arpitha
	public void addtonotification(String strwuserId, String strWomenid, int i) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("user_id", strwuserId);
		values.put("women_id", strWomenid);
		values.put("fhr", 0);
		values.put("dilatation", 0);
		values.put("contraction", 0);
		values.put("pulse_bp", 0);
		db.insert("tbl_notification", null, values);
	}

	// 17Oct2016 Arpitha
	public void updatenotification(String wid, int i) {
		db = this.getWritableDatabase();
		String columnname = "";
		if (i == 1) {
			columnname = "fhr";
		} else if (i == 3) {
			columnname = "dilatation";
		} else if (i == 4) {
			columnname = "contraction";
		} else
			columnname = "pulse_bp";
		String sql = "Update tbl_notification set " + columnname + "=0 where women_id='" + wid + "'";
		db.execSQL(sql);
	}

	// 17Oct2016 Arpitha
	public void deletetblnotificationrow(String womenId, String userid) {
		// TODO Auto-generated method stub

		db = this.getWritableDatabase();
		String sql = "Delete from tbl_notification where women_id='" + womenId + "' and user_id='" + userid + "'";
		// db.rawQuery(sql, null);
		db.execSQL(sql);

	}

	// 19Oct2016 Arpitha
	/**
	 * Register the user - add one row in TBL_REGISTEREDWOMEN
	 * 
	 * @param wpojo
	 * @param transId
	 * @return true if record is inserted else false
	 * @throws Exception
	 */
	public boolean addRetrospectiveUser(Women_Profile_Pojo wpojo, int transId) throws Exception {
		boolean isRegistered = false;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(USER_ID, wpojo.getUserId());
		byte[] b = wpojo.getWomen_Image();
		String encodedImage = (b != null) ? Base64.encodeToString(b, Base64.DEFAULT) : "";
		values.put(WOMEN_IMAGE, encodedImage);
		values.put(WOMEN_NAME, wpojo.getWomen_name());
		// values.put(DATE_OF_ADM, wpojo.getDate_of_admission());
		// values.put(TIME_OF_ADM, wpojo.getTime_of_admission());
		values.put(W_AGE, wpojo.getAge());
		values.put(ADDRESS, wpojo.getAddress());
		values.put(PHONE_NUM, wpojo.getPhone_No());
		values.put(DOC_NAME, wpojo.getDoc_name());
		values.put(NURSE_NAME, wpojo.getNurse_name());
		values.put(ATTENDANT_NAME, wpojo.getW_attendant());
		values.put(GRAVIDA, wpojo.getGravida());
		values.put(PARA, wpojo.getPara());
		values.put(HOSP_NO, wpojo.getHosp_no());
		values.put(FACILITY, wpojo.getFacility());
		values.put(SPECIAL_INSTRUCTIONS, wpojo.getSpecial_inst());
		values.put(TRANS_ID, transId);
		values.put(WOMEN_ID, wpojo.getWomenId());
		// values.put(COMMENTS, wpojo.getComments());
		values.put(GESTATION_AGE, wpojo.getGestationage());
		values.put(COND_WHILE_ADMN, wpojo.getRisk_category());
		// values.put(ADMITTED_WITH, wpojo.getAdmitted_with());
		values.put(MEMBRANES_PRES_ABS, wpojo.getMemb_pres_abs());
		values.put(STATE, wpojo.getState());
		values.put(DISTRICT, wpojo.getDistrict());
		values.put(TALUK, wpojo.getTaluk());
		values.put(FACILITY_NAME, wpojo.getFacility_name());
		values.put(DATEINSERTED, wpojo.getDateinserted());
		values.put(W_MONTH, wpojo.getWmonth());
		values.put(W_YEAR, wpojo.getWyear());
		values.put(GESTATION_AGE_DAYS, wpojo.getGest_age_days());
		// 30Nov2015
		values.put(C_THAYICARDNUM, wpojo.getThayicardnumber());

		// 25dec2015
		values.put(C_LMP, wpojo.getLmp());
		// values.put(C_RISKOPTIONS, wpojo.getRiskoptions());
		values.put(C_BLOODGROUP, wpojo.getBloodgroup());

		// updated on 27Feb2016 by Arpitha

		values.put(C_EDD, wpojo.getEdd());
		// values.put(C_AdmittedWithOther, wpojo.getOther());
		values.put(C_Height, wpojo.getHeight());
		/* values.put(C_Weight, wpojo.getWeight()); */
		values.put(C_Weight, wpojo.getW_weight());
		// updated on 20May16 by Arpitha
		values.put(C_heightunit, wpojo.getHeight_unit());

		values.put(C_EXTRACOMMENTS, wpojo.getExtracomments());

		values.put(C_REGTYPE, wpojo.getregtype());
		values.put(C_REGTYPEREASON, wpojo.getRegtypereason());

		values.put(DATE_OF_REG_ENTRY, wpojo.getDate_of_reg_entry());// 10Nov2016

		values.put(C_REGTYPEOTHERREASON, wpojo.getRegtypeOtherReason());// 21Nov2016
																		// Arpitha

		if (wpojo.getDel_type() != 0) {
			values.put(DEL_STATUS, wpojo.getDel_type());
			values.put(C_DELTYPEREASON, wpojo.getDelTypeReason());// 19Oct2016
																	// ARpitha
			values.put(C_DELTYPEOTHERREASONS, wpojo.getDeltype_otherreasons());// 19Oct2016
																				// ARpitha
			values.put(NO_OF_CHILDREN, wpojo.getNo_of_child());// 19Oct2016
																// ARpitha
			values.put(DEL_TIME, wpojo.getDel_Time());// 19Oct2016 ARpitha
			values.put(DEL_DATE, wpojo.getDel_Date());// 19Oct2016 ARpitha
			values.put(DEL_RESULT1, wpojo.getDel_result1());// 19Oct2016 ARpitha
			values.put(BABYWT1, wpojo.getBabywt1());
			values.put(BABYSEX1, wpojo.getBabysex1());
			/*
			 * values.put(DEL_TIME2, wpojo.getDel_time2());
			 * values.put(DEL_RESULT2, wpojo.getDel_result2());
			 * values.put(BABYSEX2, wpojo.getBabysex2()); values.put(BABYWT2,
			 * wpojo.getBabywt2());
			 */// 01nov2016 Arpitha
			values.put(DEL_COMMENTS, wpojo.getDel_Comments());
			values.put(MOTHERSDEATH, wpojo.getMothersdeath());
			values.put(MOTHERS_DEATH_REASON, wpojo.getMothers_death_reason());
			values.put(C_TEARS, wpojo.getTears());
			values.put(C_EPISIOTOMY, wpojo.getEpisiotomy());
			values.put(C_SUTUREMATERIAL, wpojo.getSuturematerial());
			// values.put(DATE_LASTUPDATED, wpojo.getDatelastupdated());

			// values.put(C_NORMALTYPE, wpojo.getNormaltype());// 21Nov2016
			// Arpitha

			// 01nov2016 Arpitha
			if (wpojo.getNo_of_child() == 2)

			{
				values.put(DEL_TIME2, wpojo.getDel_time2());
				values.put(DEL_RESULT2, wpojo.getDel_result2());
				values.put(BABYSEX2, wpojo.getBabysex2());
				values.put(BABYWT2, wpojo.getBabywt2());
			} // 01nov2016 Arpitha

		}

		values.put(C_NORMALTYPE, wpojo.getNormaltype());// 05May2017 Arpitha -
														// v2.6 mantis
														// id-0000231

		long id = db.insert(TBL_REGISTEREDWOMEN, null, values);
		if (id > 0) {
			isRegistered = true;
		}
		return isRegistered;
	}

	// 20Oct2016 Arpitha
	public Cursor getretrospective(String userid) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " + DEL_STATUS + " is null and user_Id = '"
				+ userid
				+ "' and regtype==2 and women_id NOT IN(Select women_id from tbl_referral) order by dateinserted desc";
		cur = db.rawQuery(sql, null);
		return cur;
	}

	// 31Oct2016 Arpitha
	public String getobjectid_to_hide(String userid) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		String objectid = "";
		ArrayList<String> objid = new ArrayList<String>();
		String sql = "Select customfield1 from tbl_settings where user_id='" + userid + "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			objectid = cur.getString(0);
			objid.add(objectid);
		}

		return objectid;
	}

	public ArrayList<String> getentereddata(String womenid) {
		ArrayList<String> val = new ArrayList<String>();
		synchronized (Lock) {

			db = this.getWritableDatabase();
			String sql;
			// sql = "select distinct case when object_id <= 9 then 'yes' else
			// 'no'
			// end as partograph , 'partograph' from tbl_propertyValues where
			// women_id='"+womenid+"' and object_id<=9 union ALL select distinct
			// case when object_id = 10 then 'yes' else 'no' end as apgar
			// ,'apgar'
			// from tbl_propertyValues where women_id='"+womenid+"' and
			// object_id=10
			// union ALL select distinct case when object_id = 11 then 'yes'
			// else
			// 'no' end as third ,'thirdstage' from tbl_propertyValues where
			// women_id='"+womenid+"' and object_id=11 union ALL select distinct
			// case when object_id = 12 then 'yes' else 'no' end as fourth
			// ,'fourthstage' from tbl_propertyValues where
			// women_id='"+womenid+"'
			// and object_id=12 union all select case when del_status is null
			// then
			// 'No' else 'Yes' end as del, 'del' from tbl_registeredwomen where
			// women_id='"+womenid+"' union all Select case when count(*)>0 then
			// 'yes' else 'no' end as ref , 'ref' from tbl_referral where
			// women_id =
			// '"+womenid+"'";
			/*
			 * sql = "select distinct case when count(*)>0 then 'yes' else 'no'"
			 * + " end as partograph , 'partograph' from tbl_propertyValues " +
			 * "where women_id='" + womenid +
			 * "' and object_id <= 9  union ALL select distinct case when count(*)>0"
			 * + " then 'yes' else 'no' end as apgar ,'apgar' " +
			 * "from tbl_propertyValues where " + "women_id='" + womenid +
			 * "' and object_id = 10 union ALL " +
			 * "select distinct case when count(*) " +
			 * "then 'yes' else 'no' end as third ,'thirdstage' " +
			 * "from tbl_propertyValues where " + "women_id='" + womenid +
			 * "'  and object_id = 11 " + "union ALL select distinct case when "
			 * + " count(*)>0 then 'yes' else 'no'" +
			 * " end as fourth ,'fourthstage' " +
			 * "from tbl_propertyValues where " + "women_id='" + womenid +
			 * "'  and object_id = 12 " +
			 * "union all select case when count(*)>0 " +
			 * " then 'No' else 'Yes' end as del, 'del' " +
			 * "from tbl_registeredwomen where " + "women_id='" + womenid +
			 * "' and del_status is null union all " +
			 * "Select case when count(*)>0 " +
			 * "then 'yes' else 'no' end as ref , 'ref' " +
			 * "from tbl_referral where women_id = '" + womenid + "' UNION ALL "
			 * +
			 * "Select distinct case when count(*)>0 then 'Yes' else 'No' end as partograph, 'retrospective' from tbl_registeredwomen where women_id='"
			 * + womenid + "' and regtype=2";
			 */

			sql = "select distinct case when count(*)>0 then 'yes' else 'no'"
					+ " end as partograph , 'partograph' from tbl_propertyValues " + "where women_id='" + womenid
					+ "' and object_id <= 9  union ALL select distinct case when count(*)>0"
					+ " then 'yes' else 'no' end as apgar ,'apgar' " + "from tbl_propertyValues where " + "women_id='"
					+ womenid + "' and object_id = 10 union ALL " + "select distinct case when count(*) "
					+ "then 'yes' else 'no' end as third ,'thirdstage' " + "from tbl_propertyValues where "
					+ "women_id='" + womenid + "'  and object_id = 11 " + "union ALL select distinct case when "
					+ " count(*)>0 then 'yes' else 'no'" + " end as fourth ,'fourthstage' "
					+ "from tbl_propertyValues where " + "women_id='" + womenid + "'  and object_id = 12 "
					+ "union all select case when count(*)>0 " + " then 'No' else 'Yes' end as del, 'del' "
					+ "from tbl_registeredwomen where " + "women_id='" + womenid + "' and del_status is null union all "
					+ "Select case when count(*)>0 " + "then 'yes' else 'no' end as ref , 'ref' "
					+ "from tbl_referral where women_id = '" + womenid + "' UNION ALL "
					+ "Select distinct case when count(*)>0 then 'Yes' else 'No' end as partograph, 'retrospective' from tbl_registeredwomen where women_id='"
					+ womenid
					+ "' and regtype=2 UNION ALL Select distinct case when count(*)>0 then 'yes' else 'No' end as partograph, 'latent' from "
					+ " tbllatentphase where  latWomanId='" + womenid
					+ "' UNION ALL Select distinct case when count(*)>0 then 'yes' else 'No' end as partograph, 'postpartum' from "
					+ "tblpostpartumcare where  postWomanId='" + womenid
					+ "' UNION ALL Select distinct case when count(*)>0 then 'yes' else 'No' end as partograph, 'discharge' from "
					+ " tbldischargedetails where  discWomanId='" + womenid + "'";
			Cursor cur = db.rawQuery(sql, null);
			cur.moveToFirst();
			if (cur.getCount() > 0) {
				do {
					String value = cur.getString(0);
					val.add(value);
				} while (cur.moveToNext());
			}
		}
		return val;

	}

	// 27oct2016 - bindu
	public int getDisplayWomanListCount(String mainSql) throws Exception {
		db = this.getWritableDatabase();
		Cursor c = db.rawQuery(mainSql, null);
		return (c != null) ? c.getCount() : 0;
	}

	// 27oct2016 - bindu
	public ArrayList<Women_Profile_Pojo> getDisplayWomanList(String mainSql) throws Exception {

		db = this.getWritableDatabase();
		Women_Profile_Pojo wdata;
		ArrayList<Women_Profile_Pojo> rowItems = new ArrayList<Women_Profile_Pojo>();
		Cursor cursor = db.rawQuery(mainSql, null);
		if (cursor.getCount() > 0) {

			cursor.moveToFirst();

			do {
				wdata = new Women_Profile_Pojo();
				wdata.setWomenId(cursor.getString(1));

				if (cursor.getType(2) > 0) {
					String b = (cursor.getString(2).length() > 1) ? cursor.getString(2) : null;
					byte[] decoded = (b != null) ? Base64.decode(b, Base64.DEFAULT) : null;
					wdata.setWomen_Image(decoded);
				}

				wdata.setWomen_name(cursor.getString(3));
				wdata.setDate_of_admission(cursor.getString(4));
				wdata.setTime_of_admission(cursor.getString(5));
				wdata.setAge(cursor.getInt(6));
				wdata.setAddress(cursor.getString(7));
				wdata.setPhone_No(cursor.getString(8));
				wdata.setDel_type(cursor.getInt(9));
				wdata.setDoc_name(cursor.getString(10));
				wdata.setNurse_name(cursor.getString(11));
				wdata.setW_attendant(cursor.getString(12));
				wdata.setGravida(cursor.getInt(13));
				wdata.setPara(cursor.getInt(14));
				wdata.setHosp_no(cursor.getString(15));
				wdata.setFacility(cursor.getString(16));
				wdata.setWomenId(cursor.getString(1));
				wdata.setSpecial_inst(cursor.getString(17));
				wdata.setUserId(cursor.getString(0));
				wdata.setComments(cursor.getString(19));
				wdata.setRisk_category(cursor.getInt(20));
				wdata.setDel_Comments(cursor.getString(21));
				wdata.setDel_Time(cursor.getString(22));
				wdata.setDel_Date(cursor.getString(23));
				wdata.setDel_result1(cursor.getInt(24));
				wdata.setNo_of_child(cursor.getInt(25));
				wdata.setBabywt1(cursor.getInt(26));
				wdata.setBabywt2(cursor.getInt(27));
				wdata.setBabysex1(cursor.getInt(28));
				wdata.setBabysex2(cursor.getInt(29));
				wdata.setGestationage(cursor.getInt(30));
				wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
				wdata.setDel_result2(cursor.getInt(31));
				wdata.setAdmitted_with(cursor.getString(34));
				wdata.setMemb_pres_abs(cursor.getInt(35));
				wdata.setMothers_death_reason(cursor.getString(36));
				wdata.setState(cursor.getString(38));
				wdata.setDistrict(cursor.getString(39));
				wdata.setTaluk(cursor.getString(40));
				wdata.setFacility_name(cursor.getString(41));
				wdata.setGest_age_days(cursor.getInt(45));
				wdata.setDel_time2(cursor.getString(33));
				wdata.setThayicardnumber(cursor.getString(46));
				wdata.setDelTypeReason(cursor.getString(47));
				wdata.setDeltype_otherreasons(cursor.getString(48));
				wdata.setLmp(cursor.getString(49));
				wdata.setRiskoptions(cursor.getString(50));
				wdata.setTears(cursor.getString(51));
				wdata.setEpisiotomy(cursor.getInt(52));
				wdata.setSuturematerial(cursor.getInt(53));
				wdata.setBloodgroup(cursor.getInt(54));
				wdata.setEdd(cursor.getString(55));
				wdata.setHeight(cursor.getString(56));
				wdata.setW_weight(cursor.getString(57));
				wdata.setOther(cursor.getString(58));
				wdata.setHeight_unit(cursor.getInt(59));
				wdata.setExtracomments(cursor.getString(60));// 13Oct2016
				// Arpitha
				wdata.setregtype(cursor.getInt(61));// 16Oct2016 Arpitha

				wdata.setRegtypereason(cursor.getString(62));// 18Oct2016

				wdata.setDate_of_reg_entry(cursor.getString(63));// 10Nov2016
																	// Arpitha

				wdata.setNormaltype(cursor.getInt(64));// 20Nov2016 Arpitha

				wdata.setRegtypeOtherReason(cursor.getString(65));// 21Nov2016
																	// Arpitha
				// Arpitha
				boolean isDanger = chkIsDanger(cursor.getString(1), cursor.getString(0));
				wdata.setDanger(isDanger);

				rowItems.add(wdata);
			} while (cursor.moveToNext());
		}
		return rowItems;
	}

	// 27oct2016 - bindu - get all the current woman Ids for option
	public ArrayList<String> getWomenIdList(String userId, int option, String fromDate, String toDate)
			throws Exception {

		db = this.getWritableDatabase();
		ArrayList<String> womanidList = new ArrayList<String>();
		String strSql = "";
		switch (option) {
		case 1:
			strSql = "Select " + WOMEN_ID + " from TBL_REGISTEREDWOMEN  where DEL_STATUS"
					+ " is null  and regtype!=2 and user_Id = '" + userId
					+ "' and ((strftime('%s', datetime('now', 'localtime')) - strftime('%s', date_of_adm  || ' '  || time_of_adm)) / (60) )<1440 and women_id NOT IN(Select women_id from tbl_referral) order by dateinserted desc";
			break;
		case 2:
			strSql = "Select " + WOMEN_ID + " from " + TBL_REGISTEREDWOMEN + " where " + DEL_STATUS
					+ " is null and user_Id = '" + userId
					+ "' and ((strftime('%s', datetime('now', 'localtime')) - strftime('%s', date_of_adm  || ' '  || time_of_adm)) / (60) )>1440 and women_id NOT IN(Select women_id from tbl_referral) order by dateinserted desc";

			break;
		case 3:
			strSql = "Select " + WOMEN_ID + " from " + TBL_REGISTEREDWOMEN + " where " + DEL_STATUS
					+ " is null and user_Id = '" + userId
					+ "' and women_Id IN(Select women_id from tbl_referral order by dateofreferral || timeofreferral desc) ";

			break;
		case 4:
			strSql = "SELECT " + WOMEN_ID + " FROM " + TBL_REGISTEREDWOMEN + " WHERE " + USER_ID + "= '" + userId
					+ "'  and del_status is not null and  " + DEL_DATE + " between " + " '" + fromDate + "' and '"
					+ toDate + "' order by " + DEL_DATE + " || " + DEL_TIME + " desc";

			break;

		case 5:
			// strSql = "Select " + WOMEN_ID + " from " + TBL_REGISTEREDWOMEN +
			// " where " + DEL_STATUS
			// + " is null and user_Id = '" + userId
			// + "' and regtype==2 and women_id NOT IN(Select women_id from
			// tbl_referral) order by dateinserted desc";
			strSql = "Select " + WOMEN_ID + " from " + TBL_REGISTEREDWOMEN + " where  user_Id = '" + userId
					+ "' and regtype==2  order by dateinserted desc";// 11May2017
																		// Arpith
																		// -v2.6

			break;
		default:
			break;
		}

		Cursor c = db.rawQuery(strSql, null);
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			do {
				womanidList.add("'" + c.getString(0) + "'");
			} while (c.moveToNext());
		}
		return womanidList;
	}

	/**
	 * Get Data from tbl_propertyvalues for input objectid,women id and user id
	 * 
	 * @param obj_Id
	 * @param strWomenid
	 * @param strwuserId
	 * @return
	 */
	public ArrayList<Add_value_pojo> getAdditionalData(int obj_Id, String strWomenid, String strwuserId) {

		ArrayList<Add_value_pojo> arrAvp = null;
		arrAvp = new ArrayList<Add_value_pojo>();
		try {
			db = this.getWritableDatabase();

			/*
			 * String mainSql = "SELECT Distinct(" + C_TRANSID + ") FROM " +
			 * TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = " + obj_Id +
			 * " and " + WOMEN_ID + " = '" + strWomenid + "'" + "and " + USER_ID
			 * + "= '" + strwuserId + "' order by " + C_TRANSID + " desc";
			 * Cursor c1 = db.rawQuery(mainSql, null); if (c1.moveToFirst() &&
			 * c1.getCount() > 0) {
			 * 
			 * do {
			 */
			String propStr = "SELECT " + C_PROP_ID + "," + C_PROP_VALUE + ", " + DATE_OF_ENTRY + "," + TIME_OF_ENTRY
					+ " FROM " + TBL_PROPERTYVALUES + " WHERE " + C_OBJECTID + " = '" + obj_Id + "' and women_id='"
					+ strWomenid + "' and user_id='" + strwuserId + "'";
			Cursor c2 = db.rawQuery(propStr, null);
			if (c2.moveToFirst() && c2.getCount() > 0) {

				Add_value_pojo avp = new Add_value_pojo();
				do {

					if (c2.getString(0).equalsIgnoreCase("13.1")) {
						avp.setBreastFeeding(c2.getInt(1));
						// avp.setStrdate(c2.getString(2));
						// avp.setStrTime(c2.getString(3));
						avp.setBreastFeedingDate(c2.getString(2));
						avp.setBreastFeedingTime(c2.getString(3));
					} else if (c2.getString(0).equalsIgnoreCase("13.2")) {
						avp.setResonForNotFeeding(c2.getString(1));
						// avp.setStrdate(c2.getString(2));
						// avp.setStrTime(c2.getString(3));
						avp.setBreastFeedingDate(c2.getString(2));
						avp.setBreastFeedingTime(c2.getString(3));
					} else if (c2.getString(0).equalsIgnoreCase("13.3")) {
						avp.setAmbulanceRequired(c2.getInt(1));
						avp.setStrdate(c2.getString(2));
						avp.setStrTime(c2.getString(3));
					} else if (c2.getString(0).equalsIgnoreCase("13.4")) {
						avp.setAmbulanceAddress(c2.getString(1));
						avp.setStrdate(c2.getString(2));
						avp.setStrTime(c2.getString(3));
					}
				} while (c2.moveToNext());
				arrAvp.add(avp);
			}

			// } while (c1.moveToNext());

			// }

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return arrAvp;
	}

	// Insert data to tbl_propertyvalues
	public boolean insertAdditionalData(Property_pojo pdata, String strWomenid, String strwuserId, int transId) {

		boolean isInserted = false;
		try {
			db = this.getWritableDatabase();

			ArrayList<String> adata = pdata.getProp_value();

			for (int i = 0; i < adata.size(); i++) {
				ContentValues values = new ContentValues();
				values.put(C_TRANSID, transId);
				values.put(USER_ID, strwuserId);
				values.put(WOMEN_ID, strWomenid);
				values.put(C_OBJECTID, pdata.getObj_Id());
				values.put(C_PROP_ID, pdata.getProp_id().get(i));
				values.put(C_PROP_NAME, pdata.getProp_name().get(i));
				values.put(C_PROP_VALUE, adata.get(i));
				values.put(DATE_OF_ENTRY, pdata.getStrdate());

				values.put(TIME_OF_ENTRY, pdata.getStrTime());

				values.put(C_ISDANGER, pdata.getIsDangerval1());

				long id = db.insert(TBL_PROPERTYVALUES, null, values);
				if (id > 0)
					isInserted = true;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return isInserted;
	}

	// 08Dec2016 Arpitha - to get propertyids
	// Get the property ids from tbl property master
	public Cursor getAdditionalDetailPropertyIds(int obj_Id, int id) {

		Cursor cursor = null;
		String condition = " ";
		try {
			db = this.getWritableDatabase();
			if (id == 1)
				condition = " LIMIT 2";

			if (id == 2)
				condition = " order by prop_id desc LIMIT 2";

			String strSelect = " SELECT " + C_PROP_ID + "," + C_PROP_NAME + " FROM " + TBL_PROPERTYMASTER + " WHERE "
					+ C_OBJECTID + " = " + obj_Id + condition;
			cursor = db.rawQuery(strSelect, null);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return cursor;
	}

	// 09Dec2016 Arpitha - to get the summary of partograph data
	public ArrayList<String> getPartographData(String userid, int cases, String fromDate, String toDate,
			int noOfMonths) {

		ArrayList<String> val = new ArrayList<String>();
		String sql = null;
		// Normal Reg count
		if (cases == 1) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%d-%m','now')=strftime('%Y-%d-%m',date_of_Adm) and regtype!=2 and user_id='"
					+ userid + "'" + " UNION ALL"
					+ " Select count(*) from tbl_registeredwomen where date_of_adm between '" + fromDate + "' and '"
					+ toDate + "' and regtype!=2 and user_id='" + userid + "'" + " UNION ALL";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i
						+ " and regtype!=2 and user_id='" + userid + "' UNION ALL";

				if (i == noOfMonths) {
					sql = sql
							+ " Select count(*) from tbl_registeredwomen where strftime('%Y','now') = strftime('%Y',date_of_Adm) and regtype!=2 and user_id='"
							+ userid + "'";
				}
			}
		}

		/*
		 * sql =
		 * "Select count(*) from tbl_registeredwomen where strftime('%d','now')=strftime('%d',date_of_Adm) and regtype!=2"
		 * + " UNION ALL" +
		 * "  Select count(*) from tbl_registeredwomen where date_of_adm between '"
		 * + fromDate + "' and '" + toDate + "' and regtype!=2" + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where  strftime('%m','now')=strftime('%m',date_of_adm) and strftime('%Y','now')=strftime('%Y', date_of_Adm) and regtype!=2"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where  strftime('%Y','now')=strftime('%Y', date_of_Adm) and regtype!=2"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where regtype!=2";
		 */
		// REtrospective Reg count
		else if (cases == 2) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%d-%m','now')=strftime('%Y-%d-%m',substr(date_of_reg_entry, 1, 10)) and regtype==2 and user_id='"
					+ userid + "'" + " UNION ALL"
					+ " Select count(*) from tbl_registeredwomen where substr(date_of_reg_entry, 1, 10) between '"
					+ fromDate + "' and '" + toDate + "' and regtype==2 and user_id='" + userid + "'" + " UNION ALL";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i
						+ " and regtype==2 and user_id='" + userid + "' UNION ALL";

				if (i == noOfMonths) {
					sql = sql
							+ " Select count(*) from tbl_registeredwomen where strftime('%Y','now') = strftime('%Y',substr(date_of_reg_entry, 1, 10)) and regtype==2 and user_id='"
							+ userid + "'";
				}
			}

		}

		// 01Sep2017 Arpitha - High risk count
		if (cases == 3) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%d-%m','now')=strftime('%Y-%d-%m',date_of_Adm) and "
					+ COND_WHILE_ADMN + "=1 and user_id='" + userid + "'" + " UNION ALL"
					+ " Select count(*) from tbl_registeredwomen where date_of_adm between '" + fromDate + "' and '"
					+ toDate + "' and " + COND_WHILE_ADMN + "=1 and user_id='" + userid + "'" + " UNION ALL";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i + " and " + COND_WHILE_ADMN
						+ "=1 and user_id='" + userid + "' UNION ALL";

				if (i == noOfMonths) {
					sql = sql
							+ " Select count(*) from tbl_registeredwomen where strftime('%Y','now') = strftime('%Y',date_of_Adm) and "
							+ COND_WHILE_ADMN + "=1 and user_id='" + userid + "'";
				}
			}
		}

		// 01Sep2017 Arpitha - breech count
		if (cases == 4) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',substr(date_of_reg_entry, 1, 10)) and "
					+ C_NORMALTYPE + "=2 and user_id='" + userid + "'" + " UNION ALL"
					+ " Select count(*) from tbl_registeredwomen where substr(date_of_reg_entry, 1, 10) between '"
					+ fromDate + "' and '" + toDate + "' and " + C_NORMALTYPE + "=2 and user_id='" + userid + "'"
					+ " UNION ALL";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i + " and " + C_NORMALTYPE
						+ "=2 and user_id='" + userid + "' UNION ALL";

				if (i == noOfMonths) {
					sql = sql
							+ " Select count(*) from tbl_registeredwomen where strftime('%Y','now') = strftime('%Y',substr(date_of_reg_entry, 1, 10)) and "
							+ C_NORMALTYPE + "=2 and user_id='" + userid + "'";
				}
			}
		}

		// 01Sep2017 Arpitha - latentphase count
		else if (cases == 5) {

			sql = "Select count(distinct latwomanid) from " + TBL_LATENTPHASE
					+ " where strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',substr(latdateofentry, 1, 10)) and "
					+ "latuserid='" + userid + "'" + " UNION ALL" + " Select count(*) from " + TBL_LATENTPHASE
					+ " where substr(latdateofentry, 1, 10) between '" + fromDate + "' and '" + toDate
					+ "' and latuserid='" + userid + "'" + " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "latdateofentry)='0";
				else
					month = "substr(latdateofentry, 1, 10))='";// 09Oct2017 Arpitha

				sql = sql + " Select count(*) from " + TBL_LATENTPHASE + " where   strftime('%m'," + month + i
						+ "' and " + "latuserid='" + userid + "'  UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(*) from " + TBL_LATENTPHASE
							+ " where strftime('%Y','now') = strftime('%Y',substr(latdateofentry, 1, 10)) "
							+ " and latuserid = '" + userid + "'";

				}
			}

		} // 01Sep2017 Arpitha - partograph entry count
		else if (cases == 6) {
			sql = "Select count(distinct women_id) from tbl_propertyValues where object_id<=9 and strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',date_of_entry) and user_id='"
					+ userid
					+ "' UNION ALL Select count(distinct women_id) from tbl_propertyValues where object_id<=9 and user_id='"
					+ userid + "' and date_of_entry between '" + fromDate + "' and '" + toDate + "' UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "date_of_entry)=='0";
				else
					month = "date_of_entry)=='";// 09Oct2017 Arpitha

				sql = sql + " Select count(distinct women_id) from tbl_propertyValues where object_id<=9 and user_id='"
						+ userid + "' and strftime('%m'," + month + i + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(distinct women_id) from tbl_propertyValues where user_id='" + userid
							+ "' and strftime('%Y',date_of_entry)=strftime('%Y', 'now') and object_id<=9 ";
				}
			}

		}

		/*
		 * sql =
		 * "Select count(*) from tbl_registeredwomen where strftime('%d','now')=strftime('%d',substr(date_of_reg_entry, 1, 10)) and regtype==2"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where substr(date_of_reg_entry, 1, 10) between '"
		 * + fromDate + "' and '" + toDate + "' and regtype==2" + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where  strftime('%m','now')=strftime('%m',substr(date_of_reg_entry, 1, 10)) and strftime('%Y','now')=strftime('%Y', substr(date_of_reg_entry, 1, 10)) and regtype==2"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where  strftime('%Y','now')=strftime('%Y', substr(date_of_reg_entry, 1, 10)) and regtype==2"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where regtype==2";
		 */

		/*
		 * else if (cases == 3) sql =
		 * "Select count(*) from tbl_registeredwomen where strftime('%d','now')=strftime('%d',del_date) and del_status is NOT NULL"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where del_date between '"
		 * + fromDate + "' and '" + toDate + "' and del_status is NOT NULL" +
		 * " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where  strftime('%m','now')=strftime('%m',del_date) and strftime('%Y','now')=strftime('%Y', del_date) and del_status is NOT NULL"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where  strftime('%Y','now')=strftime('%Y', del_date) and del_status is NOT NULL"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_registeredwomen where del_status is NOT NULL"
		 * ;
		 */
		// Deliveries count
		else if (cases == 7) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',del_date) and del_status is NOT NULL and user_id='"
					+ userid + "'" + " UNION ALL Select count(*) from tbl_registeredwomen where date_of_adm between '"
					+ fromDate + "' and ' " + toDate + "' and del_status is NOT NULL and user_id='" + userid + "' "
					+ " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i
						+ " and del_status is NOT NULL and user_id='" + userid + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql
							+ "Select count(*) from tbl_registeredwomen where del_status is NOT NULL and strftime('%Y','now') = strftime('%Y',del_date) and user_id='"
							+ userid + "'";
				}
			}

		}

		// Normal Deliveries count
		else if (cases == 8) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',del_date) and del_status = 1 and user_id='"
					+ userid + "'" + " UNION ALL Select count(*) from tbl_registeredwomen where date_of_adm between '"
					+ fromDate + "' and ' " + toDate + "' and del_status = 1 and user_id='" + userid + "' "
					+ " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i
						+ " and del_status =1 and user_id='" + userid + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql
							+ "Select count(*) from tbl_registeredwomen where del_status =1 and strftime('%Y','now') = strftime('%Y',del_date) and user_id='"
							+ userid + "'";
				}
			}

		}

		// Cesarean Deliveries count
		else if (cases == 9) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',del_date) and del_status = 2 and user_id='"
					+ userid + "'" + " UNION ALL Select count(*) from tbl_registeredwomen where date_of_adm between '"
					+ fromDate + "' and ' " + toDate + "' and del_status = 2 and user_id='" + userid + "' "
					+ " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i
						+ " and del_status =2 and user_id='" + userid + "' UNION ALL ";

				if (i == noOfMonths) {

					sql = sql
							+ "Select count(*) from tbl_registeredwomen where del_status =2 and strftime('%Y','now') = strftime('%Y',del_date) and user_id='"
							+ userid + "'";
				}
			}

		}

		// Instrumental Deliveries count
		else if (cases == 10) {

			sql = "Select count(*) from tbl_registeredwomen where strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',del_date) and del_status = 3 and user_id='"
					+ userid + "'" + " UNION ALL Select count(*) from tbl_registeredwomen where date_of_adm between '"
					+ fromDate + "' and ' " + toDate + "' and del_status = 3 and user_id='" + userid + "' "
					+ " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				sql = sql + " Select count(*) from tbl_registeredwomen where  wmonth=" + i
						+ " and del_status =3 and user_id='" + userid + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql
							+ "Select count(*) from tbl_registeredwomen where del_status =3 and strftime('%Y','now') = strftime('%Y',del_date) and user_id='"
							+ userid + "'";
				}
			}

		}

		// 01Sep2017 Arpitha - 3rd stages of labor entry count
		else if (cases == 11) {
			sql = "Select count(distinct women_id) from tbl_propertyValues where object_id='11'  and strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',date_of_entry) and user_id='"
					+ userid
					+ "' UNION ALL Select count(distinct women_id) from tbl_propertyValues where object_id='11'  and user_id='"
					+ userid + "' and date_of_entry between '" + fromDate + "' and '" + toDate + "' UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "date_of_entry)=='0";
				else
					month = "date_of_entry)=='";// 09Oct2017 Arpitha

				sql = sql
						+ " Select count(distinct women_id) from tbl_propertyValues where object_id='11'  and user_id='"
						+ userid + "' and strftime('%m'," + month + i + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(distinct women_id) from tbl_propertyValues where user_id='" + userid
							+ "' and strftime('%Y',date_of_entry)=strftime('%Y', 'now') and object_id='11' ";
				}
			}

		}

		// 01Sep2017 Arpitha - 4th stages of labor entry count
		else if (cases == 12) {
			sql = "Select count(distinct women_id) from tbl_propertyValues where  object_id='12' and strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',date_of_entry) and user_id='"
					+ userid
					+ "' UNION ALL Select count(distinct women_id) from tbl_propertyValues where  object_id='12' and user_id='"
					+ userid + "' and date_of_entry between '" + fromDate + "' and '" + toDate + "' UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "date_of_entry)=='0";
				else
					month = "date_of_entry)=='";// 09Oct2017 Arpitha

				sql = sql
						+ " Select count(distinct women_id) from tbl_propertyValues where object_id='12' and user_id='"
						+ userid + "' and strftime('%m'," + month + i + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(distinct women_id) from tbl_propertyValues where user_id='" + userid
							+ "' and strftime('%Y',date_of_entry)=strftime('%Y', 'now') and  object_id='12'";
				}
			}

		}

		// 01Sep2017 Arpitha - apgar entry count
		else if (cases == 13) {
			sql = "Select count(distinct women_id) from tbl_propertyValues where object_id='10' and strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',date_of_entry) and user_id='"
					+ userid
					+ "' UNION ALL Select count(distinct women_id) from tbl_propertyValues where object_id='10' and user_id='"
					+ userid + "' and date_of_entry between '" + fromDate + "' and '" + toDate + "' UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "date_of_entry)=='0";
				else
					month = "date_of_entry)=='";// 09Oct2017 Arpitha

				sql = sql
						+ " Select count(distinct women_id) from tbl_propertyValues where object_id='10' and user_id='"
						+ userid + "' and strftime('%m'," + month + i + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(distinct women_id) from tbl_propertyValues where user_id='" + userid
							+ "' and strftime('%Y',date_of_entry)=strftime('%Y', 'now') and object_id='10'";
				}
			}

		}

		// 01Sep2017 Arpitha - postpartum entry count
		else if (cases == 14) {

			sql = "Select count(distinct postwomanid) from " + TBL_POSTPARTUM
					+ " where strftime('%Y-%d-%m','now')=strftime('%Y-%d-%m',postdatetimeofentry) and " + "postuserid='"
					+ userid + "'" + " UNION ALL" + " Select count(*) from " + TBL_POSTPARTUM
					+ " where postdatetimeofentry between '" + fromDate + "' and '" + toDate + "' and postuserid='"
					+ userid + "'" + " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {
				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "postdatetimeofentry)='0";
				else
					month = "postdatetimeofentry)='";// 09Oct2017 Arpitha

				sql = sql + " Select count(distinct postwomanid) from " + TBL_POSTPARTUM + " where   strftime('%m',"
						+ month + i + "' and " + "postuserid='" + userid + "'  UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(distinct postwomanid) from " + TBL_POSTPARTUM
							+ " where strftime('%Y','now') = strftime('%Y',postdatetimeofentry) "
							+ " and postuserid = '" + userid + "'";

				}
			}

		}

		else if (cases == 15) {

			sql = "Select count(*) from tbl_referral where strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',dateofreferral) and user_id='"
					+ userid + "'" + " UNION ALL" + " Select count(*) from tbl_referral where dateofreferral between '"
					+ fromDate + "' and '" + toDate + "' and user_id='" + userid + "'" + " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "dateofreferral)=='0";
				else
					month = "dateofreferral)=='";// 09Oct2017 Arpitha

				sql = sql + " Select count(*) from tbl_referral where strftime('%m'," + month + i + "' and user_id='"
						+ userid + "'" + " UNION ALL ";

				if (i == noOfMonths) {
					sql = sql
							+ "Select count(*) from tbl_referral where strftime('%Y','now')=strftime('%Y',dateofreferral) and user_id='"
							+ userid + "'";
				}
			}

		}

		/*
		 * sql =
		 * "Select count(*) from tbl_referral where strftime('%d','now')=strftime('%d',dateofreferral)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_referral where dateofreferral between '" +
		 * fromDate + "' and '" + toDate + "'" + " UNION ALL" +
		 * " Select count(*) from tbl_referral where strftime('%m','now')=strftime('%m',dateofreferral)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_referral where strftime('%Y','now')=strftime('%Y',dateofreferral)"
		 * ;
		 */

		else if (cases == 16) {
			sql = "Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 and strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',date_of_entry) and user_id='"
					+ userid
					+ "' UNION ALL Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 and user_id='"
					+ userid + "' and date_of_entry between '" + fromDate + "' and '" + toDate + "' UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "date_of_entry)=='0";
				else
					month = "date_of_entry)=='";// 09Oct2017 Arpitha

				sql = sql
						+ " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 and user_id='"
						+ userid + "' and strftime('%m'," + month + i + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(*) from tbl_propertyValues where user_id='" + userid
							+ "' and strftime('%Y',date_of_entry)=strftime('%Y', 'now') and object_id='13' and prop_id='13.3' and prop_value=1";
				}
			}

		}
		/*
		 * sql =
		 * "Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 and strftime('%d','now')=strftime('%d',date_of_entry)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 and date_of_entry between '"
		 * + fromDate + "' and '" + toDate + "'" + " UNION ALL"object_id=13 +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 and strftime('%m','now')=strftime('%m',date_of_entry) and strftime('%Y','now')=strftime('%Y',date_of_entry)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 and strftime('%Y','now')=strftime('%Y',date_of_entry)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.3' and prop_value=1 "
		 * ;
		 */
		else if (cases == 17) {
			sql = "Select count(*) from tbl_propertyValues where object_id='12' and prop_id='12.8' and prop_value=2 and strftime('%Y-%m-%d','now')=strftime('%Y-%m-%d',date_of_entry) and user_id='"
					+ userid
					+ "' UNION ALL Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.1' and prop_value=2 and user_id='"
					+ userid + "' and date_of_entry between '" + fromDate + "' and '" + toDate + "' UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "date_of_entry)=='0";
				else
					month = "date_of_entry)=='";// 09Oct2017 Arpitha

				sql = sql
						+ " Select count(*) from tbl_propertyValues where object_id='12' and prop_id='12.8' and prop_value=2 and user_id='"
						+ userid + "' and strftime('%m'," + month + i + "' UNION ALL ";

				if (i == noOfMonths) {
					sql = sql + "Select count(*) from tbl_propertyValues where user_id='" + userid
							+ "' and strftime('%Y',date_of_entry)=strftime('%Y', 'now') and object_id='12' and prop_id='12.8' and prop_value=2";
				}
			}

		}
		// 01Sep2017 Arpitha - discharge count
		else if (cases == 18) {

			sql = "Select count(*) from tbldischargedetails where strftime('%Y-%d-%m','now')=strftime('%Y-%d-%m',discdatetimeofdischarge) and "
					+ "discuserid='" + userid + "'" + " UNION ALL"
					+ " Select count(*) from tbldischargedetails where discdatetimeofdischarge between '" + fromDate
					+ "' and '" + toDate + "' and discuserid='" + userid + "'" + " UNION ALL ";

			for (int i = 1; i <= noOfMonths; i++) {

				// 09Oct2017 Arpitha
				String month = "";
				if (i <= 9)
					month = "discdatetimeofdischarge)='0";
				else
					month = "discdatetimeofdischarge)='";// 09Oct2017 Arpitha

				sql = sql + " Select count(*) from tbldischargedetails where   strftime('%m'," + month + i + "' and "
						+ "discuserid='" + userid + "'  UNION ALL ";

				if (i == noOfMonths) {
					sql = sql
							+ "Select count(*) from tbldischargedetails where strftime('%Y','now') = strftime('%Y',discdatetimeofdischarge) "
							+ " and discuserid = '" + userid + "'";

				}
			}

		}
		/*
		 * sql =
		 * "Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.1' and prop_value=2 and strftime('%d','now')=strftime('%d',date_of_entry)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.1' and prop_value=2 and date_of_entry between '"
		 * + fromDate + "' and '" + toDate + "'" + " UNION ALL" +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.1' and prop_value=2 and strftime('%m','now')=strftime('%m',date_of_entry) and strftime('%Y','now')=strftime('%Y',date_of_entry)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.1' and prop_value=2 and strftime('%Y','now')=strftime('%Y',date_of_entry)"
		 * + " UNION ALL" +
		 * " Select count(*) from tbl_propertyValues where object_id='13' and prop_id='13.1' and prop_value=2 "
		 * ;
		 */
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {

			do {
				val.add(cur.getString(0));

			} while (cur.moveToNext());

		}
		return val;
	}

	// 23Dec2016 Arpitha - to update new password
	public String updateNewPassword(String pass, String userid, int transId) throws Exception {
		db = this.getWritableDatabase();

		// 28March2017 Arpitha

		ArrayList<AuditTrailPojo> auditArr = new ArrayList<AuditTrailPojo>();
		String appendStr = "";

		/*
		 * byte[] oldB = oldWObj.getWomen_Image(); String oldEncodedImage =
		 * (oldB != null) ? Base64.encodeToString(oldB, Base64.DEFAULT) : "";
		 * 
		 * byte[] b1 = wpojo.getWomen_Image(); String encodedImage1 = (b1 !=
		 * null) ? Base64.encodeToString(b1, Base64.DEFAULT) : "";
		 */

		String oldPassword = Partograph_CommonClass.user.getPwd();

		String newPassword = pass;

		if (!oldPassword.equals(newPassword)) {
			if (appendStr != "")
				appendStr = appendStr + ",";
			appendStr = appendStr + PASSWORD + " = " + (char) 39 + newPassword + (char) 39 + "";
			auditArr.add(new AuditTrailPojo(userid, "", TBL_USER, PASSWORD,
					Partograph_CommonClass.getCurrentDateandTime(), oldPassword, newPassword, transId));
		}

		String sql = null;
		if (appendStr.length() > 0) {
			sql = "Update tbl_user set password='" + pass + "'  where user_id='" + userid + "'";

			// String str = "Update tbl_user set password='" + pass + "' where
			// user_id='" + userid + "'";
			db.execSQL(sql);

			iUpdateRecordTrans(userid, transId, TBL_USER, sql, null, null);

			for (AuditTrailPojo ap : auditArr) {
				if (!insertTotblAuditTrail(ap))
					return null;
			}

			iNewRecordTrans(userid, transId, "tblAuditTrail");

		}

		// iUpdateRecordTrans(userid, transId, TBL_USER, str, null, null);
		return sql;

	}

	public ArrayList<String> getObjects(String userid, String womenId) {
		db = this.getWritableDatabase();
		ArrayList<String> objects = new ArrayList<String>();
		String sql = "Select * from tbl_notification where women_id='" + womenId + "' and user_id='" + userid + "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			// do {
			objects.add(cur.getString(2));
			objects.add(cur.getString(3));
			objects.add(cur.getString(4));
			objects.add(cur.getString(5));
			// } while (cur.moveToNext());

		}
		return objects;
	}

	// 17Jan2017 Arpitha
	public ArrayList<String> getNotificationData(String womenid, String userid) {
		// TODO Auto-generated method stub
		ArrayList<String> val = new ArrayList<String>();
		String sql = "Select * from tbl_notification where women_id='" + womenid + "' and user_id='" + userid + "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			val.add(cur.getString(2));
			val.add(cur.getString(3));
			val.add(cur.getString(4));
			val.add(cur.getString(5));
		}
		return val;
	}

	// 20Jan2017 Arpitha
	public ArrayList<String> getYears() {
		db = this.getWritableDatabase();
		ArrayList<String> years = new ArrayList<String>();
		String sql = "Select distinct " + W_YEAR + " from " + TBL_REGISTEREDWOMEN + " order by wyear desc";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				years.add(cur.getString(0));
			} while (cur.moveToNext());
		}
		return years;
	}

	// 20Jan2017 Arpitha
	public ArrayList<String> getPreviousYearData(String userId, String year, int id) {
		db = this.getWritableDatabase();
		ArrayList<String> data = new ArrayList<String>();
		String tablename = null, condition = null;
		int i = 0;
		String sql = "";

		if (id == 1 || id == 2 || id == 3 || id == 4 || id == 7 || id == 8 || id == 9 || id == 10) {
			tablename = TBL_REGISTEREDWOMEN;
			if (id == 1)
				condition = " and wyear = '" + year + "'  and regtype!=2";
			else if (id == 2)
				condition = " and wyear = '" + year + "' and regtype==2";
			else if (id == 3)
				condition = " and wyear = '" + year + "'  and cond_while_admn = 1";
			else if (id == 4)
				condition = " and wyear = '" + year + "'  and normaltype = 2 ";
			else if (id == 7)
				condition = " and wyear = '" + year + "' and del_status =1 ";
			else if (id == 9)
				condition = " and wyear = '" + year + "' and del_status =2 ";
			else if (id == 10)
				condition = " and wyear = '" + year + "' and del_status =3 ";
			else
				condition = " and wyear = '" + year + "'  and del_status is NOT NULL";

		} else if (id == 15) {
			tablename = TBL_REFERRAL;
			condition = " and strftime('%Y',dateofreferral) =='" + year + "'";
		} else if (id == 6 || id == 11 || id == 12 || id == 13 || id == 16 || id == 17) {
			tablename = TBL_PROPERTYVALUES;
			condition = " and strftime('%Y',date_of_entry) == '" + year + "'  and ";
			if (id == 16)
				condition = condition + " object_id=13 and prop_id= '13.3' and prop_value=1";
			else if (id == 17)
				condition = condition + " object_id=12 and prop_id= '12.8' and prop_value=2";
			else if (id == 6)
				condition = condition + " object_id<=9";
			else if (id == 11)
				condition = condition + " object_id=11";
			else if (id == 12)
				condition = condition + " object_id=12";
			else if (id == 13)
				condition = condition + " object_id=10";

		} else if (id == 5) {
			tablename = TBL_LATENTPHASE;
			condition = " and strftime('%Y',substr(" + LAT_DATETIME + ", 1, 10)) =='" + year + "'";
		} else if (id == 14) {
			tablename = TBL_POSTPARTUM;
			condition = " and strftime('%Y',substr(" + POST_DATETIMEOFENTRY + ", 1, 10)) =='" + year + "'";
		}

		else if (id == 18) {
			tablename = TBLdischargedetails;
			condition = " and strftime('%Y',substr(" + DISC_discDateOfDischarge + ", 1, 10)) =='" + year + "'";
		}

		for (i = 1; i <= 12; i++) {

			if (id == 1 || id == 2 || id == 3 || id == 4 || id == 7 || id == 8 || id == 9 || id == 10)
				sql = sql + " Select count(*) from " + tablename + " where user_id ='" + userId + "' and wmonth =" + i
						+ condition;
			else if (id == 15)
				sql = sql + " Select count(*) from " + tablename + " where user_id ='" + userId
						+ "' and strftime('%m',dateofreferral)=='0" + i + "' " + condition;
			else if (id == 6 || id == 11 || id == 12 || id == 13 || id == 16 || id == 17)
				sql = sql + " Select count(distinct women_id) from " + tablename + " where user_id ='" + userId
						+ "' and strftime('%m',date_of_entry)== '0" + i + "' " + condition;

			else if (id == 5)
				sql = sql + " Select count(distinct " + LAT_WOMANID + ") from " + tablename + " where " + LAT_USERID
						+ " ='" + userId + "' and strftime('%m',substr(latdateOfEntry, 1, 10))== '0" + i + "' "
						+ condition;

			else if (id == 14)
				sql = sql + " Select count(distinct " + POST_WOMANID + ") from " + tablename + " where " + POST_USERID
						+ " ='" + userId + "' and strftime('%m',substr(" + POST_DATETIMEOFENTRY + ", 1, 10))== '0" + i
						+ "' " + condition;

			else if (id == 18)
				sql = sql + " Select count(*) from " + tablename + " where " + DISC_discUserId + " ='" + userId
						+ "' and strftime('%m',substr(" + DISC_discDateOfDischarge + ", 1, 10))== '0" + i + "' "
						+ condition;

			if (i != 12)
				sql = sql + " UNION ALL";

		}
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				data.add(cur.getString(0));
			} while (cur.moveToNext());

		}
		return data;

	}

	// 01Feb2017 Arpitha
	public int getYearlyRegCount(String year, String user_id) {
		db = this.getWritableDatabase();
		int regCount = 0;
		String sql = "Select count(*) from " + TBL_REGISTEREDWOMEN + " where " + W_YEAR + " = '" + year + "' and "
				+ USER_ID + "='" + user_id + "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			regCount = cur.getInt(0);
		}
		return regCount;

	}

	// Creating tables and inserting data - 03April2017 Arpitha
	public void updateTablesandData1(String filename, SQLiteDatabase db) {
		int count = 0;
		try {
			db = this.getWritableDatabase();
			InputStream in = context.getResources().getAssets().open(filename);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			while (true) {
				int read = in.read(buffer);
				if (read == -1) {
					break;
				}
				out.write(buffer, 0, read);
			}

			byte[] bytes = out.toByteArray();
			String sql = new String(bytes);
			String[] lines = sql.split(";(\\s)*[\n\r]");
			if (true) {
				db.beginTransaction();
				int cnt = 0;
				for (String line : lines) {
					line = line.trim();
					if (line.length() > 0) {
						cnt++;
						db.execSQL(line);
						count++;
					}
				}
				db.setTransactionSuccessful();
			}
			db.endTransaction();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Get max(date_inserted) from tbl_ColorCoded Values - 03May2017 Arpitha
	public String getColoCodedMaxdate() {
		String maxDate = " ";
		db = this.getWritableDatabase();
		try {
			String strSql = "Select max(dateinserted) from tbl_colorcodedvalues ";
			Cursor cursor = db.rawQuery(strSql, null);

			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				maxDate = cursor.getString(0);
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return maxDate;
	}

	// 03May2017 Arpitha
	public void insertColorCodedValues(ContentValues values) {
		db = this.getWritableDatabase();
		try {

			db.insertOrThrow("tbl_ColorCodedValues", null, values);
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// 04May2017 Arpitha
	public Cursor getColorCodedValuesFromTable(String prop_id) {

		Cursor cur = null;
		// int returnval = 0;
		try {

			db = this.getWritableDatabase();
			String sql = "Select * from tbl_ColorCodedValues where isDefault='1' and prop_id='" + prop_id + "'";
			cur = db.rawQuery(sql, null);
			// if (cur.getCount() > 0) {
			// cur.moveToFirst();

			// Partograph_CommonClass.FHR_RED = cur.getString(4);
			// Partograph_CommonClass.FHR_AMBER = cur.getString(5);
			// Partograph_CommonClass.FHR_GREEN = cur.getString(6);

			// Integer.parseInt(
			// Partograph_CommonClass.FHR_RED.split("(or) |
			// (and)")[0].split("(>=)|(<=)|(<)|(>)")[1].trim())
			//
			// String value = Partograph_CommonClass.FHR_RED;
			// String[] spitAndOr = value.split("(or)|(and)");
			// String[] splitgtlt = spitAndOr[0].split("(>=)|(<=)|(=)|(<)|(>)");
			// if(val splitgtlt[0].charAt(0) Integer.parseInt(splitgtlt[1]) )

			// Partograph_CommonClass.FHR_RED =
			// Partograph_CommonClass.FHR_RED.replace("<", val+"<");
			// Partograph_CommonClass.FHR_RED =
			// Partograph_CommonClass.FHR_RED.replace(">=", val+">=");
			// // Partograph_CommonClass.FHR_RED =
			// Partograph_CommonClass.FHR_RED.replace("<", val+"<");
			// if(Partograph_CommonClass.FHR_RED)
			// {
			// return 1;
			//
			// }

			// }
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return cur;

	}

	//// 04May2017 Arpitha
	// public void getColorCodedValuesFromTable()
	// {
	// String var="";
	// String parameter = "Partograph_CommonClass."+var;
	// try{
	//
	// db = this.getWritableDatabase();
	// String sql = "Select * from tbl_ColorCodedValues where isDefault='1'";
	// Cursor cur = db.rawQuery(sql, null);
	// if(cur.getCount()>0)
	// {
	// cur.moveToFirst();
	// do{
	// var = cur.getString(3);
	// for(int i=4;i<=6;i++)
	// {
	// if(i==4)
	// parameter = parameter+var+"_RED";
	// else if(i==5)
	// parameter = parameter+var+"_AMBER";
	// else
	// parameter = parameter+var+"_GREEN";
	//
	// parameter = cur.getString(i);
	// }
	//// parameter = cur.getString(4);
	//// Partograph_CommonClass.FHR_AMBER = cur.getString(5);
	//// Partograph_CommonClass.FHR_GREEN = cur.getString(6);
	// }while(cur.moveToNext());
	// }
	// }catch(Exception e)
	// {
	// AppContext.addLog(
	// new RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	//// this.getClass().getSimpleName(),
	// e);
	// e.printStackTrace();
	// }
	// //return FHR_Red;
	//
	// }
	// 12MAy2017 Arpitha - v2.6
	public ArrayList<String> getAllIds() {
		ArrayList<String> propid = new ArrayList<String>();
		String sql = "Select * from tbl_ColorCodedValues where  isDefault='1' ";
		Cursor cur = db.rawQuery(sql, null);
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				propid.add(cur.getString(0));
			} while (cur.moveToNext());

		}
		return propid;
	}

	// 02Jun2017 Arpitha
	public Cursor getRegisteredWomenList(String userid, int displaylistcount) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " + DEL_STATUS
				+ " is null  and regtype!=2 and user_Id = '" + userid
				+ "' and  women_id NOT IN(Select women_id from tbl_referral) and women_id NOT IN(Select women_id from tbl_propertyValues where object_id<=9) and women_id NOT IN (Select discWomanId from tbldischargedetails) order by dateinserted desc limit "
				+ displaylistcount;

		cur = db.rawQuery(sql, null);
		return cur;
	}

	// 06Jun2017 Arpitha
	public Cursor getSearchResult(String userId, String strsearch) {
		db = this.getWritableDatabase();
		String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where " + WOMEN_NAME + " LIKE '%" + strsearch + "%' or "
				+ PHONE_NUM + " LIKE '%" + strsearch + "%' and " + USER_ID + " ='" + userId + "' ";
		Cursor cur = db.rawQuery(sql, null);

		return cur;
	}

	// 09Jun2017 Arpitha
	public Cursor getReferredWomen(String userId, int status) {
		Cursor cur = null;
		String condition;
		if (status == 1)
			condition = " (del_status is null or strftime('%s',reg.del_date|| ' '||reg.del_time) > strftime('%s',ref.dateofreferral || ' '||ref.timeofreferral)) and reg.women_id NOT IN (Select discWomanId from tbldischargedetails) and";

		else if (status == 2)
			condition = " ( strftime('%s',reg.del_date|| ' '||reg.del_time)<= strftime('%s',ref.dateofreferral || ' '||ref.timeofreferral)) and reg.women_id NOT IN (Select discWomanId from tbldischargedetails) and";
		else
			condition = "  reg.women_id NOT IN (Select discWomanId from tbldischargedetails) and";

		String sql = " Select distinct reg.user_id, reg.women_id ,reg.women_image,  reg.women_name , reg.date_of_adm, reg.time_of_adm, reg.w_age,reg.address,reg.phone_num,"
				+ " reg.del_status, reg.doc_name,reg.nurse_name, reg.attendant_name,  reg.gravida, reg.para, reg.hosp_no,reg.facility, reg.special_instructions,reg.trans_Id, reg.comments,reg.cond_while_admn,reg.del_comments,"
				+ " reg.del_time,reg.del_date, reg.del_result1, reg.no_of_children,reg.babywt1,reg.babywt2,reg.babysex1,reg.babysex2, "
				+ " reg.gestation_age, reg.del_result2, mothersDeath, del_time2, admitted_with,memb_pres_absent, mothers_death_reason , dateinserted ,state, district, taluk,facility_name, wmonth, wyear, datelastupdated, gestation_age_days , thaicardno, "
				+ " deltype_reason, deltype_otherreasons, LMP, riskoptions, tears, episiotomy, suturematerial, bloodgroup, "
				+ " EDD, Height, Weight, AdmittedWithOther, height_unit, extra_comments, regtype, regtypereason, date_of_reg_entry, normaltype, regtypeotherreason from tbl_referral "
				+ " inner join tbl_referral ref, tbl_registeredwomen reg on   reg.women_id=ref.women_id where "
				+ condition

				+ "  reg.user_id='" + userId + "'";
		cur = db.rawQuery(sql, null);
		return cur;

	}

	public boolean addDischargeDetails(DiscgargePojo dpojo, int transId) {
		// TODO Auto-generated method stub
		boolean isInserted;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(DISC_discUserId, dpojo.getUserId());
		values.put(DISC_discWomanId, dpojo.getWomanId());
		values.put(DISC_discWomanName, dpojo.getWomanName());
		values.put(DISC_discStatusOfWoman, dpojo.getWomanDeliveryStatus());
		values.put(DISC_discReferred, dpojo.getReferred());
		values.put(DISC_discReasonForDischarge, dpojo.getDischargeReasons());
		values.put(DISC_discDateOfDischarge, dpojo.getDateTimeOfDischarge());
		values.put(DISC_discMotherInvestigations, dpojo.getWomanInvestigation());
		values.put(DISC_discChildCondition, dpojo.getConditionOfChild());
		values.put(DISC_discChild2Condition, dpojo.getConditionOfChild2());
		values.put(DISC_discChildInvestigations, dpojo.getChildInvestigation());
		values.put(DISC_discChild2Investigations, dpojo.getChild2Investigation());
		values.put(DISC_discMotherCondition, dpojo.getConditionOfWoman());
		values.put(DISC_discAdvice, dpojo.getAdviceGiven());
		values.put(DISC_discDischargeDiagonsis, dpojo.getDischargeDiagnosis());
		values.put(DISC_discProcedures, dpojo.getProcedures());
		values.put(DISC_discComplicationObserved, dpojo.getComplicationObserved());
		values.put(DISC_discFindingsHistory, dpojo.getFindingHistorys());
		values.put(DISC_discActionTaken, dpojo.getActionTaken());
		values.put(DISC_discPatientDisposition, dpojo.getPatientDisposition());
		values.put(DISC_discAdmissionReasons, dpojo.getAdmissionReasons());
		values.put(DISC_discComments, dpojo.getRemarks());
		values.put(DISC_discNameOfHCProvider, dpojo.getHCProviderName());
		values.put(DISC_discDesignationOfHealthCareProvider, dpojo.getDesignation());
		values.put(DISC_discDateTimeOfNextFollowUp, dpojo.getDateOfNextFollowUp());
		values.put(DISC_discPlaceOfNextFollowUp, dpojo.getPlaceOfNextFOllowUp());
		values.put(DISC_discTransId, transId);
		// values.put(DISC_discDateOfInsertion,
		// dpojo.getDiscDateTimeOfInsertion());

		long inserted = db.insertOrThrow(TBLdischargedetails, null, values);
		if (inserted > 0) {
			isInserted = true;
		} else
			isInserted = false;

		return isInserted;

	}

	public DiscgargePojo getWomanDischargeData(String userId, String womenId) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		// ArrayList<DiscgargePojo> dValues = new ArrayList<DiscgargePojo>();
		DiscgargePojo dpojo = null;
		String sql = "Select * from " + TBLdischargedetails + " where " + DISC_discUserId + " ='" + userId + "' and "
				+ DISC_discWomanId + " = '" + womenId + "'";
		Cursor cur = db.rawQuery(sql, null);
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				dpojo = new DiscgargePojo();
				dpojo.setUserId(cur.getString(0));
				dpojo.setWomanId(cur.getString(1));
				dpojo.setWomanName(cur.getString(2));
				dpojo.setDateTimeOfDischarge(cur.getString(3));
				dpojo.setWomanInvestigation(cur.getString(4));
				dpojo.setChildInvestigation(cur.getString(5));
				dpojo.setChild2Investigation(cur.getString(6));
				dpojo.setWomanDeliveryStatus(cur.getInt(7));
				dpojo.setReferred(cur.getInt(8));
				dpojo.setDischargeReasons(cur.getString(9));
				dpojo.setDischargeDiagnosis(cur.getString(10));
				dpojo.setPatientDisposition(cur.getString(11));
				dpojo.setFindingHistorys(cur.getString(12));
				dpojo.setActionTaken(cur.getString(13));
				dpojo.setConditionOfWoman(cur.getString(14));
				dpojo.setConditionOfChild(cur.getString(15));
				dpojo.setConditionOfChild2(cur.getString(16));
				dpojo.setAdviceGiven(cur.getString(17));
				dpojo.setComplicationObserved(cur.getString(18));
				dpojo.setProcedures(cur.getString(19));
				dpojo.setDateOfNextFollowUp(cur.getString(20));
				dpojo.setPlaceOfNextFOllowUp(cur.getString(21));
				dpojo.setAdmissionReasons(cur.getString(22));
				dpojo.setHCProviderName(cur.getString(23));
				dpojo.setDesignation(cur.getString(24));
				dpojo.setRemarks(cur.getString(25));
				dpojo.setDatelastupdated(cur.getString(28));

				// dValues.add(dpojo);

			} while (cur.moveToNext());
		}
		return dpojo;

	}

	public Cursor getdischargewomenList(String userid, int displaylistcount) {
		db = this.getWritableDatabase();
		Cursor cur = null;
		String sql = "Select * from " + TBL_REGISTEREDWOMEN + " where  user_Id = '" + userid
				+ "' and women_id IN(Select discWomanId from tbldischargedetails) ";

		cur = db.rawQuery(sql, null);
		return cur;
	}

	public String updateDischargeData(DiscgargePojo dpojo, int transId, DiscgargePojo oldObj) throws Exception {
		db = this.getWritableDatabase();
		try {

			// 08aug2016 - Bindu - audittrail
			ArrayList<AuditTrailPojo> auditArr = new ArrayList<AuditTrailPojo>();
			String appendStr = "";

			/*
			 * byte[] oldB = oldWObj.getWomen_Image(); String oldEncodedImage =
			 * (oldB != null) ? Base64.encodeToString(oldB, Base64.DEFAULT) :
			 * "";
			 * 
			 * byte[] b1 = wpojo.getWomen_Image(); String encodedImage1 = (b1 !=
			 * null) ? Base64.encodeToString(b1, Base64.DEFAULT) : "";
			 * 
			 * // image - 18Oct2016 Bindu - oldwomanimage & womanimageupdated in
			 * // audit trail for woman photo updation // image- 21oct2016
			 * changed woman image to string if
			 * (!oldEncodedImage.equals(encodedImage1)) { if (appendStr != "")
			 * appendStr = appendStr + ", "; appendStr = appendStr + WOMEN_IMAGE
			 * + " = " + (char) 39 + encodedImage1 + (char) 39 + "";
			 * auditArr.add(new AuditTrailPojo(wpojo.getUserId(),
			 * wpojo.getWomenId(), TBL_REGISTEREDWOMEN, WOMEN_IMAGE,
			 * Partograph_CommonClass.getCurrentDateandTime(), "oldwomanimage",
			 * "womanimageupdated", transId)); }
			 */

			// discharge reason
			if (isEligibleEdit(oldObj.getDischargeReasons(), dpojo.getDischargeReasons())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discReasonForDischarge + " = " + (char) 39 + dpojo.getDischargeReasons()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discReasonForDischarge, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getDischargeReasons(), dpojo.getDischargeReasons(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getPlaceOfNextFOllowUp(), dpojo.getPlaceOfNextFOllowUp())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discPlaceOfNextFollowUp + " = " + (char) 39
						+ dpojo.getPlaceOfNextFOllowUp() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discPlaceOfNextFollowUp, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getPlaceOfNextFOllowUp(), dpojo.getPlaceOfNextFOllowUp(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getDateOfNextFollowUp(), dpojo.getDateOfNextFollowUp())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discDateTimeOfNextFollowUp + " = " + (char) 39
						+ dpojo.getDateOfNextFollowUp() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discDateTimeOfNextFollowUp, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getDateOfNextFollowUp(), dpojo.getDateOfNextFollowUp(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getWomanInvestigation(), dpojo.getWomanInvestigation())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discMotherInvestigations + " = " + (char) 39
						+ dpojo.getWomanInvestigation() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discMotherInvestigations, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getWomanInvestigation(), dpojo.getWomanInvestigation(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getActionTaken(), dpojo.getActionTaken())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discActionTaken + " = " + (char) 39 + dpojo.getActionTaken() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discActionTaken, Partograph_CommonClass.getCurrentDateandTime(), "old_action",
						"new_action", transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getConditionOfWoman(), dpojo.getConditionOfWoman())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discMotherCondition + " = " + (char) 39 + dpojo.getConditionOfWoman()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discMotherCondition, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getConditionOfWoman(), dpojo.getConditionOfWoman(), transId));
			}

			/*
			 * // discharge reason if
			 * (isEligibleEdit(oldObj.getDischargeDiagnosis(),
			 * dpojo.getDischargeDiagnosis())) { if (appendStr != "") appendStr
			 * = appendStr + ", "; appendStr = appendStr +
			 * DISC_discDischargeDiagonsis + " = " + (char) 39 +
			 * dpojo.getDischargeDiagnosis() + (char) 39 + ""; auditArr.add(new
			 * AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(),
			 * TBLdischargedetails, DISC_discDischargeDiagonsis,
			 * Partograph_CommonClass.getCurrentDateandTime(),
			 * oldObj.getDischargeDiagnosis(), dpojo.getDischargeDiagnosis(),
			 * transId)); }
			 */

			// discharge reason
			if (isEligibleEdit(oldObj.getDischargeDiagnosis(), dpojo.getDischargeDiagnosis())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discDischargeDiagonsis + " = " + (char) 39 + dpojo.getDischargeDiagnosis()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discDischargeDiagonsis, Partograph_CommonClass.getCurrentDateandTime(),
						"old_dischargediagnosis", "new_dischargediagnosis", transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getProcedures(), dpojo.getProcedures())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discProcedures + " = " + (char) 39 + dpojo.getProcedures() + (char) 39
						+ "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discProcedures, Partograph_CommonClass.getCurrentDateandTime(), oldObj.getProcedures(),
						dpojo.getProcedures(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getComplicationObserved(), dpojo.getComplicationObserved())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discComplicationObserved + " = " + (char) 39
						+ dpojo.getComplicationObserved() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discComplicationObserved, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getComplicationObserved(), dpojo.getComplicationObserved(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getAdviceGiven(), dpojo.getAdviceGiven())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discAdvice + " = " + (char) 39 + dpojo.getAdviceGiven() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discAdvice, Partograph_CommonClass.getCurrentDateandTime(), oldObj.getAdviceGiven(),
						dpojo.getAdviceGiven(), transId));
			}

			/*
			 * // discharge reason if (isEligibleEdit(oldObj.getAdviceGiven(),
			 * dpojo.getAdviceGiven())) { if (appendStr != "") appendStr =
			 * appendStr + ", "; appendStr = appendStr + DISC_discAdvice + " = "
			 * + (char) 39 + dpojo.getAdviceGiven() + (char) 39 + "";
			 * auditArr.add( new AuditTrailPojo(dpojo.getUserId(),
			 * dpojo.getWomanId(), TBLdischargedetails, DISC_discAdvice,
			 * Partograph_CommonClass.getCurrentDateandTime(), "old_advice",
			 * "new_advice", transId)); }
			 */

			// discharge reason
			if (isEligibleEdit(oldObj.getFindingHistorys(), dpojo.getFindingHistorys())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discFindingsHistory + " = " + (char) 39 + dpojo.getFindingHistorys()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discFindingsHistory, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getFindingHistorys(), dpojo.getFindingHistorys(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getPatientDisposition(), dpojo.getPatientDisposition())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discPatientDisposition + " = " + (char) 39 + dpojo.getPatientDisposition()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discPatientDisposition, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getPatientDisposition(), dpojo.getPatientDisposition(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getAdmissionReasons(), dpojo.getAdmissionReasons())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discAdmissionReasons + " = " + (char) 39 + dpojo.getAdmissionReasons()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discAdmissionReasons, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getAdmissionReasons(), dpojo.getAdmissionReasons(), transId));
			}

			/*
			 * // discharge reason if (isEligibleEdit(oldObj.getRemarks(),
			 * dpojo.getRemarks())) { if (appendStr != "") appendStr = appendStr
			 * + ", "; appendStr = appendStr + DISC_discComments + " = " +
			 * (char) 39 + dpojo.getRemarks() + (char) 39 + ""; auditArr.add(new
			 * AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(),
			 * TBLdischargedetails, DISC_discComments,
			 * Partograph_CommonClass.getCurrentDateandTime(),
			 * oldObj.getRemarks(), dpojo.getRemarks(), transId)); }
			 */

			// discharge reason
			if (isEligibleEdit(oldObj.getRemarks(), dpojo.getRemarks())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discComments + " = " + (char) 39 + dpojo.getRemarks() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discComments, Partograph_CommonClass.getCurrentDateandTime(), "old_comments",
						"new_comments", transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getHCProviderName(), dpojo.getHCProviderName())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discNameOfHCProvider + " = " + (char) 39 + dpojo.getHCProviderName()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discNameOfHCProvider, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getHCProviderName(), dpojo.getHCProviderName(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getDesignation(), dpojo.getDesignation())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discDesignationOfHealthCareProvider + " = " + (char) 39
						+ dpojo.getDesignation() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discDesignationOfHealthCareProvider, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getDesignation(), dpojo.getDesignation(), transId));
			}

			// discharge reason
			if (isEligibleEdit(oldObj.getChildInvestigation(), dpojo.getChildInvestigation())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discChildInvestigations + " = " + (char) 39 + dpojo.getChildInvestigation()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discChildInvestigations, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getChildInvestigation(), dpojo.getChildInvestigation(), transId));
			}

			if (isEligibleEdit(oldObj.getChild2Investigation(), dpojo.getChild2Investigation())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discChild2Investigations + " = " + (char) 39
						+ dpojo.getChild2Investigation() + (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discChild2Investigations, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getChild2Investigation(), dpojo.getChild2Investigation(), transId));
			}

			if (isEligibleEdit(oldObj.getConditionOfChild(), dpojo.getConditionOfChild())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discChildCondition + " = " + (char) 39 + dpojo.getConditionOfChild()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discChildCondition, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getConditionOfChild(), dpojo.getConditionOfChild(), transId));
			}

			if (isEligibleEdit(oldObj.getConditionOfChild2(), dpojo.getConditionOfChild2())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discChild2Condition + " = " + (char) 39 + dpojo.getConditionOfChild2()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DISC_discChild2Condition, Partograph_CommonClass.getCurrentDateandTime(),
						oldObj.getConditionOfChild2(), dpojo.getConditionOfChild2(), transId));
			}

			if (isEligibleEdit(oldObj.getDatelastupdated(), dpojo.getDatelastupdated())) {
				if (appendStr != "")
					appendStr = appendStr + ", ";
				appendStr = appendStr + DISC_discDateModified + " = " + (char) 39 + dpojo.getDatelastupdated()
						+ (char) 39 + "";
				auditArr.add(new AuditTrailPojo(dpojo.getUserId(), dpojo.getWomanId(), TBLdischargedetails,
						DATE_LASTUPDATED, Partograph_CommonClass.getCurrentDateandTime(), dpojo.getDatelastupdated(),
						dpojo.getDatelastupdated(), transId));
			}

			String strUpdate = "";
			// if(appendStr.length()>0)
			{
				if (appendStr.length() > 0) {
					strUpdate = "Update " + TBLdischargedetails + "  Set " + appendStr + " where  " + DISC_discUserId
							+ " = '" + dpojo.getUserId() + "' and " + DISC_discWomanId + " = '" + dpojo.getWomanId()
							+ "' ";

					db.execSQL(strUpdate);

					iUpdateRecordTrans(dpojo.getUserId(), transId, TBLdischargedetails, strUpdate, null, null);

					for (AuditTrailPojo ap : auditArr) {
						if (!insertTotblAuditTrail(ap))
							return null;
					}

					iNewRecordTrans(dpojo.getUserId(), transId, "tblAuditTrail");
				}
			}

			return strUpdate;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return new String();
		}
	}

	public ArrayList<DiscgargePojo> getDischargeData(String userId, String womenId) {
		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		ArrayList<DiscgargePojo> dValues = new ArrayList<DiscgargePojo>();
		DiscgargePojo dpojo;
		String sql = "Select * from " + TBLdischargedetails + " where " + DISC_discUserId + " ='" + userId + "' and "
				+ DISC_discWomanId + " = '" + womenId + "'";
		Cursor cur = db.rawQuery(sql, null);
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				dpojo = new DiscgargePojo();
				dpojo.setUserId(cur.getString(0));
				dpojo.setWomanId(cur.getString(1));
				dpojo.setWomanName(cur.getString(2));
				dpojo.setDateTimeOfDischarge(cur.getString(3));
				dpojo.setWomanInvestigation(cur.getString(4));
				dpojo.setChildInvestigation(cur.getString(5));
				dpojo.setChild2Investigation(cur.getString(6));
				dpojo.setWomanDeliveryStatus(cur.getInt(7));
				dpojo.setReferred(cur.getInt(8));
				dpojo.setDischargeReasons(cur.getString(9));
				dpojo.setDischargeDiagnosis(cur.getString(10));
				dpojo.setPatientDisposition(cur.getString(11));
				dpojo.setFindingHistorys(cur.getString(12));
				dpojo.setActionTaken(cur.getString(13));
				dpojo.setConditionOfWoman(cur.getString(14));
				dpojo.setConditionOfChild(cur.getString(15));
				dpojo.setConditionOfChild2(cur.getString(16));
				dpojo.setAdviceGiven(cur.getString(17));
				dpojo.setComplicationObserved(cur.getString(18));
				dpojo.setProcedures(cur.getString(19));
				dpojo.setDateOfNextFollowUp(cur.getString(20));
				dpojo.setPlaceOfNextFOllowUp(cur.getString(21));
				dpojo.setAdmissionReasons(cur.getString(22));
				dpojo.setHCProviderName(cur.getString(23));
				dpojo.setDesignation(cur.getString(24));
				dpojo.setRemarks(cur.getString(25));

				dpojo.setDatelastupdated(cur.getString(28));

				dValues.add(dpojo);

			} while (cur.moveToNext());
		}
		return dValues;

	}

	public boolean addLatentData(LatentPhasePojo lpojo, int transId) {
		// TODO Auto-generated method stub
		boolean isDataInserted;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(LAT_USERID, lpojo.getUserId());
		values.put(LAT_WOMANID, lpojo.getWomanId());
		values.put(LAT_WOMANAME, lpojo.getWomanName());
		values.put(LAT_PULSE, lpojo.getPulse());
		values.put(LAT_BP, lpojo.getBp());
		values.put(LAT_CONTRACTIONS, lpojo.getContractions());
		// values.put(LAT_DURATION, lpojo.getDuration());
		values.put(LAT_FHS, lpojo.getFhs());
		values.put(LAT_PV, lpojo.getPv());
		values.put(LAT_ADVICE, lpojo.getAdvice());
		values.put(LAT_DATETIME, lpojo.getDatetime());
		values.put(LAT_TRANSID, transId);

		long inserted = db.insertOrThrow(TBL_LATENTPHASE, null, values);
		if (inserted > 0) {
			isDataInserted = true;
		} else
			isDataInserted = false;
		return isDataInserted;
	}

	public Cursor getLatentPhaseData(String userId, String womanId, int order) {
		Cursor cur = null;
		db = this.getWritableDatabase();

		String var = " ";

		if (order == 1)
			var = " order by latDateOfInsertion desc ";

		String sql = "Select * from " + TBL_LATENTPHASE + " where " + LAT_USERID + " = '" + userId + "' and "
				+ LAT_WOMANID + " ='" + womanId + "' " + var;
		cur = db.rawQuery(sql, null);
		return cur;
	}

	public boolean addPostPartumData(PostPartum_Pojo postpojo, int transId) {
		// TODO Auto-generated method stub
		boolean isInserted = false;
		db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(POST_USERID, postpojo.getUserId());
		values.put(POST_WOMANID, postpojo.getWomanId());
		values.put(POST_WOMAN_NAME, postpojo.getWomanName());
		values.put(POST_COMPLAINTS, postpojo.getComplaints());
		values.put(POST_EXAMINATION, postpojo.getExamination());
		values.put(POST_PALLOR, postpojo.getPallor());
		values.put(POST_PULSE, postpojo.getPulse());
		values.put(POST_BP, postpojo.getBp());
		values.put(POST_BREASTEXAMINATION, postpojo.getBreastExamination());
		values.put(POST_INVOLUTION, postpojo.getInvolution());
		values.put(POST_LOCHIA, postpojo.getLochia());
		values.put(POST_ADVICE, postpojo.getAdvice());
		values.put(POST_PERINEALCARE, postpojo.getPerineal());
		values.put(POST_DATETIMEOFENTRY, postpojo.getDateTimeOfEntry());
		values.put(TRANS_ID, transId);
		values.put(POST_OTHER, postpojo.getOther());

		long inserted = db.insertOrThrow(TBL_POSTPARTUM, null, values);
		if (inserted > 0) {
			isInserted = true;
		} else
			isInserted = false;

		return isInserted;
	}

	public Cursor getPostPartumData(String userId, String womanId) {
		// TODO Auto-generated method stub
		String sql = "Select * from tblpostpartumcare where postuserId = '" + userId + "' and postwomanId='" + womanId
				+ "'";
		Cursor cur = db.rawQuery(sql, null);
		return cur;

	}

	// 20Sep2017 Arpitha
	public String getLatenPhaseDateTime(String womanId, String userId) {
		db = this.getWritableDatabase();
		String datetime = null;
		String sql = "Select max(" + LAT_DATETIME + ") from " + TBL_LATENTPHASE + " where " + LAT_WOMANID + "='"
				+ womanId + "' and " + LAT_USERID + "='" + userId + "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			datetime = cur.getString(0);
		}
		return datetime;
	}

	// 20Sep2017 Arpitha
	public String getPostPartumDateTime(String womanId, String userId) {
		db = this.getWritableDatabase();
		String datetime = null;
		String sql = "Select max(" + POST_DATETIMEOFENTRY + ") from " + TBL_POSTPARTUM + " where " + POST_WOMANID + "='"
				+ womanId + "' and " + POST_USERID + "='" + userId + "'";
		Cursor cur = db.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			datetime = cur.getString(0);
		}
		return datetime;
	}
}

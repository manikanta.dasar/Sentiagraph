package com.bc.partograph.common;


public class MessageLogPojo {
	
	private int msgId;
	private String userId;
	private String womanId;
	private String phoneNo;
	private String msgBody;
	private String msgSentDate;
	private String risk;
	private String riskObserved;
	private int priority;
	private int noOfFailures;
	private int msgSent;
	
	public MessageLogPojo(String userId, String womanId, String phoneno, String risk, String riskObserved,
			String msgBody, int priority, int noOfFailures){
		this.userId = userId;
		this.womanId = womanId;
		this.phoneNo = phoneno;
		this.msgBody = msgBody;
		this.risk = risk;
		this.riskObserved = riskObserved;
		this.priority = priority;
		this.noOfFailures = noOfFailures;
	}
	
	public MessageLogPojo(int msgId, String userId, String womanId,  String risk, String riskObserved,String phoneNo,
			String msgBody, String msgSentDate, int priority, int noOfFailures){
		this.msgId = msgId;
		this.userId = userId;
		this.womanId = womanId;
		this.phoneNo = phoneNo;
		this.msgBody = msgBody;
		this.msgSentDate = msgSentDate;
		this.risk = risk;
		this.riskObserved = riskObserved;
		this.priority = priority;
		this.noOfFailures = noOfFailures;
	}

	

	@Override
	public String toString() {
		String str = "Msg Obj: " + getMsgId() + getRisk() + ", " + getRiskObserved() + ", " 
				 + getUserId() + ", " + getWomanId() + ", " + getPhoneNo() + ", " + getMsgBody()
				+ ", " + getMsgSentDate() + ", " + getNoOfFailures();
		return str;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	public String getRiskObserved() {
		return riskObserved;
	}

	public void setRiskObserved(String riskObserved) {
		this.riskObserved = riskObserved;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWomanId() {
		return womanId;
	}

	public void setWomanId(String womanId) {
		this.womanId = womanId;
	}

	

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getMsgBody() {
		return msgBody;
	}

	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}

	public String getMsgSentDate() {
		return msgSentDate;
	}

	public void setMsgSentDate(String msgSentDate) {
		this.msgSentDate = msgSentDate;
	}

	

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getMsgId() {
		return msgId;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public int getNoOfFailures() {
		return noOfFailures;
	}

	public void setNoOfFailures(int noOfFailures) {
		this.noOfFailures = noOfFailures;
	}

	public int getMsgSent() {
		return msgSent;
	}

	public void setMsgSent(int msgSent) {
		this.msgSent = msgSent;
	}
	
	
}

package com.bc.partograph.common;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.bc.partograph.womenview.Fragment_DIP;
import com.bluecrimson.partograph.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class MultiSelectionSpinner extends Spinner implements OnMultiChoiceClickListener {
	String[] _items = null;
	boolean[] mSelection = null;

	public ArrayAdapter<String> simple_adapter;
	boolean[] mSelectionAtStart = null;// Arpitha on 7/6/16
	String _itemsAtStart = null;// Arpitha on 7/6/16

	public OnMultipleItemsSelectedListener listener;

	public interface OnMultipleItemsSelectedListener {
		void selectedIndices(List<Integer> indices);

		void selectedStrings(List<String> strings);
	}

	public MultiSelectionSpinner(Context context) {
		super(context);
		try {
			simple_adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
			super.setAdapter(simple_adapter);
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public MultiSelectionSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		try {
			simple_adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
			super.setAdapter(simple_adapter);
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		try {
			if (mSelection != null && which < mSelection.length) {
				mSelection[which] = isChecked;

				simple_adapter.clear();

				// Disable the multi spinner item - 26Sep2016 Arpitha
				if (Fragment_DIP.istears) {
					if (which <= 5) {
						for (int i = 0; i < 6; i++) {
							if (i != which) {
								mSelection[i] = false;
								((AlertDialog) dialog).getListView().setItemChecked(i, false);

							}
						}
					}

				} // 26Sep2016 Arpitha

				/*
				 * // 23oct2016 Arpitha if (ReferralInfo_Activity.isdip) { if
				 * (which == 2 || which == 3 || which == 6 || which == 7) {
				 * mSelection[which] = false; ((AlertDialog)
				 * dialog).getListView().setItemChecked(which, false); }
				 * 
				 * }
				 * 
				 * if (ReferralInfo_Activity.isdelivered) { if (which == 0 ||
				 * which == 1) { mSelection[which] = false; ((AlertDialog)
				 * dialog).getListView().setItemChecked(which, false); }
				 * 
				 * }
				 */// commented on 12April2017 Arpitha

				// 07Nov2016 Arpitha
				/*if (RegistrationActivity.admittedwith) {
					if (which == 3) {
						RegistrationActivity.admittedwith_other = true;
					}
					RegistrationActivity.admittedwith = false;
				} // 07Nov2016 Arpitha
*/
				simple_adapter.add(buildSelectedItemString());
			} else {
				throw new IllegalArgumentException("Argument 'which' is out of bounds.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	@Override
	public boolean performClick() {
		try {
			super.performClick();
			AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
			builder.setMultiChoiceItems(_items, mSelection, this);
			builder.setTitle(getResources().getString(R.string.select));

			builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					try {
						
						listener.selectedStrings(getSelectedStrings());
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						
						e.printStackTrace();
					}
				}
			});
			// 27Sep2016 Arpitha - set negative button
			builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					simple_adapter.clear();
					try {
						// 17April2017 Arpitha
						List<Integer> pos = getSelectedIndicies();
						for (int i = 0; i < pos.size(); i++) {
							mSelection[pos.get(i)] = false;
						} // 17April2017 Arpitha

						// simple_adapter.add(getSelectedItemsAsString());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					} // 27Sep2016 Arpitha
				}
			});

			builder.show();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
			return false;
		}
	}

	@Override
	public void setAdapter(SpinnerAdapter adapter) {
		throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
	}

	public void setItems(String[] items) {
		try {
			_items = items;
			mSelection = new boolean[_items.length];
			simple_adapter.clear();
			Arrays.fill(mSelection, false);
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setItems(List<String> items) {
		try {
			_items = items.toArray(new String[items.size()]);
			mSelection = new boolean[_items.length];
			simple_adapter.clear();
			Arrays.fill(mSelection, false);
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(String[] selection) {
		try {
			for (String cell : selection) {
				for (int j = 0; j < _items.length; ++j) {
					if (_items[j].equals(cell)) {
						mSelection[j] = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(List<String> selection) {
		try {
			for (int i = 0; i < mSelection.length; i++) {
				mSelection[i] = false;
			}
			for (String sel : selection) {
				for (int j = 0; j < _items.length; ++j) {
					if (_items[j].equals(sel)) {
						mSelection[j] = true;
					}
				}
			}
			simple_adapter.clear();
			simple_adapter.add(buildSelectedItemString());
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(int index) {
		try {
			for (int i = 0; i < mSelection.length; i++) {
				mSelection[i] = false;
			}
			if (index >= 0 && index < mSelection.length) {
				mSelection[index] = true;
			} else {
				throw new IllegalArgumentException("Index " + index + " is out of bounds.");
			}
			simple_adapter.clear();
			simple_adapter.add(buildSelectedItemString());
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(int[] selectedIndicies) {
		try {
			for (int i = 0; i < mSelection.length; i++) {
				mSelection[i] = false;
			}
			for (int index : selectedIndicies) {
				if (index >= 0 && index < mSelection.length) {
					mSelection[index] = true;
				} else {
					throw new IllegalArgumentException("Index " + index + " is out of bounds.");
				}
			}
			simple_adapter.clear();
			simple_adapter.add(buildSelectedItemString());
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public List<String> getSelectedStrings() throws Exception {
		List<String> selection = new LinkedList<String>();
		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				selection.add(_items[i]);
			}
		}
		return selection;
	}

	public List<Integer> getSelectedIndicies() throws Exception {
		List<Integer> selection = new LinkedList<Integer>();
		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				selection.add(i);
			}
		}

		return selection;
	}

	private String buildSelectedItemString() throws Exception {
		StringBuilder sb = new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				if (foundOne) {
					sb.append(", ");
				}
				foundOne = true;

				sb.append(_items[i]);
			}
		}
		return sb.toString();
	}

	public String getSelectedItemsAsString() throws Exception {
		StringBuilder sb = new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				if (foundOne) {
					sb.append(", ");
				}
				foundOne = true;
				sb.append(_items[i]);
			}
		}
		return sb.toString();
	}

}

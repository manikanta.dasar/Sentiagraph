package com.bc.partograph.common;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.SequenceInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.bc.partograph.comprehensiveview.View_Partograph;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhasePojo;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.regwomenlist.RecentCustomListAdapterSearch;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bc.partograph.womenview.Fragment_DIP;
import com.bc.partograph.womenview.Fragment_Delivered;
import com.bc.partograph.womenview.Fragment_Referred;
import com.bc.partograph.womenview.Fragment_Retrospective;
import com.bluecrimson.additionalDetails.AdditionalDetails_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.ActionItem;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.QuickAction;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.SettingsPojo;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.postpartumcare.PostPartum_Pojo;
import com.bluecrimson.viewprofile.ViewProfile_Activity;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Partograph_CommonClass extends PdfPageEventHelper {
	public static String DATABASE_NAME = "";
	// changed version from 2 to 3 by Arpitha
	// changed version from 3 to 4 - 14Oct2016 Arpitha
	// <!-- db version changed from 4 to 5 on 10Jan2017 Arpitha
	// <!-- db version changed from 5 to 6 on 17Feb2017 Arpitha -->
	// db version changed from 6 to 7 on 16May2017 Arpitha - v2.6 color coded
	// db version changed from 7 to 8 on 17Aug2017 Arpitha - v2.6.1 discahrge
	// summary and latentphase
	// values
	public static int DATABASE_VERSION = 9;
	public static Context context = AppContext.context;
	public static UserPojo user;
	public static String strSelecteddate;
	public static boolean movedToInputFolder = false;
	public static int responseAckCount = 0;
	public static ConnectivityManager connectivityManager;
	public static boolean internetOn = false;
	// public static String ipAdd = "192.168.1.100";
	public static int curr_tabpos;
	public static String startoftheweek, endoftheweek;
	public static int fhrcmt_cnt = 0, afmcmt_cnt = 0, dilcmt_cnt = 0, contcmt_cnt = 0, oxycmt_cnt = 0, drugscmt_cnt = 0,
			pbpcmt_cnt = 0, tempcmt_cnt = 0, utcmt_cnt = 0, apgar_cnt = 0;
	public static int comment_cnt = 0;
	static boolean inetOn = false;
	static String serverResult = null;
	static Partograph_DB dbh;

	// changes 04Feb2015
	public static Properties properties = null;
	public static SharedPreferences prefs = null;
	public static SharedPreferences.Editor editor = null;

	// changed on 27Mar2015
	public static String defdateformat = "dd-MM-yyyy";
	/* public static String defLanguage = "English"; */
	public static String defLanguage;

	// 18Aug2015
	public static String strDelStatustoday;
	public static String strDelStatusweekly;
	public static String strDelStatusmonthly;
	public static int iday;
	public static int imon;
	public static int iyear;

	// updated on 17Nov2015
	public static boolean isRecordExistsForSync = false;

	// bindu-29July-2016
	public static Women_Profile_Pojo oldWomanObj;

	// 22Aug2016
	public static boolean isNetworkTestAlreadyCalled = false;

	public static boolean isAsyncStarted = false;// 06April2017 Arpitha

	public static String FHR_RED;
	public static String FHR_AMBER;
	public static String FHR_GREEN;
	public static String AFM_RED;
	public static String AFM_GREEN;
	public static String CERVIX_RED;
	public static String CERVIX_AMBER;
	public static String CERVIX_GREEN;
	// 10May2017 Arpitha- v2.6
	static TextView txtdialogtitle;
	static Button btnchangepass;
	static EditText etoldpass;
	static EditText etnewpass;
	static EditText etretypepass;
	static boolean isErrorFound = false;

	static GraphicalView mChartViewFHR = null;
	public static ArrayList<Add_value_pojo> listValuesItemsFHR;
	static LinkedHashMap<Integer, Double> newDilValuesCervix;
	static LinkedHashMap<Integer, Double> newDilValuesDescent;
	public static ArrayList<Add_value_pojo> listValuesItemsDilatation;
	static GraphicalView mChartViewDilatation = null;
	// static TableLayout tbllayoutafm;
	public static ArrayList<Add_value_pojo> listValuesItemsafm;
	public static ArrayList<Add_value_pojo> listValuesItemsContractions, listValuesItemsoxytocin, listValuesItemsdrugs,
			listValuesItemsTemp, listValuesItemsUrinetest, listValuesItemsPBP;
	public static GraphicalView mChartViewContraction = null;
	public static GraphicalView mChartViewPBP = null;
	static PdfPTable tableAFM, table_urine, table_temp, table_drug, table_oxytocin;
	private static Font small = new Font(Font.HELVETICA, 8, Font.BOLD);
	static PdfPTable womanbasics;
	// static Context context;
	// static Partograph_DB dbh;
	static ImageView img1, img2, img3, img4, img5, img6, img7, imglatent, imgpostpartum, imgdiscahrge;
	static String wname;
	static int age;
	static String regdate;
	static int gest;
	static String gravida;
	static String risk;
	// 06Jun2017 Arpitha
	static ListView list;// 06Jun2017 Arpitha
	static TextView txtnoData;// 06Jun2017 Arpitha
	static ArrayList<Women_Profile_Pojo> rowItems;// 06Jun2017 Arpitha
	static Women_Profile_Pojo woman;
	public static final int ID_PROFILE = 1;
	public static final int ID_GRAPH = 2;
	public static final int ID_DELSTATUS = 3;
	public static final int ID_APGAR = 4;
	public static final int ID_COMPREHENSIVEVIEW = 5;
	public static final int ID_REFERRAL = 6;
	public static final int ID_STAGEOFLABOR = 7;
	public static final int ID_ADDITIONALDETAILS = 9;
	static QuickAction mQuickAction;
	static EditText etsearch;
	public static final int ID_PRINTPARTO = 8;// 06Jun2017 Arpitha

	// 20July2017 Arpitha
	static Dialog dialog_ref;
	static EditText etplaceofreferral;
	static EditText etreasonforreferral;
	static EditText etdateofreferral;
	static Button btnCancelRef, btnSaveRef;
	static EditText ettimeofreferral;
	static Spinner spnreferralfacility;
	static RadioButton rd_motherconscious, rd_motherunconscious, rd_babyalive, rd_babydead, rdepiyes, rdepino, rdcatgut,
			rdvicryl;
	static EditText etheatrate, etspo2;
	static TableRow trspo2, trheartrate;
	static String strRefDate;
	static MultiSelectionSpinner mspnreasonforreferral;
	static LinkedHashMap<String, Integer> reasonforreferralMap;
	static ArrayList<WomenReferral_pojo> womenrefpojoItems;
	static EditText etdescriptionofreferral, etbpsystolicreferral, etbpdiastolicreferral, etpulsereferral;
	static TextView txtrefmode;
	// Dialog dialog_ref;
	static TextView txtwname, txtwage, txtwdoa, txtwtoa, txtwgest, txtwgravida, txtwrisk;
//	static TextView txtheadingref;
	static String path;
	static ImageView imgpdf;
	static LinearLayout llbaby;

	static EditText etbabywt1, etbabywt2, etdelcomments, etdeldate, etmothersdeathreason, tvmothersdeathreason;
	static String strDeldate;
	static EditText etdeltime2;
	static boolean del_time1;
	static EditText etdeltime;
	static TableRow trsuturematerial;
	// episiotomy default value is made as 0 before it was 1 updated on
	// 12july2016 by Arpitha
	static int episiotomy = 0, suturematerial = 0;
	static Spinner spndel_type, spn_delres1, spn_delres2, spn_noofchild;
	static TableRow trchilddet1, tr_childdetsex1, tr_childdet2, tr_childsex2, tr_secondchilddetails, tr_result2,
			tr_deltime2;
	static TextView txtfirstchilddetails;
	static int delivery_type, delivery_result1, delivery_result2, babysex1, babysex2;
	static TableRow tr_deltypereason, tr_deltypeotherreasons;
	static RadioButton rdmale1, rdfemale1, rdmale2, rdfemale2;
	static RadioButton rdmotheralive, rdmotherdead;
	static TextView txtdeltype, txtnoofchild, txtdelres1, txtchildwt1, txtchildsex1, txtdelres2, txtchildwt2,
			txtchildsex2, txtdelcomments, txtdeldate, txtdeltime, txtdeltime2, txtmothersdeathreason;
	static Button imgbtnok;
	static boolean isDelUpdate = false;
	static int numofchildren;
	static MultiSelectionSpinner mspndeltypereason;
	static LinkedHashMap<String, Integer> deltypereasonMap;
	static EditText etdeltypeotherreasons;
	static MultiSelectionSpinner mspntears;
	static LinkedHashMap<String, Integer> tearsMap;
	static RadioGroup rdchildSexGrpbaby1, rdchildSexGrpbaby2;
	static EditText txtdeltypereason, txtdeltypeotherreasons;
	static EditText ettears;
	static String no_of_child, selecteddate;
	static ImageView imgwt1_warning, imgwt2_warning;
	static boolean weight;
	static int weight1, weight2;
	static int bpsysval = 0, bpdiaval = 0, pulval = 0;
	static private int hour;
	static private int minute;
	// static public static int year, mon, day;
	static String todaysDate;
	static TextView txtdeltypereasonheading, txtdeltypeotherreason;
	// static TextView txtheading;
	static TextView txtwdelstatus, txtwdeldate, txtwdeltime;
	static TableRow tr_normaltype;
	static RadioButton rdvertext, rdbreech;

	static TextView txtnormaltype;
	static int heartrate;
	static int spo2;
	static ImageView imgdelresult, imgsex2, imgdeltime2, wardeltype, wardaltypereason;
	static Dialog dialog_del;// 20July2017 Arpitha

	public static DiscgargePojo dpojo;// 09Aug2017 Arpitha

	static TextView txtdisabled;// 10Aug2017 Arpitha
	static RelativeLayout reldel;// 10Aug2017 Arpitha

	static File dir;// 30Aug2017 Arpitha
	static File latentfile, partofile, referralfile, postpartumfile, dischrgefile;// 30Aug2017
																					// Arpitha
	// static Document partographDoc, latentDoc, referraldoc, postpartumdoc,
	// dischargedoc;
	static Font heading = new Font(Font.HELVETICA, 11, Font.BOLD);
	static PdfPTable lat;
	static String advice = "", bp = "", pulse = "", fhs = "", pv = "", contractions = "";
	static PdfPTable dischargeInfo;
	static PdfPTable dischargeInfo2, childInfo2, childInfo, womandelInfo;
	static String examination = "", pallor = "", postpulse = "", postbp = "", involution = "", postadvice = "",
			breastExamination = "", lochia = "", perinealCare = "", complaints = "";
	static String referredPlace, bpsystolic, bpdiastolic, description = "", referredDate, referredTime;
	static boolean consiuous;
	static String reasons;
	static String womanID, userId;
	static CheckBox chkdischarge, chklatent, chkpartograph, chkreferral, chkpostpartum, chkall;
	static File allpdffile;
	static FileOutputStream fOut;
	public static final int ID_DISCHARGEDETAILS = 10;// 06Dec2016 Arpitha
	public static final int ID_LATENTPHASE = 11;// 10Aug2017 Arpitha
	public static final int ID_POSTPARTUM = 12;// 18Aug2017 Arpitha
	public static final int ID_PDF = 13;// 10Aug2017 Arpitha
	static Uri pdfpath;
	static Document doc;
	static PdfWriter writer;
	static File allfile;
	static boolean Otheroption;

	static {
		try {
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
			editor = prefs.edit();
			/*
			 * if(Settings_parto.sharedpreferences!=null) { defLanguage =
			 * Settings_parto.sharedpreferences.getString("language",
			 * defLanguage); }
			 */

			defLanguage = prefs.getString("language", null);

			LoadDeafultProjectProperties();
			// context = AppContext.context;
			// dbh = new Partograph_DB(context);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SettingsPojo sPojo;

	// updated 13Nov2015
	public static String requestId = "";

	public static boolean automatic_datetime = false;// 27Sep2016 Arpitha
	public static Date currentdate;

	public static boolean isMessageLogsaved = false;// 13oct2016 Arpitha

	// Get today's date
	public static String getTodaysDate() {
		String returnDateStr;
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		returnDateStr = sdf.format(d);
		return returnDateStr;
	}

	// Get Current Time
	public static String getCurrentTime() {
		String currentTime;
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("HH:mm"); // 24 hour format
		currentTime = df.format(c.getTime());
		return currentTime;
	}

	// To Check whether internet is on or not in the device
	public static boolean isConnectionAvailable(Context context) {
		boolean isOn = false;

		connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		isOn = (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected());

		if (isOn) {
			internetOn = true;
			boolean isPingSuccess = checkPingIP(Partograph_CommonClass.properties.getProperty("ipAddress"), context);
			if (isPingSuccess)
				return true;
		}
		return false;
	}

	public static String SQL2 = "INSERT INTO tblTransDetail Values (?, ?, ?, ?, ?) ";

	public static String SQL3 = "INSERT INTO tblTransHeader Values (?, ?, ?, ?)  ";

	public static String SQL4 = "UPDATE tbl_user " + "SET LastTransNumber = ? WHERE user_Id = ? ";

	public static String SQL5 = "UPDATE " + Partograph_DB.TBL_USER + " SET LastWomenNumber = LastWomenNumber + 1 "
			+ " WHERE " + Partograph_DB.USER_ID + " = ? ";

	// To check whether it is able to ping remote server or not
	public static boolean checkPingIP(String ipAdd, Context context) {
		boolean pingSuccess = false;

		if (ipAdd.length() > 0) {
			try {

				String pingCmd = "ping -n 5 " + ipAdd;
				String pingResult = "";
				Runtime r = Runtime.getRuntime();
				Process p = r.exec(pingCmd);
				BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					pingResult += inputLine;
				}
				int mExitValue = p.waitFor();

				if (mExitValue != 0) {
					if (pingResult.toLowerCase().contains("0 received"))
						pingSuccess = false;
					else
						pingSuccess = true;
				} else
					pingSuccess = true;

			} catch (IOException e) {
				e.printStackTrace();
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ Partograph_CommonClass.context.getClass().getSimpleName(), e);
			} catch (InterruptedException e) {
				e.printStackTrace();
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ Partograph_CommonClass.context.getClass().getSimpleName(), e);
			}
		} else {
		}

		return pingSuccess;
	}

	/**
	 * Async Class to Sync
	 *
	 */
	public static class AsyncCallWS extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			if (Activity_WomenView.menuItem != null) {
				Activity_WomenView.menuItem.collapseActionView();
				Activity_WomenView.menuItem.setActionView(null);

			}

			if (SlidingActivity.menuItem != null) {
				SlidingActivity.menuItem.collapseActionView();
				SlidingActivity.menuItem.setActionView(null);

			}

			if (!inetOn)

				Toast.makeText(context, context.getResources().getString(R.string.chk_internet), Toast.LENGTH_LONG)
						.show();

			else {
				if (Partograph_CommonClass.movedToInputFolder && Partograph_CommonClass.responseAckCount <= 5) {
					if (!SyncFunctions.syncUptoDate) {
						AsyncCallWS task = new AsyncCallWS();
						task.execute();
					} else {
						AsyncCallWS.this.cancel(true);
						Toast.makeText(context, context.getResources().getString(R.string.sync_success),
								Toast.LENGTH_LONG).show();
						refreshActionBarMenu(KillAllActivitesAndGoToLogin.context);
					}
				} else {
					AsyncCallWS.this.cancel(true);
					Toast.makeText(context, context.getResources().getString(R.string.sync_failed), Toast.LENGTH_LONG)
							.show();
					refreshActionBarMenu(KillAllActivitesAndGoToLogin.context);
				}
			}

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(context);
				if (inetOn) {
					if (Partograph_CommonClass.sPojo != null) {
						serverResult = new SyncFunctions(context).SyncFunction(user.getUserId(),
								Partograph_CommonClass.sPojo.getServerDb(),
								Partograph_CommonClass.sPojo.getIpAddress());
					}
				}
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ Partograph_CommonClass.context.getClass().getSimpleName(), e);
				return e.getLocalizedMessage();

			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

		}
	}

	/**
	 * Get Date from String
	 * 
	 * @param inputDate
	 * @return
	 */
	public static Date getDateS(String inputDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date sendDate = null;
		try {
			sendDate = sdf.parse(inputDate);
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ Partograph_CommonClass.context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
		return sendDate;
	}

	/**
	 * Get string date in the specified format
	 * 
	 * @param date
	 * @param def_dateFormat
	 * @return string date in the specified format
	 * @throws Exception
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getConvertedDateFormat(String date, String def_dateFormat) throws Exception {
		String dateValue = null;
		// String dateFormat = getDateFormat();
		Date d = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		dateValue = new SimpleDateFormat(def_dateFormat).format(d);
		return dateValue;
	}

	/**
	 * Get Current date in the format dd-MM-yyyy
	 * 
	 * @param format
	 * @return
	 */
	public static String getTodaysDate(String format) {
		String returnDateStr;
		Date d = new Date();
		/* SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); */
		SimpleDateFormat sdf = new SimpleDateFormat(format);// 25Jan2017 Arpitha
		returnDateStr = sdf.format(d);
		return returnDateStr;
	}

	/**
	 * Validate time duration between 2 date/time
	 * 
	 * @param prevdate
	 * @param currdate
	 * @param min
	 * @return true if cond is valid else false
	 * @throws Exception
	 */
	public static boolean getisValidTime(String prevdate, String currdate, int min) throws Exception {
		boolean isValid = false;
		Date d1, d2;

		Calendar cal1 = Calendar.getInstance();

		Calendar cal2 = Calendar.getInstance();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm");

		d1 = sdf.parse(prevdate);
		d2 = sdf.parse(currdate);

		cal1.setTime(d1);
		cal2.setTime(d2);

		// Time Difference Calculations Begin
		long milliSec1 = cal1.getTimeInMillis();
		long milliSec2 = cal2.getTimeInMillis();

		long timeDifInMilliSec;
		if (milliSec1 >= milliSec2) {
			timeDifInMilliSec = milliSec1 - milliSec2;
		} else {
			timeDifInMilliSec = milliSec2 - milliSec1;
		}

		// long timeDifSeconds = timeDifInMilliSec / 1000;
		long timeDifMinutes = timeDifInMilliSec / (60 * 1000);
		// long timeDifHours = timeDifInMilliSec / (60 * 60 * 1000);
		// long timeDifDays = timeDifInMilliSec / (24 * 60 * 60 * 1000);

		if (timeDifMinutes >= min) {
			isValid = true;
		} else
			isValid = false;

		return isValid;
	}

	// Changes 04Feb2015

	/**
	 * Load default project properties
	 */
	public static void LoadDeafultProjectProperties() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		try {
			properties = new Properties();
			properties.load(context.getAssets().open("partograph.properties"));

		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ Partograph_CommonClass.context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	/**
	 * Return date from string
	 * 
	 * @param inputDate
	 * @param def_dateFormat
	 * @return
	 * @throws Exception
	 */
	public static Date getDate(String inputDate, String def_dateFormat) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(def_dateFormat);
		Date sendDate = null;
		try {
			sendDate = sdf.parse(inputDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendDate;
	}

	/**
	 * Get the Date From Date Format Set
	 * 
	 * @param date
	 * @param def_dateFormat
	 * @return
	 * @throws Exception
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getConvdate_ddMMYYYY(String date, String def_dateFormat) throws Exception {
		String dateValue = null;
		// String dateFormat = getDateFormat();
		Date d = new SimpleDateFormat("dd-MM-yyyy").parse(date);
		dateValue = new SimpleDateFormat(def_dateFormat).format(d);
		return dateValue;
	}

	/**
	 * Get the start and End date of the current week
	 * 
	 * @throws Exception
	 */
	public static void getCurrentWeek() throws Exception {
		Calendar calendarFirstDay = Calendar.getInstance();
		Calendar calendarLastDay = Calendar.getInstance();
		Date now = new Date();

		calendarFirstDay.setTime(now);
		calendarLastDay.setTime(now);

		startoftheweek = firstDayOfWeek(calendarFirstDay);
		endoftheweek = lastDayOfWeek(calendarLastDay);

	}

	/**
	 * Get first day of the week
	 * 
	 * @param calendar
	 * @return
	 * @throws Exception
	 */
	public static String firstDayOfWeek(Calendar calendar) throws Exception {
		Calendar cal = (Calendar) calendar.clone();
		int day = cal.get(Calendar.DAY_OF_YEAR);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
			cal.set(Calendar.DAY_OF_YEAR, --day);
		}
		int year1 = cal.get(Calendar.YEAR);
		int month1 = cal.get(Calendar.MONTH) + 1;
		int day1 = cal.get(Calendar.DATE);

		startoftheweek = String.valueOf(year1) + "-" + String.format("%02d", month1) + "-"
				+ String.format("%02d", day1);
		return startoftheweek;
	}

	// Last day of the week

	/**
	 * @param calendar
	 * @return
	 * @throws Exception
	 */
	public static String lastDayOfWeek(Calendar calendar) throws Exception {
		Calendar cal = (Calendar) calendar.clone();
		int day = cal.get(Calendar.DAY_OF_YEAR);
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			cal.set(Calendar.DAY_OF_YEAR, ++day);
		}

		int year7 = cal.get(Calendar.YEAR);
		int month7 = cal.get(Calendar.MONTH) + 1;
		int day7 = cal.get(Calendar.DATE);

		endoftheweek = String.valueOf(year7) + "-" + String.format("%02d", month7) + "-" + String.format("%02d", day7);
		return endoftheweek;
	}

	/**
	 * Get the month, day and year
	 * 
	 * @param todaysDate
	 * @throws Exception
	 */

	public static void getMonthYear(String todaysDate) throws Exception {
		Date today = Partograph_CommonClass.getDateS(todaysDate);
		iday = Integer.parseInt(new SimpleDateFormat("dd").format(today));
		imon = Integer.parseInt(new SimpleDateFormat("M").format(today));
		iyear = Integer.parseInt(new SimpleDateFormat("yyyy").format(today));

	}

	// Refresh the action bar menu items
	public static void refreshActionBarMenu(Context cntxt) {
		try {
			Activity act = (Activity) cntxt;
			act.invalidateOptionsMenu();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// updated on 13nov2015 - Validate Time between dates

	/**
	 * check is time valid between 2 date/time params
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static boolean isTimeValid(String date1, String date2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofaction;
		try {
			dateofreg = sdf.parse(date1);
			dateofaction = sdf.parse(date2);

			if (dateofaction.before(dateofreg)) {
				return false;
			} else
				return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Get Current Date and Time
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String getCurrentDateandTime() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String currentDateandTime = sdf.format(new Date());
		return currentDateandTime;
	}

	/**
	 * get days between two dates
	 * 
	 * @param regdate
	 * @param lmpdate
	 * @return
	 */
	public static int getDaysBetweenDates(Date regdate, Date lmpdate) {
		return (int) ((regdate.getTime() - lmpdate.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * get no of days between dates
	 * 
	 * @param regdate
	 * @param lmpdate
	 * @return
	 * @throws Exception
	 */
	public static int getDaysBetweenDates(String regdate, String lmpdate) throws Exception {
		Date regDt = getDateDDMMYYYY(regdate);
		Date lmpDt = getDateDDMMYYYY(lmpdate);
		return getDaysBetweenDates(regDt, lmpDt);
	}

	/**
	 * get date from the string in the format - yyyy-MM-dd
	 * 
	 * @param inputDate
	 * @return
	 * @throws Exception
	 */
	public static Date getDateDDMMYYYY(String inputDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date sendDate = null;
		sendDate = sdf.parse(inputDate);
		return sendDate;
	}

	/**
	 * get Number of weeks
	 * 
	 * @param lmp
	 * @param ADD
	 * @return
	 * @throws ParseException
	 * @throws java.text.ParseException
	 */
	public static int getNumberOfWeeks(String lmp, String ADD) throws ParseException, java.text.ParseException {
		Date lmpDt = new SimpleDateFormat("yyyy-MM-dd").parse(lmp);
		Date ADDate = new SimpleDateFormat("yyyy-MM-dd").parse(ADD);

		Calendar cLmp = Calendar.getInstance();
		cLmp.setTime(lmpDt);
		Calendar cADDate = Calendar.getInstance();
		cADDate.setTime(ADDate);

		long diff = cADDate.getTimeInMillis() - cLmp.getTimeInMillis();

		float dayCount = (float) diff / (24 * 60 * 60 * 1000);

		return (int) (dayCount / 7);
	}

	/**
	 * Copy the DB file to the Database Backup Directory
	 * 
	 * @param context
	 *            the Context of application
	 * @param dbName
	 *            The name of the DB
	 */
	public static void copyDbToExternalStorage(Context context, String dbName, int oldVersion) {

		try {
			File name = context.getDatabasePath(dbName);

			File path = new File(Environment.getExternalStorageDirectory(), "/Partograph/DatabaseBackUp");
			if (!path.exists())
				path.mkdir();

			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_HHmmss");
			String currentDateandTime = sdf.format(new Date());

			String dbnewfilename = "partograph_" + currentDateandTime + "_v" + oldVersion + ".db";// 21Nov2016
																									// -
																									// included
																									// version
																									// number

			File sdcardFile = new File(path + "/" + dbnewfilename);// The name
																	// of output
																	// file
			sdcardFile.createNewFile();
			InputStream inputStream = null;
			OutputStream outputStream = null;
			inputStream = new FileInputStream(name);
			outputStream = new FileOutputStream(sdcardFile);
			byte[] buffer = new byte[1024];
			int read;
			while ((read = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, read);
			}
			inputStream.close();
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	// to add particular time(i.e particular minutes) to the given time
	// updated on 20july2016 by Arpitha
	public static String getnewtime(String time, int minutes) throws java.text.ParseException {
		String newTime;
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		Date d = df.parse(time);
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.MINUTE, minutes);
		newTime = df.format(cal.getTime());
		return newTime;

	}

	// to convert time from 24hors to 12hours format
	// updated on 20july2016 by Arpitha
	public static String gettimein12hrformat(String time) throws Exception {
		String time_12hrs = null;
		try {

			String _24HourTime = time;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			Date _24HourDt = _24HourSDF.parse(_24HourTime);
			time_12hrs = _12HourSDF.format(_24HourDt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time_12hrs;
	}

	// updated on 3August2016 by Arpitha
	public static ArrayList<String> getphonenumber(int option) {
		dbh = new Partograph_DB(context);
		ArrayList<String> phno = new ArrayList<String>();
		phno = dbh.phn_number(Partograph_CommonClass.user.getUserId(), option);
		return phno;
	}

	// 28July2016 - bindu
	public static String getCurrentDateTime() {
		String returnDateStr;
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm");
		returnDateStr = sdf.format(d);
		return returnDateStr;
	}

	// 11Aug2016 - bindu
	public static String SQL6 = "Select * from tblMessageLog " + "Where MsgSent = 0 and NoOfFailures < 1 ";

	public static String SQL7 = "Update tblMessageLog Set msgSentDate = ?, MsgSent = 1 "
			+ " Where UserId = ? and womanId = ? and MsgId = ? ";

	public static String SQL8 = "Update tblMessageLog Set NoOfFailures = (NoOfFailures + 1) "
			+ " Where UserId = ? and womanId = ? and MsgId = ? ";

	// 11Aug2016 - bindu Get Date From String
	public static Date getDateSYYYYMMDD(String inputDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date sendDate = null;
		sendDate = sdf.parse(inputDate);
		return sendDate;
	}

	// 31Aug2016 - bindu - get the number of hours between 2 dates
	public static int getHoursBetDates(String str1, String str2) throws java.text.ParseException {
		Date d1, d2;

		Calendar cal1 = Calendar.getInstance();

		Calendar cal2 = Calendar.getInstance();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm");

		d1 = sdf.parse(str1);
		d2 = sdf.parse(str2);

		cal1.setTime(d1);
		cal2.setTime(d2);

		// Time Difference Calculations Begin
		long milliSec1 = cal1.getTimeInMillis();
		long milliSec2 = cal2.getTimeInMillis();

		long timeDifInMilliSec;
		if (milliSec1 >= milliSec2) {
			timeDifInMilliSec = milliSec1 - milliSec2;
		} else {
			timeDifInMilliSec = milliSec2 - milliSec1;
		}

		long timeDifHours = timeDifInMilliSec / (60 * 60 * 1000);

		return (int) timeDifHours;
	}

	/**
	 * Function to show settings alert dialog On pressing Settings button will
	 * launch Settings Options
	 */// 27Sep2016 Arpitha
	public static void showSettingsAlert(final Context context, String message) throws Exception {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

		// Setting Dialog Title
		alertDialog.setTitle(context.getResources().getString(R.string.date_settings));

		// Setting Dialog Message
		if (message.length() <= 0) {
			alertDialog.setMessage(
					context.getResources().getString(R.string.automatic_date_not_enabled_want_to_go_to_settings));
		} else
			alertDialog.setMessage(
					context.getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));

		// On pressing Settings button
		alertDialog.setPositiveButton(context.getResources().getString(R.string.settings),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(Settings.ACTION_DATE_SETTINGS);
						context.startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton(context.getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						automatic_datetime = false;// 27Sep2016 Arpitha
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	// 27Sep2016 Arpitha
	public static boolean autodatetime(Context context) throws NotFoundException, Exception {
		automatic_datetime = false;// 27Sep2016 Arpitha

		Date now = new SimpleDateFormat("yyyy-MM-dd").parse(Partograph_CommonClass.getTodaysDate());
		if ((Settings.Global.getInt(context.getContentResolver(), Global.AUTO_TIME) == 0
				|| (!now.equals(Partograph_CommonClass.currentdate))) && (!LoginActivity.istesting)) {
			showSettingsAlert(context,
					context.getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));

		} else
			automatic_datetime = true;

		return automatic_datetime;
	}

	// 13Oct2016 Arpitha
	public static void display_messagedialog(final Context context, final String userid) throws Exception {
		try {
			final Dialog sms_dialog = new Dialog(context);
			sms_dialog.setTitle(context.getResources().getString(R.string.send_sms));

			sms_dialog.setContentView(R.layout.sms_dialog);

			dbh = Partograph_DB.getInstance(context);

			sms_dialog.show();

			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
			final EditText etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
			final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);

			String p_no = "";
			ArrayList<String> values;
			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(3);
			p_no = "";
			for (int i = 0; i < values.size(); i++) {

				String s = values.get(i);
				p_no = p_no + values.get(i) + ",";
			}
			etphn.setText(p_no);

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					try {

						if (etphn.getText().toString().trim().length() <= 0) {
							Toast.makeText(context, context.getResources().getString(R.string.enter_valid_phn_no),
									Toast.LENGTH_LONG).show();
						} else if (etmess.getText().toString().trim().length() <= 0) {
							Toast.makeText(context, context.getResources().getString(R.string.enter_message),
									Toast.LENGTH_LONG).show();
						} else {
							sms_dialog.cancel();
							InserttblMessageLog(etphn.getText().toString(), etmess.getText().toString(), userid);
							if (isMessageLogsaved) {
								Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
										Toast.LENGTH_LONG).show();
								sendSMSFunction();

							}
						}

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgcancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sms_dialog.cancel();
				}
			});
			sms_dialog.setCancelable(false);
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

	}

	/**
	 * This method invokes after successfull save of record Sends SMS to the
	 * Specified Number
	 */// 13Oct2016 Arpitha
	private static void sendSMSFunction() throws Exception {
		new SendSMS();

	}

	// }

	// 13Oct2016 Arpitha
	public static void InserttblMessageLog(String phoneno, String content, String user_id) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

		if (phoneno.length() > 0) {
			String[] phn = phoneno.split("\\,");
			for (int i = 0; i < phn.length; i++) {
				String num = phn[i];
				if (num != null && num.length() > 0) {
					MessageLogPojo mlp = new MessageLogPojo(user_id, "", num, "", "", content, 1, 0);
					mlpArr.add(mlp);
				}
			}
		}

		if (mlpArr != null) {
			for (MessageLogPojo mlpp : mlpArr) {
				dbh.db.beginTransaction();
				int transId = dbh.iCreateNewTrans(user_id);// 15Oct2016 Arpitha
				boolean isinserted = dbh.insertToMessageLog(mlpp, transId);// 15Oct2016
																			// Arpitha
				if (isinserted) {
					isMessageLogsaved = true;
					dbh.iNewRecordTrans(user_id, transId, Partograph_DB.TBL_MESSAGELOG);// 15Oct2016
																						// Arpitha
					commitTrans();
				} else
					throw new Exception("InserttblMessageLog(): Failed to Insert Message to tblMessageLog ");
			}
		}

	}

	public static void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		calSyncMtd();
		// Toast.makeText(context,
		// context.getResources().getString(R.string.sending_sms),
		// Toast.LENGTH_SHORT).show();
	}

	// Sync
	private static void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	// 08Nov2016
	// get number of weeks from LMP
	public static String getNumberOfWeeksDays(String lmp, String ADD) throws ParseException {
		String wksdays;
		try {
			Date lmpDt = new SimpleDateFormat("yyyy-MM-dd").parse(lmp);
			Date ADDate = new SimpleDateFormat("yyyy-MM-dd").parse(ADD);

			Calendar cLmp = Calendar.getInstance();
			cLmp.setTime(lmpDt);
			Calendar cADDate = Calendar.getInstance();
			cADDate.setTime(ADDate);

			long diff = cADDate.getTimeInMillis() - cLmp.getTimeInMillis();

			float dayCount = (float) diff / (24 * 60 * 60 * 1000);

			int weeks = (int) (dayCount / 7);

			double d = (dayCount / 7);
			BigDecimal bd = new BigDecimal(d - Math.floor(d));
			bd = bd.setScale(4, RoundingMode.HALF_DOWN);

			double ddays = Double.parseDouble(bd.toString());
			int days = (int) (ddays / 0.142857);

			// String wksdays = (String.valueOf(weeks) +" wks , "+
			// String.valueOf(days) + " days");

			wksdays = ((String.valueOf(weeks)) + "," + (String.valueOf(days)));

		} catch (Exception e) {
			e.printStackTrace();

			return "Error";
		}
		return wksdays;
	}

	// 10Jan2017 Arpitha
	// Async cls to sync
	public static class AsyncCallWS_ChangePassword extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			if (!inetOn)
				System.out.println("Internet :" + context.getResources().getString(R.string.chk_internet));

			else {
				if (Partograph_CommonClass.movedToInputFolder && Partograph_CommonClass.responseAckCount <= 5) {
					if (!SyncFunctions.syncUptoDate) {
						AsyncCallWS_ChangePassword task = new AsyncCallWS_ChangePassword();
						task.execute();
					} else {
						AsyncCallWS_ChangePassword.this.cancel(true);

						try {
							Partograph_CommonClass.sPojo = dbh.getSettingsDetails(user.getUserId());
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				} else {
					AsyncCallWS_ChangePassword.this.cancel(true);

				}

			}

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(context);
				if (inetOn)

					user = dbh.getUserProfile();// 03April2017 Arpitha

				serverResult = new SyncFunctions(context).SyncFunctionUpdateUserDetails(user.getUserId(),
						user.getLastWomennumber(), user.getLastTransNumber(), user.getLastRequestNumber(),
						user.getPwd());

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				return e.getLocalizedMessage();
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// 25Jan2017 Arpitha
	public static String getCureentMonthName() {
		String month_name;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat today = new SimpleDateFormat("MMMM");
		month_name = today.format(cal.getTime());
		return month_name;
	}

	// 25Jan2017 Arpitha
	public static String getPreviousDate(String pattern, int days) {
		String prevDate = null;
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_YEAR, -days);
		Date BeforeDate = cal.getTime();
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		prevDate = format.format(BeforeDate);
		return prevDate;
	}

	// 10May2017 Arpitha- v2.6
	public static void dispalyChangePasswordDialog(final Context context) {
		try {
			// AppContext.addToTrace(new
			// RuntimeException().getStackTrace()[0].getMethodName() + " - "
			// + this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(context);

			dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			dialog.setContentView(R.layout.dialog_changepassword);
			dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setText(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor)
					+ "'>" + context.getResources().getString(R.string.change_password) + "</font>"));
			ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						dialog.cancel();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
			dialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
					+ context.getResources().getString(R.string.change_password) + "</font>"));

			int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = dialog.findViewById(dividerId);
			divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

			dialog.show();

			btnchangepass = (Button) dialog.findViewById(R.id.btnchangepass);
			etoldpass = (EditText) dialog.findViewById(R.id.etoldpass);
			etnewpass = (EditText) dialog.findViewById(R.id.etnewpass);
			etretypepass = (EditText) dialog.findViewById(R.id.etreenterpass);
			//
			// etoldpass.addTextChangedListener(watcher);
			// etnewpass.addTextChangedListener(watcher);
			// etretypepass.addTextChangedListener(watcher);

			btnchangepass.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!ValidateAllFileds()) {

						dbh.db.beginTransaction();
						int transId = dbh.iCreateNewTrans(user.getUserId());

						// 05Jan2017 Arpitha- to encrypt password
						String newpass = etnewpass.getText().toString();

						// byte[] data = null;

						// data = newpass.getBytes("UTF-8");

						// String encrptedPassword = Base64.encodeToString(data,
						// Base64.DEFAULT);

						try {
							dbh.updateNewPassword(newpass, user.getUserId(), transId);
							// SyncFunctionUpdateUserDetails(user.getUserId(),user.getLastWomennumber(),user.getLastTransNumber(),
							// user.getLastRequestNumber() ,user.getPwd());
							//

							// serverResult = new
							// SyncFunctions(getApplicationContext()).SyncFunctionUpdateUserDetails(user.getUserId(),user.getLastWomennumber(),user.getLastTransNumber(),
							// user.getLastRequestNumber()
							// ,etnewpass.getText().toString());

							commitTrans();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
						/*
						 * try { commitTrans(); } catch (Exception e) { // TODO
						 * Auto-generated catch block AppContext.addLog(new
						 * RuntimeException().getStackTrace()[0].getMethodName()
						 * + " - " + this.getClass().getSimpleName(), e);
						 * e.printStackTrace(); }
						 */

						etnewpass.setText("");
						etoldpass.setText("");
						etretypepass.setText("");
						dialog.cancel();
						Toast.makeText(context,
								context.getResources().getString(R.string.new_pass_updated_successfully),
								Toast.LENGTH_LONG).show();

					}

				}

			});

			dialog.setCancelable(false);// 03April2017 Arpitha

		} catch (Exception e) {
			// AppContext.addLog(
			// new RuntimeException().getStackTrace()[0].getMethodName() + " - "
			// + this.getClass().getSimpleName(),
			// e);
			e.printStackTrace();
		}

	}

	// 10May2017 Arpitha- v2.6
	static boolean ValidateAllFileds() {
		isErrorFound = false;

		if (etoldpass.getText().toString().trim().length() > 0) {

			if (!(etoldpass.getText().toString().equals(Partograph_CommonClass.user.getPwd()))) {

				etoldpass.setError(
						context.getResources().getString(R.string.entered_password_does_not_match_stored_password));
				// return false;
				isErrorFound = true;

			}
		} else {
			etoldpass.setError(context.getResources().getString(R.string.enter_old_password));
			isErrorFound = true;
		}

		if (etnewpass.getText().toString().trim().length() <= 0) {
			etnewpass.setError(context.getResources().getString(R.string.enter_new_password));
			isErrorFound = true;
		} else {
			if (etoldpass.getText().toString().equals(etnewpass.getText().toString())) {
				etnewpass.setError(context.getResources().getString(R.string.new_password_is_same_as_old_password));
				isErrorFound = true;
			}

			else if (etnewpass.getText().length() < 6) {
				etnewpass.setError(
						context.getResources().getString(R.string.new_password_must_contain_atleast_six_characters));
				isErrorFound = true;
			}
		}

		if (etretypepass.getText().toString().trim().length() <= 0) {
			etretypepass.setError(context.getResources().getString(R.string.re_enter_new_password));
			isErrorFound = true;
		} else {
			if (!(etnewpass.getText().toString().equals(etretypepass.getText().toString()))) {
				etretypepass.setError(
						context.getResources().getString(R.string.reentered_pass_does_not_match_new_password));
				isErrorFound = true;

			}
			// 03April2017 Arpitha
			if (etoldpass.getText().toString().equals(etretypepass.getText().toString())) {
				etretypepass.setError(context.getResources().getString(R.string.new_password_is_same_as_old_password));
				isErrorFound = true;
			} // 03April2017 Arpitha
		}
		return isErrorFound;

	}

	// public static void loadListDataFHR(String strWomenid, String strwuserId,
	// Context context) throws Exception {
	// context = context;
	//
	// dbh = Partograph_DB.getInstance(context);
	//
	// if (!(LoginActivity.objectid.contains(1)))
	//
	// {
	// listValuesItemsFHR = new ArrayList<Add_value_pojo>();
	// listValuesItemsFHR = dbh.getFHRGraphData(1, strWomenid, strwuserId);//
	// 12Oct2016
	// // Arpitha
	// createChart(context);
	// }
	// }

	// Create the graph
	public static GraphicalView createChart(String strWomenid, String strwuserId, Context context, int objId) {
		// if (listValuesItemsFHR.size() > 0) {02Nov2016 Arpitha
		// tbllayoutfhr.setVisibility(View.VISIBLE);
		try {

			context = context;

			dbh = Partograph_DB.getInstance(context);

			// if (!(LoginActivity.objectid.contains(1)))
			//
			// {
			listValuesItemsFHR = new ArrayList<Add_value_pojo>();
			listValuesItemsFHR = dbh.getFHRGraphData(objId, strWomenid, strwuserId);// 12Oct2016
			// Arpitha
			// createChart(context);

			String yLbl = context.getResources().getString(R.string.fhr);

			// Creating an XYSeries for values
			XYSeries valueseries = new XYSeries("Values");

			for (int i = 0; i < listValuesItemsFHR.size(); i++) {
				valueseries.add(i + 1, Double.parseDouble(listValuesItemsFHR.get(i).getStrVal()));

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(valueseries);

			// Creating XYSeriesRenderer to customize valueSeries
			XYSeriesRenderer valueRenderer = new XYSeriesRenderer();
			valueRenderer.setColor(Color.BLACK);
			valueRenderer.setPointStyle(PointStyle.CIRCLE);
			valueRenderer.setFillPoints(true);
			valueRenderer.setLineWidth(4);
			valueRenderer.setDisplayChartValues(true);
			valueRenderer.setChartValuesTextSize(15);

			// Creating a XYMultipleSeriesRenderer to customize the whole
			// chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle(context.getResources().getString(R.string.fetal_heart_rate));
			multiRenderer.setChartTitleTextSize(15);
			multiRenderer.setXTitle(context.getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(yLbl);
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(15);
			multiRenderer.setLegendTextSize(15);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 60, 80, 60, 60 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(15);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(false, false);

			// updated on 17Nov2015 - YAxisMin from 80 to 40
			multiRenderer.setYAxisMin(40);

			multiRenderer.setFitLegend(true);
			multiRenderer.setShowGrid(true);

			multiRenderer.setShowGridX(true);
			multiRenderer.setShowGridY(true);
			multiRenderer.setGridColor(context.getResources().getColor(R.color.lightgray));

			multiRenderer.setBarSpacing(.5);
			multiRenderer.setXLabelsAngle(45f);

			// 06Jan2016
			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);

			// multiRenderer.setShowCustomTextGrid(true);
			multiRenderer.setZoomEnabled(true);

			// 02Nov2016 Arpitha
			multiRenderer.setYAxisMax(300);
			multiRenderer.setYLabels(15);// 02Nov2016 Arpitha

			for (int i = 0; i < listValuesItemsFHR.size(); i++) {
				multiRenderer.addXTextLabel(i + 1, listValuesItemsFHR.get(i).getStrTime());

				// * multiRenderer.setYAxisMax(300);
				// multiRenderer.setYLabels(15);
				// 02Nov2016 Arpitha
			}

			// 08Nov2016
			if (listValuesItemsFHR.size() <= 0) {
				for (int i = 0; i < 2; i++) {
					valueseries.add(i + 1, i);

				}
			} // 08Nov2016

			multiRenderer.addSeriesRenderer(valueRenderer);

			mChartViewFHR = null;
			mChartViewFHR = ChartFactory.getLineChartView(context, dataset, multiRenderer);

			// }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mChartViewFHR;
	}

	static public GraphicalView createDilatationGraph(String womenId, String userId, Context context, int objId)
			throws ParseException, java.text.ParseException {
		// if (!(LoginActivity.objectid.contains(objId))) {
		listValuesItemsDilatation = dbh.getDilatationData(objId, womenId, userId, true);

		// if (listValuesItemsDilatation.size() > 0) {
		// 31Aug2016 - bindu
		getValuesForDilGraphHours();

		// Creating an XYSeries for values
		XYSeries cervixseries = new XYSeries(context.getResources().getString(R.string.cervix));
		XYSeries desc_headseries = new XYSeries(context.getResources().getString(R.string.desc_head));

		XYSeries alertline = new XYSeries(context.getResources().getString(R.string.alertline));
		XYSeries actionline = new XYSeries(context.getResources().getString(R.string.actionline));

		int[] x = { 0, 1, 2, 3, 4, 5, 6 };
		int[] alertval = { 4, 5, 6, 7, 8, 9, 10 };

		int[] xactionline = { 4, 5, 6, 7, 8, 9, 10 };
		int[] actionval = { 4, 5, 6, 7, 8, 9, 10 };

		double descofhead = 0;

		// 31Aug2016 - bindu
		for (Map.Entry<Integer, Double> cervixval : newDilValuesCervix.entrySet()) {
			cervixseries.add(cervixval.getKey(), cervixval.getValue());
		}
		// 31Aug2016 - bindu
		for (Map.Entry<Integer, Double> desval : newDilValuesDescent.entrySet()) {
			desc_headseries.add(desval.getKey(), desval.getValue());
		}

		for (int i = 0; i < x.length; i++) {
			alertline.add(x[i], alertval[i]);
		}

		for (int i = 0; i < xactionline.length; i++) {
			actionline.add(xactionline[i], actionval[i]);
		}
		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding value Series to the dataset
		dataset.addSeries(cervixseries);
		dataset.addSeries(desc_headseries);
		dataset.addSeries(alertline);
		dataset.addSeries(actionline);

		// Creating XYSeriesRenderer to customize alertlineSeries
		XYSeriesRenderer alertRenderer = new XYSeriesRenderer();
		alertRenderer.setColor(context.getResources().getColor(R.color.amber));
		alertRenderer.setPointStyle(PointStyle.POINT);
		alertRenderer.setFillPoints(true);
		alertRenderer.setLineWidth(5);
		alertRenderer.setDisplayChartValues(false);

		// Creating XYSeriesRenderer to customize actionlineSeries
		XYSeriesRenderer actionRenderer = new XYSeriesRenderer();
		actionRenderer.setColor(context.getResources().getColor(R.color.red));
		actionRenderer.setPointStyle(PointStyle.POINT);
		actionRenderer.setFillPoints(true);
		actionRenderer.setLineWidth(5);
		actionRenderer.setDisplayChartValues(false);

		// Creating XYSeriesRenderer to customize pulseSeries
		XYSeriesRenderer cervixRenderer = new XYSeriesRenderer();
		cervixRenderer.setColor(context.getResources().getColor(R.color.springgreen));
		cervixRenderer.setPointStyle(PointStyle.TRIANGLE);
		cervixRenderer.setFillPoints(true);
		cervixRenderer.setLineWidth(3);
		cervixRenderer.setDisplayChartValues(true);
		cervixRenderer.setChartValuesTextSize(20);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer descheadRenderer = new XYSeriesRenderer();
		descheadRenderer.setColor(context.getResources().getColor(R.color.steel_blue));
		descheadRenderer.setPointStyle(PointStyle.CIRCLE);
		descheadRenderer.setFillPoints(true);
		descheadRenderer.setLineWidth(3);
		descheadRenderer.setDisplayChartValues(true);
		descheadRenderer.setChartValuesTextSize(15);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		multiRenderer.setChartTitle(context.getResources().getString(R.string.dilatation));
		multiRenderer.setChartTitleTextSize(15);
		multiRenderer.setXTitle(context.getResources().getString(R.string.txttime));
		multiRenderer.setYTitle(context.getResources().getString(R.string.cervixanddescentofhead));
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setLabelsTextSize(15);
		multiRenderer.setLegendTextSize(15);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setMargins(new int[] { 30, 60, 60, 30 });
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.WHITE);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setAxisTitleTextSize(15);
		multiRenderer.setPointSize(5);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setShowGrid(true);
		// Disable Pan on two axis
		multiRenderer.setPanEnabled(false, false);
		multiRenderer.setYAxisMax(10);
		multiRenderer.setYAxisMin(0);
		multiRenderer.setYLabels(11);
		multiRenderer.setXAxisMin(0);
		multiRenderer.setXAxisMax(12);

		/*
		 * // 31Aug2016 - bindu for (int i = 0; i < 13; i++) { for
		 * (Map.Entry<Integer, Double> xaxisval : newDilValuesCervix.entrySet())
		 * { if (i == xaxisval.getKey())
		 * multiRenderer.addXTextLabel(xaxisval.getKey(), "" +
		 * xaxisval.getKey()); else multiRenderer.addXTextLabel((i), "" + (i));
		 * } }
		 */
		// 18Aug2017 arpitha
		for (int i = 0; i < 12; i++) {
			for (Map.Entry<Integer, Double> xaxisval : newDilValuesCervix.entrySet()) {
				// String val = "" + (xaxisval.getKey()+1);
				if (i == xaxisval.getKey()) {
					int xa = xaxisval.getKey() + 1;
					String val = "" + xa;
					multiRenderer.addXTextLabel(xaxisval.getKey(), val);
				} else
					multiRenderer.addXTextLabel((i), "" + (i + 1));
			}
		}

		// 08Nov2016
		if (listValuesItemsDilatation.size() <= 0) {
			for (int i = 0; i < 2; i++) {
				// cervixseries.add(i + 1, i);//commented on 29Sep2017 - some
				// line was disaplying

			}
		} // 08Nov2016
		multiRenderer.addSeriesRenderer(cervixRenderer);
		multiRenderer.addSeriesRenderer(descheadRenderer);
		multiRenderer.addSeriesRenderer(alertRenderer);
		multiRenderer.addSeriesRenderer(actionRenderer);

		mChartViewDilatation = null;
		mChartViewDilatation = ChartFactory.getLineChartView(context, dataset, multiRenderer);

		// * } else tbllayoutdil.setVisibility(View.GONE);

		// }
		return mChartViewDilatation;

	}

	private static void getValuesForDilGraphHours() throws ParseException, java.text.ParseException {
		newDilValuesCervix = new LinkedHashMap<Integer, Double>();
		newDilValuesDescent = new LinkedHashMap<Integer, Double>();
		int hrs = 0;
		String str1, str2;
		if (listValuesItemsDilatation != null && listValuesItemsDilatation.size() > 0) {
			for (int i = 0; i < listValuesItemsDilatation.size(); i++) {
				if (i == 0) {
					// hrs = 1; commented on 05May2017 Arpitha v2.6 for - mantis
					// id 0000236
					newDilValuesCervix.put(hrs, Double.parseDouble(listValuesItemsDilatation.get(i).getCervix()));
					newDilValuesDescent.put(hrs,
							Double.parseDouble(listValuesItemsDilatation.get(i).getDesc_of_head()));
					hrs = 0;
				} else {
					str1 = listValuesItemsDilatation.get(i).getStrdate() + "_"
							+ listValuesItemsDilatation.get(i).getStrTime();
					str2 = listValuesItemsDilatation.get(i - 1).getStrdate() + "_"
							+ listValuesItemsDilatation.get(i - 1).getStrTime();
					hrs = hrs + Partograph_CommonClass.getHoursBetDates(str1, str2);
					newDilValuesCervix.put(hrs, Double.parseDouble(listValuesItemsDilatation.get(i).getCervix()));
					newDilValuesDescent.put(hrs,
							Double.parseDouble(listValuesItemsDilatation.get(i).getDesc_of_head()));
				}
			}
		}

	}

	public static TableLayout dispalyAFMData(Context context, int objId, String womenId, String userId,
			TableLayout tbllayoutafm) throws Exception {

		// if (!(LoginActivity.objectid.contains(2))) {
		listValuesItemsafm = dbh.getAFMData(2, womenId, userId);

		// if (listValuesItemsafm.size() > 0) {02Nov2016 Arpitha

		tbllayoutafm.removeAllViews();

		TableRow trlabel = new TableRow(context);
		trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

		TextView texttimelbl = new TextView(context);
		texttimelbl.setTextSize(16);
		texttimelbl.setPadding(10, 10, 10, 10);
		texttimelbl.setTextColor(context.getResources().getColor(R.color.black));
		texttimelbl.setTextSize(16);
		texttimelbl.setGravity(Gravity.CENTER);
		trlabel.addView(texttimelbl);

		TextView textdatelbl = new TextView(context);
		textdatelbl.setTextSize(16);
		textdatelbl.setPadding(10, 10, 10, 10);
		textdatelbl.setTextColor(context.getResources().getColor(R.color.black));
		textdatelbl.setTextSize(16);
		textdatelbl.setGravity(Gravity.CENTER);
		trlabel.addView(textdatelbl);

		TextView textval1lbl = new TextView(context);
		textval1lbl.setTextSize(16);
		textval1lbl.setPadding(10, 10, 10, 10);
		textval1lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval1lbl.setTextSize(16);
		textval1lbl.setGravity(Gravity.CENTER);
		trlabel.addView(textval1lbl);

		TextView textval2lbl = new TextView(context);
		textval2lbl.setTextSize(16);
		textval2lbl.setPadding(10, 10, 10, 10);
		textval2lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval2lbl.setTextSize(16);
		textval2lbl.setGravity(Gravity.CENTER);
		trlabel.addView(textval2lbl);

		texttimelbl.setText(context.getResources().getString(R.string.txttime));
		textdatelbl.setText(context.getResources().getString(R.string.txtdate));

		TableRow tr = null;// 02Nov2016 Arpitha
		for (int i = 0; i < listValuesItemsafm.size(); i++) {
			tr = new TableRow(context);
			/* TableRow tr = new TableRow(getApplicationContext()); */
			tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView txttime = new TextView(context);
			txttime.setText(listValuesItemsafm.get(i).getStrTime());
			txttime.setTextSize(16);
			txttime.setPadding(10, 10, 10, 10);
			txttime.setTextColor(context.getResources().getColor(R.color.black));
			txttime.setTextSize(16);
			txttime.setGravity(Gravity.CENTER);
			tr.addView(txttime);

			TextView txtdate = new TextView(context);
			txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItemsafm.get(i).getStrdate(),
					Partograph_CommonClass.defdateformat));
			txtdate.setTextSize(16);
			txtdate.setPadding(10, 10, 10, 10);
			txtdate.setTextColor(context.getResources().getColor(R.color.black));
			txtdate.setTextSize(16);
			txtdate.setGravity(Gravity.CENTER);
			tr.addView(txtdate);

			String val1 = "", val2 = "";

			val1 = listValuesItemsafm.get(i).getAmnioticfluid();
			val2 = listValuesItemsafm.get(i).getMoulding();

			if (val1.equals("0"))
				val1 = context.getResources().getString(R.string.c);
			else if (val1.equals("1"))
				val1 = context.getResources().getString(R.string.m);
			// 26Nov2015
			else if (val1.equals("2"))
				val1 = context.getResources().getString(R.string.i);
			else if (val1.equals("3"))
				val1 = context.getResources().getString(R.string.b);

			if (val2.equalsIgnoreCase("0"))
				val2 = context.getResources().getString(R.string.m1);
			else if (val2.equalsIgnoreCase("1"))
				val2 = context.getResources().getString(R.string.m2);
			else
				val2 = context.getResources().getString(R.string.m3);

			TextView txtvalue1 = new TextView(context);
			txtvalue1.setText(val1);
			txtvalue1.setTextSize(16);
			txtvalue1.setPadding(10, 10, 10, 10);
			txtvalue1.setTextColor(context.getResources().getColor(R.color.black));
			txtvalue1.setTextSize(16);
			txtvalue1.setGravity(Gravity.CENTER);

			if (val1.equalsIgnoreCase(context.getResources().getString(R.string.c)))
				txtvalue1.setTextColor(context.getResources().getColor(R.color.green));
			else if (val1.equalsIgnoreCase(context.getResources().getString(R.string.m)))
				txtvalue1.setTextColor(context.getResources().getColor(R.color.red));

			tr.addView(txtvalue1);

			TextView txtvalue2 = new TextView(context);

			txtvalue2.setTextColor(context.getResources().getColor(R.color.black));

			txtvalue2.setText(val2);
			txtvalue2.setTextSize(16);
			txtvalue2.setPadding(10, 10, 10, 10);
			txtvalue2.setTextSize(16);
			txtvalue2.setGravity(Gravity.CENTER);
			tr.addView(txtvalue2);
			/*
			 * tbllayoutafm.addView(tr); }
			 */
			View view3 = new View(context);
			view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view3.setBackgroundColor(Color.DKGRAY);
			tbllayoutafm.addView(view3);

			if (i == 0) {// 02Nov2016 Arpitha
				tbllayoutafm.addView(trlabel);
				View view1 = new View(context);
				view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view1.setBackgroundColor(Color.DKGRAY);
				tbllayoutafm.addView(view1);

			}
			tbllayoutafm.addView(tr);
		}

		// }//02Nov2016 Arpitha

		/* tbllayoutafm.addView(tr); */
		// 02Nov2016 Arpitha
		textval1lbl.setText(context.getResources().getString(R.string.amniotic_fluid));
		textval2lbl.setText(context.getResources().getString(R.string.moulding));

		if (listValuesItemsafm.size() <= 0) {

			tbllayoutafm.addView(trlabel);
			View view1 = new View(context);
			view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view1.setBackgroundColor(Color.DKGRAY);
			tbllayoutafm.addView(view1);
			TableRow tr_empty = new TableRow(context);
			tr_empty.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));
			// tbllayoutafm.addView(tr_empty);

			TextView texttimelbl1 = new TextView(context);
			texttimelbl1.setTextSize(16);
			texttimelbl1.setPadding(10, 10, 10, 10);
			texttimelbl1.setTextColor(context.getResources().getColor(R.color.black));
			texttimelbl1.setTextSize(16);
			texttimelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(texttimelbl1);

			TextView textdatelbl1 = new TextView(context);
			textdatelbl1.setTextSize(16);
			textdatelbl1.setPadding(10, 10, 10, 10);
			textdatelbl1.setTextColor(context.getResources().getColor(R.color.black));
			textdatelbl1.setTextSize(16);
			textdatelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textdatelbl1);

			TextView textval1lbl1 = new TextView(context);
			textval1lbl1.setTextSize(16);
			textval1lbl1.setPadding(10, 10, 10, 10);
			textval1lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval1lbl1.setTextSize(16);
			textval1lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval1lbl1);

			TextView textval2lbl1 = new TextView(context);
			textval2lbl1.setTextSize(16);
			textval2lbl1.setPadding(10, 10, 10, 10);
			textval2lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval2lbl1.setTextSize(16);
			textval2lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval2lbl1);
			tbllayoutafm.addView(tr_empty);
			View view4 = new View(context);
			view4.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view4.setBackgroundColor(Color.DKGRAY);
			tbllayoutafm.addView(view4);

		}

		// }

		return tbllayoutafm;

	}

	public static GraphicalView DisplayContraction(int objId, String womenId, String userId, Context context) {
		// if (!(LoginActivity.objectid.contains(4))) {
		listValuesItemsContractions = dbh.getContractionData(4, womenId, userId, true);

		// if (listValuesItemsContractions.size() > 0) {02nov2016 Arpitha
		// tbllayoutcontract.setVisibility(View.VISIBLE);
		// Creating an XYSeries for values
		XYSeries cont_L20sec = new XYSeries("Less than 20 sec");
		XYSeries cont_B20_40sec = new XYSeries("Between 20 and 40 sec");
		XYSeries cont_M40sec = new XYSeries("More than 40 sec");
		XYSeries cont_M60sec = new XYSeries("More than 60 sec");

		for (int i = 0; i < listValuesItemsContractions.size(); i++) {
			if (listValuesItemsContractions.get(i).getDuration() < 20)
				cont_L20sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));
			if (listValuesItemsContractions.get(i).getDuration() >= 20
					&& listValuesItemsContractions.get(i).getDuration() <= 40) {
				cont_B20_40sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));
			}

			if (listValuesItemsContractions.get(i).getDuration() > 40
					&& listValuesItemsContractions.get(i).getDuration() <= 60)
				cont_M40sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));

			if (listValuesItemsContractions.get(i).getDuration() > 60) {
				cont_M60sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));
			}

		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding value Series to the dataset
		dataset.addSeries(cont_L20sec);
		dataset.addSeries(cont_B20_40sec);
		dataset.addSeries(cont_M40sec);
		dataset.addSeries(cont_M60sec);

		// Creating XYSeriesRenderer to customize contractions < 20 sec
		XYSeriesRenderer contraction_L20Renderer = new XYSeriesRenderer();
		contraction_L20Renderer.setColor(context.getResources().getColor(R.color.vividorange));
		contraction_L20Renderer.setPointStyle(PointStyle.DIAMOND);
		contraction_L20Renderer.setFillPoints(true);
		contraction_L20Renderer.setLineWidth((float) 5.5d);
		contraction_L20Renderer.setDisplayChartValues(true);
		contraction_L20Renderer.setChartValuesTextSize(15);

		// contraction_L20Renderer.setFillBelowLine(true);
		// contraction_L20Renderer.setFillBelowLineColor(Color.CYAN);

		// Creating XYSeriesRenderer to customize contractions bet 20 and 40
		XYSeriesRenderer contraction_B20_40Renderer = new XYSeriesRenderer();
		contraction_B20_40Renderer.setColor(context.getResources().getColor(R.color.darkgreen));
		contraction_B20_40Renderer.setPointStyle(PointStyle.CIRCLE);
		contraction_B20_40Renderer.setFillPoints(true);
		contraction_B20_40Renderer.setLineWidth((float) 10.5d);
		contraction_B20_40Renderer.setDisplayChartValues(true);
		contraction_B20_40Renderer.setChartValuesTextSize(15);
		// contraction_B20_40Renderer.setFillBelowLine(true);
		// contraction_B20_40Renderer.setFillBelowLineColor(Color.MAGENTA);

		// Creating XYSeriesRenderer to customize contractions > 40
		XYSeriesRenderer contraction_M40Renderer = new XYSeriesRenderer();
		contraction_M40Renderer.setColor(context.getResources().getColor(R.color.darkred));
		contraction_M40Renderer.setPointStyle(PointStyle.CIRCLE);
		contraction_M40Renderer.setFillPoints(true);
		contraction_M40Renderer.setLineWidth((float) 10.5d);
		contraction_M40Renderer.setDisplayChartValues(true);
		contraction_M40Renderer.setChartValuesTextSize(15);
		// contraction_M40Renderer.setFillBelowLine(true);
		// contraction_M40Renderer.setFillBelowLineColor(Color.GRAY);

		// Creating XYSeriesRenderer to customize contractions > 40
		XYSeriesRenderer contraction_M60Renderer = new XYSeriesRenderer();
		contraction_M60Renderer.setColor(context.getResources().getColor(R.color.red));
		contraction_M60Renderer.setPointStyle(PointStyle.CIRCLE);
		contraction_M60Renderer.setFillPoints(true);
		contraction_M60Renderer.setLineWidth((float) 10.5d);
		contraction_M60Renderer.setDisplayChartValues(true);
		contraction_M60Renderer.setChartValuesTextSize(15);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		multiRenderer.setChartTitle(context.getResources().getString(R.string.contraction));
		multiRenderer.setChartTitleTextSize(15);
		multiRenderer.setXTitle(context.getResources().getString(R.string.txttime));
		multiRenderer.setYTitle(context.getResources().getString(R.string.contraction));
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setLabelsTextSize(15);
		multiRenderer.setLegendTextSize(15);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setMargins(new int[] { 30, 60, 60, 30 });
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.WHITE);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setAxisTitleTextSize(15);
		multiRenderer.setPointSize(10);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setShowGrid(true);
		// Disable Pan on two axis
		multiRenderer.setPanEnabled(false, false);
		multiRenderer.setYAxisMax(11);
		multiRenderer.setYAxisMin(1);
		multiRenderer.setGridColor(context.getResources().getColor(R.color.lightgray));
		multiRenderer.setYLabels(15);
		multiRenderer.setFitLegend(true);
		multiRenderer.setShowGrid(true);
		// multiRenderer.setAntialiasing(true);
		// multiRenderer.setInScroll(true);
		multiRenderer.setShowGridX(true);
		multiRenderer.setShowGridY(true);

		multiRenderer.setXAxisMin(0);
		multiRenderer.setXAxisMax(12);
		// multiRenderer.setZoomButtonsVisible(true);
		multiRenderer.setBarSpacing(.5);
		multiRenderer.setBarWidth(15);

		// multiRenderer.setScale(3f);

		multiRenderer.setShowCustomTextGrid(true);

		for (int i = 0; i < listValuesItemsContractions.size(); i++) {

			multiRenderer.addXTextLabel(i + 1, listValuesItemsContractions.get(i).getStrTime() + "\n"
					+ listValuesItemsContractions.get(i).getDuration());
			//
			// multiRenderer.

			// * multiRenderer.setYAxisMax(10); multiRenderer.setYLabels(10);
			// 02nov2016 Arpitha
		}

		// 10nov2016 Arpitha
		if (listValuesItemsContractions.size() <= 0) {
			for (int i = 0; i < 1; i++) {// 22Nov2016 Arpitha - Changed i<2 to
											// i<1
				cont_L20sec.add(i + 1, i);

			}
		}

		multiRenderer.setYAxisMax(10);
		multiRenderer.setYLabels(10);// 10nov2016 Arpitha

		multiRenderer.addSeriesRenderer(contraction_L20Renderer);
		multiRenderer.addSeriesRenderer(contraction_B20_40Renderer);
		multiRenderer.addSeriesRenderer(contraction_M40Renderer);
		multiRenderer.addSeriesRenderer(contraction_M60Renderer);

		mChartViewContraction = null;

		mChartViewContraction = ChartFactory.getBarChartView(context, dataset, multiRenderer, Type.DEFAULT);

		// * } else tbllayoutcontract.setVisibility(View.GONE);
		// 02Nov2016 Arpitha

		// }
		return mChartViewContraction;

	}

	public static TableLayout DisplayOxytocinData(Context context, int objectId, String strWomenid, String strwuserId,
			TableLayout tbllayoutoxytocin) throws Exception {

		// if (!(LoginActivity.objectid.contains(objectId))) {
		listValuesItemsoxytocin = dbh.getOxytocinData(objectId, strWomenid, strwuserId);

		tbllayoutoxytocin.setVisibility(View.VISIBLE);

		// if (listValuesItemsoxytocin.size() > 0) {//02Nov2016 Arpitha

		tbllayoutoxytocin.removeAllViews();

		TableRow trlabel = new TableRow(context);
		trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

		TextView texttimelbl = new TextView(context.getApplicationContext());
		texttimelbl.setPadding(10, 10, 10, 10);
		texttimelbl.setTextColor(context.getResources().getColor(R.color.black));
		texttimelbl.setTextSize(16);
		texttimelbl.setGravity(Gravity.CENTER);
		trlabel.addView(texttimelbl);

		TextView textdatelbl = new TextView(context);
		textdatelbl.setPadding(10, 10, 10, 10);
		textdatelbl.setTextColor(context.getResources().getColor(R.color.black));
		textdatelbl.setTextSize(16);
		textdatelbl.setGravity(Gravity.CENTER);
		trlabel.addView(textdatelbl);

		TextView textval1lbl = new TextView(context);
		textval1lbl.setPadding(10, 10, 10, 10);
		textval1lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval1lbl.setTextSize(16);
		textval1lbl.setGravity(Gravity.CENTER);
		trlabel.addView(textval1lbl);

		// Add feb04
		TextView textval2lbl = new TextView(context);
		textval2lbl.setTextSize(16);
		textval2lbl.setPadding(10, 10, 10, 10);
		textval2lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval2lbl.setGravity(Gravity.CENTER);
		textval2lbl.setText(context.getResources().getString(R.string.units));
		trlabel.addView(textval2lbl);

		texttimelbl.setText(context.getResources().getString(R.string.txttime));
		textdatelbl.setText(context.getResources().getString(R.string.txtdate));

		TableRow tr;

		View view1 = new View(context);
		view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
		view1.setBackgroundColor(Color.DKGRAY);
		tbllayoutoxytocin.addView(view1);
		tbllayoutoxytocin.addView(trlabel);
		for (int i = 0; i < listValuesItemsoxytocin.size(); i++) {
			tr = new TableRow(context);
			tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView txttime = new TextView(context);
			txttime.setText(listValuesItemsoxytocin.get(i).getStrTime());
			txttime.setTextSize(16);
			txttime.setPadding(10, 10, 10, 10);
			txttime.setTextColor(context.getResources().getColor(R.color.black));
			txttime.setGravity(Gravity.CENTER);
			tr.addView(txttime);

			TextView txtdate = new TextView(context);
			// txtdate.setText(listValuesItems.get(i).getStrdate());
			txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItemsoxytocin.get(i).getStrdate(),
					Partograph_CommonClass.defdateformat));
			txtdate.setTextSize(16);
			txtdate.setPadding(10, 10, 10, 10);
			txtdate.setTextColor(context.getResources().getColor(R.color.black));
			txtdate.setGravity(Gravity.CENTER);
			tr.addView(txtdate);

			String val1 = "", val2 = "";

			val1 = listValuesItemsoxytocin.get(i).getOxytocindrops();
			val2 = listValuesItemsoxytocin.get(i).getOxytocinunits();

			// *
			// textval1lbl.setText(getResources().getString(R.string.oxytocin));

			TextView txtvalue1 = new TextView(context);
			txtvalue1.setText(val1);
			txtvalue1.setPadding(10, 10, 10, 10);
			txtvalue1.setTextColor(context.getResources().getColor(R.color.black));
			txtvalue1.setTextSize(16);
			txtvalue1.setGravity(Gravity.CENTER);
			tr.addView(txtvalue1);

			TextView txtvalue2 = new TextView(context);
			txtvalue2.setText(val2);
			txtvalue2.setPadding(10, 10, 10, 10);
			txtvalue2.setTextColor(context.getResources().getColor(R.color.black));
			txtvalue2.setTextSize(16);
			txtvalue2.setGravity(Gravity.CENTER);
			tr.addView(txtvalue2);
			// }

			View view3 = new View(context);
			view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view3.setBackgroundColor(Color.DKGRAY);
			tbllayoutoxytocin.addView(view3);

			if (listValuesItemsoxytocin.size() <= 0) {
				TableRow tr_empty = new TableRow(context);
				tr_empty.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));
				// tbllayoutafm.addView(tr_empty);

				TextView texttimelbl1 = new TextView(context);
				texttimelbl1.setTextSize(16);
				texttimelbl1.setPadding(10, 10, 10, 10);
				texttimelbl1.setTextColor(context.getResources().getColor(R.color.black));
				texttimelbl1.setTextSize(16);
				texttimelbl1.setGravity(Gravity.CENTER);
				tr_empty.addView(texttimelbl1);

				TextView textdatelbl1 = new TextView(context);
				textdatelbl1.setTextSize(16);
				textdatelbl1.setPadding(10, 10, 10, 10);
				textdatelbl1.setTextColor(context.getResources().getColor(R.color.black));
				textdatelbl1.setTextSize(16);
				textdatelbl1.setGravity(Gravity.CENTER);
				tr_empty.addView(textdatelbl1);

				TextView textval1lbl1 = new TextView(context);
				textval1lbl1.setTextSize(16);
				textval1lbl1.setPadding(10, 10, 10, 10);
				textval1lbl1.setTextColor(context.getResources().getColor(R.color.black));
				textval1lbl1.setTextSize(16);
				textval1lbl1.setGravity(Gravity.CENTER);
				tr_empty.addView(textval1lbl1);

				TextView textval2lbl1 = new TextView(context);
				textval2lbl1.setTextSize(16);
				textval2lbl1.setPadding(10, 10, 10, 10);
				textval2lbl1.setTextColor(context.getResources().getColor(R.color.black));
				textval2lbl1.setTextSize(16);
				textval2lbl1.setGravity(Gravity.CENTER);
				tr_empty.addView(textval2lbl1);
				tbllayoutoxytocin.addView(tr_empty);
				View view4 = new View(context);
				view4.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view4.setBackgroundColor(Color.DKGRAY);
				tbllayoutoxytocin.addView(view4);
			}

			tbllayoutoxytocin.addView(tr);
		}

		if (listValuesItemsoxytocin.size() <= 0) {
			TableRow tr_empty = new TableRow(context);
			tr_empty.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));
			// tbllayoutafm.addView(tr_empty);

			TextView texttimelbl1 = new TextView(context);
			texttimelbl1.setTextSize(16);
			texttimelbl1.setPadding(10, 10, 10, 10);
			texttimelbl1.setTextColor(context.getResources().getColor(R.color.black));
			texttimelbl1.setTextSize(16);
			texttimelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(texttimelbl1);

			TextView textdatelbl1 = new TextView(context);
			textdatelbl1.setTextSize(16);
			textdatelbl1.setPadding(10, 10, 10, 10);
			textdatelbl1.setTextColor(context.getResources().getColor(R.color.black));
			textdatelbl1.setTextSize(16);
			textdatelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textdatelbl1);

			TextView textval1lbl1 = new TextView(context);
			textval1lbl1.setTextSize(16);
			textval1lbl1.setPadding(10, 10, 10, 10);
			textval1lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval1lbl1.setTextSize(16);
			textval1lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval1lbl1);

			TextView textval2lbl1 = new TextView(context);
			textval2lbl1.setTextSize(16);
			textval2lbl1.setPadding(10, 10, 10, 10);
			textval2lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval2lbl1.setTextSize(16);
			textval2lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval2lbl1);

			View view4 = new View(context);
			view4.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view4.setBackgroundColor(Color.DKGRAY);
			tbllayoutoxytocin.addView(view4);
			tbllayoutoxytocin.addView(tr_empty);
			View view5 = new View(context);
			view5.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view5.setBackgroundColor(Color.DKGRAY);
			tbllayoutoxytocin.addView(view5);
		}
		textval1lbl.setText(context.getResources().getString(R.string.oxytocin));

		// * } } else tbllayoutoxytocin.setVisibility(View.GONE);
		// 02Nov2016 Arpitha

		// }
		return tbllayoutoxytocin;

	}

	public static TableLayout DisplayDrugsData(TableLayout tbllayoutdrugs, int objId, String strWomenid,
			String strwuserId, Context context) throws Exception {

		listValuesItemsdrugs = dbh.getDrugdata(6, strWomenid, strwuserId);

		// if (listValuesItemsdrugs.size() > 0) {

		tbllayoutdrugs.removeAllViews();

		TableRow trlabel = new TableRow(context);
		trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

		TextView texttimelbl = new TextView(context);
		texttimelbl.setTextSize(16);
		texttimelbl.setPadding(10, 10, 10, 10);
		texttimelbl.setTextColor(context.getResources().getColor(R.color.black));
		texttimelbl.setTextSize(16);
		texttimelbl.setGravity(Gravity.CENTER);
		trlabel.addView(texttimelbl);

		TextView textdatelbl = new TextView(context);
		textdatelbl.setTextSize(16);
		textdatelbl.setPadding(10, 10, 10, 10);
		textdatelbl.setTextColor(context.getResources().getColor(R.color.black));
		textdatelbl.setTextSize(16);
		textdatelbl.setGravity(Gravity.CENTER);
		trlabel.addView(textdatelbl);

		TextView textval1lbl = new TextView(context);
		textval1lbl.setTextSize(16);
		textval1lbl.setPadding(10, 10, 10, 10);
		textval1lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval1lbl.setTextSize(16);
		textval1lbl.setGravity(Gravity.CENTER);
		trlabel.addView(textval1lbl);

		texttimelbl.setText(context.getResources().getString(R.string.txttime));
		textdatelbl.setText(context.getResources().getString(R.string.txtdate));

		TableRow tr;

		View view1 = new View(context);
		view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
		view1.setBackgroundColor(Color.DKGRAY);
		tbllayoutdrugs.addView(view1);
		tbllayoutdrugs.addView(trlabel);
		for (int i = 0; i < listValuesItemsdrugs.size(); i++) {
			tr = new TableRow(context);
			tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView txttime = new TextView(context);
			txttime.setText(listValuesItemsdrugs.get(i).getStrTime());
			txttime.setTextSize(16);
			txttime.setPadding(10, 10, 10, 10);
			txttime.setTextColor(context.getResources().getColor(R.color.black));
			txttime.setTextSize(16);
			txttime.setGravity(Gravity.CENTER);
			tr.addView(txttime);

			TextView txtdate = new TextView(context);
			// txtdate.setText(listValuesItems.get(i).getStrdate());
			txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItemsdrugs.get(i).getStrdate(),
					Partograph_CommonClass.defdateformat));
			txtdate.setTextSize(16);
			txtdate.setPadding(10, 10, 10, 10);
			txtdate.setTextColor(context.getResources().getColor(R.color.black));
			txtdate.setTextSize(16);
			txtdate.setGravity(Gravity.CENTER);
			tr.addView(txtdate);

			String val1 = "";

			val1 = listValuesItemsdrugs.get(i).getDrug();

			TextView txtvalue1 = new TextView(context);
			txtvalue1.setText(val1);
			txtvalue1.setTextSize(16);
			txtvalue1.setPadding(10, 10, 10, 10);
			txtvalue1.setTextColor(context.getResources().getColor(R.color.black));
			txtvalue1.setTextSize(16);
			txtvalue1.setGravity(Gravity.CENTER);
			tr.addView(txtvalue1);
			// }

			View view3 = new View(context);
			view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view3.setBackgroundColor(Color.DKGRAY);
			tbllayoutdrugs.addView(view3);

			tbllayoutdrugs.addView(tr);
		}

		if (listValuesItemsdrugs.size() <= 0) {
			TableRow tr_empty = new TableRow(context);
			tr_empty.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));
			// tbllayoutafm.addView(tr_empty);

			TextView texttimelbl1 = new TextView(context);
			texttimelbl1.setTextSize(16);
			texttimelbl1.setPadding(10, 10, 10, 10);
			texttimelbl1.setTextColor(context.getResources().getColor(R.color.black));
			texttimelbl1.setTextSize(16);
			texttimelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(texttimelbl1);

			TextView textdatelbl1 = new TextView(context);
			textdatelbl1.setTextSize(16);
			textdatelbl1.setPadding(10, 10, 10, 10);
			textdatelbl1.setTextColor(context.getResources().getColor(R.color.black));
			textdatelbl1.setTextSize(16);
			textdatelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textdatelbl1);

			TextView textval1lbl1 = new TextView(context);
			textval1lbl1.setTextSize(16);
			textval1lbl1.setPadding(10, 10, 10, 10);
			textval1lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval1lbl1.setTextSize(16);
			textval1lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval1lbl1);

			TextView textval2lbl1 = new TextView(context);
			textval2lbl1.setTextSize(16);
			textval2lbl1.setPadding(10, 10, 10, 10);
			textval2lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval2lbl1.setTextSize(16);
			textval2lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval2lbl1);

			View view4 = new View(context);
			view4.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view4.setBackgroundColor(Color.DKGRAY);
			tbllayoutdrugs.addView(view4);
			tbllayoutdrugs.addView(tr_empty);
			View view5 = new View(context);
			view5.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view5.setBackgroundColor(Color.DKGRAY);
			tbllayoutdrugs.addView(view5);
		}
		textval1lbl.setText(context.getResources().getString(R.string.drugs_given));
		return tbllayoutdrugs;

	}

	public static TableLayout DisplayUrineTestData(Context context, int objId, String womenId, String userId,
			TableLayout tbllayouturinetest) throws Exception {
		// TODO Auto-generated method stub

		listValuesItemsUrinetest = dbh.getUrineTestData(objId, womenId, userId);

		tbllayouturinetest.removeAllViews();

		TableRow trlabel = new TableRow(context);
		trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

		TextView texttimelbl = new TextView(context);
		texttimelbl.setTextSize(16);
		texttimelbl.setPadding(10, 10, 10, 10);
		texttimelbl.setTextColor(context.getResources().getColor(R.color.black));
		texttimelbl.setTextSize(16);
		texttimelbl.setGravity(Gravity.CENTER);
		trlabel.addView(texttimelbl);

		TextView textdatelbl = new TextView(context);
		textdatelbl.setTextSize(16);
		textdatelbl.setPadding(10, 10, 10, 10);
		textdatelbl.setTextColor(context.getResources().getColor(R.color.black));
		textdatelbl.setGravity(Gravity.CENTER);
		trlabel.addView(textdatelbl);

		TextView textval1lbl = new TextView(context);
		textval1lbl.setTextSize(16);
		textval1lbl.setPadding(10, 10, 10, 10);
		textval1lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval1lbl.setGravity(Gravity.CENTER);
		trlabel.addView(textval1lbl);

		TextView textval2lbl = new TextView(context);
		textval2lbl.setTextSize(16);
		textval2lbl.setPadding(10, 10, 10, 10);
		textval2lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval2lbl.setGravity(Gravity.CENTER);
		trlabel.addView(textval2lbl);

		TextView textval3lbl = new TextView(context);
		textval3lbl.setTextSize(16);
		textval3lbl.setPadding(10, 10, 10, 10);
		textval3lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval3lbl.setGravity(Gravity.CENTER);
		trlabel.addView(textval3lbl);

		texttimelbl.setText(context.getResources().getString(R.string.txttime));
		textdatelbl.setText(context.getResources().getString(R.string.txtdate));

		View view1 = new View(context);
		view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
		view1.setBackgroundColor(Color.DKGRAY);
		tbllayouturinetest.addView(view1);
		tbllayouturinetest.addView(trlabel);
		TableRow tr = null;
		int i;
		for (i = 0; i < listValuesItemsUrinetest.size(); i++) {
			tr = new TableRow(context);
			// TableRow tr = new TableRow(getApplicationContext());
			tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView txttime = new TextView(context);
			txttime.setText(listValuesItemsUrinetest.get(i).getStrTime());
			txttime.setTextSize(16);
			txttime.setPadding(10, 10, 10, 10);
			txttime.setTextColor(context.getResources().getColor(R.color.black));
			txttime.setGravity(Gravity.CENTER);
			tr.addView(txttime);

			TextView txtdate = new TextView(context);
			// txtdate.setText(listValuesItems.get(i).getStrdate());
			txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItemsUrinetest.get(i).getStrdate(),
					Partograph_CommonClass.defdateformat));
			txtdate.setTextSize(16);
			txtdate.setPadding(10, 10, 10, 10);
			txtdate.setTextColor(context.getResources().getColor(R.color.black));
			txtdate.setGravity(Gravity.CENTER);
			tr.addView(txtdate);

			String val1 = "", val2 = "", val3 = "";

			val1 = listValuesItemsUrinetest.get(i).getStrProteinval();
			val2 = listValuesItemsUrinetest.get(i).getStrAcetoneval();
			val3 = listValuesItemsUrinetest.get(i).getStrVolumeval();

			// updated on 17Nov2015
			if (val1.equalsIgnoreCase("0"))
				val1 = context.getResources().getString(R.string.nil);
			else if (val1.equalsIgnoreCase("1"))
				val1 = context.getResources().getString(R.string.oneplus);
			else if (val1.equalsIgnoreCase("2"))
				val1 = context.getResources().getString(R.string.twoplus);
			else if (val1.equalsIgnoreCase("3"))
				val1 = context.getResources().getString(R.string.threeplus);
			else if (val1.equalsIgnoreCase("4"))
				val1 = context.getResources().getString(R.string.fourplus);

			if (val2.equalsIgnoreCase("0"))
				val2 = context.getResources().getString(R.string.nil);
			else if (val2.equalsIgnoreCase("1"))
				val2 = context.getResources().getString(R.string.oneplus);
			else if (val2.equalsIgnoreCase("2"))
				val2 = context.getResources().getString(R.string.twoplus);
			else if (val2.equalsIgnoreCase("3"))
				val2 = context.getResources().getString(R.string.threeplus);
			else if (val2.equalsIgnoreCase("4"))
				val2 = context.getResources().getString(R.string.fourplus);

			TextView txtvalue1 = new TextView(context);
			txtvalue1.setText(val1);
			txtvalue1.setTextSize(16);
			txtvalue1.setPadding(10, 10, 10, 10);
			txtvalue1.setTextColor(context.getResources().getColor(R.color.black));
			txtvalue1.setGravity(Gravity.CENTER);
			tr.addView(txtvalue1);

			TextView txtvalue2 = new TextView(context);
			txtvalue2.setText(val2);
			txtvalue2.setTextSize(16);
			txtvalue2.setPadding(10, 10, 10, 10);
			txtvalue2.setTextColor(context.getResources().getColor(R.color.black));
			txtvalue2.setGravity(Gravity.CENTER);
			tr.addView(txtvalue2);

			TextView txtvalue3 = new TextView(context);
			txtvalue3.setText(val3);
			txtvalue3.setTextSize(16);
			txtvalue3.setPadding(10, 10, 10, 10);
			txtvalue3.setTextColor(context.getResources().getColor(R.color.black));
			txtvalue3.setGravity(Gravity.CENTER);
			tr.addView(txtvalue3);

			View view3 = new View(context);
			view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view3.setBackgroundColor(Color.DKGRAY);
			tbllayouturinetest.addView(view3);

			tbllayouturinetest.addView(tr);
		}

		if (listValuesItemsUrinetest.size() <= 0) {
			TableRow tr_empty = new TableRow(context);
			tr_empty.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));
			// tbllayoutafm.addView(tr_empty);

			TextView texttimelbl1 = new TextView(context);
			texttimelbl1.setTextSize(16);
			texttimelbl1.setPadding(10, 10, 10, 10);
			texttimelbl1.setTextColor(context.getResources().getColor(R.color.black));
			texttimelbl1.setTextSize(16);
			texttimelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(texttimelbl1);

			TextView textdatelbl1 = new TextView(context);
			textdatelbl1.setTextSize(16);
			textdatelbl1.setPadding(10, 10, 10, 10);
			textdatelbl1.setTextColor(context.getResources().getColor(R.color.black));
			textdatelbl1.setTextSize(16);
			textdatelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textdatelbl1);

			TextView textval1lbl1 = new TextView(context);
			textval1lbl1.setTextSize(16);
			textval1lbl1.setPadding(10, 10, 10, 10);
			textval1lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval1lbl1.setTextSize(16);
			textval1lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval1lbl1);

			TextView textval2lbl1 = new TextView(context);
			textval2lbl1.setTextSize(16);
			textval2lbl1.setPadding(10, 10, 10, 10);
			textval2lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval2lbl1.setTextSize(16);
			textval2lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval2lbl1);

			View view4 = new View(context);
			view4.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view4.setBackgroundColor(Color.DKGRAY);
			tbllayouturinetest.addView(view4);
			tbllayouturinetest.addView(tr_empty);
			View view5 = new View(context);
			view5.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view5.setBackgroundColor(Color.DKGRAY);
			tbllayouturinetest.addView(view5);
		}

		textval1lbl.setText(context.getResources().getString(R.string.protein));
		textval2lbl.setText(context.getResources().getString(R.string.acetone));
		textval3lbl.setText(context.getResources().getString(R.string.volume));
		// if (listValuesItemsUrinetest.size() > 0)
		// tbllayouturinetest.addView(tr);

		return null;
	}

	public static TableLayout DisplayTemperatureData(Context context, int objId, String womenId, String userId,
			TableLayout tbllayouttemp) throws Exception {
		// TODO Auto-generated method stub
		listValuesItemsTemp = dbh.getTempdata(objId, womenId, userId);

		// if (listValuesItemsTemp.size() > 0) {

		tbllayouttemp.removeAllViews();

		TableRow trlabel = new TableRow(context);
		trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

		TextView texttimelbl = new TextView(context);
		texttimelbl.setTextSize(16);
		texttimelbl.setPadding(10, 10, 10, 10);
		texttimelbl.setTextColor(context.getResources().getColor(R.color.black));
		texttimelbl.setTextSize(16);
		texttimelbl.setGravity(Gravity.CENTER);
		trlabel.addView(texttimelbl);

		TextView textdatelbl = new TextView(context);
		textdatelbl.setTextSize(16);
		textdatelbl.setPadding(10, 10, 10, 10);
		textdatelbl.setTextColor(context.getResources().getColor(R.color.black));
		textdatelbl.setTextSize(16);
		textdatelbl.setGravity(Gravity.CENTER);
		trlabel.addView(textdatelbl);

		TextView textval1lbl = new TextView(context);
		textval1lbl.setPadding(10, 10, 10, 10);
		textval1lbl.setTextColor(context.getResources().getColor(R.color.black));
		textval1lbl.setTextSize(16);
		textval1lbl.setGravity(Gravity.CENTER);

		trlabel.addView(textval1lbl);

		texttimelbl.setText(context.getResources().getString(R.string.txttime));
		textdatelbl.setText(context.getResources().getString(R.string.txtdate));

		TableRow tr;

		View view1 = new View(context);
		view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
		view1.setBackgroundColor(Color.DKGRAY);
		tbllayouttemp.addView(view1);
		tbllayouttemp.addView(trlabel);

		for (int i = 0; i < listValuesItemsTemp.size(); i++) {
			tr = new TableRow(context);
			tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView txttime = new TextView(context);
			txttime.setText(listValuesItemsTemp.get(i).getStrTime());
			txttime.setTextSize(16);
			txttime.setPadding(10, 10, 10, 10);
			txttime.setTextColor(context.getResources().getColor(R.color.black));
			txttime.setTextSize(16);
			txttime.setGravity(Gravity.CENTER);
			tr.addView(txttime);

			TextView txtdate = new TextView(context);
			txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItemsTemp.get(i).getStrdate(),
					Partograph_CommonClass.defdateformat));
			txtdate.setTextSize(16);
			txtdate.setPadding(10, 10, 10, 10);
			txtdate.setTextColor(context.getResources().getColor(R.color.black));
			txtdate.setTextSize(16);
			txtdate.setGravity(Gravity.CENTER);
			tr.addView(txtdate);

			String val1 = "";

			val1 = listValuesItemsTemp.get(i).getStrVal();

			TextView txtvalue1 = new TextView(context);
			double val = Double.parseDouble(val1);

			if (val >= 99 && val <= 101)
				txtvalue1.setTextColor(context.getResources().getColor(R.color.amber));
			else if (val > 101)
				txtvalue1.setTextColor(context.getResources().getColor(R.color.red));
			else if (val < 99)
				txtvalue1.setTextColor(context.getResources().getColor(R.color.green));

			txtvalue1.setText(val1);
			txtvalue1.setTextSize(16);
			txtvalue1.setPadding(10, 10, 10, 10);
			txtvalue1.setTextSize(16);
			txtvalue1.setGravity(Gravity.CENTER);
			tr.addView(txtvalue1);
			// }

			View view3 = new View(context);
			view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view3.setBackgroundColor(Color.DKGRAY);
			tbllayouttemp.addView(view3);

			tbllayouttemp.addView(tr);
		}

		if (listValuesItemsTemp.size() <= 0) {
			TableRow tr_empty = new TableRow(context);
			tr_empty.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));
			// tbllayoutafm.addView(tr_empty);

			TextView texttimelbl1 = new TextView(context);
			texttimelbl1.setTextSize(16);
			texttimelbl1.setPadding(10, 10, 10, 10);
			texttimelbl1.setTextColor(context.getResources().getColor(R.color.black));
			texttimelbl1.setTextSize(16);
			texttimelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(texttimelbl1);

			TextView textdatelbl1 = new TextView(context);
			textdatelbl1.setTextSize(16);
			textdatelbl1.setPadding(10, 10, 10, 10);
			textdatelbl1.setTextColor(context.getResources().getColor(R.color.black));
			textdatelbl1.setTextSize(16);
			textdatelbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textdatelbl1);

			TextView textval1lbl1 = new TextView(context);
			textval1lbl1.setTextSize(16);
			textval1lbl1.setPadding(10, 10, 10, 10);
			textval1lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval1lbl1.setTextSize(16);
			textval1lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval1lbl1);

			TextView textval2lbl1 = new TextView(context);
			textval2lbl1.setTextSize(16);
			textval2lbl1.setPadding(10, 10, 10, 10);
			textval2lbl1.setTextColor(context.getResources().getColor(R.color.black));
			textval2lbl1.setTextSize(16);
			textval2lbl1.setGravity(Gravity.CENTER);
			tr_empty.addView(textval2lbl1);

			View view4 = new View(context);
			view4.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view4.setBackgroundColor(Color.DKGRAY);
			tbllayouttemp.addView(view4);
			tbllayouttemp.addView(tr_empty);
			View view5 = new View(context);
			view5.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
			view5.setBackgroundColor(Color.DKGRAY);
			tbllayouttemp.addView(view5);
		}

		textval1lbl.setText(context.getResources().getString(R.string.temperature));

		// * } } else tbllayouttemp.setVisibility(View.GONE);

		return tbllayouttemp;
	}

	public static GraphicalView DisplayPulseBPData(Context context, String womenId, String userId, int objId) {
		listValuesItemsPBP = dbh.getPulseBPData(objId, womenId, userId, true);

		// if (listValuesItemsPBP.size() > 0) {
		// tbllayoutpbp.setVisibility(View.VISIBLE);
		// Creating an XYSeries for values

		XYSeries bpsystolicseries = new XYSeries(context.getResources().getString(R.string.systolic));
		XYSeries bpdiastolicseries = new XYSeries(context.getResources().getString(R.string.diastolic));
		XYSeries pulseseries = new XYSeries(context.getResources().getString(R.string.pulse));

		for (int i = 0; i < listValuesItemsPBP.size(); i++) {

			if (listValuesItemsPBP.get(i).getStrBPsystolicval().length() > 0
					&& listValuesItemsPBP.get(i).getStrBPdiastolicval().length() > 0) {
				bpsystolicseries.add((i + 1), Double.parseDouble(listValuesItemsPBP.get(i).getStrBPsystolicval()));
				bpdiastolicseries.add((i + 1), Double.parseDouble(listValuesItemsPBP.get(i).getStrBPdiastolicval()));
			} else {
				bpsystolicseries.add((i + 1), 0);
				bpdiastolicseries.add((i + 1), 0);
			}

			if (listValuesItemsPBP.get(i).getStrPulseval().length() > 0
					&& !listValuesItemsPBP.get(i).getStrPulseval().isEmpty()) {
				pulseseries.add((i + 1), Double.parseDouble(listValuesItemsPBP.get(i).getStrPulseval()));
			} else {
				pulseseries.add((i + 1), 0);
			}

		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding value Series to the dataset

		dataset.addSeries(bpsystolicseries);
		dataset.addSeries(bpdiastolicseries);
		dataset.addSeries(pulseseries);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer bpsystolicRenderer = new XYSeriesRenderer();
		bpsystolicRenderer.setColor(context.getResources().getColor(R.color.blue));
		bpsystolicRenderer.setPointStyle(PointStyle.CIRCLE);
		bpsystolicRenderer.setFillPoints(true);
		bpsystolicRenderer.setLineWidth(5);
		bpsystolicRenderer.setDisplayChartValues(true);
		bpsystolicRenderer.setChartValuesTextSize(20);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer bpdiastolicRenderer = new XYSeriesRenderer();
		bpdiastolicRenderer.setColor(context.getResources().getColor(R.color.yellow));
		bpdiastolicRenderer.setPointStyle(PointStyle.CIRCLE);
		bpdiastolicRenderer.setFillPoints(true);
		bpdiastolicRenderer.setLineWidth(5);
		bpdiastolicRenderer.setDisplayChartValues(true);
		bpdiastolicRenderer.setChartValuesTextSize(20);

		// Creating XYSeriesRenderer to customize pulseSeries
		XYSeriesRenderer pulseRenderer = new XYSeriesRenderer();
		pulseRenderer.setColor(context.getResources().getColor(R.color.green));
		pulseRenderer.setPointStyle(PointStyle.CIRCLE);
		pulseRenderer.setFillPoints(true);
		pulseRenderer.setLineWidth(3);
		pulseRenderer.setDisplayChartValues(true);
		pulseRenderer.setChartValuesTextSize(20);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		multiRenderer.setChartTitle(context.getResources().getString(R.string.pulse_bp));
		multiRenderer.setChartTitleTextSize(20);
		multiRenderer.setXTitle(context.getResources().getString(R.string.txttime));
		multiRenderer.setYTitle(context.getResources().getString(R.string.pulse_bp));
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setLabelsTextSize(15);
		multiRenderer.setLegendTextSize(15);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setMargins(new int[] { 60, 80, 60, 30 });
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.WHITE);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setAxisTitleTextSize(15);
		multiRenderer.setPointSize(5);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setShowGrid(true);
		multiRenderer.setPanEnabled(false, false);

		multiRenderer.setYAxisMax(200);
		multiRenderer.setYAxisMin(40);
		multiRenderer.setYLabels(15);
		// multiRenderer.setYAxisMin(40);
		multiRenderer.setBarSpacing(10);
		multiRenderer.setXAxisMin(0);
		multiRenderer.setXAxisMax(12);
		multiRenderer.setXLabelsAngle(45); // 18jan2016

		// multiRenderer.setZoomButtonsVisible(true);
		for (int i = 0; i < listValuesItemsPBP.size(); i++) {
			multiRenderer.addXTextLabel((i + 1), listValuesItemsPBP.get(i).getStrTime());
			// multiRenderer.addXTextLabel(i, ""+i+1);
			// 05jan2016

			// * multiRenderer.setYAxisMax(300); multiRenderer.setYLabels(15);

		}

		// 08Nov2016
		if (listValuesItemsPBP.size() <= 0) {
			for (int i = 0; i < 2; i++) {
				bpsystolicseries.add(i + 1, i);

			}
		} // 08Nov2016
		multiRenderer.setYAxisMax(300);
		multiRenderer.setYLabels(15);

		multiRenderer.addSeriesRenderer(bpsystolicRenderer);
		multiRenderer.addSeriesRenderer(bpdiastolicRenderer);
		multiRenderer.addSeriesRenderer(pulseRenderer);
		mChartViewPBP = null;

		String[] types = new String[] { BarChart.TYPE, BarChart.TYPE, LineChart.TYPE };

		mChartViewPBP = ChartFactory.getCombinedXYChartView(context, dataset, multiRenderer, types);

		return mChartViewPBP;

		// String[] types = new String[] { BarChart.TYPE, BarChart.TYPE,
		// LineChart.TYPE };

		// * } else tbllayoutpbp.setVisibility(View.GONE);

	}

	public static Image generatePulseBPGraph(Context context) throws Exception {
		Image myImg = null;

		XYSeries bpsystolicseries = new XYSeries("Systolic");
		XYSeries bpdiastolicseries = new XYSeries("Diastolic");
		XYSeries pulseseries = new XYSeries("Pulse");

		listValuesItemsPBP = dbh.getPulseBPData(7, womanID, userId, true);

		for (int i = 0; i < Partograph_CommonClass.listValuesItemsPBP.size(); i++) {

			if (Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPsystolicval().length() > 0
					&& Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPdiastolicval().length() > 0) {
				bpsystolicseries.add((i + 1),
						Double.parseDouble(Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPsystolicval()));
				bpdiastolicseries.add((i + 1),
						Double.parseDouble(Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPdiastolicval()));
			} else {
				bpsystolicseries.add((i + 1), 0);
				bpdiastolicseries.add((i + 1), 0);
			}

			if (Partograph_CommonClass.listValuesItemsPBP.get(i).getStrPulseval().length() > 0
					&& !Partograph_CommonClass.listValuesItemsPBP.get(i).getStrPulseval().isEmpty()) {
				pulseseries.add((i + 1),
						Double.parseDouble(Partograph_CommonClass.listValuesItemsPBP.get(i).getStrPulseval()));
			} else {
				pulseseries.add((i + 1), 0);
			}

		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding value Series to the dataset

		dataset.addSeries(bpsystolicseries);
		dataset.addSeries(bpdiastolicseries);
		dataset.addSeries(pulseseries);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer bpsystolicRenderer = new XYSeriesRenderer();
		bpsystolicRenderer.setColor(context.getResources().getColor(R.color.blue));
		bpsystolicRenderer.setPointStyle(PointStyle.CIRCLE);
		bpsystolicRenderer.setFillPoints(true);
		bpsystolicRenderer.setLineWidth(1);
		bpsystolicRenderer.setDisplayChartValues(true);
		bpsystolicRenderer.setChartValuesTextSize(10);
		// bpsystolicRenderer.setShowLegendItem(true);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer bpdiastolicRenderer = new XYSeriesRenderer();
		bpdiastolicRenderer.setColor(context.getResources().getColor(R.color.yellow));
		bpdiastolicRenderer.setPointStyle(PointStyle.CIRCLE);
		bpdiastolicRenderer.setFillPoints(true);
		bpdiastolicRenderer.setLineWidth(1);
		bpdiastolicRenderer.setDisplayChartValues(true);
		bpdiastolicRenderer.setChartValuesTextSize(10);
		// bpdiastolicRenderer.setShowLegendItem(true);

		// Creating XYSeriesRenderer to customize pulseSeries
		XYSeriesRenderer pulseRenderer = new XYSeriesRenderer();
		pulseRenderer.setColor(context.getResources().getColor(R.color.green));
		pulseRenderer.setPointStyle(PointStyle.CIRCLE);
		pulseRenderer.setFillPoints(true);
		pulseRenderer.setLineWidth(1);
		pulseRenderer.setDisplayChartValues(true);
		pulseRenderer.setChartValuesTextSize(10);
		// pulseRenderer.setShowLegendItem(true);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		// multiRenderer.setDisplayChartValues(false);
		// multiRenderer.setChartTitle("Bp");
		multiRenderer.setChartTitleTextSize(10);

		multiRenderer.setXLabelsColor(context.getResources().getColor(R.color.black));
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setLabelsTextSize(8);
		multiRenderer.setFitLegend(true);
		multiRenderer.setLegendTextSize(8);
		multiRenderer.setLegendHeight(10);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setMargins(new int[] { 10, 13, -20, 12 });
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.WHITE);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setAxisTitleTextSize(10);
		multiRenderer.setPointSize(1);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setShowGrid(true);
		multiRenderer.setPanEnabled(false, false);

		multiRenderer.setShowGridX(true);
		multiRenderer.setShowGridY(true);

		multiRenderer.setYAxisMax(200);
		multiRenderer.setYAxisMin(60);
		multiRenderer.setYLabels(15);
		// multiRenderer.setYAxisMin(40);
		multiRenderer.setBarSpacing(1);
		multiRenderer.setXAxisMin(0);
		multiRenderer.setXAxisMax(12);
		multiRenderer.setXLabelsAngle(0); // 18jan2016

		// multiRenderer.setZoomButtonsVisible(true);
		for (int i = 0; i < Partograph_CommonClass.listValuesItemsPBP.size(); i++) {
			multiRenderer.addXTextLabel((i + 1), Partograph_CommonClass.listValuesItemsPBP.get(i).getStrTime());
			// multiRenderer.addXTextLabel(i, ""+i+1);
			// 05jan2016
			multiRenderer.setYAxisMax(180);
			multiRenderer.setYLabels(15);
		}

		multiRenderer.addSeriesRenderer(bpsystolicRenderer);
		multiRenderer.addSeriesRenderer(bpdiastolicRenderer);
		multiRenderer.addSeriesRenderer(pulseRenderer);

		mChartViewPBP = null;

		String[] types = new String[] { BarChart.TYPE, BarChart.TYPE, LineChart.TYPE };

		mChartViewPBP = ChartFactory.getCombinedXYChartView(context, dataset, multiRenderer, types);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				300);
		mChartViewPBP.setLayoutParams(params);
		mChartViewPBP.repaint();

		Bitmap y = bitmapFromChartView(mChartViewPBP, 530, 150);
		if (y != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			y.compress(Bitmap.CompressFormat.PNG, 50, stream);
			try {
				myImg = Image.getInstance(stream.toByteArray());
			} catch (BadElementException e) {
				// AppContext.addLog(new
				// RuntimeException().getStackTrace()[0].getMethodName() + " - "
				// + this.getClass().getSimpleName(), e);
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// AppContext.addLog(new
				// RuntimeException().getStackTrace()[0].getMethodName() + " - "
				// + this.getClass().getSimpleName(), e);
				e.printStackTrace();
			} catch (IOException e) {
				// AppContext.addLog(new
				// RuntimeException().getStackTrace()[0].getMethodName() + " - "
				// + this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
			myImg.setAlignment(Image.MIDDLE);
		}

		return myImg;
	}

	private static Bitmap bitmapFromChartView(GraphicalView v, int width, int height) throws Exception {
		Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		v.layout(0, 0, width, height);
		v.draw(c);
		return b;
	}

	public static Image generateFHRGraph(Context context) throws Exception {

		Image myImg = null;
		try {

			String yLbl = context.getResources().getString(R.string.fhr);

			// Creating an XYSeries for values
			XYSeries valueseries = new XYSeries("Values");

			listValuesItemsFHR = new ArrayList<Add_value_pojo>();
			listValuesItemsFHR = dbh.getFHRGraphData(1, womanID, userId);// 12Oct2016

			for (int i = 0; i < Partograph_CommonClass.listValuesItemsFHR.size(); i++) {
				valueseries.add(i + 1,
						Double.parseDouble(Partograph_CommonClass.listValuesItemsFHR.get(i).getStrVal()));

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(valueseries);

			// Creating XYSeriesRenderer to customize valueSeries
			XYSeriesRenderer valueRenderer = new XYSeriesRenderer();
			valueRenderer.setColor(Color.BLACK);
			valueRenderer.setPointStyle(PointStyle.CIRCLE);
			valueRenderer.setFillPoints(true);
			valueRenderer.setLineWidth(2);
			valueRenderer.setDisplayChartValues(true);
			valueRenderer.setChartValuesTextSize(15);
			valueRenderer.setShowLegendItem(true);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			// multiRenderer.setChartTitle(getResources().getString(R.string.fetal_heart_rate));
			multiRenderer.setChartTitleTextSize(10);

			multiRenderer.setXLabelsColor(context.getResources().getColor(R.color.black));
			multiRenderer.setYLabelsColor(0, context.getResources().getColor(R.color.black));
			multiRenderer.setLabelsTextSize(8);
			multiRenderer.setLegendTextSize(10);
			multiRenderer.setLegendHeight(10);
			multiRenderer.setFitLegend(true);
			multiRenderer.setLabelsColor(context.getResources().getColor(R.color.black));
			multiRenderer.setMargins(new int[] { 10, 15, -15, 12 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(context.getResources().getColor(R.color.black));
			multiRenderer.setAxisTitleTextSize(10);
			multiRenderer.setPointSize(2);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(true, true);
			multiRenderer.setShowLegend(true);

			multiRenderer.setDisplayChartValues(true);

			// updated on 17Nov2015 - YAxisMin from 80 to 40
			multiRenderer.setYAxisMin(80);

			multiRenderer.setFitLegend(true);
			// multiRenderer.setShowGrid(false);

			multiRenderer.setShowGridX(true);
			multiRenderer.setShowGridY(true);
			// multiRenderer.setg
			// multiRenderer.setsho
			multiRenderer.setGridColor(context.getResources().getColor(R.color.lightgray));

			multiRenderer.setBarSpacing(.05);
			multiRenderer.setXLabelsAngle(0f);

			// 06Jan2016
			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);

			multiRenderer.setShowCustomTextGrid(true);
			multiRenderer.setZoomEnabled(true);

			multiRenderer.setYAxisMax(200);
			multiRenderer.setYLabels(15);

			for (int i = 0; i < Partograph_CommonClass.listValuesItemsFHR.size(); i++) {
				multiRenderer.addXTextLabel(i + 1, Partograph_CommonClass.listValuesItemsFHR.get(i).getStrTime());
				// multiRenderer.setYAxisMax(300);
				// multiRenderer.setYLabels(15);
			}

			// 08Nov2016
			if (Partograph_CommonClass.listValuesItemsFHR.size() <= 0) {
				for (int i = 0; i < 2; i++) {
					valueseries.add(i + 1, i);

				}
			} // 08Nov2016

			multiRenderer.addSeriesRenderer(valueRenderer);

			mChartViewFHR = null;

			mChartViewFHR = ChartFactory.getLineChartView(context, dataset, multiRenderer);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT, 200);
			mChartViewFHR.setLayoutParams(params);

			mChartViewFHR.setLayoutParams(params);

			params.setMargins(0, 0, 0, 0);

			mChartViewFHR.repaint();

			Bitmap y = bitmapFromChartView(mChartViewFHR, 530, 150);
			if (y != null) {
				ByteArrayOutputStream stream = new ByteArrayOutputStream();

				y.compress(Bitmap.CompressFormat.PNG, 50, stream);
				try {
					myImg = Image.getInstance(stream.toByteArray());
				} catch (BadElementException e) {
					/*
					 * AppContext.addLog(new
					 * RuntimeException().getStackTrace()[0].getMethodName() +
					 * " - " + this.getClass().getSimpleName(), e);
					 */
					e.printStackTrace();
				} catch (MalformedURLException e) {
					/*
					 * AppContext.addLog(new
					 * RuntimeException().getStackTrace()[0].getMethodName() +
					 * " - " + this.getClass().getSimpleName(), e);
					 */
					e.printStackTrace();
				} catch (IOException e) {
					/*
					 * AppContext.addLog(new
					 * RuntimeException().getStackTrace()[0].getMethodName() +
					 * " - " + this.getClass().getSimpleName(), e);
					 */
					e.printStackTrace();
				}
				myImg.setAlignment(Image.MIDDLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return myImg;
	}

	public static PdfPTable generateAFM(Context context) {
		try {
			ArrayList<String> amniotic_row = new ArrayList<String>();
			amniotic_row.add("Amniotic");

			listValuesItemsafm = dbh.getAFMData(2, womanID, userId);

			for (int k = 0; k < Partograph_CommonClass.listValuesItemsafm.size(); k++) {

				String val = Partograph_CommonClass.listValuesItemsafm.get(k).getAmnioticfluid();
				if (val.equals("0"))
					val = context.getResources().getString(R.string.c);
				else if (val.equals("1"))
					val = context.getResources().getString(R.string.m);
				// 26Nov2015
				else if (val.equals("2"))
					val = context.getResources().getString(R.string.i);
				else if (val.equals("3"))
					val = context.getResources().getString(R.string.b);
				amniotic_row.add(val);
			}

			tableAFM = new PdfPTable(13);
			tableAFM.setWidthPercentage(90);
			tableAFM.setWidths(new int[] { 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });
			for (int k = 0; k < 13; k++) {
				String header = " ";
				if (k < amniotic_row.size()) {
					header = amniotic_row.get(k);
				}
				PdfPCell cell = new PdfPCell();
				// cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPadding(1);
				tableAFM.addCell(cell);
			}

			tableAFM.completeRow();

			ArrayList<String> moulding = new ArrayList<String>();
			moulding.add("Moulding");
			for (int l = 0; l < Partograph_CommonClass.listValuesItemsafm.size(); l++) {
				String mouldingVal = Partograph_CommonClass.listValuesItemsafm.get(l).getMoulding();

				if (mouldingVal.equalsIgnoreCase("0"))
					mouldingVal = context.getResources().getString(R.string.m1);
				else if (mouldingVal.equalsIgnoreCase("1"))
					mouldingVal = context.getResources().getString(R.string.m2);
				else
					mouldingVal = context.getResources().getString(R.string.m3);

				moulding.add(mouldingVal);

			}

			for (int x = 0; x < 13; x++) {
				String strmoulding = " ";
				if (x < moulding.size()) {
					strmoulding = moulding.get(x);
				}
				PdfPCell cell_moulding = new PdfPCell();
				cell_moulding.setPhrase(new Phrase(strmoulding, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell_moulding.setHorizontalAlignment(Element.ALIGN_CENTER);
				// cell_moulding.setPadding(3);
				tableAFM.addCell(cell_moulding);
			}
			tableAFM.completeRow();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return tableAFM;
	}

	public static PdfPTable generateOxytocinGraph() {
		try {

			ArrayList<String> oxytocinDrops = new ArrayList<String>();
			ArrayList<String> oxytocinUnits = new ArrayList<String>();

			oxytocinDrops.add("Oxytocin U/L");
			oxytocinUnits.add("Units");

			listValuesItemsoxytocin = dbh.getOxytocinData(5, womanID, userId);

			if (!(LoginActivity.objectid.contains(5)) && Partograph_CommonClass.listValuesItemsoxytocin != null)// 27March2017
			// Arpitha

			{// 27March2017 Arpitha
				for (int i = 0; i < Partograph_CommonClass.listValuesItemsoxytocin.size(); i++) {
					String oxyVal = Partograph_CommonClass.listValuesItemsoxytocin.get(i).getOxytocindrops();
					String oxyUnits = Partograph_CommonClass.listValuesItemsoxytocin.get(i).getOxytocinunits();
					oxytocinDrops.add(oxyVal);
					oxytocinUnits.add(oxyUnits);
				}
			}

			table_oxytocin = new PdfPTable(13);

			table_oxytocin.setWidthPercentage(90);

			table_oxytocin.setWidths(new int[] { 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });
			// table_oxytocin = new PdfPTable(oxytocinDrops.size());
			for (int k = 0; k < 13; k++) {
				String header_oxy = " ";
				if (k < oxytocinDrops.size()) {
					header_oxy = oxytocinDrops.get(k);
				}
				PdfPCell cell = new PdfPCell();
				// cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header_oxy, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPadding(1);

				table_oxytocin.addCell(cell);

			}

			table_oxytocin.completeRow();

			for (int j = 0; j < 13; j++) {
				String units = " ";
				if (j < oxytocinUnits.size()) {
					units = oxytocinUnits.get(j);
				}
				PdfPCell cell_oxyUnits = new PdfPCell();
				cell_oxyUnits.setPhrase(new Phrase(units, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell_oxyUnits.setHorizontalAlignment(Element.ALIGN_CENTER);
				// cell_oxyUnits.setPadding(3);
				table_oxytocin.addCell(cell_oxyUnits);
			}
			table_oxytocin.setSpacingAfter(2);

			table_oxytocin.completeRow();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return table_oxytocin;
	}

	public static PdfPTable generateDrugsGraph() {
		try {

			ArrayList<String> arrDrugs = new ArrayList<String>();

			arrDrugs.add("Drugs Given");
			listValuesItemsdrugs = dbh.getDrugdata(6, womanID, userId);
			for (int i = 0; i < Partograph_CommonClass.listValuesItemsdrugs.size(); i++) {
				String drug = Partograph_CommonClass.listValuesItemsdrugs.get(i).getDrug();
				arrDrugs.add(drug);
			}

			table_drug = new PdfPTable(13);
			table_drug.setWidthPercentage(90);
			table_drug.setWidths(new int[] { 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });
			for (int k = 0; k < 13; k++) {
				String header_drug_given = " ";
				if (k < arrDrugs.size()) {
					header_drug_given = arrDrugs.get(k);
				}
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header_drug_given, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				// cell.setPadding(3);
				table_drug.addCell(cell);
			}

			table_drug.completeRow();

			// table_drug.setSpacingAfter(10);
			return table_drug;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return table_drug;
	}

	public static PdfPTable generateTemperatureGraph() {
		try {

			ArrayList<String> arrTemp = new ArrayList<String>();
			arrTemp.add("Temperature");

			listValuesItemsTemp = dbh.getTempdata(8, womanID, userId);

			for (int i = 0; i < Partograph_CommonClass.listValuesItemsTemp.size(); i++) {
				String tempVal = Partograph_CommonClass.listValuesItemsTemp.get(i).getStrVal();
				arrTemp.add(tempVal);
			}

			table_temp = new PdfPTable(13);
			table_temp.setWidthPercentage(90);
			table_temp.setWidths(new int[] { 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });
			for (int k = 0; k < 13; k++) {
				String header_temperature = " ";
				if (k < arrTemp.size()) {
					header_temperature = arrTemp.get(k);
				}
				PdfPCell cell = new PdfPCell();
				// cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header_temperature, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPadding(3);
				table_temp.addCell(cell);

			}

			table_temp.completeRow();

			table_temp.setSpacingBefore(2);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return table_temp;
	}

	public static PdfPTable generateUrineTestGraph() {
		try {

			ArrayList<String> arrProtein = new ArrayList<String>();
			ArrayList<String> arrAcetone = new ArrayList<String>();
			ArrayList<String> arrVolume = new ArrayList<String>();

			arrProtein.add("Protein");
			arrAcetone.add("Acetone");
			arrVolume.add("Volume");

			listValuesItemsUrinetest = dbh.getUrineTestData(9, womanID, userId);

			for (int i = 0; i < Partograph_CommonClass.listValuesItemsUrinetest.size(); i++) {
				String protein = Partograph_CommonClass.listValuesItemsUrinetest.get(i).getStrProteinval();
				String acetone = Partograph_CommonClass.listValuesItemsUrinetest.get(i).getStrAcetoneval();
				String volume = Partograph_CommonClass.listValuesItemsUrinetest.get(i).getStrVolumeval();

				if (protein.equalsIgnoreCase("0"))
					protein = context.getResources().getString(R.string.nil);
				else if (protein.equalsIgnoreCase("1"))
					protein = context.getResources().getString(R.string.oneplus);
				else if (protein.equalsIgnoreCase("2"))
					protein = context.getResources().getString(R.string.twoplus);
				else if (protein.equalsIgnoreCase("3"))
					protein = context.getResources().getString(R.string.threeplus);
				else if (protein.equalsIgnoreCase("4"))
					protein = context.getResources().getString(R.string.fourplus);

				if (acetone.equalsIgnoreCase("0"))
					acetone = context.getResources().getString(R.string.nil);
				else if (acetone.equalsIgnoreCase("1"))
					acetone = context.getResources().getString(R.string.oneplus);
				else if (acetone.equalsIgnoreCase("2"))
					acetone = context.getResources().getString(R.string.twoplus);
				else if (acetone.equalsIgnoreCase("3"))
					acetone = context.getResources().getString(R.string.threeplus);
				else if (acetone.equalsIgnoreCase("4"))
					acetone = context.getResources().getString(R.string.fourplus);
				arrAcetone.add(acetone);
				arrProtein.add(protein);
				arrVolume.add(volume);
			}

			table_urine = new PdfPTable(13);

			table_urine.setWidthPercentage(90);

			table_urine.setWidths(new int[] { 9, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });

			for (int k = 0; k < 13; k++) {
				String protein = " ";
				if (k < arrProtein.size()) {
					protein = arrProtein.get(k);
				}
				PdfPCell cell = new PdfPCell();
				// cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(protein, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPadding(1);
				table_urine.addCell(cell);
			}

			table_urine.completeRow();

			for (int i = 0; i < 13; i++) {
				String acetone = " ";
				if (i < arrAcetone.size()) {
					acetone = arrAcetone.get(i);
				}
				PdfPCell cellAcetone = new PdfPCell();
				cellAcetone.setPhrase(new Phrase(acetone, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cellAcetone.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellAcetone.setPadding(1);
				table_urine.addCell(cellAcetone);
			}

			table_urine.completeRow();

			for (int i = 0; i < 13; i++) {
				String volume = " ";
				if (i < arrVolume.size()) {
					volume = arrVolume.get(i);
				}
				PdfPCell cellVolume = new PdfPCell();
				cellVolume.setPhrase(new Phrase(volume, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cellVolume.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellVolume.setPadding(3);
				table_urine.addCell(cellVolume);
			}
			table_urine.setSpacingBefore(2);
			table_urine.completeRow();

			// table_drug.setSpacingAfter(10);
			// return table_urine;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return table_urine;
	}

	public static Image generateDilatationGraph() throws Exception {
		Image myImg = null;

		XYSeries cervixseries = new XYSeries("Cervix");
		// XYSeries cervixseries = new XYSeries("C");
		XYSeries desc_headseries = new XYSeries("Descent of head");

		XYSeries alertline = new XYSeries("Alert Line");
		XYSeries actionline = new XYSeries("Action Line");

		int[] x = { 0, 1, 2, 3, 4, 5, 6 };
		int[] alertval = { 4, 5, 6, 7, 8, 9, 10 };

		int[] xactionline = { 4, 5, 6, 7, 8, 9, 10 };
		int[] actionval = { 4, 5, 6, 7, 8, 9, 10 };

		double descofhead = 0;

		int hrs = 0;

		listValuesItemsDilatation = dbh.getDilatationData(3, womanID, userId, true);

		for (int i = 0; i < Partograph_CommonClass.listValuesItemsDilatation.size(); i++) {
			// 17May2017
			// Arpitha
			// -
			// v2.6
			// i+1
			// to
			// i

			if (Partograph_CommonClass.listValuesItemsDilatation.get(i).getDesc_of_head() != null
					&& !Partograph_CommonClass.listValuesItemsDilatation.get(i).getDesc_of_head().isEmpty()) {
				descofhead = Double
						.parseDouble(Partograph_CommonClass.listValuesItemsDilatation.get(i).getDesc_of_head());
				if (i == 0) {
					desc_headseries.add((hrs), descofhead);// 17May2017 Arpitha
															// - v2.6
															// i+1 to i

					cervixseries.add((hrs),
							Double.parseDouble(Partograph_CommonClass.listValuesItemsDilatation.get(i).getCervix()));
					hrs = 0;
				} else {
					String str1 = listValuesItemsDilatation.get(i).getStrdate() + "_"
							+ listValuesItemsDilatation.get(i).getStrTime();
					String str2 = listValuesItemsDilatation.get(i - 1).getStrdate() + "_"
							+ listValuesItemsDilatation.get(i - 1).getStrTime();

					hrs = hrs + Partograph_CommonClass.getHoursBetDates(str1, str2);
					cervixseries.add(hrs, Double.parseDouble(listValuesItemsDilatation.get(i).getCervix()));
					desc_headseries.add((hrs), descofhead);
				}

			}

		}

		for (int i = 0; i < x.length; i++) {
			alertline.add(x[i], alertval[i]);
		}

		for (int i = 0; i < xactionline.length; i++) {
			actionline.add(xactionline[i], actionval[i]);
		}
		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding value Series to the dataset
		dataset.addSeries(cervixseries);
		dataset.addSeries(desc_headseries);
		dataset.addSeries(alertline);
		dataset.addSeries(actionline);

		// Creating XYSeriesRenderer to customize alertlineSeries
		XYSeriesRenderer alertRenderer = new XYSeriesRenderer();
		alertRenderer.setColor(context.getResources().getColor(R.color.amber));
		alertRenderer.setPointStyle(PointStyle.POINT);
		alertRenderer.setFillPoints(true);
		alertRenderer.setLineWidth(1);
		alertRenderer.setDisplayChartValues(false);
		// alertRenderer.setShowLegendItem(false);

		// Creating XYSeriesRenderer to customize actionlineSeries
		XYSeriesRenderer actionRenderer = new XYSeriesRenderer();
		actionRenderer.setColor(context.getResources().getColor(R.color.red));
		actionRenderer.setPointStyle(PointStyle.POINT);
		actionRenderer.setFillPoints(true);
		actionRenderer.setLineWidth(1);
		actionRenderer.setDisplayChartValues(false);
		// actionRenderer.setShowLegendItem(false);

		// Creating XYSeriesRenderer to customize pulseSeries
		XYSeriesRenderer cervixRenderer = new XYSeriesRenderer();
		cervixRenderer.setColor(context.getResources().getColor(R.color.springgreen));
		cervixRenderer.setPointStyle(PointStyle.TRIANGLE);
		cervixRenderer.setFillPoints(true);
		cervixRenderer.setLineWidth(1);
		cervixRenderer.setDisplayChartValues(false);
		cervixRenderer.setChartValuesTextSize(10);
		// cervixRenderer.setShowLegendItem(false);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer descheadRenderer = new XYSeriesRenderer();
		descheadRenderer.setColor(context.getResources().getColor(R.color.steel_blue));
		descheadRenderer.setPointStyle(PointStyle.CIRCLE);
		descheadRenderer.setFillPoints(true);
		descheadRenderer.setLineWidth(1);
		descheadRenderer.setDisplayChartValues(false);

		descheadRenderer.setChartValuesTextSize(10);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		// multiRenderer.setChartTitle(getResources().getString(R.string.dilatation));
		multiRenderer.setChartTitleTextSize(10);
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setLabelsTextSize(8);
		multiRenderer.setLegendTextSize(10);
		multiRenderer.setFitLegend(true);
		multiRenderer.setLegendHeight(10);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setMargins(new int[] { 10, 13, -10, 12 });
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.WHITE);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setAxisTitleTextSize(10);
		multiRenderer.setPointSize(3);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setShowGrid(true);
		// Disable Pan on two axis
		multiRenderer.setPanEnabled(false, false);
		multiRenderer.setYAxisMax(10);
		multiRenderer.setYAxisMin(0);
		multiRenderer.setYLabels(11);
		multiRenderer.setXAxisMin(0);
		multiRenderer.setXAxisMax(12);

		/*
		 * // changes made on 04-02-2015 for (int i = 0; i < 12; i++) {
		 * multiRenderer.addXTextLabel((i + 1), "" + (i + 1)); } // 04feb
		 */

		newDilValuesCervix = new LinkedHashMap<Integer, Double>();
		newDilValuesDescent = new LinkedHashMap<Integer, Double>();

		for (int i = 0; i < 12; i++) {
			for (Map.Entry<Integer, Double> xaxisval : newDilValuesCervix.entrySet()) {
				// String val = "" + (xaxisval.getKey()+1);
				if (i == xaxisval.getKey()) {
					int xa = xaxisval.getKey() + 1;
					String val = "" + xa;
					multiRenderer.addXTextLabel(xaxisval.getKey(), val);
				} else
					multiRenderer.addXTextLabel((i), "" + (i + 1));
			}
		}
		multiRenderer.addSeriesRenderer(cervixRenderer);
		multiRenderer.addSeriesRenderer(descheadRenderer);
		multiRenderer.addSeriesRenderer(alertRenderer);
		multiRenderer.addSeriesRenderer(actionRenderer);

		mChartViewDilatation = null;
		mChartViewDilatation = ChartFactory.getLineChartView(context, dataset, multiRenderer);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				450);

		mChartViewDilatation.setLayoutParams(params);
		mChartViewDilatation.repaint();

		Bitmap y = bitmapFromChartView(mChartViewDilatation, 530, 150);
		if (y != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			y.compress(Bitmap.CompressFormat.PNG, 50, stream);
			try {
				myImg = Image.getInstance(stream.toByteArray());
			} catch (BadElementException e) {
				/*
				 * AppContext.addLog(new
				 * RuntimeException().getStackTrace()[0].getMethodName() + " - "
				 * + this.getClass().getSimpleName(), e);
				 */
				e.printStackTrace();
			} catch (MalformedURLException e) {
				/*
				 * AppContext.addLog(new
				 * RuntimeException().getStackTrace()[0].getMethodName() + " - "
				 * + this.getClass().getSimpleName(), e);
				 */
				e.printStackTrace();
			} catch (IOException e) {
				/*
				 * AppContext.addLog(new
				 * RuntimeException().getStackTrace()[0].getMethodName() + " - "
				 * + this.getClass().getSimpleName(), e);
				 */
				e.printStackTrace();
			}
			myImg.setAlignment(Image.MIDDLE);
		}

		return myImg;
	}

	public static Image generateContractionGraph() throws Exception {
		Image myImg = null;

		// Creating an XYSeries for values
		XYSeries cont_L20sec = new XYSeries("Less than 20 sec");
		XYSeries cont_B20_40sec = new XYSeries("Between 20 and 40 sec");
		XYSeries cont_M40sec = new XYSeries("More than 40 sec");
		XYSeries cont_M60sec = new XYSeries("More than 60 sec");

		listValuesItemsContractions = dbh.getContractionData(4, womanID, userId, true);

		for (int i = 0; i < Partograph_CommonClass.listValuesItemsContractions.size(); i++) {
			if (Partograph_CommonClass.listValuesItemsContractions.get(i).getDuration() < 20)
				cont_L20sec.add(i + 1, Double
						.parseDouble(Partograph_CommonClass.listValuesItemsContractions.get(i).getContractions()));
			if (Partograph_CommonClass.listValuesItemsContractions.get(i).getDuration() >= 20
					&& Partograph_CommonClass.listValuesItemsContractions.get(i).getDuration() <= 40) {
				cont_B20_40sec.add(i + 1, Double
						.parseDouble(Partograph_CommonClass.listValuesItemsContractions.get(i).getContractions()));
			}

			if (Partograph_CommonClass.listValuesItemsContractions.get(i).getDuration() > 40
					&& listValuesItemsContractions.get(i).getDuration() <= 60)
				cont_M40sec.add(i + 1, Double
						.parseDouble(Partograph_CommonClass.listValuesItemsContractions.get(i).getContractions()));

			if (Partograph_CommonClass.listValuesItemsContractions.get(i).getDuration() > 60) {
				cont_M60sec.add(i + 1, Double
						.parseDouble(Partograph_CommonClass.listValuesItemsContractions.get(i).getContractions()));
			}

		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding value Series to the dataset
		dataset.addSeries(cont_L20sec);
		dataset.addSeries(cont_B20_40sec);
		dataset.addSeries(cont_M40sec);
		dataset.addSeries(cont_M60sec);

		// Creating XYSeriesRenderer to customize contractions < 20 sec
		XYSeriesRenderer contraction_L20Renderer = new XYSeriesRenderer();
		contraction_L20Renderer.setColor(context.getResources().getColor(R.color.vividorange));
		contraction_L20Renderer.setPointStyle(PointStyle.DIAMOND);
		contraction_L20Renderer.setFillPoints(true);
		contraction_L20Renderer.setLineWidth((float) 3.5d);
		contraction_L20Renderer.setDisplayChartValues(true);
		contraction_L20Renderer.setChartValuesTextSize(10);
		// contraction_L20Renderer.setShowLegendItem(false);

		// Creating XYSeriesRenderer to customize contractions bet 20 and 40
		XYSeriesRenderer contraction_B20_40Renderer = new XYSeriesRenderer();
		contraction_B20_40Renderer.setColor(context.getResources().getColor(R.color.darkgreen));
		contraction_B20_40Renderer.setPointStyle(PointStyle.CIRCLE);
		contraction_B20_40Renderer.setFillPoints(true);
		contraction_B20_40Renderer.setLineWidth((float) 6.5d);
		contraction_B20_40Renderer.setDisplayChartValues(true);
		contraction_B20_40Renderer.setChartValuesTextSize(10);
		// contraction_B20_40Renderer.setFillBelowLine(true);
		// contraction_B20_40Renderer.setFillBelowLineColor(Color.MAGENTA);
		// contraction_B20_40Renderer.setShowLegendItem(false);

		// Creating XYSeriesRenderer to customize contractions > 40
		XYSeriesRenderer contraction_M40Renderer = new XYSeriesRenderer();
		contraction_M40Renderer.setColor(context.getResources().getColor(R.color.darkred));
		contraction_M40Renderer.setPointStyle(PointStyle.CIRCLE);
		contraction_M40Renderer.setFillPoints(true);
		contraction_M40Renderer.setLineWidth((float) 6.5d);
		contraction_M40Renderer.setDisplayChartValues(true);
		contraction_M40Renderer.setChartValuesTextSize(10);

		// Creating XYSeriesRenderer to customize contractions > 40
		XYSeriesRenderer contraction_M60Renderer = new XYSeriesRenderer();
		contraction_M60Renderer.setColor(context.getResources().getColor(R.color.red));
		contraction_M60Renderer.setPointStyle(PointStyle.CIRCLE);
		contraction_M60Renderer.setFillPoints(true);
		contraction_M60Renderer.setLineWidth((float) 6.5d);
		contraction_M60Renderer.setDisplayChartValues(true);
		contraction_M60Renderer.setChartValuesTextSize(10);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		multiRenderer.setChartTitleTextSize(10);
		multiRenderer.setDisplayChartValues(false);
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setLabelsTextSize(8);
		multiRenderer.setLegendTextSize(10);
		multiRenderer.setLabelsColor(Color.BLACK);
		// multiRenderer.setMargins(new int[] { 10, 13, -5, 12 });
		multiRenderer.setMargins(new int[] { 10, 13, -5, 8 });
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.WHITE);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setAxisTitleTextSize(9);
		multiRenderer.setPointSize(3);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setShowGrid(true);
		// Disable Pan on two axis
		multiRenderer.setPanEnabled(false, false);
		multiRenderer.setYAxisMax(5);
		multiRenderer.setYAxisMin(1);
		multiRenderer.setGridColor(context.getResources().getColor(R.color.lightgray));
		multiRenderer.setFitLegend(true);

		multiRenderer.setShowGridX(true);
		multiRenderer.setShowGridY(true);

		multiRenderer.setXAxisMin(0);
		multiRenderer.setXAxisMax(12);
		multiRenderer.setBarSpacing(.3);
		multiRenderer.setBarWidth(15);

		multiRenderer.setShowCustomTextGrid(true);

		for (int i = 0; i < Partograph_CommonClass.listValuesItemsContractions.size(); i++) {

			multiRenderer.addXTextLabel(i + 1, Partograph_CommonClass.listValuesItemsContractions.get(i).getStrTime()
					+ "\n" + Partograph_CommonClass.listValuesItemsContractions.get(i).getDuration());

			multiRenderer.setYAxisMax(5);
			multiRenderer.setYLabels(5);
		}

		multiRenderer.addSeriesRenderer(contraction_L20Renderer);
		multiRenderer.addSeriesRenderer(contraction_B20_40Renderer);
		multiRenderer.addSeriesRenderer(contraction_M40Renderer);
		multiRenderer.addSeriesRenderer(contraction_M60Renderer);

		mChartViewContraction = null;

		mChartViewContraction = ChartFactory.getBarChartView(context, dataset, multiRenderer, Type.DEFAULT);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		mChartViewContraction.setLayoutParams(params);
		mChartViewContraction.repaint();

		Bitmap x = bitmapFromChartView(mChartViewContraction, 530, 150);
		if (x != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			x.compress(Bitmap.CompressFormat.PNG, 50, stream);
			try {
				myImg = Image.getInstance(stream.toByteArray());
			} catch (BadElementException e) {
				/*
				 * AppContext.addLog(new
				 * RuntimeException().getStackTrace()[0].getMethodName() + " - "
				 * + this.getClass().getSimpleName(), e);
				 */
				e.printStackTrace();
			} catch (MalformedURLException e) {
				/*
				 * AppContext.addLog(new
				 * RuntimeException().getStackTrace()[0].getMethodName() + " - "
				 * + this.getClass().getSimpleName(), e);
				 */
				e.printStackTrace();
			} catch (IOException e) {
				/*
				 * AppContext.addLog(new
				 * RuntimeException().getStackTrace()[0].getMethodName() + " - "
				 * + this.getClass().getSimpleName(), e);
				 */
				e.printStackTrace();
			}
			myImg.setAlignment(Image.MIDDLE);
		}

		return myImg;
	}

	private Image generatePulseBPGraph() throws Exception {
		Image myImg = null;

		XYSeries bpsystolicseries = new XYSeries("Systolic");
		XYSeries bpdiastolicseries = new XYSeries("Diastolic");
		XYSeries pulseseries = new XYSeries("Pulse");

		for (int i = 0; i < Partograph_CommonClass.listValuesItemsPBP.size(); i++) {

			if (Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPsystolicval().length() > 0
					&& Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPdiastolicval().length() > 0) {
				bpsystolicseries.add((i + 1),
						Double.parseDouble(Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPsystolicval()));
				bpdiastolicseries.add((i + 1),
						Double.parseDouble(Partograph_CommonClass.listValuesItemsPBP.get(i).getStrBPdiastolicval()));
			} else {
				bpsystolicseries.add((i + 1), 0);
				bpdiastolicseries.add((i + 1), 0);
			}

			if (Partograph_CommonClass.listValuesItemsPBP.get(i).getStrPulseval().length() > 0
					&& !Partograph_CommonClass.listValuesItemsPBP.get(i).getStrPulseval().isEmpty()) {
				pulseseries.add((i + 1),
						Double.parseDouble(Partograph_CommonClass.listValuesItemsPBP.get(i).getStrPulseval()));
			} else {
				pulseseries.add((i + 1), 0);
			}

		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding value Series to the dataset

		dataset.addSeries(bpsystolicseries);
		dataset.addSeries(bpdiastolicseries);
		dataset.addSeries(pulseseries);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer bpsystolicRenderer = new XYSeriesRenderer();
		bpsystolicRenderer.setColor(context.getResources().getColor(R.color.blue));
		bpsystolicRenderer.setPointStyle(PointStyle.CIRCLE);
		bpsystolicRenderer.setFillPoints(true);
		bpsystolicRenderer.setLineWidth(1);
		bpsystolicRenderer.setDisplayChartValues(true);
		bpsystolicRenderer.setChartValuesTextSize(10);
		// bpsystolicRenderer.setShowLegendItem(true);

		// Creating XYSeriesRenderer to customize bpsystolicSeries
		XYSeriesRenderer bpdiastolicRenderer = new XYSeriesRenderer();
		bpdiastolicRenderer.setColor(context.getResources().getColor(R.color.yellow));
		bpdiastolicRenderer.setPointStyle(PointStyle.CIRCLE);
		bpdiastolicRenderer.setFillPoints(true);
		bpdiastolicRenderer.setLineWidth(1);
		bpdiastolicRenderer.setDisplayChartValues(true);
		bpdiastolicRenderer.setChartValuesTextSize(10);
		// bpdiastolicRenderer.setShowLegendItem(true);

		// Creating XYSeriesRenderer to customize pulseSeries
		XYSeriesRenderer pulseRenderer = new XYSeriesRenderer();
		pulseRenderer.setColor(context.getResources().getColor(R.color.green));
		pulseRenderer.setPointStyle(PointStyle.CIRCLE);
		pulseRenderer.setFillPoints(true);
		pulseRenderer.setLineWidth(1);
		pulseRenderer.setDisplayChartValues(true);
		pulseRenderer.setChartValuesTextSize(10);
		// pulseRenderer.setShowLegendItem(true);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		// multiRenderer.setDisplayChartValues(false);
		// multiRenderer.setChartTitle("Bp");
		multiRenderer.setChartTitleTextSize(10);

		multiRenderer.setXLabelsColor(context.getResources().getColor(R.color.black));
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setLabelsTextSize(8);
		multiRenderer.setFitLegend(true);
		multiRenderer.setLegendTextSize(8);
		multiRenderer.setLegendHeight(10);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setMargins(new int[] { 10, 13, -10, 12 });
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.WHITE);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setAxisTitleTextSize(10);
		multiRenderer.setPointSize(1);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setShowGrid(true);
		multiRenderer.setPanEnabled(false, false);

		multiRenderer.setShowGridX(true);
		multiRenderer.setShowGridY(true);

		multiRenderer.setYAxisMax(200);
		multiRenderer.setYAxisMin(60);
		multiRenderer.setYLabels(15);
		// multiRenderer.setYAxisMin(40);
		multiRenderer.setBarSpacing(1);
		multiRenderer.setXAxisMin(0);
		multiRenderer.setXAxisMax(12);
		multiRenderer.setXLabelsAngle(0); // 18jan2016

		// multiRenderer.setZoomButtonsVisible(true);
		for (int i = 0; i < Partograph_CommonClass.listValuesItemsPBP.size(); i++) {
			multiRenderer.addXTextLabel((i + 1), Partograph_CommonClass.listValuesItemsPBP.get(i).getStrTime());
			// multiRenderer.addXTextLabel(i, ""+i+1);
			// 05jan2016
			multiRenderer.setYAxisMax(180);
			multiRenderer.setYLabels(15);
		}

		multiRenderer.addSeriesRenderer(bpsystolicRenderer);
		multiRenderer.addSeriesRenderer(bpdiastolicRenderer);
		multiRenderer.addSeriesRenderer(pulseRenderer);

		mChartViewPBP = null;

		String[] types = new String[] { BarChart.TYPE, BarChart.TYPE, LineChart.TYPE };

		mChartViewPBP = ChartFactory.getCombinedXYChartView(context, dataset, multiRenderer, types);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				300);
		mChartViewPBP.setLayoutParams(params);
		mChartViewPBP.repaint();

		Bitmap y = bitmapFromChartView(mChartViewPBP, 520, 150);
		if (y != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			y.compress(Bitmap.CompressFormat.PNG, 50, stream);
			try {
				myImg = Image.getInstance(stream.toByteArray());
			} catch (BadElementException e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			} catch (MalformedURLException e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			} catch (IOException e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
			myImg.setAlignment(Image.MIDDLE);
		}

		return myImg;
	}

	// 26May2017 Arpitha
	public static boolean CheckAutomaticDateTime(String userId, String classname, Context context)
			throws NotFoundException, Exception {
		Date lastregdate = null;
		dbh = new Partograph_DB(context);
		String strlastinserteddate = dbh.getlastmaxdate(userId, classname);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault());// 15May2017
																									// Arpitha
		if (strlastinserteddate != null) {
			lastregdate = format.parse(strlastinserteddate);
		}
		Date currentdatetime = new Date();
		if (lastregdate != null && currentdatetime.before(lastregdate)) {
			Partograph_CommonClass.showSettingsAlert(context,
					context.getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
			return false;
		}

		return true;

	}

	// Alert dialog
	public static void displayAlertDialog(String message, Context context) throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(context.getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	// 16Jan2017 Arpitha
	public static void pdfAlertDialog(final Context context, final Women_Profile_Pojo woman, final String classname,
			final ArrayList<LatentPhasePojo> arrval, final ArrayList<DiscgargePojo> dvalues) {

		final Dialog dialog = new Dialog(context);

		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.dialog_action);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(context.getResources().getString(R.string.pdf));

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		final RadioButton rdsavepdf = (RadioButton) dialog.findViewById(R.id.rdsave);
		final RadioButton rdviewpdf = (RadioButton) dialog.findViewById(R.id.rdview);
		Button btndone = (Button) dialog.findViewById(R.id.btndone);

		btndone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				dbh = Partograph_DB.getInstance(context);

				if (rdsavepdf.isChecked()) {
					try {
						dialog.cancel();
						if (classname.equalsIgnoreCase("partograph")) {

							Toast.makeText(context, context.getResources().getString(R.string.pdf_file_is_saved_at_path)
									+ " " + "Phone Storage/Partograph/PDF/PartographSheet", Toast.LENGTH_LONG).show();
							createPdfDocument(woman);
						} else if (classname.equalsIgnoreCase("referral")) {
							Toast.makeText(context, context.getResources().getString(R.string.pdf_file_is_saved_at_path)
									+ " " + "Phone Storage/Partograph/PDF/ReferralSheet", Toast.LENGTH_LONG).show();

							generateReferralPdf(context, woman);

						} else if (classname.equalsIgnoreCase("latentphase")) {
							Toast.makeText(context, context.getResources().getString(R.string.pdf_file_is_saved_at_path)
									+ " " + "Phone Storage/Partograph/PDF/Latent Phase", Toast.LENGTH_LONG).show();
							generateLatentPhaseinpdfFormat(context, woman);

						} else if (classname.equalsIgnoreCase("discharge")) {
							Toast.makeText(context, context.getResources().getString(R.string.pdf_file_is_saved_at_path)
									+ " " + "Phone Storage/Partograph/PDF/Discharge Slip", Toast.LENGTH_LONG).show();
							generateDischargeSlipinpdfFormat(context, woman);
						} else if (classname.equalsIgnoreCase("postpartum")) {
							Toast.makeText(context, context.getResources().getString(R.string.pdf_file_is_saved_at_path)
									+ " " + "Phone Storage/Partograph/PDF/PostPartum Care", Toast.LENGTH_LONG).show();
							generatePostPartumCareDatainpdfFormat(context, woman);
						} else if (classname.equalsIgnoreCase("all")) {
							generatepdf(woman, context);
							Toast.makeText(context, context.getResources().getString(R.string.pdf_file_is_saved_at_path)
									+ " " + "Phone Storage/Partograph/PDF ", Toast.LENGTH_LONG).show();

							// generateAllPdfFiles(context, woman);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} else if (rdviewpdf.isChecked())

				{
					try {
						dialog.cancel();
						if (classname.equalsIgnoreCase("partograph")) {

							createPdfDocument(woman);
							pdfpath = Uri.fromFile(partofile);
						} else if (classname.equalsIgnoreCase("referral")) {
							generateReferralPdf(context, woman);
							pdfpath = Uri.fromFile(referralfile);

						} else if (classname.equalsIgnoreCase("latentphase")) {
							generateLatentPhaseinpdfFormat(context, woman);
							pdfpath = Uri.fromFile(latentfile);
						}

						else if (classname.equalsIgnoreCase("discharge")) {
							generateDischargeSlipinpdfFormat(context, woman);
							pdfpath = Uri.fromFile(dischrgefile);
						} else if (classname.equalsIgnoreCase("postpartum")) {
							generatePostPartumCareDatainpdfFormat(context, woman);
							pdfpath = Uri.fromFile(postpartumfile);
						} else if (classname.equalsIgnoreCase("all")) {
							// generateAllPdfFiles(context, woman);
							generatepdf(woman, context);
							pdfpath = Uri.fromFile(allfile);
						}
						// to view pdf file from appliction
						if (pdfpath != null) {

							// Uri path = Uri.fromFile(partofile);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(pdfpath, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								context.startActivity(intent);
							} catch (ActivityNotFoundException e) {// 24Jan2017
																	// Arpitha
								// No application to view, ask to download one
								AlertDialog.Builder builder = new AlertDialog.Builder(context);
								builder.setTitle("No Application Found");
								builder.setMessage("Download one from Android Market?");
								builder.setPositiveButton("Yes, Please", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent marketIntent = new Intent(Intent.ACTION_VIEW);
										marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
										context.startActivity(marketIntent);
									}
								});
								builder.setNegativeButton("No, Thanks", null);
								builder.create().show();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} // 24Jan2017

				else {
					Toast.makeText(context, context.getResources().getString(R.string.please_select_one_option),
							Toast.LENGTH_LONG).show();
				}

			}
		});
		dialog.show();

	}

	public static void createPdfDocument(Women_Profile_Pojo woman) throws Exception {
		// TODO Auto-generated method stub

		// partographDoc = new Document(PageSize.A4);

		try {

			/*
			 * path = AppContext.mainDir + "/PDF/PartographSheet";// 03April2017
			 * // Arpitha
			 * 
			 * 
			 * dir = new File(path); if (!dir.exists()) dir.mkdirs();
			 * 
			 * 
			 * dir = new File(path); if (!dir.exists()) dir.mkdirs();
			 * 
			 * Log.d("PDFCreator", "PDF Path: " + path);
			 * 
			 * partofile = new File(dir, woman.getWomen_name() +
			 * "_PartographSheet.pdf");
			 * 
			 * // file = new File(dir, woman.getWomen_name() + //
			 * "_PartographSheet.pdf"); FileOutputStream fOut = new
			 * FileOutputStream(partofile);
			 * 
			 * PdfWriter.getInstance(partographDoc, fOut);
			 * 
			 * partographDoc.setMarginMirroring(false);
			 * partographDoc.setMargins(0, 0, 0, 0);
			 * 
			 * partographDoc.open();
			 * 
			 * womanID = woman.getWomenId(); userId = woman.getUserId();
			 * 
			 * Paragraph p_heading = new
			 * Paragraph(context.getResources().getString(R.string.partograph));
			 * Font paraFont = new Font(Font.HELVETICA);
			 * p_heading.setAlignment(Paragraph.ALIGN_CENTER);
			 * p_heading.setFont(paraFont); partographDoc.add(p_heading);
			 * 
			 * // InputStream ims = getAssets().open("myImage.png"); //
			 * InputStream img = //
			 * context.getResources().getDrawable(R.drawable.bc_icon); // Bitmap
			 * bmp = BitmapFactory.decodeStream(ims); // ByteArrayOutputStream
			 * stream = new ByteArrayOutputStream(); //
			 * bmp.compress(Bitmap.CompressFormat.PNG, 100, stream); // Image
			 * image = Image.getInstance(stream.toByteArray()); //
			 * doc.add(image);
			 * 
			 * Drawable d =
			 * context.getResources().getDrawable(R.drawable.sentiagraph);
			 * 
			 * BitmapDrawable bitDw = ((BitmapDrawable) d);
			 * 
			 * Bitmap bmp = bitDw.getBitmap();
			 * 
			 * ByteArrayOutputStream stream = new ByteArrayOutputStream();
			 * 
			 * bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);
			 * 
			 * Image image = Image.getInstance(stream.toByteArray());
			 * 
			 * image.setIndentationLeft(6); partographDoc.add(image);
			 * 
			 * // document.close();
			 * 
			 * // strdatetime = strdatetime.replace(" ", "_"); String
			 * strdatetime = Partograph_CommonClass.getConvertedDateFormat(
			 * Partograph_CommonClass.getTodaysDate(),
			 * Partograph_CommonClass.defdateformat) + " " +
			 * Partograph_CommonClass.getCurrentTime() + " "; Paragraph p1 = new
			 * Paragraph(strdatetime, small); p1.setIndentationLeft(530); //
			 * p1.setAlignment(Paragraph.ALIGN_RIGHT); partographDoc.add(p1);
			 * 
			 * womanBasicInfo(woman);
			 * 
			 * partographDoc.add(womanbasics);
			 * 
			 */

			String path = AppContext.mainDir + "/PDF/PartographSheet";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			partofile = new File(dir, woman.getWomen_name() + "_PatographSheet.pdf");

			basic(woman, context, partofile, context.getResources().getString(R.string.partograph));

			partograpgdata(woman, context);

			close();

			/*
			 * // Rectangle rect = new Rectangle(10, 5, 585, 835); Rectangle
			 * rect = new Rectangle(5, 5, 592, 816); rect.enableBorderSide(1);
			 * rect.enableBorderSide(1); rect.enableBorderSide(1);
			 * rect.enableBorderSide(1); rect.setBorder(2); //
			 * rect.setBorderColor(context.getResources().getColor(R.color.black
			 * )); rect.setBorder(Rectangle.BOX); rect.setBorderWidth(1);
			 * partographDoc.add(rect);
			 */

		} catch (

		DocumentException de)

		{
			Log.e("PDFCreator", "DocumentException:" + de);
		} catch (

		IOException e)

		{
			Log.e("PDFCreator", "ioException:" + e);
		} finally

		{
			doc.close();
		}

	}

	private static void partograpgdata(Women_Profile_Pojo woman2, Context context2) throws Exception {
		// TODO Auto-generated method stub

		womanID = woman2.getWomenId();
		userId = woman2.getUserId();
		if (!(LoginActivity.objectid.contains(1)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			Image img_fhr = generateFHRGraph(context);
			doc.add(img_fhr);
		}
		if (!(LoginActivity.objectid.contains(2)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			tableAFM = generateAFM(context);
			doc.add(tableAFM);
		}

		if (!(LoginActivity.objectid.contains(3)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			Image bar_dilatation = generateDilatationGraph();
			doc.add(bar_dilatation);
		}

		if (!(LoginActivity.objectid.contains(4)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			Image imgcontraction = generateContractionGraph();
			doc.add(imgcontraction);
		}

		if (!(LoginActivity.objectid.contains(5)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			table_oxytocin = generateOxytocinGraph();
			doc.add(table_oxytocin);
		} // 27March2017 Arpitha

		if (!(LoginActivity.objectid.contains(6)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			table_drug = generateDrugsGraph();
			doc.add(table_drug);
		}
		if (!(LoginActivity.objectid.contains(7)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			Image img_pulsepb = generatePulseBPGraph(context);
			doc.add(img_pulsepb);
		}

		if (!(LoginActivity.objectid.contains(8)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			table_temp = generateTemperatureGraph();
			doc.add(table_temp);
		}

		if (!(LoginActivity.objectid.contains(9)))// 27March2017 Arpitha

		{// 27March2017 Arpitha
			table_urine = generateUrineTestGraph();
			doc.add(table_urine);
		}

	}

	public static void womanBasicInfo(Women_Profile_Pojo woman) {
		try {
			String delstatus;
			ArrayList<String> val = new ArrayList<String>();
			val.add(woman.getWomen_name());
			val.add(context.getResources().getString(R.string.age) + ":" + woman.getAge()
					+ context.getResources().getString(R.string.yrs));

			if (woman.getGestationage() == 0)
				val.add(context.getResources().getString(R.string.gest_short_label) + ":"
						+ context.getResources().getString(R.string.notknown));
			else
				val.add(context.getResources().getString(R.string.gest_short_label) + ":" + woman.getGestationage()
						+ context.getResources().getString(R.string.wks) + " " + woman.getGest_age_days()
						+ context.getResources().getString(R.string.days));
			if (woman.getregtype() != 2) {// 12Jan2017 Arpitha
				String strDOA = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				val.add(context.getResources().getString(R.string.doadm) + ":" + strDOA + "/"
						+ woman.getTime_of_admission());
			} else {// 12Jan2017 Arpitha
				val.add(context.getResources().getString(R.string.doadm) + ":" + woman.getDate_of_reg_entry());
			}

			womanbasics = new PdfPTable(val.size());

			womanbasics.setWidthPercentage(90);

			womanbasics.setWidths(new int[] { 15, 3, 4, 7 });
			// womanbasics.setTotalWidth(300);

			for (int k = 0; k < val.size(); k++) {
				String header = val.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(Rectangle.BOX);

				womanbasics.addCell(cell);
			}

			ArrayList<String> arrVal = new ArrayList<String>();
			arrVal.add(context.getResources().getString(R.string.facility) + ": " + woman.getFacility_name() + "["
					+ woman.getFacility() + "]");
			arrVal.add(context.getResources().getString(R.string.gravida) + ":" + woman.getGravida());
			arrVal.add(context.getResources().getString(R.string.para) + ":" + woman.getPara());

			if (woman.getDel_type() != 0) {
				delstatus = context.getResources().getString(R.string.delivered);
			} else {
				dbh = Partograph_DB.getInstance(context);

				if (dbh.getPartoDilData(woman.getWomenId(), woman.getUserId()))
					delstatus = context.getResources().getString(R.string.del_prog);
				else
					delstatus = context.getResources().getString(R.string.registered);

			}

			arrVal.add(context.getResources().getString(R.string.delstatus) + ": " + delstatus);

			for (int k = 0; k < arrVal.size(); k++) {
				String header = arrVal.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(Rectangle.BOX);

				womanbasics.addCell(cell);
			}

			ArrayList<String> address = new ArrayList<String>();
			address.add(context.getResources().getString(R.string.address) + ": " + woman.getAddress());

			for (int k = 0; k < address.size(); k++) {
				String header = address.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 8, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(Rectangle.BOX);

				womanbasics.addCell(cell);
			}

			// womanbasics.setSpacingBefore(3);

			womanbasics.completeRow();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 02Jun2017 Arpitha
	public static boolean displayHighRiskReasons(String exit_msg, String risk_observed, Context context)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		try {

			ArrayList<String> op = new ArrayList<String>();
			String[] riskoptions = exit_msg.split(",");
			String[] riskoptArr = null;

			riskoptArr = context.getResources().getStringArray(R.array.riskoptionsvalues);

			if (riskoptArr != null) {
				if (riskoptions != null && riskoptions.length > 0) {
					for (String str : riskoptions) {
						if (str != null && str.length() > 0)
							op.add(riskoptArr[Integer.parseInt(str)]);
					}
				}
			}

			if (risk_observed != null && risk_observed.trim().length() > 0) {
				op.add(context.getResources().getString(R.string.other_risk_observed) + " " + risk_observed);
			}

			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

			alertDialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor)
					+ "'>" + context.getResources().getString(R.string.highriskreasons) + "</font>"));// 02Feb2017
																										// Arpitha

			View convertView = LayoutInflater.from(context).inflate(R.layout.alertdialog_danger, null);
			ListView lv = (ListView) convertView.findViewById(R.id.listoptions);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, op);
			alertDialog.setView(convertView).setCancelable(true).setPositiveButton(
					context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			lv.setAdapter(adapter);

			// 08Feb2017 Arpitha
			Dialog d = alertDialog.show();
			int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = d.findViewById(dividerId);
			divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017
																							// Arpitha

			d.show();

		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

		return true;
	}

	// 02Jun2017 Arpitha
	public static boolean displayDangervalues(ArrayList<String> exit_msg, Context context) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());

		ArrayList<String> op = new ArrayList<String>();
		ArrayList<String> riskoptions = exit_msg;
		String[] riskoptArr = null;

		riskoptArr = context.getResources().getStringArray(R.array.parameter);

		if (riskoptArr != null) {
			if (riskoptions != null && riskoptions.size() > 0) {
				for (String str : riskoptions) {
					if (str != null && str.length() > 0)
						op.add(riskoptArr[Integer.parseInt(str)]);
				}
			}
		}

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

		alertDialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
				+ context.getResources().getString(R.string.partographparametersindanger) + "</font>"));// 02Feb2017

		View convertView = LayoutInflater.from(context).inflate(R.layout.alertdialog_danger, null);
		ListView lv = (ListView) convertView.findViewById(R.id.listoptions);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, op);
		alertDialog.setView(convertView).setCancelable(true).setPositiveButton(
				context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		lv.setAdapter(adapter);

		Dialog d = alertDialog.show();
		int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = d.findViewById(dividerId);
		divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017
																						// Arpitha

		d.show();

		return true;
	}

	// 20Oct2016 Arpitha
	public static boolean displaySummaryDialog(ArrayList<Women_Profile_Pojo> data, int pos, Context context)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.activity_summary);

		dialog.show();

		img1 = (ImageView) dialog.findViewById(R.id.img1);
		img2 = (ImageView) dialog.findViewById(R.id.img2);
		img3 = (ImageView) dialog.findViewById(R.id.img3);
		img4 = (ImageView) dialog.findViewById(R.id.img4);
		img5 = (ImageView) dialog.findViewById(R.id.img5);
		img6 = (ImageView) dialog.findViewById(R.id.img6);
		img7 = (ImageView) dialog.findViewById(R.id.img7);// 01nov2016 Arpitha
		// 31Aug2017 Arpitha
		imglatent = (ImageView) dialog.findViewById(R.id.imglatent);
		imgpostpartum = (ImageView) dialog.findViewById(R.id.imgpost);
		imgdiscahrge = (ImageView) dialog.findViewById(R.id.imgdischarge);

		TextView txtname = (TextView) dialog.findViewById(R.id.wname);
		TextView txtage = (TextView) dialog.findViewById(R.id.wage);
		TextView txtregdate = (TextView) dialog.findViewById(R.id.wdoa);
		TextView txtgest = (TextView) dialog.findViewById(R.id.wgest);
		TextView txtgravida = (TextView) dialog.findViewById(R.id.wgravida);
		TextView txtrisk = (TextView) dialog.findViewById(R.id.wtrisk);
		TextView txtdelstatus = (TextView) dialog.findViewById(R.id.wdelstatus);
		TextView txtdeldate = (TextView) dialog.findViewById(R.id.wdeldate);

		txtdeldate.setVisibility(View.GONE);
		txtdelstatus.setVisibility(View.GONE);

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);

		// 01Nov2016 Arpitha

		wname = data.get(pos).getWomen_name() == null ? " " : data.get(pos).getWomen_name();
		age = data.get(pos).getAge();
		if (data.get(pos).getregtype() != 2)
			regdate = Partograph_CommonClass.getConvertedDateFormat(data.get(pos).getDate_of_admission(),
					Partograph_CommonClass.defdateformat) + "/" + data.get(pos).getTime_of_admission();
		else
			regdate = Partograph_CommonClass.getConvertedDateFormat(data.get(pos).getDate_of_reg_entry().split("/")[0],
					Partograph_CommonClass.defdateformat + "/" + data.get(pos).getDate_of_reg_entry().split("/")[1]);

		gravida = context.getResources().getString(R.string.gravida_short_label) + ":" + data.get(pos).getGravida()
				+ "," + context.getResources().getString(R.string.para_short_label) + ":" + data.get(pos).getPara();
		if (data.get(pos).getRisk_category() == 0)
			risk = context.getResources().getString(R.string.low);
		else
			risk = context.getResources().getString(R.string.high);
		gest = data.get(pos).getGestationage();

		txtname.setText(wname);
		txtage.setText("" + age + context.getResources().getString(R.string.yrs));
		txtgravida.setText(gravida);
		txtregdate.setText(context.getResources().getString(R.string.reg) + ":" + regdate);
		if (gest == 0) {
			txtgest.setText(context.getResources().getString(R.string.gest_age) + ":"
					+ context.getResources().getString(R.string.notknown));
		} else// 18Nov2016 Arpitha
		{
			if (data.get(pos).getGest_age_days() == 0) {
				txtgest.setText(context.getResources().getString(R.string.gest_age) + ":" + "" + gest);
			} else
				txtgest.setText(context.getResources().getString(R.string.gest_age) + ":" + "" + gest + " "
						+ context.getResources().getString(R.string.wks) + data.get(pos).getGest_age_days()
						+ context.getResources().getString(R.string.days));
		} // 18Nov2016 Arpitha
		txtrisk.setText(context.getResources().getString(R.string.risk_short_label) + ":" + risk);

		dbh = new Partograph_DB(context);

		ArrayList<String> values = new ArrayList<String>();
		values = dbh.getentereddata(data.get(pos).getWomenId());

		if (values.get(0).equalsIgnoreCase("yes")) {
			img1.setImageResource(R.drawable.ic_ok);
		} else
			img1.setImageResource(R.drawable.ic_cancel);

		if (values.get(4).equalsIgnoreCase("yes")) {
			img2.setImageResource(R.drawable.ic_ok);
		} else
			img2.setImageResource(R.drawable.ic_cancel);

		if (values.get(5).equalsIgnoreCase("yes")) {
			img3.setImageResource(R.drawable.ic_ok);
		} else
			img3.setImageResource(R.drawable.ic_cancel);
		if (values.get(2).equalsIgnoreCase("yes")) {
			img4.setImageResource(R.drawable.ic_ok);
		} else
			img4.setImageResource(R.drawable.ic_cancel);
		if (values.get(1).equalsIgnoreCase("yes")) {
			img5.setImageResource(R.drawable.ic_ok);
		} else
			img5.setImageResource(R.drawable.ic_cancel);
		if (values.get(3).equalsIgnoreCase("yes")) {
			img6.setImageResource(R.drawable.ic_ok);
		} else
			img6.setImageResource(R.drawable.ic_cancel);
		if (values.get(6).equalsIgnoreCase("yes")) {
			img7.setImageResource(R.drawable.ic_ok);
		} else
			img7.setImageResource(R.drawable.ic_cancel);// 01Nov2016 Arpitha

		// 31Aug2017 Arpitha
		if (values.get(7).equalsIgnoreCase("yes")) {
			imglatent.setImageResource(R.drawable.ic_ok);
		} else
			imglatent.setImageResource(R.drawable.ic_cancel);
		if (values.get(8).equalsIgnoreCase("yes")) {
			imgpostpartum.setImageResource(R.drawable.ic_ok);
		} else
			imgpostpartum.setImageResource(R.drawable.ic_cancel);
		if (values.get(9).equalsIgnoreCase("yes")) {
			imgdiscahrge.setImageResource(R.drawable.ic_ok);
		} else
			imgdiscahrge.setImageResource(R.drawable.ic_cancel);

		imgbtncanceldialog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();

			}
		});

		return true;
	}

	// 05Jun2017 Arpitha
	public static void dialogSearch(final Context context) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_search);
		// dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
		// R.layout.dialog_title);
		// txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		// txtdialogtitle.setText(context.getResources().getString(R.string.search));

		Button btnsearch = (Button) dialog.findViewById(R.id.btnsearch);
		etsearch = (EditText) dialog.findViewById(R.id.etsearch);
		list = (ListView) dialog.findViewById(R.id.listsearchresult);
		txtnoData = (TextView) dialog.findViewById(R.id.txtnodata);

		Button btnback = (Button) dialog.findViewById(R.id.btnback);

		dbh = new Partograph_DB(context);

		dialog.show();
		// ImageButton imgbtncanceldialog = (ImageButton)
		// dialog.findViewById(R.id.imgbtncanceldialog);
		// imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// try {
		// dialog.cancel();
		//
		// } catch (Exception e) {
		// AppContext.addLog(new
		// RuntimeException().getStackTrace()[0].getMethodName() + " - "
		// + this.getClass().getSimpleName(), e);
		// e.printStackTrace();
		// }
		// }
		// });

		mQuickAction = new QuickAction(context);
		displayOptions(context);

		btnsearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String strSearch = etsearch.getText().toString();
				if (strSearch.trim().length() > 0)
					getSearchData(strSearch, context);
				else
					Toast.makeText(context, context.getResources().getString(R.string.enter_search_string),
							Toast.LENGTH_LONG).show();
			}
		});

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
				// TODO Auto-generated method stub

				woman = (Women_Profile_Pojo) adapter.getItemAtPosition(pos);

				mQuickAction.show(v);

			}
		});

		btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();

			}
		});

		etsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					String strSearch = etsearch.getText().toString();
					if (strSearch.trim().length() > 0)
						getSearchData(strSearch, context);
					else
						Toast.makeText(context, context.getResources().getString(R.string.enter_search_string),
								Toast.LENGTH_LONG).show();
					return true;
				}
				return false;
			}
		});
	}

	static void getSearchData(String strSearch, Context context) {

		// TODO Auto-generated method stub

		rowItems = new ArrayList<Women_Profile_Pojo>();
		Women_Profile_Pojo wdata;
		Cursor cursor = dbh.getSearchResult(user.getUserId(), strSearch);
		if (cursor.getCount() > 0) {

			cursor.moveToFirst();

			do {

				int pos = cursor.getPosition();
				// if (pos <= displayListCount) {
				wdata = new Women_Profile_Pojo();
				wdata.setWomenId(cursor.getString(1));

				if (cursor.getType(2) > 0) {
					String b = (cursor.getString(2).length() > 1) ? cursor.getString(2) : null;
					byte[] decoded = (b != null) ? Base64.decode(b, Base64.DEFAULT) : null;
					wdata.setWomen_Image(decoded);
				}

				wdata.setWomen_name(cursor.getString(3));
				wdata.setDate_of_admission(cursor.getString(4));
				wdata.setTime_of_admission(cursor.getString(5));
				wdata.setAge(cursor.getInt(6));
				wdata.setAddress(cursor.getString(7));
				wdata.setPhone_No(cursor.getString(8));
				wdata.setDel_type(cursor.getInt(9));
				wdata.setDoc_name(cursor.getString(10));
				wdata.setNurse_name(cursor.getString(11));
				wdata.setW_attendant(cursor.getString(12));
				wdata.setGravida(cursor.getInt(13));
				wdata.setPara(cursor.getInt(14));
				wdata.setHosp_no(cursor.getString(15));
				wdata.setFacility(cursor.getString(16));
				wdata.setWomenId(cursor.getString(1));
				wdata.setSpecial_inst(cursor.getString(17));
				wdata.setUserId(cursor.getString(0));
				wdata.setComments(cursor.getString(19));
				wdata.setRisk_category(cursor.getInt(20));
				wdata.setDel_Comments(cursor.getString(21));
				wdata.setDel_Time(cursor.getString(22));
				wdata.setDel_Date(cursor.getString(23));
				wdata.setDel_result1(cursor.getInt(24));
				wdata.setNo_of_child(cursor.getInt(25));
				wdata.setBabywt1(cursor.getInt(26));
				wdata.setBabywt2(cursor.getInt(27));
				wdata.setBabysex1(cursor.getInt(28));
				wdata.setBabysex2(cursor.getInt(29));
				wdata.setGestationage(cursor.getInt(30));
				wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
				wdata.setDel_result2(cursor.getInt(31));
				wdata.setAdmitted_with(cursor.getString(34));
				wdata.setMemb_pres_abs(cursor.getInt(35));
				wdata.setMothers_death_reason(cursor.getString(36));
				wdata.setState(cursor.getString(38));
				wdata.setDistrict(cursor.getString(39));
				wdata.setTaluk(cursor.getString(40));
				wdata.setFacility_name(cursor.getString(41));
				wdata.setGest_age_days(cursor.getInt(45));

				// updated on 23Nov2015
				wdata.setDel_time2(cursor.getString(33));

				// updated on 30Nov2015
				wdata.setThayicardnumber(cursor.getString(46));
				wdata.setDelTypeReason(cursor.getString(47));
				wdata.setDeltype_otherreasons(cursor.getString(48));

				// 27dec2015
				wdata.setLmp(cursor.getString(49));
				wdata.setRiskoptions(cursor.getString(50));

				// 04jan2016
				wdata.setTears(cursor.getString(51));
				wdata.setEpisiotomy(cursor.getInt(52));
				wdata.setSuturematerial(cursor.getInt(53));

				// 19jan2016
				wdata.setBloodgroup(cursor.getInt(54));
				// updated on 23/2/16 by Arpitha
				wdata.setEdd(cursor.getString(55));
				wdata.setHeight(cursor.getString(56));
				wdata.setW_weight(cursor.getString(57));
				wdata.setOther(cursor.getString(58));

				// updated on 29May by Arpitha
				wdata.setHeight_unit(cursor.getInt(59));

				wdata.setExtracomments(cursor.getString(60));// 13Oct2016
																// Arpitha
				wdata.setregtype(cursor.getInt(61));// 16Oct2016 Arpitha

				wdata.setRegtypereason(cursor.getString(62));// 18Oct2016
																// Arpitha
				wdata.setDate_of_reg_entry(cursor.getString(63));// 10Nov2016
																	// Arpitha

				wdata.setNormaltype(cursor.getInt(64));// 20Nov2016
														// Arpitha

				wdata.setRegtypeOtherReason(cursor.getString(65));// 21Nov2016
																	// Arpitha

				// boolean isDanger =
				// dbh.chkIsDanger(cursor.getString(1),
				// user.getUserId());

				// wdata.setDanger(isDanger);

				rowItems.add(wdata);
				// }
			} while (cursor.moveToNext());

			RecentCustomListAdapterSearch adapterDIP = new RecentCustomListAdapterSearch(context,
					R.layout.activity_womenlist, rowItems, dbh);
			list.setAdapter(adapterDIP);
			list.setVisibility(View.VISIBLE);
			txtnoData.setVisibility(View.GONE);

		} else {
			txtnoData.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}

	}

	// Display options on click of list item
	private static void displayOptions(final Context context) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());

		try {

			ActionItem viewprofile_Item = new ActionItem(ID_PROFILE,
					context.getResources().getString(R.string.view_profile),
					context.getResources().getDrawable(R.drawable.ic_viewprofile_menu));
			ActionItem graph_Item = new ActionItem(ID_GRAPH, context.getResources().getString(R.string.partograph),
					context.getResources().getDrawable(R.drawable.ic_add_parto_menu));

			ActionItem deliverystatus_Item = new ActionItem(ID_DELSTATUS,
					context.getResources().getString(R.string.delivery_status),
					context.getResources().getDrawable(R.drawable.ic_del_sttaus_menu));
			ActionItem apgarscore_Item = new ActionItem(ID_APGAR,
					context.getResources().getString(R.string.apgar_score),
					context.getResources().getDrawable(R.drawable.ic_apgar_menu));

			// updated on 16Nov2015
			ActionItem referredto_Item = new ActionItem(ID_REFERRAL,
					context.getResources().getString(R.string.referrral_info),
					context.getResources().getDrawable(R.drawable.ic_referral_menu));

			// updated on 26Nov2015
			ActionItem stagesoflabor_Item = new ActionItem(ID_STAGEOFLABOR,
					context.getResources().getString(R.string.stagesoflabor),
					context.getResources().getDrawable(R.drawable.thid_fourth_menu));

			// updated on 23dec2015
			ActionItem print_partoitem = new ActionItem(ID_PRINTPARTO,
					context.getResources().getString(R.string.printparto),
					context.getResources().getDrawable(R.drawable.ic_view_graph_menu));
			// updated on 17Oct2016 Arpitha

			ActionItem additonal_details = new ActionItem(ID_ADDITIONALDETAILS,
					context.getResources().getString(R.string.additional_details),
					context.getResources().getDrawable(R.drawable.ic_additional));// 06Dec2016
			// Arpitha

			// 21July2017 Arpitha
			ActionItem dischargeDetails = new ActionItem(ID_DISCHARGEDETAILS,
					context.getResources().getString(R.string.discharge_details),
					context.getResources().getDrawable(R.drawable.discharge));

			// 10Agu2017 Arpitha
			ActionItem latentphase = new ActionItem(ID_LATENTPHASE,
					context.getResources().getString(R.string.latent_phase),
					context.getResources().getDrawable(R.drawable.latentphase));

			// 18Agu2017 Arpitha
			ActionItem postpartumcare = new ActionItem(ID_POSTPARTUM, "PostPartum",
					context.getResources().getDrawable(R.drawable.postpartum));

			// 31Agu2017 Arpitha
			ActionItem pdf = new ActionItem(ID_PDF, context.getResources().getString(R.string.export_pdf),
					context.getResources().getDrawable(R.drawable.pdf5));

			mQuickAction.addActionItem(latentphase);// 10Aug2017 Arpitha
			// updated on 23dec2015
			mQuickAction.addActionItem(print_partoitem);
			mQuickAction.addActionItem(graph_Item);
			mQuickAction.addActionItem(deliverystatus_Item);
			mQuickAction.addActionItem(apgarscore_Item);

			// updated on 26Nov2015
			mQuickAction.addActionItem(stagesoflabor_Item);

			mQuickAction.addActionItem(postpartumcare);// 18Aug2017 Arpitha

			// changed by Arpitha 26Feb2016
			mQuickAction.addActionItem(referredto_Item);

			mQuickAction.addActionItem(additonal_details);// 06Dec2016 Arpitha

			mQuickAction.addActionItem(viewprofile_Item);

			mQuickAction.addActionItem(dischargeDetails);// 21July2017 Arpitha

			mQuickAction.addActionItem(pdf);// 31Aug2017 Arpitha

			// Arpitha

			// setup the action item click listener
			mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
				@Override
				public void onItemClick(QuickAction quickAction, int pos, int actionId) {

					try {

						if (actionId == ID_PROFILE) { // Message item
														// selected

							if (Partograph_CommonClass.autodatetime(context)) {// 27Sep2016
																				// Arpitha
								Intent view = new Intent(context, ViewProfile_Activity.class);
								view.putExtra("woman", woman);
								context.startActivity(view);
							}

						}
						if (actionId == ID_GRAPH) {

							if (Partograph_CommonClass.autodatetime(context)) {// 27Sep2016
																				// Arpitha

								Intent graph = new Intent(context, SlidingActivity.class);
								graph.putExtra("woman", woman);
								context.startActivity(graph);
							}

						}
						if (actionId == ID_DELSTATUS) {

							if (Partograph_CommonClass.autodatetime(context)) {// 27Sep2016
																				// Arpitha

								if (woman != null) {

									Intent graph = new Intent(context, DeliveryInfo_Activity.class);
									graph.putExtra("woman", woman);
									context.startActivity(graph);
								}

							}
						}
						if (actionId == ID_APGAR) {

							if (Partograph_CommonClass.autodatetime(context)) {// 27Sep2016
																				// Arpitha

								if (woman.getDel_type() != 0 &&

										(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent viewapgar = new Intent(context, Activity_Apgar.class);
									viewapgar.putExtra("woman", woman);
									context.startActivity(viewapgar);
								} else {
									Toast.makeText(context,
											context.getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated on 16Nov2015
						if (actionId == ID_REFERRAL) {

							if (Partograph_CommonClass.autodatetime(context)) {// 27Sep2016
																				// Arpitha
								Intent ref = new Intent(context, ReferralInfo_Activity.class);
								ref.putExtra("woman", woman);
								context.startActivity(ref);
							}
						}

						// updated on 26Nov2015
						if (actionId == ID_STAGEOFLABOR) {

							if (Partograph_CommonClass.autodatetime(context)) {// 27Sep2016
																				// Arpitha
								if (woman.getDel_type() != 0 &&

										(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent stagesoflabor = new Intent(context, StageofLabor.class);
									stagesoflabor.putExtra("woman", woman);
									context.startActivity(stagesoflabor);

								} else {
									Toast.makeText(context,
											context.getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated bindu - 26july2016
						// updated on 23Dec2015
						if (actionId == ID_PRINTPARTO) {
							try {

								Intent view_partograph = new Intent(context, View_Partograph.class);
								view_partograph.putExtra("woman", woman);

								context.startActivity(view_partograph);

							} catch (Exception e) {
								AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName());
								e.printStackTrace();

							}
						}

						// 06Dec2016 Arpitha
						if (actionId == ID_ADDITIONALDETAILS) {

							Intent addtionalDetails = new Intent(context, AdditionalDetails_Activity.class);
							addtionalDetails.putExtra("woman", woman);

							context.startActivity(addtionalDetails);
						} // 06Dec2016 Arpitha

						// 21July2017 Arpitha
						if (actionId == ID_DISCHARGEDETAILS) {
							Intent dischargeDetails = new Intent(context, DiscargeDetails_Activity.class);
							dischargeDetails.putExtra("woman", woman);
							context.startActivity(dischargeDetails);

						} // 21July2017 Arpitha

						// 10Aug2017 Arpitha
						if (actionId == ID_LATENTPHASE) {
							Intent latent = new Intent(context, LatentPhase_Activity.class);
							latent.putExtra("woman", woman);
							context.startActivity(latent);
						}

						// 18Aug2017 Arpitha
						if (actionId == ID_POSTPARTUM) {
							Intent postpartum = new Intent(context, PostPartumCare_Activity.class);
							postpartum.putExtra("woman", woman);
							context.startActivity(postpartum);
						}

						// 31Aug2017 Arpitha
						if (actionId == ID_PDF) {
							Partograph_CommonClass.exportPDFs(context, woman);

						}

					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName());
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName());
						e.printStackTrace();
					}

				}
			});

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName());

		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	// 02Feb2017 Arpitha
	public static void generateReferralPdf(Context context, Women_Profile_Pojo woman) throws Exception {
		// referraldoc = new Document(PageSize.A4);

		try {

			String path = AppContext.mainDir + "/PDF/ReferralSheet";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			referralfile = new File(dir, woman.getWomen_name() + "_ReferralSlip.pdf");

			basic(woman, context, referralfile, context.getResources().getString(R.string.referral_slips));

			referraldata(woman, context);

			close();

		} catch (

		DocumentException de)

		{
			Log.e("PDFCreator", "DocumentException:" + de);
		} catch (

		IOException e)

		{
			Log.e("PDFCreator", "ioException:" + e);
		} finally

		{
			doc.close();
		}

	}

	private static void referraldata(Women_Profile_Pojo woman, Context context) throws Exception {
		// TODO Auto-generated method stub
		// PdfWriter writer = PdfWriter.getInstance(doc, fOut);

		/*
		 * path = AppContext.mainDir + "/PDF/ReferralSheet";// 03April2017 //
		 * Arpitha
		 * 
		 * File dir = new File(path); if (!dir.exists()) dir.mkdirs();
		 * 
		 * Configuration config = new Configuration(); config.locale = new
		 * Locale("en"); context.getResources().updateConfiguration(config,
		 * context.getResources().getDisplayMetrics());
		 * 
		 * AppContext.setNamesAccordingToRes(context.getResources());//
		 * 05Nov2016 // Arpitha
		 * 
		 * Log.d("PDFCreator", "PDF Path: " + path);
		 * 
		 * referralfile = new File(dir, woman.getWomen_name() +
		 * "_ReferralSheet.pdf"); FileOutputStream fOut = new
		 * FileOutputStream(referralfile);
		 * 
		 * 
		 * 
		 * referraldoc.setMarginMirroring(false); referraldoc.setMargins(-25,
		 * -20, 65, -50); // doc.setMargins(0,0,0,0);
		 * 
		 * referraldoc.open();
		 * 
		 * Drawable d =
		 * context.getResources().getDrawable(R.drawable.sentiagraph);
		 * 
		 * BitmapDrawable bitDw = ((BitmapDrawable) d);
		 * 
		 * Bitmap bmp = bitDw.getBitmap();
		 * 
		 * ByteArrayOutputStream stream = new ByteArrayOutputStream();
		 * 
		 * bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);
		 * 
		 * Image image = Image.getInstance(stream.toByteArray());
		 * 
		 * image.setIndentationLeft(25); referraldoc.add(image);
		 * 
		 * Font fontTitle = new Font(Font.HELVETICA, 20, Font.BOLD); Font
		 * fontHeading = new Font(Font.HELVETICA, 10, Font.BOLD); Font font =
		 * new Font(Font.HELVETICA, 10, Font.NORMAL);
		 * 
		 * Paragraph p_heading = new
		 * Paragraph(context.getResources().getString(R.string. referral_slips),
		 * fontTitle); p_heading.setAlignment(Paragraph.ALIGN_CENTER);
		 * p_heading.setFont(fontTitle); referraldoc.add(p_heading);
		 */
		Font fontTitle = new Font(Font.HELVETICA, 20, Font.BOLD);
		Font fontHeading = new Font(Font.HELVETICA, 10, Font.BOLD);
		Font font = new Font(Font.HELVETICA, 10, Font.NORMAL);

		PdfPTable pdffacilityDetails = new PdfPTable(1);

		// pdffacilityDetails.setKeepTogether(true);
		addEmptyLine(pdffacilityDetails, 3);

		pdffacilityDetails.setWidthPercentage(90);

		PdfPCell cellFacilityName = new PdfPCell();
		cellFacilityName.setPhrase(new Phrase(context.getResources().getString(R.string.name_of_the_referring_facility)
				+ " " + woman.getFacility_name() + ", " + woman.getFacility(), font));
		cellFacilityName.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellFacilityName.setPaddingTop(5);
		cellFacilityName.setPaddingLeft(5);
		;
		cellFacilityName.setBorderWidthBottom(0);

		pdffacilityDetails.addCell(cellFacilityName);

		pdffacilityDetails.completeRow();

		PdfPCell cellFacilityAddress = new PdfPCell();
		cellFacilityAddress.setPhrase(new Phrase(context.getResources().getString(R.string.facility) + " "
				+ context.getResources().getString(R.string.address) + ": " + woman.getFacility_name() + ", "
				+ woman.getTaluk() + "[T], " + woman.getDistrict() + "[D]", font));
		cellFacilityAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellFacilityAddress.setBorderWidthBottom(0);
		cellFacilityAddress.setBorderWidthTop(0);
		cellFacilityAddress.setPaddingTop(5);

		pdffacilityDetails.addCell(cellFacilityAddress);

		pdffacilityDetails.completeRow();

		PdfPCell cellTelephone = new PdfPCell();
		cellTelephone.setPhrase(new Phrase(context.getResources().getString(R.string.facility) + " "
				+ context.getResources().getString(R.string.telephone) + ": " + "\n", font));
		cellTelephone.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellTelephone.setBorderWidthTop(0);
		cellTelephone.setPaddingTop(3);
		cellTelephone.setPaddingBottom(5);

		pdffacilityDetails.addCell(cellTelephone);

		pdffacilityDetails.completeRow();

		String woman_name = "";
		String woman_age = "";
		if (woman.getWomen_name().length() > 0) {
			woman_name = woman.getWomen_name();
		} else {
			woman_name = context.getResources().getString(R.string.doted_line);
		}

		if (woman.getAge() > 0) {
			woman_age = "" + woman.getAge();
		} else
			woman_age = context.getResources().getString(R.string.doted_line);

		PdfPCell cellPatientName = new PdfPCell();
		cellPatientName.setPhrase(new Phrase(context.getResources().getString(R.string.name_of_the_patient) + ": "
				+ woman_name + "    " + context.getResources().getString(R.string.age) + ":  " + "" + woman_age + " "
				+ context.getResources().getString(R.string.yrs), font));
		cellPatientName.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellPatientName.setBorder(0);
		cellPatientName.setPaddingTop(10);

		pdffacilityDetails.addCell(cellPatientName);
		pdffacilityDetails.completeRow();

		PdfPCell cellHusbandName = new PdfPCell();
		cellHusbandName.setPhrase(new Phrase(context.getResources().getString(R.string.husbands_name) + ": "
				+ context.getResources().getString(R.string.doted_line), font));
		cellHusbandName.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellHusbandName.setBorder(0);
		cellHusbandName.setPaddingTop(7);
		cellHusbandName.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellHusbandName);
		pdffacilityDetails.completeRow();

		String woman_address = "";

		if (woman.getAddress().length() > 0) {
			woman_address = woman.getAddress();
		} else
			woman_address = ".............." + context.getResources().getString(R.string.doted_line);

		PdfPCell cellAddress = new PdfPCell();
		cellAddress.setPhrase(
				new Phrase(context.getResources().getString(R.string.address) + ": " + woman_address + "\n", font));
		cellAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellAddress.setBorder(0);
		cellAddress.setPaddingTop(7);
		cellAddress.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellAddress);
		pdffacilityDetails.completeRow();

		if (woman.getAddress().length() <= 0) {
			PdfPCell cellAddressExtra = new PdfPCell();
			cellAddressExtra.setPhrase(new Phrase(
					context.getResources().getString(R.string.doted_line) + "...........................", font));
			cellAddressExtra.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellAddressExtra.setBorder(0);
			cellAddressExtra.setPaddingTop(7);
			cellAddressExtra.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellAddressExtra);
			pdffacilityDetails.completeRow();
		}

		addEmptyLine(pdffacilityDetails, 2);

		addHorizantalLine(pdffacilityDetails, 1);

		Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());

		String referralPlace = referredPlace;

		if (cur.getCount() > 0) {
			cur.moveToFirst();
			pulse = cur.getString(11);
			bpsystolic = cur.getString(10).split("/")[0];
			reasons = cur.getString(7);
			bpdiastolic = cur.getString(10).split("/")[1];
			referralPlace = cur.getString(6);
			referredDate = cur.getString(13);
			referredTime = cur.getString(14);
			description = cur.getString(8);
			if (cur.getInt(9) == 1)
				consiuous = true;
			else
				consiuous = false;

		}

		String refralDate = Partograph_CommonClass
				.getConvertedDateFormat(referredDate == null ? getTodaysDate() : referredDate, "dd/MM/yyyy");

		String referralTime = "";
		if (referredTime != null && referredTime.trim().length() > 0)
			referralTime = Partograph_CommonClass.gettimein12hrformat(referredTime);

		if (referralPlace == null || referralPlace.length() < 0) {
			referralPlace = context.getResources().getString(R.string.doted_line);
		}

		String facilityName = "";
		facilityName = referralPlace;
		if (facilityName.trim().length() <= 0) {
			facilityName = context.getResources().getString(R.string.doted_line);
		}

		PdfPCell cellReferedDate = new PdfPCell();
		cellReferedDate
				.setPhrase(
						new Phrase(
								context.getResources().getString(R.string.referred_on) + ": " + refralDate + " "
										+ context.getResources().getString(R.string.at) + " " + referralTime + " "
										+ context.getResources().getString(R.string.time_to) + "  " + facilityName + " "
										+ context.getResources().getString(R.string.name_of_facility_for_management),
								font));
		cellReferedDate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellReferedDate.setBorder(0);
		cellReferedDate.setPaddingTop(5);
		cellReferedDate.setPaddingBottom(5);

		pdffacilityDetails.addCell(cellReferedDate);
		pdffacilityDetails.completeRow();

		PdfPCell cellProvisionalDiagnosis = new PdfPCell();
		cellProvisionalDiagnosis.setPhrase(
				new Phrase(context.getResources().getString(R.string.provisional_diagnosis) + " : ", fontHeading));
		cellProvisionalDiagnosis.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellProvisionalDiagnosis.setBorder(0);
		cellProvisionalDiagnosis.setPaddingBottom(10);
		pdffacilityDetails.addCell(cellProvisionalDiagnosis);
		pdffacilityDetails.completeRow();

		String gravida = "G" + woman.getGravida() + " " + "P" + woman.getPara();

		PdfPCell ProvisionalDiagnosis = new PdfPCell();
		ProvisionalDiagnosis.setPhrase(new Phrase(gravida, font));
		ProvisionalDiagnosis.setHorizontalAlignment(Element.ALIGN_LEFT);
		ProvisionalDiagnosis.setPaddingBottom(40);
		ProvisionalDiagnosis.setPaddingTop(5);

		pdffacilityDetails.addCell(ProvisionalDiagnosis);
		pdffacilityDetails.completeRow();

		String regDate;
		String regTime = "";
		if (woman.getregtype() != 2)
			regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(), "dd/MM/yyyy");
		else
			regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_reg_entry(), "dd/MM/yyyy");

		if (woman.getregtype() != 2)
			regTime = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());

		PdfPCell cellAdmittedDate = new PdfPCell();
		cellAdmittedDate
				.setPhrase(new Phrase(context.getResources().getString(R.string.admitted_in_the_referring_facility_on)
						+ " " + regDate + " " + context.getResources().getString(R.string.at) + " " + regTime + " "
						+ context.getResources().getString(R.string.time_with), font));
		cellAdmittedDate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellAdmittedDate.setBorder(0);
		cellAdmittedDate.setPaddingTop(10);

		pdffacilityDetails.addCell(cellAdmittedDate);
		pdffacilityDetails.completeRow();

		PdfPCell cellChiefCompaints = new PdfPCell();
		cellChiefCompaints
				.setPhrase(new Phrase(context.getResources().getString(R.string.chief_complaints_of), fontHeading));
		cellChiefCompaints.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellChiefCompaints.setBorder(0);

		pdffacilityDetails.addCell(cellChiefCompaints);
		pdffacilityDetails.completeRow();

		String reasonforHRDisplay = "";
		String AdmittedWithDisplay = "";
		if (woman.getregtype() != 2) {
			String[] reasonforhighrisk = woman.getRiskoptions().split(",");
			String[] reasonforHRArr = null;
			reasonforHRArr = context.getResources().getStringArray(R.array.riskoptionsvalues);

			if (reasonforHRArr != null) {
				if (reasonforhighrisk != null && reasonforhighrisk.length > 0) {
					for (String str : reasonforhighrisk) {
						if (str != null && str.length() > 0)
							reasonforHRDisplay = reasonforHRDisplay + reasonforHRArr[Integer.parseInt(str)] + ", ";
					}
				}
			}
		}

		if (woman.getregtype() != 2 && woman.getAdmitted_with() != null && woman.getAdmitted_with().length() > 0) {
			String[] AdmittedWith = woman.getAdmitted_with().split(",");
			String[] AdmittedWithArray = null;
			AdmittedWithArray = context.getResources().getStringArray(R.array.admittedarraylist);

			if (AdmittedWithArray != null) {
				if (AdmittedWith != null && AdmittedWith.length > 0) {
					for (String str : AdmittedWith) {
						if (str != null && str.length() > 0)
							AdmittedWithDisplay = AdmittedWithDisplay + AdmittedWithArray[Integer.parseInt(str)] + ", ";
					}
				}
			}
		}

		PdfPCell ChiefCompaintsRisk = new PdfPCell();
		if (reasonforHRDisplay.length() <= 0) {
			reasonforHRDisplay = context.getResources().getString(R.string.doted_line)
					+ context.getResources().getString(R.string.short_doted_line);
			ChiefCompaintsRisk.setPaddingBottom(10);
		}

		Phrase risk = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 10)));
		risk.add(new Phrase(
				new Chunk(reasonforHRDisplay + " ", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL))));

		ChiefCompaintsRisk.setPhrase(risk);
		ChiefCompaintsRisk.setHorizontalAlignment(Element.ALIGN_LEFT);
		ChiefCompaintsRisk.setBorder(0);
		ChiefCompaintsRisk.setPaddingLeft(25);

		pdffacilityDetails.addCell(ChiefCompaintsRisk);
		pdffacilityDetails.completeRow();

		PdfPCell ChiefCompaintsAdmittedWith = new PdfPCell();
		if (AdmittedWithDisplay.length() <= 0) {
			AdmittedWithDisplay = context.getResources().getString(R.string.doted_line)
					+ context.getResources().getString(R.string.short_doted_line);
			ChiefCompaintsAdmittedWith.setPaddingBottom(10);
		}
		Phrase admitted = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 10)));
		admitted.add(new Phrase(
				new Chunk(AdmittedWithDisplay + " ", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL))));

		ChiefCompaintsAdmittedWith.setPhrase(admitted);
		ChiefCompaintsAdmittedWith.setHorizontalAlignment(Element.ALIGN_LEFT);
		ChiefCompaintsAdmittedWith.setBorder(0);
		ChiefCompaintsAdmittedWith.setPaddingLeft(25);

		pdffacilityDetails.addCell(ChiefCompaintsAdmittedWith);
		pdffacilityDetails.completeRow();

		Phrase compl = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 10)));
		compl.add(new Phrase(new Chunk(
				context.getResources().getString(R.string.doted_line)
						+ context.getResources().getString(R.string.short_doted_line) + " ",
				FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL))));

		PdfPCell ChiefCompaints = new PdfPCell();
		ChiefCompaints.setPhrase(compl);
		ChiefCompaints.setHorizontalAlignment(Element.ALIGN_LEFT);
		ChiefCompaints.setBorder(0);
		ChiefCompaints.setPaddingLeft(25);
		ChiefCompaints.setPaddingBottom(5);

		pdffacilityDetails.addCell(ChiefCompaints);
		pdffacilityDetails.completeRow();

		PdfPCell cellsummary = new PdfPCell();
		cellsummary.setPhrase(new Phrase(context.getResources().getString(R.string.summary_of_management), font));
		cellsummary.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellsummary.setBorder(0);
		cellsummary.setPaddingTop(2);
		cellsummary.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellsummary);
		pdffacilityDetails.completeRow();

		PdfPCell cellInvestigation = new PdfPCell();
		cellInvestigation
				.setPhrase(new Phrase(context.getResources().getString(R.string.investigations) + "     :", font));
		cellInvestigation.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellInvestigation.setBorder(0);
		cellInvestigation.setPaddingTop(2);
		cellInvestigation.setPaddingBottom(1);

		pdffacilityDetails.addCell(cellInvestigation);
		pdffacilityDetails.completeRow();

		addEmptyLine(pdffacilityDetails, 2);

		int grpId = woman.getBloodgroup();

		String[] bldgrps = context.getResources().getStringArray(R.array.bloodgrp);

		PdfPCell cellBloodGroup = new PdfPCell();
		cellBloodGroup.setPhrase(
				new Phrase(context.getResources().getString(R.string.bloodgroup) + "       : " + bldgrps[grpId], font));
		cellBloodGroup.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellBloodGroup.setBorder(0);
		cellBloodGroup.setPaddingTop(2);
		cellBloodGroup.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellBloodGroup);
		pdffacilityDetails.completeRow();

		PdfPCell cellHb = new PdfPCell();
		cellHb.setPhrase(new Phrase(context.getResources().getString(R.string.hb) + "                       :", font));
		cellHb.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellHb.setBorder(0);
		cellHb.setPaddingTop(2);
		cellHb.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellHb);
		pdffacilityDetails.completeRow();

		PdfPCell cellUrine = new PdfPCell();
		cellUrine.setPhrase(new Phrase(context.getResources().getString(R.string.urine_re) + "            :", font));
		cellUrine.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellUrine.setBorder(0);
		cellUrine.setPaddingTop(2);
		cellUrine.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellUrine);
		pdffacilityDetails.completeRow();

		String consiuousness;
		if (consiuous) {
			consiuousness = context.getResources().getString(R.string.conscious);
		} else
			consiuousness = context.getResources().getString(R.string.unconscious);

		PdfPCell cellCondition = new PdfPCell();
		cellCondition.setPhrase(new Phrase(
				context.getResources().getString(R.string.condtion_when_referred) + " : " + consiuousness, font));
		cellCondition.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellCondition.setBorder(0);
		cellCondition.setPaddingTop(2);
		cellCondition.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellCondition);
		pdffacilityDetails.completeRow();

		String bp = "";
		if (bpdiastolic != null && bpsystolic != null && bpdiastolic.length() > 0 && bpsystolic.length() > 0) {
			bp = bpsystolic + " / " + bpdiastolic;
		}

		PdfPCell cellcounsiousness = new PdfPCell();
		cellcounsiousness.setPhrase(new Phrase(context.getResources().getString(R.string.consciousness)
				+ " :                " + context.getResources().getString(R.string.temperatur) + ":                   "
				+ context.getResources().getString(R.string.pulse) + "(bpm)" + " : " + pulse + "                  "
				+ context.getResources().getString(R.string.txtbpvalue) + "(mmHg)" + " : " + bp, font));

		cellcounsiousness.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcounsiousness.setBorder(0);
		cellcounsiousness.setPaddingTop(4);
		cellcounsiousness.setPaddingBottom(4);
		pdffacilityDetails.addCell(cellcounsiousness);
		pdffacilityDetails.completeRow();

		String reasonforrefDisplay = "";
		if (reasons != null && reasons.length() > 0) {
			String[] reasonforreferral = reasons.split("/");
			String[] reasonforrefArr = null;
			reasonforrefArr = context.getResources().getStringArray(R.array.reasonforreferralvalues);

			if (reasonforrefArr != null) {
				if (reasonforreferral != null && reasonforreferral.length > 0) {
					for (String str : reasonforreferral) {
						if (str != null && str.length() > 0)
							reasonforrefDisplay = reasonforrefDisplay + reasonforrefArr[Integer.parseInt(str)] + ", ";
					}
				}
			}
		}

		PdfPCell cellOthers = new PdfPCell();
		cellOthers.setPhrase(
				new Phrase(context.getResources().getString(R.string.others_specify) + " " + reasonforrefDisplay + " "
						+ context.getResources().getString(R.string.desc) + " : " + description + "\n", font));
		cellOthers.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellOthers.setBorder(0);
		cellOthers.setPaddingTop(2);
		cellOthers.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellOthers);
		pdffacilityDetails.completeRow();

		PdfPCell cellOthersExtra = new PdfPCell();
		cellOthersExtra.setPhrase(new Phrase(context.getResources().getString(R.string.doted_line)
				+ context.getResources().getString(R.string.short_doted_line), font));
		cellOthersExtra.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellOthersExtra.setBorder(0);
		cellOthersExtra.setPaddingTop(2);
		cellOthersExtra.setPaddingBottom(2);

		pdffacilityDetails.addCell(cellOthersExtra);
		pdffacilityDetails.completeRow();

		addEmptyLine(pdffacilityDetails, 2);

		Phrase infophase = new Phrase(
				new Chunk(context.getResources().getString(R.string.information_on_referral) + " ",
						FontFactory.getFont(FontFactory.HELVETICA, 15, Font.BOLD)));
		infophase
				.add(new Phrase(new Chunk(
						context.getResources().getString(R.string.yes) + " / "
								+ context.getResources().getString(R.string.no),
						FontFactory.getFont(FontFactory.HELVETICA, 15))));

		PdfPCell cellInform = new PdfPCell();
		cellInform.setPhrase(infophase);
		cellInform.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellInform.setBorder(0);

		pdffacilityDetails.addCell(cellInform);
		pdffacilityDetails.completeRow();

		addEmptyLine(pdffacilityDetails, 1);

		PdfPCell cellpersonName = new PdfPCell();
		cellpersonName.setPhrase(new Phrase(context.getResources().getString(R.string.name_of_person), fontHeading));
		cellpersonName.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpersonName.setBorder(0);
		cellAdmittedDate.setPaddingTop(20);

		pdffacilityDetails.addCell(cellpersonName);
		pdffacilityDetails.completeRow();

		addEmptyLine(pdffacilityDetails, 1);

		PdfPCell cellTransportation = new PdfPCell();

		Phrase datePhrase = new Phrase(
				new Chunk(context.getResources().getString(R.string.mode_of_transportation) + " ",
						FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD)));
		datePhrase.add(new Phrase(new Chunk(context.getResources().getString(R.string.govt_outsorced),
				FontFactory.getFont(FontFactory.HELVETICA, 13))));

		cellTransportation.setPhrase(datePhrase);
		cellTransportation.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellTransportation.setBorder(0);
		cellAdmittedDate.setPaddingTop(20);

		pdffacilityDetails.addCell(cellTransportation);
		pdffacilityDetails.completeRow();

		PdfPCell cellSignature = new PdfPCell();
		cellSignature.setPhrase(new Phrase(context.getResources().getString(R.string.signature), font));
		cellSignature.setHorizontalAlignment(Element.ALIGN_BOTTOM);
		cellSignature.setBorder(0);
		cellSignature.setPaddingLeft(300);
		cellSignature.setPaddingRight(0);
		cellSignature.setPaddingBottom(0);
		cellSignature.setBorderWidthBottom(0);
		cellSignature.setPaddingTop(15);

		pdffacilityDetails.addCell(cellSignature);
		pdffacilityDetails.completeRow();

		doc.add(pdffacilityDetails);

		String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
				Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";

		// PdfContentByte cb = writer.getDirectContent();

		// Font fontfootere = new Font(Font.HELVETICA, 10, Font.NORMAL);
		//
		// Phrase footer = new Phrase(strdatetime, fontfootere);

		// ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footer,
		// doc.right() - 40, doc.bottom() + 60, 0);

		/*
		 * // Rectangle rect = new Rectangle(575, 770, 20, 20); Rectangle rect =
		 * new Rectangle(575, 770, 10, 10); rect.enableBorderSide(1);
		 * rect.enableBorderSide(1); rect.enableBorderSide(1);
		 * rect.enableBorderSide(1); rect.setBorder(2); //
		 * rect.setBorderColor(context.getResources().getColor(R.color.black ));
		 * rect.setBorder(Rectangle.BOX); rect.setBorderWidth(1);
		 * referraldoc.add(rect);
		 */

	}

	private static void addEmptyLine(PdfPTable p1, int number) {

		for (int i = 0; i < number; i++) {

			PdfPCell cell = new PdfPCell();
			cell.setBorder(0);
			p1.addCell(cell);

		}
	}

	private static void addHorizantalLine(PdfPTable p1, int number) {

		for (int i = 0; i < number; i++) {

			PdfPCell cell = new PdfPCell();
			cell.setBorderWidthTop(0);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthBottom(1);

			p1.addCell(cell);

		}
	}

	// 20July2017 Arpitha
	// Show dialog to update delivery status
	public static void showDialogToUpdateReferral(final Context context, final Women_Profile_Pojo woman) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName());
			dialog_ref = new Dialog(context);
			dialog_ref.setContentView(R.layout.activity_reference);
			dialog_ref.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
					+ context.getResources().getString(R.string.app_name) + " - "
					+ context.getResources().getString(R.string.referredto) + "</font>"));

			dbh = new Partograph_DB(context);
			AssignIdsToReferralWidgets(dialog_ref);

			initialViewReferral(woman, context);

			Fragment_DIP.istears = false;// 01Oct2016 Arpitha

			btnCancelRef.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog_ref.cancel();
				}
			});

			imgpdf.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						pdfAlertDialog(context, woman, "referral", null, null);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			int dividerId = dialog_ref.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = dialog_ref.findViewById(dividerId);
			divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

			dialog_ref.show();
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	// Assign Ids to Referral Widgets
	private static void AssignIdsToReferralWidgets(Dialog dialog) throws Exception {
		// TODO Auto-generated method stub
		etplaceofreferral = (EditText) dialog.findViewById(R.id.etplaceofreferral);
		etreasonforreferral = (EditText) dialog.findViewById(R.id.etreasonforreferral);
		etdateofreferral = (EditText) dialog.findViewById(R.id.etdateofreferrence);
		ettimeofreferral = (EditText) dialog.findViewById(R.id.ettimeofreferrence);
		spnreferralfacility = (Spinner) dialog.findViewById(R.id.spnreferredtofacility);
		btnCancelRef = (Button) dialog.findViewById(R.id.btncancelref);
		btnSaveRef = (Button) dialog.findViewById(R.id.btnsaveref);
		trspo2 = (TableRow) dialog.findViewById(R.id.trspo2);
		trheartrate = (TableRow) dialog.findViewById(R.id.trheartrate);

		// 04jan2016
		etdescriptionofreferral = (EditText) dialog.findViewById(R.id.etdescforreferral);
		etbpsystolicreferral = (EditText) dialog.findViewById(R.id.etbpsystolicreferral);
		etbpdiastolicreferral = (EditText) dialog.findViewById(R.id.etbpdiastolicreferral);
		etpulsereferral = (EditText) dialog.findViewById(R.id.etpulsereferral);
		rd_motherconscious = (RadioButton) dialog.findViewById(R.id.rd_motherconscious);
		rd_motherunconscious = (RadioButton) dialog.findViewById(R.id.rd_motherunconscious);
		rd_babyalive = (RadioButton) dialog.findViewById(R.id.rd_babyalive);
		rd_babydead = (RadioButton) dialog.findViewById(R.id.rd_babydead);
		mspnreasonforreferral = (MultiSelectionSpinner) dialog.findViewById(R.id.mspnreasonforreferral);

		// 24jan2016
		txtrefmode = (TextView) dialog.findViewById(R.id.txtmode);

		// updated on 27Feb2016 by Arpitha
		etheatrate = (EditText) dialog.findViewById(R.id.etheartrate);
		etspo2 = (EditText) dialog.findViewById(R.id.etspo2);

		// 23Aug2016 - bindu
		txtwage = (TextView) dialog.findViewById(R.id.wage);
		txtwdoa = (TextView) dialog.findViewById(R.id.wdoa);
		txtwgravida = (TextView) dialog.findViewById(R.id.wgravida);
		txtwname = (TextView) dialog.findViewById(R.id.wname);
		txtwrisk = (TextView) dialog.findViewById(R.id.wtrisk);
		txtwtoa = (TextView) dialog.findViewById(R.id.wtoa);
		txtwgest = (TextView) dialog.findViewById(R.id.wgest);

//		txtheadingref = (TextView) dialog.findViewById(R.id.txtheadingref);
//		txtheadingref.setVisibility(View.GONE);

		imgpdf = (ImageView) dialog.findViewById(R.id.imgpdf);

		llbaby = (LinearLayout) dialog.findViewById(R.id.llbaby);// 20March2017
																	// Arpitha
	}

	// Initial View of the referral Screen
	private static void initialViewReferral(Women_Profile_Pojo woman, Context contex) throws Exception {

		// 23Aug2016
		getwomanbasicdata(woman, context);
		// 4jan2016
		etreasonforreferral.setVisibility(View.GONE);
		setReasonForReferral();

		String todaysDate = Partograph_CommonClass.getTodaysDate(); // 12jan2016
		ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());

		// 14Jan2016
		String refTime = ettimeofreferral.getText().toString();
		Calendar calendar = Calendar.getInstance();
		Date d = null;

		d = dbh.getTime(refTime);
		calendar.setTime(d);
		calendar.get(Calendar.HOUR_OF_DAY);
		calendar.get(Calendar.MINUTE);

		womenrefpojoItems = new ArrayList<WomenReferral_pojo>();
		WomenReferral_pojo wrefdata;

		Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());

		if (cur.getCount() > 0) {
			cur.moveToFirst();

			do {
				wrefdata = new WomenReferral_pojo();
				wrefdata.setReferredtofacilityid(cur.getInt(5));
				wrefdata.setPlaceofreferral(cur.getString(6));
				wrefdata.setReasonforreferral(cur.getString(7));

				// 04jan2016
				wrefdata.setDescriptionofreferral(cur.getString(8));
				wrefdata.setConditionofmother(cur.getInt(9));
				wrefdata.setBp(cur.getString(10));
				wrefdata.setPulse(cur.getString(11));
				wrefdata.setConditionofbaby(cur.getInt(12));
				wrefdata.setDateofreferral(cur.getString(13));
				wrefdata.setTimeofreferral(cur.getString(14));

				// updated on 27Feb2016 by Arpitha
				wrefdata.setHeartrate(cur.getString(18));
				wrefdata.setSpo2(cur.getString(19));
				womenrefpojoItems.add(wrefdata);
			} while (cur.moveToNext());
		}

		if (womenrefpojoItems != null && womenrefpojoItems.size() > 0) {
			for (int i = 0; i < womenrefpojoItems.size(); i++) {
				spnreferralfacility.setSelection(womenrefpojoItems.get(i).getReferredtofacilityid());
				etplaceofreferral.setText(womenrefpojoItems.get(i).getPlaceofreferral());

				// 04jan2016
				String[] reasonforreferral = womenrefpojoItems.get(i).getReasonforreferral().split(",");
				String[] reasonforrefArr = null;
				// reasonforrefArr =
				// getResources().getStringArray(R.array.reasonforreferral);
				// updated on 6july2016 by Arpitha
				reasonforrefArr = context.getResources().getStringArray(R.array.reasonforreferralvalues);

				String reasonforrefDisplay = "";
				if (reasonforrefArr != null) {
					if (reasonforreferral != null && reasonforreferral.length > 0) {
						for (String str : reasonforreferral) {
							if (str != null && str.length() > 0)
								reasonforrefDisplay = reasonforrefDisplay + reasonforrefArr[Integer.parseInt(str)]
										+ "\n";
						}
					}
				}

				etreasonforreferral.setText(reasonforrefDisplay);
				// updated on 31july2106 by Arpitha
				String refdate = womenrefpojoItems.get(i).getDateofreferral();
				etdateofreferral.setText(
						Partograph_CommonClass.getConvertedDateFormat(refdate, Partograph_CommonClass.defdateformat));
				ettimeofreferral.setText(womenrefpojoItems.get(i).getTimeofreferral());

				// updated on 30july2016 by Arpitha
				String strreftime = womenrefpojoItems.get(i).getDateofreferral() + " "
						+ womenrefpojoItems.get(i).getTimeofreferral();

				// updated on 27feb2016 by Arpitha

				etheatrate.setText(womenrefpojoItems.get(i).getHeartrate());
				etspo2.setText((womenrefpojoItems.get(i).getSpo2()));

				String[] str = womenrefpojoItems.get(i).getTimeofreferral().split(":");
				Integer.parseInt(str[0]);
				Integer.parseInt(str[1]);

				// 04jan2016
				etdescriptionofreferral.setText(womenrefpojoItems.get(i).getDescriptionofreferral());

				String[] bp = womenrefpojoItems.get(i).getBp().split("/");
				String strbpsys = bp[0];
				String strbpdia = bp[1];
				etbpsystolicreferral.setText(strbpsys);
				etbpdiastolicreferral.setText(strbpdia);
				etpulsereferral.setText("" + womenrefpojoItems.get(i).getPulse());

				if (womenrefpojoItems.get(i).getConditionofmother() == 1)
					rd_motherconscious.setChecked(true);
				else
					rd_motherunconscious.setChecked(true);

				if (womenrefpojoItems.get(i).getConditionofbaby() == 1)
					rd_babyalive.setChecked(true);
				else
					rd_babydead.setChecked(true);
				// updated on 14july2016 by ARpitha
				if (rd_babydead.isChecked()) {
					etheatrate.setVisibility(View.GONE);
					etspo2.setVisibility(View.GONE);
					trspo2.setVisibility(View.GONE);
					trheartrate.setVisibility(View.GONE);
				}

				mspnreasonforreferral.setVisibility(View.GONE);
				etreasonforreferral.setVisibility(View.VISIBLE);

			}
			disableReferredToFields();
		}

		// 20March2017 Arpitha
		if (woman.getDel_type() == 0) {
			llbaby.setVisibility(View.GONE);
		} // 20March2017 Arpitha

	}

	// Disable fields
	private static void disableReferredToFields() throws Exception {

		spnreferralfacility.setEnabled(false);
		etplaceofreferral.setEnabled(false);
		etplaceofreferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		etreasonforreferral.setEnabled(false);
		etreasonforreferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		etdateofreferral.setEnabled(false);
		etdateofreferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		ettimeofreferral.setEnabled(false);
		ettimeofreferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		btnSaveRef.setEnabled(false);
		btnSaveRef.setBackgroundColor(context.getResources().getColor(R.color.ashgray));
		// 04jan2016
		etdescriptionofreferral.setEnabled(false);
		etdescriptionofreferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		rd_motherconscious.setEnabled(false);
		rd_motherconscious.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		rd_motherunconscious.setEnabled(false);
		rd_motherunconscious.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		rd_babyalive.setEnabled(false);
		rd_babyalive.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		rd_babydead.setEnabled(false);
		rd_babydead.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		etpulsereferral.setEnabled(false);
		etpulsereferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		etbpdiastolicreferral.setEnabled(false);
		etbpdiastolicreferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		etbpsystolicreferral.setEnabled(false);
		etbpsystolicreferral.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha

		// 24jan2016
		txtrefmode.setText(context.getResources().getString(R.string.viewmode));
		// updated on 27Feb by Arpitha
		etheatrate.setEnabled(false);
		etheatrate.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha
		etspo2.setEnabled(false);
		etspo2.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
		// on
		// 22jun
		// by
		// Arpitha

	}

	// Set options for reason for referral - 04jan0216
	protected static void setReasonForReferral() throws Exception {
		int i = 0;
		reasonforreferralMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;

		// 06jul2016 - assign diff array to get the position.
		List<String> reasonStrArrvalues = null;

		reasonStrArrvalues = Arrays.asList(context.getResources().getStringArray(R.array.reasonforreferralvalues));

		// to get the values(pos) for the options
		if (reasonStrArrvalues != null) {
			for (String str : reasonStrArrvalues) {
				reasonforreferralMap.put(str, i);
				i++;
			}
		}
		reasonStrArr = Arrays.asList(context.getResources().getStringArray(R.array.reasonforreferral));

		if (reasonStrArr != null) {
			mspnreasonforreferral.setItems(reasonStrArr);
		}

	}

	/*
	 * // 16Jan2017 Arpitha public static void ReferralPdfDialogwrere(final
	 * Context context, final Women_Profile_Pojo woman) {
	 * 
	 * final Dialog dialog = new Dialog(context);
	 * 
	 * // dialog.setTitle(getResources().getString(R.string.afm_abrreivation));
	 * dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
	 * dialog.setContentView(R.layout.dialog_action);
	 * dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
	 * R.layout.dialog_title); txtdialogtitle = (TextView)
	 * dialog.findViewById(R.id.graph_title);
	 * txtdialogtitle.setText(context.getResources().getString(R.string.
	 * partograph));
	 * 
	 * ImageButton imgbtncanceldialog = (ImageButton)
	 * dialog.findViewById(R.id.imgbtncanceldialog);
	 * imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { try { dialog.cancel(); } catch
	 * (Exception e) { AppContext.addLog(new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName(), e); e.printStackTrace(); } } });
	 * 
	 * final RadioButton rdsavepdf = (RadioButton)
	 * dialog.findViewById(R.id.rdsave); final RadioButton rdviewpdf =
	 * (RadioButton) dialog.findViewById(R.id.rdview); Button btndone = (Button)
	 * dialog.findViewById(R.id.btndone);
	 * 
	 * btndone.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { // TODO Auto-generated method
	 * stub
	 * 
	 * if (rdsavepdf.isChecked()) { try { dialog.cancel();
	 * generateReferralPdf(context, woman); Toast.makeText(context,
	 * context.getResources().getString(R.string.pdf_file_is_saved_at_path) +
	 * " " + "Phone Storage/Partograph/PDF/ReferralSheet",
	 * Toast.LENGTH_LONG).show(); } catch (Exception e) { // TODO Auto-generated
	 * catch block AppContext.addLog(new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName(), e); e.printStackTrace(); } } else if
	 * (rdviewpdf.isChecked())
	 * 
	 * { try { dialog.cancel(); String path = AppContext.mainDir +
	 * "/ReferralSheet";// 03April2017 // Arpitha
	 * 
	 * File dir = new File(path); if (!dir.exists()) dir.mkdirs(); File file =
	 * new File(dir, woman.getWomen_name() + "_ReferralSheet.pdf"); //
	 * FileOutputStream fOut = new FileOutputStream(file);
	 * 
	 * Partograph_CommonClass.generateReferralPdf(context, woman); //
	 * generateReferralPdf(); // to view pdf file from appliction if
	 * (file.exists()) {
	 * 
	 * Uri pdfpath = Uri.fromFile(file); Intent intent = new
	 * Intent(Intent.ACTION_VIEW); intent.setDataAndType(pdfpath,
	 * "application/pdf"); intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	 * 
	 * try { context.startActivity(intent); } catch (ActivityNotFoundException
	 * e) {// 24Jan2017 // Arpitha // No application to view, ask to download
	 * one AlertDialog.Builder builder = new AlertDialog.Builder(context);
	 * builder.setTitle("No Application Found"); builder.setMessage(
	 * "Download one from Android Market?"); builder.setPositiveButton(
	 * "Yes, Please", new DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) { Intent
	 * marketIntent = new Intent(Intent.ACTION_VIEW);
	 * marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
	 * context.startActivity(marketIntent); } }); builder.setNegativeButton(
	 * "No, Thanks", null); builder.create().show(); } } } catch (Exception e) {
	 * // TODO Auto-generated catch block AppContext.addLog(new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName(), e); e.printStackTrace(); } }
	 * 
	 * else { Toast.makeText(context,
	 * context.getResources().getString(R.string.please_select_one_option),
	 * Toast.LENGTH_LONG).show(); }
	 * 
	 * } }); dialog.show();
	 * 
	 * }
	 */

	// updated on 23Aug2016 by bindu
	public static void getwomanbasicdata(Women_Profile_Pojo woman, Context context) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		if (woman != null) {

			// 26Sep2016 Arpitha
			String doa = "", toa = "", risk = "", dod = "", tod = "";

			txtwname.setText(woman.getWomen_name());
			txtwage.setText(woman.getAge() + context.getResources().getString(R.string.yrs));// 02Oct2016
			// Arpitha
			// -
			// string
			// value
			// from

			if (woman.getregtype() != 2) {// strings.xml
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				// toa =
				// Partograph_CommonClass.gettimein12hrformat(rowItems.get(mSelectedRow).getTime_of_admission());
				toa = woman.getTime_of_admission();
				// Removed Name label on 31August2016 - Arpitha
				txtwdoa.setText(context.getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {
				txtwdoa.setText(context.getResources().getString(R.string.reg) + ": " + woman.getDate_of_reg_entry());
			}
			txtwgest.setText(context.getResources().getString(R.string.gest) + ":"
					+ (woman.getGestationage() == 0 ? context.getResources().getString(R.string.notknown)
							: woman.getGestationage() + context.getResources().getString(R.string.wks)));// 23Aug2016
			// -
			// bindu
			// -
			// gest
			// age
			// not
			// known
			// chk
			txtwgravida.setText(
					context.getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida() + ", "
							+ context.getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = context.getResources().getString(R.string.high);// 02Oct2016
				// Arpitha -
				// string value
				// from
				// strings.xml
			} else
				risk = context.getResources().getString(R.string.low);// 02Oct2016
			// Arpitha -
			// string value
			// from
			// strings.xml
			// 31August2016 Arpitha - label changed
			txtwrisk.setText(context.getResources().getString(R.string.risk_short_label) + ":" + risk);

			/*
			 * if (woman.getDel_type() > 0) { String[] deltype =
			 * getResources().getStringArray(R.array.del_type);
			 * aq.id(R.id.wdelstatus)
			 * .text(getResources().getString(R.string.delstatus) + ":" +
			 * deltype[woman.getDel_type() - 1]); dod =
			 * Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date()
			 * , Partograph_CommonClass.defdateformat); tod =
			 * Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
			 * // 31August2016 Arpitha
			 * aq.id(R.id.wdeldate).text(getResources().getString(R.string.del)
			 * + ": " + dod + "/" + tod);
			 * 
			 * }
			 */

		}

	}

	// 20July2017 Arpitha
	// Show dialog to update delivery status
	public static void showDialogToUpdateStatus(final Context context, final Women_Profile_Pojo woman) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName());
			dialog_del = new Dialog(context);
			dialog_del.setContentView(R.layout.edit_status_dialog);
			dialog_del.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
					+ context.getResources().getString(R.string.update_status) + "</font>"));

			dbh = Partograph_DB.getInstance(context);
			AssignIdsToWidgets(dialog_del);

			initialViewDialog(woman, context);

			etbabywt1.addTextChangedListener(watcher);
			etbabywt2.addTextChangedListener(watcher);

			// 04jan2016
			rdepiyes.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.VISIBLE);
					episiotomy = 1;

					// 24Sep2016 -Arpitha
					rdcatgut.setVisibility(View.VISIBLE);
					rdvicryl.setVisibility(View.VISIBLE);
				}
			});

			rdepino.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.GONE);
					episiotomy = 0;
					// 24Sep2016 -Arpitha
					rdcatgut.setVisibility(View.GONE);
					rdvicryl.setVisibility(View.GONE);

				}
			});

			rdcatgut.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					suturematerial = 1;
				}
			});

			rdvicryl.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					suturematerial = 2;
				}
			});

			// Num of children
			spn_noofchild.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View view, int pos, long id) {
					no_of_child = (String) adapter.getSelectedItem();

					if (pos == 0) {
						trchilddet1.setVisibility(View.VISIBLE);
						tr_childdetsex1.setVisibility(View.VISIBLE);
						tr_secondchilddetails.setVisibility(View.GONE);
						tr_result2.setVisibility(View.GONE);
						tr_childdet2.setVisibility(View.GONE);
						tr_childsex2.setVisibility(View.GONE);
						tr_deltime2.setVisibility(View.GONE);

						// 05Jan2016
						txtfirstchilddetails.setText(context.getResources().getString(R.string.child_details));

						// 24Sep2016 -Arpitha
						etdeltime2.setVisibility(View.GONE);
						rdchildSexGrpbaby2.setVisibility(View.GONE);
						etbabywt2.setVisibility(View.GONE);
						imgwt2_warning.setVisibility(View.GONE);
						spn_delres2.setVisibility(View.GONE);
						imgdelresult.setVisibility(View.GONE);
						imgsex2.setVisibility(View.GONE);
						imgdeltime2.setVisibility(View.GONE);
					} else {
						tr_childdet2.setVisibility(View.VISIBLE);
						tr_childsex2.setVisibility(View.VISIBLE);
						tr_secondchilddetails.setVisibility(View.VISIBLE);
						tr_result2.setVisibility(View.VISIBLE);
						trchilddet1.setVisibility(View.VISIBLE);
						tr_childdetsex1.setVisibility(View.VISIBLE);
						tr_deltime2.setVisibility(View.VISIBLE);

						// 05Jan2016
						txtfirstchilddetails.setText(context.getResources().getString(R.string.first_child_details));

						// 24Sep2016 -Arpitha
						etdeltime2.setVisibility(View.VISIBLE);
						rdchildSexGrpbaby2.setVisibility(View.VISIBLE);
						etbabywt2.setVisibility(View.VISIBLE);
						// imgwt2_warning.setVisibility(View.VISIBLE);// changed
						// invisible
						// to
						// visible
						// -
						// 17Nov2016
						// Arpitha
						spn_delres2.setVisibility(View.VISIBLE);
						imgdelresult.setVisibility(View.INVISIBLE);
						imgsex2.setVisibility(View.INVISIBLE);
						imgdeltime2.setVisibility(View.INVISIBLE);

					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// 29Sep2016 ARpitha
			mspndeltypereason.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Fragment_DIP.istears = false;// 29Sep2016 ARpitha
					// Toast.makeText(getActivity(), "MUlti",
					// Toast.LENGTH_LONG).show();
					return false;
				}
			});
			// 29Sep2016 ARpitha

			// Delivery result 1
			spn_delres1.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View view, int pos, long id) {
					delivery_result1 = adapter.getSelectedItemPosition() + 1;

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// Delivery result 2

			spn_delres2.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1, int arg2, long arg3) {
					delivery_result2 = adapter.getSelectedItemPosition() + 1;

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			rdmale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex1 = 0;

				}
			});

			rdfemale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex1 = 1;
				}
			});

			rdmale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex2 = 0;
				}
			});

			rdfemale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex2 = 1;
				}
			});

			etdeldate.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							// isDateRefClicked = false;
							// caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			etdeltime.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						del_time1 = true;

						// showtimepicker();
					}
					return true;
				}
			});

			etdeltime2.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						del_time1 = false;

						// showtimepicker();
					}
					return true;
				}
			});

			rdmotheralive.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					txtmothersdeathreason.setVisibility(View.GONE);
					etmothersdeathreason.setVisibility(View.GONE);
					// updated on 20july2016 by Arpitha
					etmothersdeathreason.setText("");
				}
			});

			rdmotherdead.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					txtmothersdeathreason.setVisibility(View.VISIBLE);
					etmothersdeathreason.setVisibility(View.VISIBLE);

					etmothersdeathreason.requestFocus();
				}
			});

			final Calendar c = Calendar.getInstance();
			hour = c.get(Calendar.HOUR_OF_DAY);
			minute = c.get(Calendar.MINUTE);

			Button imgbtncancel = (Button) dialog_del.findViewById(R.id.btnclear);

			// 06OCt2016 Arpitha

			imgbtncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialog_del.cancel();

				}
			});

			imgbtnok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					String updateres = "";

					if (etbabywt1.getText().toString().trim().length() > 0) {
						weight1 = Integer.parseInt(etbabywt1.getText().toString());
					}
					if (etbabywt2.getText().toString().trim().length() > 0) {
						weight2 = Integer.parseInt(etbabywt2.getText().toString());
					}

					try {

						if (validateDelFields(context)) {

							// 01Oct2016 ARpitha
							if ((weight1 > 0 && (weight1 < 2500 || weight1 > 4000))
									|| (weight2 > 0 && (weight2 < 2500 || weight2 > 4000))) {

								displayConfirmationAlert_pulse("", context.getResources().getString(R.string.deldata),
										context, woman);// 05OCt2016
								// ARpitha
								// -
								// string
								// val
								// from
								// strings.xml
							}

							else {

								// 27Sep2016 Arpitha
								if (Partograph_CommonClass.autodatetime(context)) {

									Date lastregdate = null;
									String strlastinserteddate = dbh.getlastmaxdate(user.getUserId(),
											context.getResources().getString(R.string.viewprofile));
									SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
									if (strlastinserteddate != null) {
										lastregdate = format.parse(strlastinserteddate);
									}
									Date currentdatetime = new Date();
									if (strlastinserteddate != null && currentdatetime.before(lastregdate)) {
										Partograph_CommonClass.showSettingsAlert(context, context.getResources()
												.getString(R.string.plz_enable_automatic_date_and_set_current_date));
									} else {// 27Sep2016 Arpitha

										// updated bindu - 25Aug2016 - check
										// confirmation of Del date and time
										String msg = context.getResources().getString(R.string.deldatetimerecheck)
												+ etdeldate.getText().toString() + " / "
												+ etdeltime.getText().toString()
												+ context.getResources().getString(R.string.confirmrecheck);
										// commented on 12Jan2017 Arpitha
										// ConfirmAlert(msg,
										// getResources().getString(R.string.deldata));//
										// 02OCt2016
										deldata(woman);// 12Jan2017 Arpitha //
														// ARpitha
														// -
														// string
														// val
														// from
														// strings.xml
									}
								}
							}
						}
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgbtncancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog_del.cancel();
				}
			});

			int dividerId = dialog_del.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = dialog_del.findViewById(dividerId);
			divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

			dialog_del.show();
			dialog_del.setCancelable(false);// 31Oct2016 Arpitha
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	// Assign Ids To Widgets
	private static void AssignIdsToWidgets(Dialog dialog) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());

		etdelcomments = (EditText) dialog.findViewById(R.id.etdelcomments);
		etdeldate = (EditText) dialog.findViewById(R.id.etdeldate);
		etdeltime = (EditText) dialog.findViewById(R.id.etdeltime);
		etdeltime2 = (EditText) dialog.findViewById(R.id.etdeltime2);
		etbabywt1 = (EditText) dialog.findViewById(R.id.etchildwt1);
		etbabywt2 = (EditText) dialog.findViewById(R.id.etchildwt2);

		rdmale1 = (RadioButton) dialog.findViewById(R.id.rd_male1);
		rdfemale1 = (RadioButton) dialog.findViewById(R.id.rd_female1);
		rdmale2 = (RadioButton) dialog.findViewById(R.id.rd_male2);
		rdfemale2 = (RadioButton) dialog.findViewById(R.id.rd_female2);

		tr_childdet2 = (TableRow) dialog.findViewById(R.id.tr_childdet2);
		tr_childsex2 = (TableRow) dialog.findViewById(R.id.tr_childsex2);
		trchilddet1 = (TableRow) dialog.findViewById(R.id.tr_childdet1);
		tr_childdetsex1 = (TableRow) dialog.findViewById(R.id.tr_childdetsex1);
		tr_secondchilddetails = (TableRow) dialog.findViewById(R.id.tr_secondchilddetails);
		tr_result2 = (TableRow) dialog.findViewById(R.id.tr_result2);
		tr_deltime2 = (TableRow) dialog.findViewById(R.id.tr_deltime2);

		spn_delres1 = (Spinner) dialog.findViewById(R.id.spndelresult);
		spn_delres2 = (Spinner) dialog.findViewById(R.id.spndelresult2);
		spndel_type = (Spinner) dialog.findViewById(R.id.spndeltype);
		spn_noofchild = (Spinner) dialog.findViewById(R.id.spnnoofchild);
		imgbtnok = (Button) dialog.findViewById(R.id.btnsave);

		rdchildSexGrpbaby1 = (RadioGroup) dialog.findViewById(R.id.rdsex1);
		rdchildSexGrpbaby2 = (RadioGroup) dialog.findViewById(R.id.rdsex2);

		etmothersdeathreason = (EditText) dialog.findViewById(R.id.etmothersdeathreason);
		txtmothersdeathreason = (TextView) dialog.findViewById(R.id.txtmothersdeathreason);

		rdmotheralive = (RadioButton) dialog.findViewById(R.id.rdmotheralive);
		rdmotherdead = (RadioButton) dialog.findViewById(R.id.rdmotherdead);

		// 25Nov2015
		mspndeltypereason = (MultiSelectionSpinner) dialog.findViewById(R.id.mspndeltypereason);
		tr_deltypereason = (TableRow) dialog.findViewById(R.id.tr_deltypereason);
		tr_deltypeotherreasons = (TableRow) dialog.findViewById(R.id.tr_deltypeotherreasons);
		etdeltypeotherreasons = (EditText) dialog.findViewById(R.id.etdeltypeotherreasons);

		// 04jan2016
		mspntears = (MultiSelectionSpinner) dialog.findViewById(R.id.mspntears);
		rdepiyes = (RadioButton) dialog.findViewById(R.id.rd_epiyes);
		rdepino = (RadioButton) dialog.findViewById(R.id.rd_epino);
		rdcatgut = (RadioButton) dialog.findViewById(R.id.rd_catgut);
		rdvicryl = (RadioButton) dialog.findViewById(R.id.rd_vicryl);
		trsuturematerial = (TableRow) dialog.findViewById(R.id.tr_suturematerial);

		// 05jan2016
		txtfirstchilddetails = (TextView) dialog.findViewById(R.id.txtfirstchilddetails);
		txtdeltypereason = (EditText) dialog.findViewById(R.id.txtdeltypereasonval);
		ettears = (EditText) dialog.findViewById(R.id.ettears);

		// 24jan2016
		trsuturematerial.setVisibility(View.GONE);

		// 31August2016 -Arpitha
		rdcatgut.setVisibility(View.GONE);
		rdvicryl.setVisibility(View.GONE);

		// updated on 11july2016 by Arpitha
		imgwt1_warning = (ImageView) dialog.findViewById(R.id.warbabywt1);
		imgwt2_warning = (ImageView) dialog.findViewById(R.id.warbabywt2);

		// 23Aug2016 - bindu
		txtwage = (TextView) dialog.findViewById(R.id.wage);
		txtwdoa = (TextView) dialog.findViewById(R.id.wdoa);
		txtwgravida = (TextView) dialog.findViewById(R.id.wgravida);
		txtwname = (TextView) dialog.findViewById(R.id.wname);
		txtwrisk = (TextView) dialog.findViewById(R.id.wtrisk);
		txtwtoa = (TextView) dialog.findViewById(R.id.wtoa);
		txtwgest = (TextView) dialog.findViewById(R.id.wgest);

		txtwdelstatus = (TextView) dialog.findViewById(R.id.wdelstatus);// 17Jan2017
																		// Arpitha
		txtwdeldate = (TextView) dialog.findViewById(R.id.wdeldate);// 17Jan2017
																	// Arpitha

		// 24Sep2016 ARpitha
		imgdelresult = (ImageView) dialog.findViewById(R.id.imgdelresult);
		imgsex2 = (ImageView) dialog.findViewById(R.id.imgsex2);
		imgdeltime2 = (ImageView) dialog.findViewById(R.id.imgdeltime2);
		wardeltype = (ImageView) dialog.findViewById(R.id.wardeltype);
		wardaltypereason = (ImageView) dialog.findViewById(R.id.wardaltypereason);

		imgdelresult.setVisibility(View.GONE);
		imgwt2_warning.setVisibility(View.GONE);
		imgsex2.setVisibility(View.GONE);
		imgdeltime2.setVisibility(View.GONE);
		wardeltype.setVisibility(View.GONE);
		wardaltypereason.setVisibility(View.GONE);

		txtdeltypereasonheading = (TextView) dialog.findViewById(R.id.txtdeltypereason);// 05Oct2016
																						// Arpitha
		txtdeltypeotherreason = (TextView) dialog.findViewById(R.id.txtdeltypeotherreasons);// 05Oct2016
																							// Arpitha

		txtdeltypereasonheading.setVisibility(View.GONE);// 05Oct2016 Arpitha
		txtdeltypeotherreason.setVisibility(View.GONE);// 05Oct2016 Arpitha

		// txtheading = (TextView) dialog.findViewById(R.id.txtheading);//
		// 23oct2016
		// Arpitha

		// txtheading.setVisibility(View.GONE);// 23oct2016 Arpitha

		rdbreech = (RadioButton) dialog.findViewById(R.id.rd_breech);// 20Nov2016
		// Arpitha
		rdvertext = (RadioButton) dialog.findViewById(R.id.rd_vertex);// 20Nov2016
		// Arpitha

		txtnormaltype = (TextView) dialog.findViewById(R.id.txtnormaltype);// 23Nov2016
																			// Arpitha

		tr_normaltype = (TableRow) dialog.findViewById(R.id.tr_normaltype);// 27Nov2016
																			// Arpitha

		txtdisabled = (TextView) dialog.findViewById(R.id.txtdisable);// 10Aug2017
		// Arpitha

		reldel = (RelativeLayout) dialog.findViewById(R.id.reldel);// 10Aug2017
																	// Arpitha
	}

	private static void initialViewDialog(Women_Profile_Pojo woman, Context context) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());

		// Bindu - Aug08-2016 - get woman details
		Women_Profile_Pojo wdataold = new Women_Profile_Pojo();

		wdataold = dbh.getWomanDetails(woman.getUserId(), woman.getWomenId());

		if (wdataold != null)
			Partograph_CommonClass.oldWomanObj = wdataold;

		// 23Aug2016-bindu
		getwomanbasicdata(woman, context);

		// updated on 13july2016 by Arpitha

		imgwt1_warning.setVisibility(View.INVISIBLE);

		// changed invsible to gone on 24Sep2016 Arpitha
		imgwt2_warning.setVisibility(View.GONE);

		// 05Jan2016
		if (woman.getDel_type() != 0) {
			setDelReportData(wdataold);
			isDelUpdate = true;
		} else {
			isDelUpdate = false;
			txtdeltypereason.setVisibility(View.GONE);
			txtmothersdeathreason.setVisibility(View.GONE);
			etmothersdeathreason.setVisibility(View.GONE);
			mspndeltypereason.setVisibility(View.VISIBLE);
			mspntears.setVisibility(View.VISIBLE);
			ettears.setVisibility(View.GONE);
			spndel_type.setEnabled(true);
			etdeltypeotherreasons.setEnabled(true);
			spn_noofchild.setEnabled(true);
			spn_delres1.setEnabled(true);
			spn_delres2.setEnabled(true);
			etdeltime.setEnabled(true);
			etdeltime2.setEnabled(true);
			etdeldate.setEnabled(true);
			rdepiyes.setEnabled(true);
			rdepino.setEnabled(true);
			rdcatgut.setEnabled(true);
			rdvicryl.setEnabled(true);

			etdeldate.setText(woman.getDel_Date() == null
					? Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
							Partograph_CommonClass.defdateformat)
					: Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat));

			etdeltime.setText(
					woman.getDel_Time() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_Time());

			etdeltime2.setText(
					woman.getDel_time2() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_time2());

			todaysDate = Partograph_CommonClass.getTodaysDate();
			selecteddate = etdeldate.getText().toString();

			strDeldate = todaysDate;
			selecteddate = todaysDate;

			String delTime = etdeltime.getText().toString();

			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getTime(delTime);
			calendar.setTime(d);
			hour = calendar.get(Calendar.HOUR_OF_DAY);
			minute = calendar.get(Calendar.MINUTE);

			// 25Nov2015
			tr_deltypereason.setVisibility(View.GONE);
			tr_deltypeotherreasons.setVisibility(View.GONE);

			// 04jan2016
			setTears(context);

			// 05Jan2016
			txtfirstchilddetails.setText(context.getResources().getString(R.string.child_details));

			imgwt1_warning.setVisibility(View.INVISIBLE);

			// changed invsible to gone on 24Sep2016 Arpitha
			imgwt2_warning.setVisibility(View.GONE);

		}

		// 10Aug2017 Atpitha
		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (arrVal.size() > 0 && !(DiscargeDetails_Activity.updateDel)) {

			if (woman.getDel_type() == 0) {
				txtdisabled.setVisibility(View.VISIBLE);
				reldel.setVisibility(View.GONE);
			} else {
				imgbtnok.setEnabled(false);
				imgbtnok.setBackgroundColor(context.getResources().getColor(R.color.ashgray));
				// aq.id(R.id.btnclear).enabled(false);
				// aq.id(R.id.btnclear).backgroundColor(context.getResources().getColor(R.color.ashgray));

				etbabywt1.setEnabled(false);
				etbabywt2.setEnabled(false);
				etdelcomments.setEnabled(false);
				etmothersdeathreason.setEnabled(false);
				rdmotherdead.setEnabled(false);
				rdmotheralive.setEnabled(false);
				rdmotherdead.setEnabled(false);
				rdmotherdead.setEnabled(false);
				rdmotherdead.setEnabled(false);
				rdmotherdead.setEnabled(false);
				rdmale2.setEnabled(false);
				rdmale1.setEnabled(false);
				rdfemale1.setEnabled(false);
				rdfemale2.setEnabled(false);
			}

		} // 10Aug2017 Atpitha

	}

	// Set options for tears - 04jan2016
	protected static void setTears(Context context) throws Exception {
		int i = 0;
		tearsMap = new LinkedHashMap<String, Integer>();
		List<String> tearsStrArr = null;

		// updated on 6july2016

		List<String> tearsStrArrvalues = null;

		tearsStrArrvalues = Arrays.asList(context.getResources().getStringArray(R.array.tearsvalues));

		if (tearsStrArrvalues != null) {
			for (String str : tearsStrArrvalues) {
				tearsMap.put(str, i);
				i++;
			}
		}

		tearsStrArr = Arrays.asList(context.getResources().getStringArray(R.array.tears));

		if (tearsStrArr != null) {
			mspntears.setItems(tearsStrArr);
		}

	}

	// Validate Delivery Status Fields
	protected static boolean validateDelFields(Context context) throws Exception {

		// 06jan2016
		if (isDelUpdate) {

			if (etbabywt1.getText().length() < 3) {
				displayAlertDialog(context.getResources().getString(R.string.child_wt), context);
				etbabywt1.requestFocus();
				return false;
			}

			// updated on 6/6/16
			if (Integer.parseInt(etbabywt1.getText().toString()) == 0) {
				displayAlertDialog(context.getResources().getString(R.string.child_wt_val), context);
				etbabywt1.requestFocus();
				return false;
			}

			if (no_of_child != null) {
				if (Integer.parseInt(no_of_child) == 2) {
					if (etbabywt2.getText().length() < 3) {
						displayAlertDialog(context.getResources().getString(R.string.child_wt), context);
						etbabywt2.requestFocus();
						return false;
					}
					// updated on 6/6/16
					if (Integer.parseInt(etbabywt2.getText().toString()) == 0) {
						displayAlertDialog(context.getResources().getString(R.string.child_wt_val), context);
						etbabywt2.requestFocus();
						return false;
					}
				}
			}

			if (rdmotherdead.isChecked()) {
				if (etmothersdeathreason.getText().toString().trim().length() <= 0) {
					displayAlertDialog(context.getResources().getString(R.string.enter_mothersdeathreason), context);
					etmothersdeathreason.requestFocus();
					return false;
				}
			}

		} else {

			// updated on 6/6/16
			if (etbabywt1.getText().length() > 0) {
				if (Integer.parseInt(etbabywt1.getText().toString()) == 0) {
					displayAlertDialog(context.getResources().getString(R.string.child_wt_val), context);
					etbabywt1.requestFocus();
					return false;
				}
			}

			if (etdeldate.getText().length() <= 0) {
				displayAlertDialog(context.getResources().getString(R.string.ent_deldate), context);
				etdeldate.requestFocus();
				return false;
			}

			if (etdeltime.getText().length() <= 0) {
				displayAlertDialog(context.getResources().getString(R.string.ent_deltime), context);
				etdeltime.requestFocus();
				return false;
			}

			if (etbabywt1.getText().length() < 3) {
				displayAlertDialog(context.getResources().getString(R.string.child_wt), context);
				etbabywt1.requestFocus();
				return false;
			}

			// updated on 13nov2015 - validating delivery date and time with reg
			// date
			// if (etdeltime.getText().length() >= 1) {
			// String date1, date2;
			// date1 = strRegdate;
			// String dtime = etdeltime.getText().toString();
			// date2 = strDeldate + " " + dtime;
			// etdeltime.requestFocus();
			// if (!isDeliveryTimevalid(date1, date2, 0))
			// return false;
			// }

			if (no_of_child != null) {
				if (Integer.parseInt(no_of_child) == 2) {
					if (etbabywt2.getText().length() < 3) {
						displayAlertDialog(context.getResources().getString(R.string.child_wt), context);
						etbabywt2.requestFocus();
						return false;
					}
					// updated on 6/6/16
					if (etbabywt2.getText().length() > 0) {
						if (Integer.parseInt(etbabywt2.getText().toString()) == 0) {
							displayAlertDialog(context.getResources().getString(R.string.child_wt_val), context);
							etbabywt2.requestFocus();
							return false;
						}
					}

					// updated on 13nov2015 - validating delivery date and time
					// with reg date
					// if (etdeltime2.getText().length() >= 1) {
					// String date1, date2;
					// date1 = strRegdate;
					// String dtime = etdeltime2.getText().toString();
					// date2 = strDeldate + " " + dtime;
					// etdeltime2.requestFocus();
					// if (!isDeliveryTimevalid(date1, date2, 0))
					// return false;
					// }

				}
			}

			if (rdmotherdead.isChecked()) {
				if (etmothersdeathreason.getText().length() <= 0) {
					displayAlertDialog(context.getResources().getString(R.string.enter_mothersdeathreason), context);
					etmothersdeathreason.requestFocus();
					return false;
				}
			}
		}

		return true;
	}

	static void deldata(Women_Profile_Pojo woman) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());

		Women_Profile_Pojo wpojo = new Women_Profile_Pojo();
		if (isDelUpdate) {
			int childwt1 = Integer.parseInt(etbabywt1.getText().toString());

			// 06Jan2016
			int childwt2 = 0;
			if (etbabywt2.getText().toString().length() > 0) {
				childwt2 = Integer.parseInt(etbabywt2.getText().toString());
				wpojo.setDel_Comments(etdelcomments.getText().toString());
				wpojo.setBabywt1(childwt1);
				wpojo.setBabywt2(childwt2);
				wpojo.setBabysex1(babysex1);
				wpojo.setBabysex2(babysex2);
				int motherdead;

				/**
				 * if (chkmotherdead.isChecked()) motherdead = 1; else
				 * motherdead = 0;
				 */

				if (rdmotherdead.isChecked())
					motherdead = 1;
				else
					motherdead = 0;

				wpojo.setMothersdeath(motherdead);
				wpojo.setMothers_death_reason(etmothersdeathreason.getText().toString());
				wpojo.setUserId(woman.getUserId());
				wpojo.setWomenId(woman.getWomenId());

			}
		} else {

			if (!(no_of_child.equals("") || (no_of_child == null)))
				numofchildren = Integer.parseInt(no_of_child);

			int childwt1 = Integer.parseInt(etbabywt1.getText().toString());

			// 06Jan2016
			int childwt2 = 0;
			if (etbabywt2.getText().toString().length() > 0) {
				childwt2 = Integer.parseInt(etbabywt2.getText().toString());
			}

			wpojo.setDel_type(delivery_type);
			wpojo.setDel_Comments(etdelcomments.getText().toString());
			wpojo.setDel_result1(delivery_result1);
			wpojo.setDel_result2(delivery_result2);
			wpojo.setNo_of_child(numofchildren);
			wpojo.setBabywt1(childwt1);
			wpojo.setBabywt2(childwt2);
			wpojo.setBabysex1(babysex1);
			wpojo.setBabysex2(babysex2);

			int motherdead;

			if (rdmotherdead.isChecked())
				motherdead = 1;
			else
				motherdead = 0;

			wpojo.setMothersdeath(motherdead);
			wpojo.setDel_Time(etdeltime.getText().toString());
			wpojo.setDel_Date(strDeldate);
			if (numofchildren == 2)
				wpojo.setDel_time2(etdeltime2.getText().toString());
			else
				wpojo.setDel_time2("");

			wpojo.setMothers_death_reason(etmothersdeathreason.getText().toString());
			wpojo.setUserId(woman.getUserId());
			wpojo.setWomenId(woman.getWomenId());

			/**
			 * updateres = dbh.updateStatus(wpojo, wUserId, womenId);
			 */

			// 02Dec2015
			String selIds = "";
			if (delivery_type > 1) {
				List<String> selecteddeltypereason = mspndeltypereason.getSelectedStrings();
				if (selecteddeltypereason != null && selecteddeltypereason.size() > 0) {
					for (String str : selecteddeltypereason) {
						if (selecteddeltypereason.indexOf(str) != selecteddeltypereason.size() - 1)
							selIds = selIds + deltypereasonMap.get(str) + ",";
						else
							selIds = selIds + deltypereasonMap.get(str);
					}
				}
			}
			wpojo.setDelTypeReason(selIds);

			wpojo.setDeltype_otherreasons("" + etdeltypeotherreasons.getText().toString());

			// 04jan2016
			String seltearsIds = "";
			List<String> selectedtears = mspntears.getSelectedStrings();
			if (selectedtears != null && selectedtears.size() > 0) {
				for (String str : selectedtears) {
					if (selectedtears.indexOf(str) != selectedtears.size() - 1)
						seltearsIds = seltearsIds + tearsMap.get(str) + ",";
					else
						seltearsIds = seltearsIds + tearsMap.get(str);
				}
			}

			wpojo.setTears(seltearsIds);
			wpojo.setEpisiotomy(episiotomy);
			wpojo.setSuturematerial(suturematerial);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String currentDateandTime = sdf.format(new Date());

		wpojo.setDatelastupdated(currentDateandTime);

		dbh.db.beginTransaction();
		String regSql = "";

		int transId = dbh.iCreateNewTrans(woman.getUserId());

		// 06Jan2016
		if (isDelUpdate)
			regSql = dbh.updateDelDetailsData(wpojo, transId, Partograph_CommonClass.oldWomanObj); // Bindu
																									// -
																									// Aug
																									// 082016
																									// add
																									// parameter
																									// oldWomanObj
		else
			regSql = dbh.updateDelData(wpojo, transId, Partograph_CommonClass.oldWomanObj); // Bindu
																							// -
																							// Aug
																							// 082016
																							// add
																							// parameter
																							// oldWomanObj

		if (regSql.length() > 0) {
			// if (Fragment_Delivered.adapter != null)
			// Fragment_Delivered.adapter.notifyDataSetChanged();
			Toast.makeText(context, context.getResources().getString(R.string.del_sucess), Toast.LENGTH_LONG).show();
			Fragment_Delivered.loadWomendata(10);
			Fragment_Referred.loadWomendata(10);
			Fragment_Retrospective.loadWomendata(10);
			commitTrans();

			// dbh.deletetblnotificationrow(woman.getWomenId(),
			// user.getUserId());// 17Oct2016
			// Arpitha

		} else
			rollbackTrans(context);

		dialog_del.cancel();
	}

	private static void setDelReportData(Women_Profile_Pojo woman) {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		try {

			mspndeltypereason.setVisibility(View.GONE);
			mspntears.setVisibility(View.GONE);

			ettears.setVisibility(View.VISIBLE);

			spndel_type.setEnabled(false);

			etdeltypeotherreasons.setEnabled(false);
			etdeltypeotherreasons.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
			// on
			// 22jun16
			// by
			// Arpitha
			spn_noofchild.setEnabled(false);
			spn_delres1.setEnabled(false);
			spn_delres2.setEnabled(false);
			etdeltime.setEnabled(false);
			etdeltime.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
			// on
			// 22jun16
			// by
			// Arpitha

			etdeltime2.setEnabled(false);
			etdeltime2.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
			// on
			// 22jun16
			// by
			// Arpitha

			etdeldate.setEnabled(false);
			etdeldate.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));// updated
			// on
			// 22jun16
			// by
			// Arpitha

			rdepiyes.setEnabled(false);
			rdepino.setEnabled(false);
			rdcatgut.setEnabled(false);
			rdvicryl.setEnabled(false);

			etbabywt1.setText("" + woman.getBabywt1());
			int babyweight1 = woman.getBabywt1();
			if (babyweight1 < 2500 || babyweight1 > 4000) {
				imgwt1_warning.setVisibility(View.VISIBLE);
			} else
				imgwt1_warning.setVisibility(View.INVISIBLE);
			etbabywt2.setText("" + woman.getBabywt2());
			int bbayweight2 = woman.getBabywt2();
			if (bbayweight2 < 2500 || bbayweight2 > 4000) {
				imgwt2_warning.setVisibility(View.VISIBLE);
			} else
				imgwt2_warning.setVisibility(View.INVISIBLE);

			babysex1 = woman.getBabysex1();
			babysex2 = woman.getBabysex2();

			if (babysex1 == 0)
				rdmale1.setChecked(true);
			else if (babysex1 == 1)
				rdfemale1.setChecked(true);

			if (rdmale1.isChecked())
				babysex1 = 0;
			else
				babysex1 = 1;

			if (babysex2 == 0)
				rdmale2.setChecked(true);
			else if (babysex2 == 1)
				rdfemale2.setChecked(true);

			if (rdmale2.isChecked())
				babysex2 = 0;
			else
				babysex2 = 1;

			etdelcomments.setText(woman.getDel_Comments() == null ? " " : woman.getDel_Comments());

			etdeldate.setText(woman.getDel_Date() == null ? ""
					: Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat));

			etdeltime.setText(
					woman.getDel_Time() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_Time());

			etdeltime2.setText(
					woman.getDel_time2() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_time2());

			if (woman.isMothersdeath() == 1) {
				rdmotherdead.setChecked(true);
				etmothersdeathreason
						.setText(woman.getMothers_death_reason() == null ? "" : woman.getMothers_death_reason());

				txtmothersdeathreason.setVisibility(View.VISIBLE);
			} else {
				rdmotheralive.setChecked(true);
				etmothersdeathreason.setVisibility(View.GONE);
				txtmothersdeathreason.setVisibility(View.GONE);
			}

			// Delivery type
			spndel_type.setSelection(woman.getDel_type() - 1);

			// 24Sep2016 Arpitha
			if (woman.getDel_type() - 1 == 0) {
				etdeltypeotherreasons.setVisibility(View.GONE);
				txtdeltypereason.setVisibility(View.GONE);
				// txtdeltypeotherreason.setVisibility(View.GONE);//05Oct2016
				// Arpitha
				// txtdeltypereasons.setVisibility(View.GONE);//05Oct2016
				// Arpitha
				wardaltypereason.setVisibility(View.GONE);// 06Oct2016 Arpitha
				txtdeltypeotherreason.setVisibility(View.GONE);// 06Oct2016
																// Arpitha
				txtdeltypereasonheading.setVisibility(View.GONE);// 06Oct2016
																	// Arpitha

				tr_deltypereason.setVisibility(View.GONE);// 06Oct2016 Arpitha
				tr_deltypeotherreasons.setVisibility(View.GONE);// 06Oct2016
																// Arpitha

				rdbreech.setVisibility(View.VISIBLE);// 20Nov2016 Arpitha
				rdvertext.setVisibility(View.VISIBLE);// 20Nov2016 Arpitha

				// txtnormaltype.setVisibility(View.VISIBLE);// 23Nov2016
				// Arpitha

				tr_normaltype.setVisibility(View.VISIBLE);// 27Nov2016 Arpitha

				rdbreech.setEnabled(false);// 26Nov2016 Arpitha
				rdvertext.setEnabled(false);// 26Nov2016 Arpitha

				// 20Nov2016 Arpitha
				if (woman.getNormaltype() == 2) {
					rdbreech.setChecked(true);

				} else if (woman.getNormaltype() == 1) {
					rdvertext.setChecked(true);

				} // 20Nov2016 Arpitha

			} else {// 30sep2016 Arpitha
				wardaltypereason.setVisibility(View.INVISIBLE);// 30sep2016
																// Arpitha
				txtdeltypeotherreason.setVisibility(View.VISIBLE);// 05Oct2016
																	// Arpitha
				txtdeltypereasonheading.setVisibility(View.VISIBLE);// 05Oct2016
																	// Arpitha
				txtdeltypereason.setVisibility(View.VISIBLE);// 05Oct2016
																// Arpitha
				wardeltype.setVisibility(View.VISIBLE);// 06Oct2016 Arpitha

				rdbreech.setVisibility(View.GONE);// 20Nov2016 Arpitha
				rdvertext.setVisibility(View.GONE);// 20Nov2016 Arpitha

				// txtnormaltype.setVisibility(View.GONE);// 23Nov2016 Arpitha

				tr_normaltype.setVisibility(View.GONE);// 27Nov2016 Arpitha

			}

			// No of child
			ArrayList<String> listnoofchild = new ArrayList(
					Arrays.asList(context.getResources().getStringArray(R.array.num_of_children))); // array
			// id
			// of
			// string
			// resource
			int noofchildpos = listnoofchild.indexOf(String.valueOf(woman.getNo_of_child()));
			spn_noofchild.setSelection(noofchildpos);

			if (noofchildpos == 0) {
				trchilddet1.setVisibility(View.VISIBLE);
				tr_childdetsex1.setVisibility(View.VISIBLE);
				tr_secondchilddetails.setVisibility(View.GONE);
				tr_result2.setVisibility(View.GONE);
				tr_childdet2.setVisibility(View.GONE);
				tr_childsex2.setVisibility(View.GONE);
				tr_deltime2.setVisibility(View.GONE);
				txtfirstchilddetails.setText(context.getResources().getString(R.string.child_details));

			} else {
				tr_childdet2.setVisibility(View.VISIBLE);
				tr_childsex2.setVisibility(View.VISIBLE);
				tr_secondchilddetails.setVisibility(View.VISIBLE);
				tr_result2.setVisibility(View.VISIBLE);
				trchilddet1.setVisibility(View.VISIBLE);
				tr_childdetsex1.setVisibility(View.VISIBLE);
				tr_deltime2.setVisibility(View.VISIBLE);

				txtfirstchilddetails.setText(context.getResources().getString(R.string.first_child_details));
			}

			spn_delres1.setSelection(woman.getDel_result1() - 1);

			// delivery result2

			spn_delres2.setSelection(woman.getDel_result2() - 1);

			if (woman.getDel_type() > 1) {
				String[] delTypeRes = null;
				if (woman.getDelTypeReason() != null)// 05Oct2016
														// Arpitha
				{
					delTypeRes = woman.getDelTypeReason().split(",");
				}
				String[] delTypeResArr = null;
				if (woman.getDel_type() == 2)
					// delTypeResArr =
					// getResources().getStringArray(R.array.spnreasoncesarean);//commented
					// on 28Sep2016 Arpitha
					delTypeResArr = context.getResources().getStringArray(R.array.spnreasoncesareanValues);// 28Sep2016
				// Arpitha
				else if (woman.getDel_type() == 3)
					delTypeResArr = context.getResources().getStringArray(R.array.spnreasoninstrumental);

				String delTypeResDisplay = "";
				if (delTypeResArr != null) {
					if (delTypeRes != null && delTypeRes.length > 0) {
						for (String str : delTypeRes) {
							if (str != null && str.length() > 0)
								delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)] + "\n";
						}
					}
				}
				txtdeltypereason.setText(delTypeResDisplay);
				txtdeltypereason.setEnabled(false);
				txtdeltypereason.setTextColor(context.getResources().getColor(R.color.fontcolor_disable));
				etdeltypeotherreasons.setText(woman.getDeltype_otherreasons());
				txtdeltypereason.setVisibility(View.VISIBLE);// 05Oct2016
																// Arpitha
			}

			// Arpitha - 10Aug2016 check for null
			String[] tearsResArr;
			String[] tearsres = null;
			if (woman.getTears() != null) {
				tearsres = woman.getTears().split(",");
			}

			// updated on 6july2016
			tearsResArr = context.getResources().getStringArray(R.array.tearsvalues);

			String tearsResDisplay = "";
			if (tearsResArr != null) {
				if (tearsres != null && tearsres.length > 0) {
					for (String str : tearsres) {
						if (str != null && str.length() > 0)
							tearsResDisplay = tearsResDisplay + tearsResArr[Integer.parseInt(str)] + "\n";
					}
				}
			}

			ettears.setText(tearsResDisplay);

			if (woman.getEpisiotomy() == 1) {

				rdepiyes.setChecked(true);
				// trsuturematerial.setVisibility(View.VISIBLE);

				if (woman.getSuturematerial() == 1) {
					rdcatgut.setChecked(true);
					rdcatgut.setVisibility(View.VISIBLE);// 31oct2016 Arpitha
					rdvicryl.setVisibility(View.VISIBLE);// 31oct2016 Arpitha
					trsuturematerial.setVisibility(View.VISIBLE);// 23Nov2016
																	// Arpitha

				} else if (woman.getSuturematerial() == 2) {
					rdvicryl.setChecked(true);
					rdcatgut.setVisibility(View.VISIBLE);// 31oct2016 Arpitha
					rdvicryl.setVisibility(View.VISIBLE);// 31oct2016 Arpitha
					trsuturematerial.setVisibility(View.VISIBLE);// 23Nov2016
																	// Arpitha
				} else {
					trsuturematerial.setVisibility(View.GONE);
				}
			} else {
				rdepino.setChecked(true);

			}
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();

		}
	}

	static TextWatcher watcher=new TextWatcher(){

	@Override public void onTextChanged(CharSequence s,int start,int before,int count){
	// TODO Auto-generated method stub
	try{AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName()+" - "+this.getClass().getSimpleName());if(s==etbabywt1.getEditableText()){if(count>0){int babywt1=Integer.parseInt(etbabywt1.getText().toString());

	if((babywt1<2500||babywt1>4000)){imgwt1_warning.setVisibility(View.VISIBLE);}else{imgwt1_warning.setVisibility(View.INVISIBLE);}}else imgwt1_warning.setVisibility(View.INVISIBLE);

	}else if(s==etbabywt2.getEditableText()){if(count>0){int babywt2=Integer.parseInt(etbabywt2.getText().toString());

	if((babywt2<2500||babywt2>4000)){imgwt2_warning.setVisibility(View.VISIBLE);}else{imgwt2_warning.setVisibility(View.INVISIBLE);}}else imgwt2_warning.setVisibility(View.INVISIBLE);}

	}catch(Exception e){AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+" - "+this.getClass().getSimpleName(),e);e.printStackTrace();}

	}

	@Override public void beforeTextChanged(CharSequence s,int start,int count,int after){
	// TODO Auto-generated method stub

	}

	@Override public void afterTextChanged(Editable s){
	// TODO Auto-generated method stub

	}};

	private static void displayConfirmationAlert_pulse(String exit_msg, final String classname, final Context context,
			final Women_Profile_Pojo woman) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		final Dialog dialog = new Dialog(context);
		dialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
				+ context.getResources().getString(R.string.warning) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		String strwarningmess = "";
		String strval = "";

		if (classname.equalsIgnoreCase(context.getResources().getString(R.string.confirmation))) {

			if (pulval > 120 || pulval < 50) {
				strwarningmess = strwarningmess + context.getResources().getString(R.string.pul_val) + "\n";
				strval = strval + "" + pulval + "\n";

			}
			if (bpsysval < 80 || bpsysval > 160) {
				strwarningmess = strwarningmess + context.getResources().getString(R.string.bp_sys) + "\n";
				strval = strval + "" + bpsysval + "\n";

			}
			if (bpdiaval < 50 || bpdiaval >= 110) {

				strwarningmess = strwarningmess + context.getResources().getString(R.string.bp_dia) + "\n";
				strval = strval + "" + bpdiaval + "\n";
			}

			// 22Sep2016 Arpitha
			if (heartrate > 0 && (heartrate < 50 || heartrate > 200)) {
				strwarningmess = strwarningmess + context.getResources().getString(R.string.heart_rate_val_is) + "\n";
				strval = strval + "" + heartrate + "\n";
			}
			if (spo2 > 0 && (spo2 < 40 || spo2 > 100)) {
				strwarningmess = strwarningmess + context.getResources().getString(R.string.spo2_val_is) + "\n";
				strval = strval + "" + spo2 + "\n";

			}
		}

		if (classname.equalsIgnoreCase(context.getResources().getString(R.string.deldata))) {
			if (weight1 > 0 && (weight1 < 2500 || weight1 > 4000)) {
				if (Integer.parseInt(no_of_child) == 2)
					strwarningmess = strwarningmess + context.getResources().getString(R.string.first_baby_weight)
							+ "\n";
				else
					strwarningmess = strwarningmess + context.getResources().getString(R.string.baby_weight) + "\n";
				strval = strval + "" + weight1 + context.getResources().getString(R.string.grams) + "\n";
			}

			if (weight2 > 0 && (weight2 < 2500 || weight2 > 4000)) {
				strwarningmess = strwarningmess + context.getResources().getString(R.string.second_baby_weight) + "\n";
				strval = strval + "" + weight2 + context.getResources().getString(R.string.grams) + "\n";
			}
		}

		txtdialog.setText(strwarningmess);
		txtdialog1.setText(strval);
		txtdialog6.setText(context.getResources().getString(R.string.bp_val));// 17Nov2016
		// Arpitha

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();

					if (classname.equalsIgnoreCase(context.getResources().getString(R.string.confirmation))) {
						// updated bindu - 29Aug2016 - check confirmation of Ref
						// date and time
						String msg = context.getResources().getString(R.string.refdatetimerecheck)
								+ etdateofreferral.getText().toString() + " / " + ettimeofreferral.getText().toString()
								+ context.getResources().getString(R.string.confirmrecheck);
						ConfirmAlert(msg, context.getResources().getString(R.string.referral), woman);// 02Oct2016
						// Arpitha
						// -
						// string
						// val
						// from
						// strings.xml

					}
					if (classname.equalsIgnoreCase(context.getResources().getString(R.string.deldata))) {
						// date and time
						String msg = context.getResources().getString(R.string.deldatetimerecheck)
								+ etdeldate.getText().toString() + " / " + etdeltime.getText().toString()
								+ context.getResources().getString(R.string.confirmrecheck);
						// commented on 12Jan2017 Arpitha ConfirmAlert(msg,
						// getResources().getString(R.string.deldata));
						deldata(woman);// 12Jan2017 Arpitha
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				// }

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017
		dialog.show();
		dialog.setCancelable(false);// 31Oct2016 Arpitha

	}

	/** Method to rollback the trnsaction */
	public static void rollbackTrans(Context context) throws Exception {
		Partograph_DB dbh = new Partograph_DB(context);
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(context, context.getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/**
	 * updated bindu - 25Aug2016 Save Confirmation Alert
	 * 
	 * @param goToScreen
	 */
	private static void ConfirmAlert(String msg, final String goToScreen, final Women_Profile_Pojo woman) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set dialog message
		alertDialogBuilder.setMessage(msg).setCancelable(false).setNegativeButton(
				context.getResources().getString(R.string.recheck), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						if (goToScreen.equalsIgnoreCase(context.getResources().getString(R.string.deldata))) {// 02OCt2016
							// ARpitha
							// -
							// string
							// val
							// from
							// strings.xml
							etdeldate.requestFocus();
						} else if (goToScreen.equalsIgnoreCase(context.getResources().getString(R.string.referral))) {// 02OCt2016
							// ARpitha
							// -
							// string
							// val
							// from
							// strings.xml
							etdateofreferral.requestFocus();
						}

						dialog.cancel();
					}
				}).setPositiveButton(context.getResources().getString(R.string.save),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								try {
									if (goToScreen
											.equalsIgnoreCase(context.getResources().getString(R.string.deldata))) {// 02OCt2016
										// ARpitha
										// -
										// string
										// val
										// from
										// strings.xml
										deldata(woman);
										dialog.cancel();
									}
									// dialog_del.cancel();
									// } else if
									// (goToScreen.equalsIgnoreCase(getResources().getString(R.string.referral)))
									// {// 02OCt2016
									// // ARpitha
									// // -
									// // string
									// // val
									// // from
									// // strings.xml
									//// referral_data();
									// dialog.cancel();
									// }
									else
										dialog.cancel();
								} catch (Exception e) {
									AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
											+ this.getClass().getSimpleName(), e);

									e.printStackTrace();
								}

							}
						});
		AlertDialog alertDialog1 = alertDialogBuilder.create();
		alertDialog1.show();
		alertDialog1.setCancelable(false);// 31Oct2016 Arpitha
	}

	private static void generateLatentPhaseinpdfFormat(Context context, Women_Profile_Pojo woman) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		// doc = new Document(PageSize.A4);

		try {

			String path = AppContext.mainDir + "/PDF/Latent Phase";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			latentfile = new File(dir, woman.getWomen_name() + "_LatentPhase.pdf");

			// basic(woman, context, latentfile);

			/*
			 * String path = AppContext.mainDir + "/PDF/Latent Phase";
			 * 
			 * File dir = new File(path); if (!dir.exists()) dir.mkdirs();
			 * 
			 * Log.d("PDFCreator", "PDF Path: " + path);
			 * 
			 * latentfile = new File(dir, woman.getWomen_name() +
			 * "_LatentPhase.pdf"); fOut = new FileOutputStream(latentfile);
			 * 
			 * PdfWriter.getInstance(doc, fOut);
			 * 
			 * doc.setMarginMirroring(false); // doc.setMargins(-25, -20, 65,
			 * -50); doc.setMargins(0, 10, 10, 0);
			 * 
			 * doc.open();
			 * 
			 * Paragraph p_heading = new
			 * Paragraph(context.getResources().getString(R.string.latent_phase)
			 * , heading); // Font paraFont = new Font(Font.HELVETICA); //
			 * p_heading.setFont(paraFont);
			 * p_heading.setAlignment(Paragraph.ALIGN_CENTER);
			 * 
			 * doc.add(p_heading);
			 * 
			 * Drawable d =
			 * context.getResources().getDrawable(R.drawable.sentiagraph);
			 * 
			 * BitmapDrawable bitDw = ((BitmapDrawable) d);
			 * 
			 * Bitmap bmp = bitDw.getBitmap();
			 * 
			 * ByteArrayOutputStream stream = new ByteArrayOutputStream();
			 * 
			 * bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);
			 * 
			 * Image image = Image.getInstance(stream.toByteArray());
			 * 
			 * image.setIndentationLeft(10); latentDoc.add(image); String
			 * strdatetime = Partograph_CommonClass.getConvertedDateFormat(
			 * Partograph_CommonClass.getTodaysDate(),
			 * Partograph_CommonClass.defdateformat) + " " +
			 * Partograph_CommonClass.getCurrentTime() + " "; Paragraph p1 = new
			 * Paragraph(strdatetime, small);
			 * p1.setAlignment(Paragraph.ALIGN_RIGHT); latentDoc.add(p1);
			 * 
			 * womanBasicInfo(woman);
			 * 
			 * Paragraph basicInfo = new Paragraph("Woman Basic Information",
			 * heading); basicInfo.setAlignment(Paragraph.ALIGN_LEFT);
			 * basicInfo.setIndentationLeft(30); latentDoc.add(basicInfo);
			 * 
			 * latentDoc.add(womanbasics);
			 */

			basic(woman, context, latentfile, context.getResources().getString(R.string.latent_phase));
			dispalyLatentPhase(context, woman);

			close();

			/*
			 * // Rectangle rect = new Rectangle(10, 10, 590, 808); Rectangle
			 * rect = new Rectangle(10, 10, 585, 808); rect.enableBorderSide(1);
			 * rect.enableBorderSide(1); rect.enableBorderSide(1);
			 * rect.enableBorderSide(1); rect.setBorder(2); //
			 * rect.setBorderColor(context.getResources().getColor(R.color.black
			 * )); rect.setBorder(Rectangle.BOX); rect.setBorderWidth(1);
			 * doc.add(rect);
			 */

			// doc.close();
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

	}

	static PdfPTable dispalyLatentPhase(Context context, Women_Profile_Pojo woman) {
		try {
			ArrayList<String> latphase = new ArrayList<String>();
			latphase.add(context.getResources().getString(R.string.date) + " & "
					+ context.getResources().getString(R.string.time));
			latphase.add(context.getResources().getString(R.string.pulse) + "(bpm)");
			latphase.add(context.getResources().getString(R.string.txtbpvalue) + "(mmHg)");
			latphase.add(context.getResources().getString(R.string.contraction));
			latphase.add(context.getResources().getString(R.string.fhs));
			latphase.add(context.getResources().getString(R.string.pv));
			latphase.add(context.getResources().getString(R.string.advice));

			/*
			 * for (int k = 0; k <
			 * Partograph_CommonClass.listValuesItemsafm.size(); k++) {
			 * 
			 * String val =
			 * Partograph_CommonClass.listValuesItemsafm.get(k).getAmnioticfluid
			 * (); if (val.equals("0")) val =
			 * context.getResources().getString(R.string.c); else if
			 * (val.equals("1")) val =
			 * context.getResources().getString(R.string.m); // 26Nov2015 else
			 * if (val.equals("2")) val =
			 * context.getResources().getString(R.string.i); else if
			 * (val.equals("3")) val =
			 * context.getResources().getString(R.string.b);
			 * amniotic_row.add(val); }
			 */

			lat = new PdfPTable(7);
			lat.setWidthPercentage(90);
			lat.setSpacingBefore(20);
			lat.setWidths(new int[] { 8, 4, 5, 7, 4, 5, 9 });
			for (int k = 0; k < 7; k++) {
				String header = " ";
				if (k < latphase.size()) {
					header = latphase.get(k);
				}
				PdfPCell cell = new PdfPCell();
				// cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPadding(8);
				lat.addCell(cell);
			}

			lat.completeRow();

			/*
			 * ArrayList<String> latVal1 = new ArrayList<String>();
			 * moulding.add("Moulding"); for (int l = 0; l <
			 * Partograph_CommonClass.listValuesItemsafm.size(); l++) { String
			 * mouldingVal =
			 * Partograph_CommonClass.listValuesItemsafm.get(l).getMoulding();
			 * 
			 * if (mouldingVal.equalsIgnoreCase("0")) mouldingVal =
			 * getResources().getString(R.string.m1); else if
			 * (mouldingVal.equalsIgnoreCase("1")) mouldingVal =
			 * getResources().getString(R.string.m2); else mouldingVal =
			 * getResources().getString(R.string.m3);
			 * 
			 * moulding.add(mouldingVal);
			 * 
			 * }
			 * 
			 * ArrayList<LatentPhasePojo> latVal = new
			 * ArrayList<LatentPhasePojo>(); //latVal =
			 */
			// ArrayList<LatentPhasePojo> latval = new
			// ArrayList<LatentPhasePojo>();

			Partograph_DB dbh = Partograph_DB.getInstance(context);

			Cursor cur = dbh.getLatentPhaseData(woman.getUserId(), woman.getWomenId(), 0);

			ArrayList<LatentPhasePojo> arrVal = new ArrayList<LatentPhasePojo>();
			if (cur != null && cur.getCount() > 0) {
				cur.moveToFirst();
				do {
					LatentPhasePojo lpojo = new LatentPhasePojo();
					lpojo.setUserId(cur.getString(0));
					lpojo.setWomanId(cur.getString(1));
					lpojo.setWomanName(cur.getString(2));
					lpojo.setPulse(cur.getInt(4));
					lpojo.setBp(cur.getString(5));
					lpojo.setContractions(cur.getInt(6));
					// lpojo.setDuration(cur.getString(7));
					lpojo.setFhs(cur.getInt(7));
					lpojo.setPv(cur.getString(8));
					lpojo.setAdvice(cur.getString(9));
					lpojo.setDatetime(cur.getString(3));
					arrVal.add(lpojo);

				} while (cur.moveToNext());
			}

			for (int i = 0; i < arrVal.size(); i++)

			{
				ArrayList<String> val = new ArrayList<String>();
				if (arrVal.size() > 0 && i < arrVal.size()) {

					String date = Partograph_CommonClass
							.getConvertedDateFormat(arrVal.get(i).getDatetime().split("/")[0], "dd-MM-yyyy") + "/"
							+ arrVal.get(i).getDatetime().split("/")[1];
					val.add(date);

					if (arrVal.get(i).getPulse() > 0) {
						pulse = "" + arrVal.get(i).getPulse();
						//
						val.add(pulse);
					} else
						val.add("");

					if (arrVal.get(i).getBp() != null && arrVal.get(i).getBp().trim().length() > 0) {
						bp = arrVal.get(i).getBp();
						val.add(bp);
					} else
						val.add("");
					if (arrVal.get(i).getContractions() > 0) {
						contractions = "" + arrVal.get(i).getContractions();
						val.add(contractions);
					} else
						val.add("");
					// val.add("" + arrVal.get(0).getDuration());
					if (arrVal.get(i).getFhs() > 0) {
						fhs = "" + arrVal.get(i).getFhs();
						val.add(fhs);
					} else
						val.add("");
					if (arrVal.get(i).getPv() != null && arrVal.get(i).getPv().trim().length() > 0) {
						pv = "" + arrVal.get(i).getPv();
						val.add(pv);
					} else
						val.add("");
					advice = arrVal.get(i).getAdvice();
					val.add(advice);
				}

				for (int x = 0; x < 7; x++) {
					String strVal = " ";
					// if (x < arrVal.size()) {
					// if (val.size() < x)
					// if(x < 7)
					if (val.size() > 0)
						strVal = val.get(x);
					// else
					// strVal = "";

					// }
					// String val = latval.get(x).getDatetime();
					PdfPCell cell_moulding = new PdfPCell();
					// if(arrVal.size()>0)
					cell_moulding.setPhrase(new Phrase(strVal, new Font(Font.HELVETICA, 10, Font.NORMAL)));
					// else
					// cell_moulding.setPhrase(new Phrase(strVal, new
					// Font(Font.HELVETICA, 10, Font.NORMAL)));
					cell_moulding.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell_moulding.setPadding(5);
					lat.addCell(cell_moulding);
				}

				if (arrVal.size() <= 0) {
					for (int x = 0; x < 30; x++) {
						String strVal = " ";
						PdfPCell cell_moulding = new PdfPCell();
						// if(arrVal.size()>0)
						cell_moulding.setPhrase(new Phrase(strVal, new Font(Font.HELVETICA, 10, Font.NORMAL)));
						// else
						// cell_moulding.setPhrase(new Phrase(strVal, new
						// Font(Font.HELVETICA, 10, Font.NORMAL)));
						cell_moulding.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell_moulding.setPadding(5);
						lat.addCell(cell_moulding);
					}
				}

				lat.completeRow();
			}

			if (arrVal.size() <= 0) {
				for (int i = 0; i <= 35; i++) {
					PdfPCell cell_moulding1 = new PdfPCell();
					cell_moulding1.setPhrase(new Phrase("", new Font(Font.HELVETICA, 10, Font.NORMAL)));
					cell_moulding1.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell_moulding1.setPadding(15);
					cell_moulding1.setBorder(Rectangle.BOX);
					lat.addCell(cell_moulding1);
				}

			}

			doc.add(lat);

		} catch (Exception e) {
			// TODO: handle exception
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
		return lat;
	}

	/*
	 * try { ArrayList<String> latphase = new ArrayList<String>();
	 * latphase.add(context.getResources().getString(R.string.date) + " & " +
	 * context.getResources().getString(R.string.time));
	 * latphase.add(context.getResources().getString(R.string.pulse));
	 * latphase.add(context.getResources().getString(R.string.txtbpvalue));
	 * latphase.add(context.getResources().getString(R.string.contraction));
	 * latphase.add(context.getResources().getString(R.string.fhs));
	 * latphase.add(context.getResources().getString(R.string.pv));
	 * latphase.add(context.getResources().getString(R.string.advice));
	 * 
	 * 
	 * for (int k = 0; k < Partograph_CommonClass.listValuesItemsafm.size();
	 * k++) {
	 * 
	 * String val =
	 * Partograph_CommonClass.listValuesItemsafm.get(k).getAmnioticfluid (); if
	 * (val.equals("0")) val = context.getResources().getString(R.string.c);
	 * else if (val.equals("1")) val =
	 * context.getResources().getString(R.string.m); // 26Nov2015 else if
	 * (val.equals("2")) val = context.getResources().getString(R.string.i);
	 * else if (val.equals("3")) val =
	 * context.getResources().getString(R.string.b); amniotic_row.add(val); }
	 * 
	 * 
	 * lat = new PdfPTable(7); lat.setSpacingBefore(20); lat.setWidths(new int[]
	 * { 8, 4, 5, 7, 4, 5, 9 }); for (int k = 0; k < 7; k++) { String header =
	 * " "; if (k < latphase.size()) { header = latphase.get(k); } PdfPCell cell
	 * = new PdfPCell(); // cell.setGrayFill(0.9f); cell.setPhrase(new
	 * Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
	 * cell.setHorizontalAlignment(Element.ALIGN_CENTER); cell.setPadding(8);
	 * lat.addCell(cell); }
	 * 
	 * lat.completeRow();
	 * 
	 * 
	 * ArrayList<String> latVal1 = new ArrayList<String>();
	 * moulding.add("Moulding"); for (int l = 0; l <
	 * Partograph_CommonClass.listValuesItemsafm.size(); l++) { String
	 * mouldingVal =
	 * Partograph_CommonClass.listValuesItemsafm.get(l).getMoulding();
	 * 
	 * if (mouldingVal.equalsIgnoreCase("0")) mouldingVal =
	 * getResources().getString(R.string.m1); else if
	 * (mouldingVal.equalsIgnoreCase("1")) mouldingVal =
	 * getResources().getString(R.string.m2); else mouldingVal =
	 * getResources().getString(R.string.m3);
	 * 
	 * moulding.add(mouldingVal);
	 * 
	 * }
	 * 
	 * ArrayList<LatentPhasePojo> latVal = new ArrayList<LatentPhasePojo>();
	 * //latVal =
	 * 
	 * // ArrayList<LatentPhasePojo> latval = new //
	 * ArrayList<LatentPhasePojo>();
	 * 
	 * Partograph_DB dbh = Partograph_DB.getInstance(context);
	 * 
	 * Cursor cur = dbh.getLatentPhaseData(woman.getUserId(),
	 * woman.getWomenId(), 0);
	 * 
	 * ArrayList<LatentPhasePojo> arrVal = new ArrayList<LatentPhasePojo>(); if
	 * (cur != null && cur.getCount() > 0) { cur.moveToFirst(); do {
	 * LatentPhasePojo lpojo = new LatentPhasePojo();
	 * lpojo.setUserId(cur.getString(0)); lpojo.setWomanId(cur.getString(1));
	 * lpojo.setWomanName(cur.getString(2)); lpojo.setPulse(cur.getInt(4));
	 * lpojo.setBp(cur.getString(5)); lpojo.setContractions(cur.getInt(6)); //
	 * lpojo.setDuration(cur.getString(7)); lpojo.setFhs(cur.getInt(7));
	 * lpojo.setPv(cur.getString(8)); lpojo.setAdvice(cur.getString(9));
	 * lpojo.setDatetime(cur.getString(3)); arrVal.add(lpojo);
	 * 
	 * } while (cur.moveToNext()); }
	 * 
	 * for (int i = 0; i < arrVal.size(); i++)
	 * 
	 * { ArrayList<String> val = new ArrayList<String>(); if(arrVal.size()>0) {
	 * 
	 * String date =
	 * Partograph_CommonClass.getConvertedDateFormat(arrVal.get(i).getDatetime()
	 * .split("/")[0], "dd-MM-yyyy") + "/" +
	 * arrVal.get(i).getDatetime().split("/")[1]; val.add(date);
	 * 
	 * if (arrVal.get(i).getPulse() > 0) { pulse = "" +
	 * arrVal.get(i).getPulse(); // val.add(pulse); } else val.add("");
	 * 
	 * if (arrVal.get(i).getBp() != null &&
	 * arrVal.get(i).getBp().trim().length() > 0) { bp = arrVal.get(i).getBp();
	 * val.add(bp); } else val.add(""); if (arrVal.get(i).getContractions() > 0)
	 * { contractions = "" + arrVal.get(i).getContractions();
	 * val.add(contractions); } else val.add(""); // val.add("" +
	 * arrVal.get(0).getDuration()); if (arrVal.get(i).getFhs() > 0) { fhs = ""
	 * + arrVal.get(i).getFhs(); val.add(fhs); } else val.add(""); if
	 * (arrVal.get(i).getPv() != null && arrVal.get(i).getPv().trim().length() >
	 * 0) { pv = "" + arrVal.get(i).getPv(); val.add(pv); } else val.add("");
	 * advice = arrVal.get(i).getAdvice(); val.add(advice); }
	 * 
	 * for (int x = 0; x < 7; x++) { String strVal = " ";
	 * 
	 * strVal = val.get(x); // if (x < arrVal.size()) { // if (val.size() < x)
	 * 
	 * // else
	 * 
	 * // if(val.size()>0 && val.size()>x) // // strVal = "";
	 * 
	 * // } // String val = latval.get(x).getDatetime(); PdfPCell cell_moulding
	 * = new PdfPCell(); // if(x < 7) // {
	 * 
	 * cell_moulding.setPhrase(new Phrase(strVal, new Font(Font.HELVETICA, 10,
	 * Font.NORMAL))); //} // else // cell_moulding.setPhrase(new Phrase("", new
	 * Font(Font.HELVETICA, 10, Font.NORMAL)));
	 * 
	 * cell_moulding.setHorizontalAlignment(Element.ALIGN_CENTER);
	 * cell_moulding.setPadding(5); lat.addCell(cell_moulding); }
	 * 
	 * lat.completeRow(); }
	 * 
	 * if (arrVal.size() <= 0) { for (int i = 0; i <= 150; i++) { PdfPCell
	 * cell_moulding1 = new PdfPCell(); cell_moulding1.setPhrase(new Phrase("",
	 * new Font(Font.HELVETICA, 10, Font.NORMAL)));
	 * cell_moulding1.setHorizontalAlignment(Element.ALIGN_CENTER);
	 * cell_moulding1.setPadding(15); cell_moulding1.setBorder(Rectangle.BOX);
	 * lat.addCell(cell_moulding1); }
	 * 
	 * } doc.add(lat); } catch (Exception e) { // TODO: handle exception
	 * AppContext.addLog(new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * context.getClass().getSimpleName(), e); e.printStackTrace(); } return
	 * lat; //
	 */

	private static void generateDischargeSlipinpdfFormat(Context context, Women_Profile_Pojo woman) {
		// TODO Auto-generated method stub
		// doc = new Document(PageSize.A4);

		try {

			String path = AppContext.mainDir + "/PDF/DischargeSlip";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			dischrgefile = new File(dir, woman.getWomen_name() + "_DischargeSlip.pdf");

			basic(woman, context, dischrgefile, context.getResources().getString(R.string.dischrage_slip));

			discahrfgedata(woman, context);

			/*
			 * PdfPTable table = new PdfPTable(2); table.setWidthPercentage(98);
			 * table.addCell(getCell(getResources().getString(R.string.
			 * date_of_discharge) + ": " +
			 * dvalues.get(0).getDateTimeOfDischarge(), PdfPCell.ALIGN_LEFT));
			 * table.addCell(getCell(getResources().getString(R.string.
			 * place_of_next_folow_up) + ": " +
			 * dvalues.get(0).getPlaceOfNextFOllowUp(), PdfPCell.ALIGN_RIGHT));
			 * 
			 * doc.add(table);
			 * 
			 * PdfPTable table7 = new PdfPTable(2);
			 * table7.setWidthPercentage(98);
			 * table7.addCell(getCell(getResources().getString(R.string.
			 * date_of_next_folow_up) + ": " +
			 * dvalues.get(0).getDateOfNextFollowUp(), PdfPCell.ALIGN_LEFT));
			 * table7.addCell(
			 * getCell(getResources().getString(R.string.advice_given) + ": " +
			 * dvalues.get(0).getAdviceGiven(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table7);
			 * 
			 * PdfPTable table1 = new PdfPTable(2);
			 * table1.setWidthPercentage(98);
			 * 
			 * table1.addCell(
			 * getCell(getResources().getString(R.string.procedures) + ": " +
			 * dvalues.get(0).getProcedures(), PdfPCell.ALIGN_LEFT));
			 * table1.addCell(getCell(getResources().getString(R.string.
			 * discharge_diagnosis) + ": " +
			 * dvalues.get(0).getDischargeDiagnosis(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table1);
			 * 
			 * PdfPTable table2 = new PdfPTable(2);
			 * table2.setWidthPercentage(98);
			 * table2.addCell(getCell(getResources().getString(R.string.
			 * complications_observed) + ": " +
			 * dvalues.get(0).getComplicationObserved(), PdfPCell.ALIGN_LEFT));
			 * table2.addCell(getCell(
			 * getResources().getString(R.string.finding_history) + ": " +
			 * dvalues.get(0).getFindingHistorys(), PdfPCell.ALIGN_RIGHT));
			 * 
			 * doc.add(table2);
			 * 
			 * PdfPTable table8 = new PdfPTable(2);
			 * table8.setWidthPercentage(98);
			 * table8.addCell(getCell(getResources().getString(R.string.
			 * patient_disposition) + ": " +
			 * dvalues.get(0).getPatientDisposition(), PdfPCell.ALIGN_LEFT));
			 * table8.addCell(getCell(
			 * getResources().getString(R.string.admisiion_reasons) + ": " +
			 * dvalues.get(0).getAdmissionReasons(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table8);
			 * 
			 * PdfPTable table3 = new PdfPTable(1);
			 * table3.setWidthPercentage(98);
			 * 
			 * table3.addCell(
			 * getCell(getResources().getString(R.string.overall_remarks) + ": "
			 * + dvalues.get(0).getRemarks(), PdfPCell.ALIGN_LEFT)); //
			 * table3.addCell(getCell(getResources().getString(R.string.
			 * patient_disposition) // + ": " // +
			 * dvalues.get(0).getPatientDisposition(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table3);
			 */

			// Rectangle rect = new Rectangle(100, 100, 900, 500);
			// Rectangle rect = new Rectangle(5, 5, 500, 500);
			// Rectangle rect = new Rectangle(5, 5, 585, 770);
			/*
			 * Rectangle rect = new Rectangle(10, 20, 585, 808);
			 * rect.enableBorderSide(1); rect.enableBorderSide(1);
			 * rect.enableBorderSide(1); rect.enableBorderSide(1);
			 * rect.setBorder(2); //
			 * rect.setBorderColor(context.getResources().getColor(R.color.black
			 * )); rect.setBorder(Rectangle.BOX); rect.setBorderWidth(1);
			 * dischargedoc.add(rect);
			 * 
			 * dischargedoc.close();
			 */
			close();
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

	}

	static void DeliveryInformation(Women_Profile_Pojo woman) throws NotFoundException, Exception {

		if (woman.getDel_type() != 0) {
			ArrayList<String> deliveryInfo = new ArrayList<String>();
			String delType = "";
			if (woman.getDel_type() == 1)
				delType = "Normal";
			else if (woman.getDel_type() == 2)
				delType = "Cesarean";
			else if (woman.getDel_type() == 3)
				delType = "Instrumental";

			deliveryInfo.add(delType);

			if (woman.getDelTypeReason() != null) {
				String[] delTypeRes = woman.getDelTypeReason().split(",");
				String[] delTypeResArr = null;
				if (woman.getDel_type() == 2)
					delTypeResArr = context.getResources().getStringArray(R.array.spnreasoncesareanValues);
				else if (woman.getDel_type() == 3)
					delTypeResArr = context.getResources().getStringArray(R.array.spnreasoninstrumental);

				String delTypeResDisplay = "";
				if (delTypeResArr != null) {
					if (delTypeRes != null && delTypeRes.length > 0) {
						for (String str : delTypeRes) {
							if (str != null && str.length() > 0)
								delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)] + "\n";
						}
					}
				}

				deliveryInfo.add(delTypeResDisplay);
			}

			deliveryInfo.add(context.getResources().getString(R.string.dod) + ": " + Partograph_CommonClass
					.getConvertedDateFormat(woman.getDel_Date(), Partograph_CommonClass.defdateformat) + " "
					+ woman.getDel_Time());

			deliveryInfo.add(context.getResources().getString(R.string.noofchild) + ": " + "" + woman.getNo_of_child());

			womandelInfo = new PdfPTable(deliveryInfo.size());

			womandelInfo.setWidthPercentage(90);

			// womandelInfo.setWidths(new int[] { 7, 5, 11, 11 });

			for (int k = 0; k < deliveryInfo.size(); k++) {
				String header = deliveryInfo.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.BOLD, 10, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womandelInfo.addCell(cell);

				womandelInfo.setSpacingBefore(6);

			}

			womandelInfo.setWidthPercentage(90);
			womandelInfo.completeRow();

			// }

		}

	}

	static void childInformation(ArrayList<DiscgargePojo> dvalues) throws DocumentException {

		ArrayList<String> valChildInfo = new ArrayList<String>();

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());
		String conditionOfFIrstChild, conditionOfSecondChild;
		conditionOfFIrstChild = context.getResources().getString(R.string.condition_of_child);
		conditionOfSecondChild = context.getResources().getString(R.string.child_investigation);

		if (dvalues.size() > 0) {

			conditionOfFIrstChild = conditionOfFIrstChild + ": " + dvalues.get(0).getConditionOfChild();
			conditionOfSecondChild = conditionOfSecondChild + ": " + dvalues.get(0).getChildInvestigation();

		}

		valChildInfo.add(conditionOfFIrstChild);
		valChildInfo.add(conditionOfSecondChild);

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());

		childInfo = new PdfPTable(valChildInfo.size());

		childInfo.setWidthPercentage(90);

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valChildInfo.size(); k++) {
			String header = valChildInfo.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			childInfo.addCell(cell);

			childInfo.setSpacingBefore(6);

		}
		childInfo.setWidthPercentage(90);
		childInfo.completeRow();

	}

	static void child2Information(ArrayList<DiscgargePojo> dvalues) throws DocumentException {

		ArrayList<String> valChildInfo = new ArrayList<String>();

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());
		/*
		 * if (dvalues.size() > 0) {
		 * valChildInfo.add(context.getResources().getString(R.string.
		 * condition_of_child) + ": " + dvalues.get(0).getConditionOfChild2());
		 * valChildInfo.add(context.getResources().getString(R.string.
		 * child_investigation) + ": " +
		 * dvalues.get(0).getChild2Investigation()); }
		 */

		String conditionOfFIrstChild, conditionOfSecondChild;
		conditionOfFIrstChild = context.getResources().getString(R.string.condition_of_child);
		conditionOfSecondChild = context.getResources().getString(R.string.child_investigation);

		if (dvalues.size() > 0) {

			conditionOfFIrstChild = conditionOfFIrstChild + ": " + dvalues.get(0).getConditionOfChild2();
			conditionOfSecondChild = conditionOfSecondChild + ": " + dvalues.get(0).getChild2Investigation();

		}

		valChildInfo.add(conditionOfFIrstChild);
		valChildInfo.add(conditionOfSecondChild);

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());

		childInfo2 = new PdfPTable(valChildInfo.size());

		childInfo2.setWidthPercentage(90);

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valChildInfo.size(); k++) {
			String header = valChildInfo.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			childInfo2.addCell(cell);

			childInfo2.setSpacingBefore(6);

		}
		childInfo2.setWidthPercentage(90);
		childInfo2.completeRow();

	}

	static void dischargeInformation(ArrayList<DiscgargePojo> dvalues) throws DocumentException {

		ArrayList<String> valDischargeInfo = new ArrayList<String>();

		if (dvalues.size() > 0) {
			valDischargeInfo.add(context.getResources().getString(R.string.date_of_discharge) + ": " + ""
					+ dvalues.get(0).getDateTimeOfDischarge());

			valDischargeInfo.add(context.getResources().getString(R.string.place_of_next_folow_up) + ": " + ""
					+ dvalues.get(0).getConditionOfChild());
			valDischargeInfo.add(context.getResources().getString(R.string.date_of_next_folow_up) + ": " + ""
					+ dvalues.get(0).getChildInvestigation());
		}

		valDischargeInfo
				.add(context.getResources().getString(R.string.advice_given) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.procedures) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo.add(
				context.getResources().getString(R.string.discharge_diagnosis) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo.add(
				context.getResources().getString(R.string.complications_observed) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.finding_history) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo.add(
				context.getResources().getString(R.string.patient_disposition) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.admisiion_reasons) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.overall_remarks) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.advice_given) + ": " + "" + woman.getNo_of_child());

		dischargeInfo = new PdfPTable(valDischargeInfo.size());

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valDischargeInfo.size(); k++) {
			String header = valDischargeInfo.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			dischargeInfo.addCell(cell);

			dischargeInfo.setSpacingBefore(6);

		}
		dischargeInfo.completeRow();

	}

	static void dischargeInformation2() throws DocumentException {

		ArrayList<String> valDischargeInfo2 = new ArrayList<String>();

		/*
		 * if (dvalues.size() > 0) {
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * date_of_discharge) + ": " + "" +
		 * dvalues.get(0).getDateTimeOfDischarge());
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * place_of_next_folow_up) + ": " + "" +
		 * dvalues.get(0).getConditionOfChild());
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * date_of_next_folow_up) + ": " + "" +
		 * dvalues.get(0).getChildInvestigation()); }
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * advice_given) + ": " + "" + woman.getNo_of_child());
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * procedures) + ": " + "" + woman.getNo_of_child());
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * discharge_diagnosis) + ": " + "" + woman.getNo_of_child());
		 */

		valDischargeInfo2.add(
				context.getResources().getString(R.string.complications_observed) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.finding_history) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2.add(
				context.getResources().getString(R.string.patient_disposition) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.admisiion_reasons) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.overall_remarks) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.advice_given) + ": " + "" + woman.getNo_of_child());

		dischargeInfo2 = new PdfPTable(valDischargeInfo2.size());

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valDischargeInfo2.size(); k++) {
			String header = valDischargeInfo2.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			dischargeInfo2.addCell(cell);

			dischargeInfo2.setSpacingBefore(6);

		}
		dischargeInfo2.completeRow();

	}

	public static PdfPCell getCellHeading(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.HELVETICA, 10, Font.BOLD)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.HELVETICA, 10, Font.NORMAL)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public static void generatePostPartumCareDatainpdfFormat(Context context, Women_Profile_Pojo woman) {
		// TODO Auto-generated method stub

		try {

			String path = AppContext.mainDir + "/PDF/PostPartumCare";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			postpartumfile = new File(dir, woman.getWomen_name() + "_PostPartumCare.pdf");

			basic(woman, context, postpartumfile, context.getResources().getString(R.string.postpartum_care));

			getPostPartumData(woman, context);

			close();

		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

	}

	private static void postpartumdata(Women_Profile_Pojo woman, Context context) throws NotFoundException, Exception {
		// TODO Auto-generated method stub

		/*
		 * PdfPTable womanAddress = new PdfPTable(1);
		 * womanAddress.setWidthPercentage(80);
		 * womanAddress.setSpacingBefore(1);
		 * womanAddress.addCell(getCellHeading(
		 * context.getResources().getString(R.string.address) + ": " +
		 * woman.getAddress(), PdfPCell.ALIGN_LEFT));
		 * 
		 * doc.add(womanAddress);
		 */

		ArrayList<PostPartum_Pojo> pvalues = new ArrayList<PostPartum_Pojo>();
		Cursor cur = dbh.getPostPartumData(woman.getUserId(), woman.getWomenId());
		// Cursor cur = PostPartumCare_Activity.cur;
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				PostPartum_Pojo postpojo = new PostPartum_Pojo();
				postpojo.setUserId(cur.getString(0));
				postpojo.setWomanId(cur.getString(1));
				postpojo.setWomanName(cur.getString(2));
				postpojo.setComplaints(cur.getString(3));
				postpojo.setOther(cur.getString(4));
				postpojo.setExamination(cur.getString(5));
				postpojo.setPallor(cur.getString(6));
				postpojo.setPulse(cur.getInt(7));
				postpojo.setBp(cur.getString(8));
				postpojo.setBreastExamination(cur.getString(9));
				postpojo.setInvolution(cur.getString(10));
				postpojo.setLochia(cur.getString(11));
				postpojo.setPerineal(cur.getString(12));
				postpojo.setAdvice(cur.getString(13));
				postpojo.setDateTimeOfEntry(cur.getString(15));
				pvalues.add(postpojo);

				String admResDisplay = "";
				if (pvalues.get(cur.getPosition()).getComplaints() != null) {
					String[] admres = pvalues.get(cur.getPosition()).getComplaints().split(",");
					String[] admResArr = null;

					admResArr = context.getResources().getStringArray(R.array.presenting_complaints);

					if (admResArr != null) {
						if (admres != null && admres.length > 0) {
							for (String str : admres) {
								if (str != null && str.length() > 0)
									admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + ",";
							}
						}
					}
				}

				if (!(cur.isFirst())) {

					complaints = complaints + "\n" + admResDisplay;

					examination = examination + "\n" + pvalues.get(cur.getPosition()).getExamination();
					pallor = pallor + "\n" + pvalues.get(cur.getPosition()).getPallor();
					postbp = postbp + "," + pvalues.get(cur.getPosition()).getBp();
					postpulse = postpulse + "\n" + "" + pvalues.get(cur.getPosition()).getPulse();
					involution = involution + "\n" + pvalues.get(cur.getPosition()).getInvolution();
					postadvice = postadvice + "\n" + pvalues.get(cur.getPosition()).getAdvice();
					lochia = lochia + "\n" + pvalues.get(cur.getPosition()).getLochia();
					perinealCare = perinealCare + "\n" + pvalues.get(cur.getPosition()).getPerineal();
					breastExamination = breastExamination + "\n"
							+ pvalues.get(cur.getPosition()).getBreastExamination();
				} else {
					examination = pvalues.get(cur.getPosition()).getExamination();
					pallor = pvalues.get(cur.getPosition()).getPallor();
					postbp = "" + pvalues.get(cur.getPosition()).getBp();
					postpulse = "" + pvalues.get(cur.getPosition()).getPulse();
					involution = pvalues.get(cur.getPosition()).getInvolution();
					postadvice = pvalues.get(cur.getPosition()).getAdvice();
					lochia = pvalues.get(cur.getPosition()).getLochia();
					perinealCare = pvalues.get(cur.getPosition()).getPerineal();
					breastExamination = pvalues.get(cur.getPosition()).getBreastExamination();
					complaints = admResDisplay;
				}

			} while (cur.moveToNext());

		}

		if (woman.getDel_type() != 0) {
			Paragraph deliveryInfo = new Paragraph("Woman Delivery Information", heading);
			deliveryInfo.setAlignment(Paragraph.ALIGN_LEFT);
			deliveryInfo.setIndentationLeft(30);
			doc.add(deliveryInfo);
			DeliveryInformation(woman);

			doc.add(womandelInfo);

		}

		Paragraph dischargedetials = new Paragraph(context.getResources().getString(R.string.postpartum_care_details),
				heading);
		dischargedetials.setAlignment(Paragraph.ALIGN_LEFT);
		dischargedetials.setIndentationLeft(30);
		doc.add(dischargedetials);

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(90);
		table.setWidths(new int[] { 3, 10 });
		table.setSpacingBefore(10);

		PdfPCell cellTwo = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.presenting_complaints), Element.ALIGN_LEFT));

		PdfPCell cellOne = new PdfPCell(getCell(complaints, Element.ALIGN_LEFT));

		cellOne.setPadding(5);
		cellTwo.setPadding(5);
		cellTwo.setBorder(Rectangle.BOX);
		cellOne.setBorder(Rectangle.BOX);

		// cellOne.set
		table.addCell(cellTwo);
		table.addCell(cellOne);

		doc.add(table);

		PdfPTable womanInvestigation = new PdfPTable(2);

		womanInvestigation.setWidthPercentage(90);

		PdfPCell cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.examination), PdfPCell.CELL));

		PdfPCell cell2 = new PdfPCell(getCell(examination, PdfPCell.CELL));

		cell1.setPadding(5);
		cell2.setPadding(5);
		cell1.setBorder(Rectangle.BOX);
		cell2.setBorder(Rectangle.BOX);

		womanInvestigation.addCell(cell1);
		womanInvestigation.addCell(cell2);
		womanInvestigation.setWidths(new int[] { 3, 10 });

		doc.add(womanInvestigation);

		PdfPTable womanCondition = new PdfPTable(2);

		womanCondition.setWidthPercentage(90);

		PdfPCell womanConditioncell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.pallor), PdfPCell.CELL));

		PdfPCell womanConditioncell2 = new PdfPCell(getCell(pallor, PdfPCell.CELL));

		womanConditioncell1.setPadding(5);
		womanConditioncell2.setPadding(5);
		womanConditioncell1.setBorder(Rectangle.BOX);
		womanConditioncell2.setBorder(Rectangle.BOX);

		womanCondition.addCell(womanConditioncell1);
		womanCondition.addCell(womanConditioncell2);

		womanCondition.setWidths(new int[] { 3, 10 });

		doc.add(womanCondition);

		PdfPTable table10 = new PdfPTable(2);
		table10.setWidthPercentage(90);

		PdfPCell table10cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.pulse), PdfPCell.CELL));

		PdfPCell table10cell2 = new PdfPCell(getCell(postpulse, PdfPCell.CELL));

		table10cell1.setPadding(5);
		table10cell2.setPadding(5);
		table10cell1.setBorder(Rectangle.BOX);
		table10cell2.setBorder(Rectangle.BOX);

		table10.addCell(table10cell1);
		table10.addCell(table10cell2);
		table10.setWidths(new int[] { 3, 10 });
		doc.add(table10);

		PdfPTable table7 = new PdfPTable(2);
		table7.setWidthPercentage(90);

		PdfPCell table7cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.txtbpvalue), PdfPCell.CELL));

		PdfPCell table7cell2 = new PdfPCell(getCell(postbp, PdfPCell.CELL));

		table7cell1.setPadding(5);
		table7cell2.setPadding(5);
		table7cell1.setBorder(Rectangle.BOX);
		table7cell2.setBorder(Rectangle.BOX);

		table7.setWidths(new int[] { 3, 10 });

		table7.addCell(table7cell1);
		table7.addCell(table7cell2);
		doc.add(table7);

		PdfPTable table1 = new PdfPTable(2);
		table1.setWidthPercentage(90);

		PdfPTable table9 = new PdfPTable(2);
		table9.setWidthPercentage(90);

		table1.setWidths(new int[] { 3, 10 });
		table9.setWidths(new int[] { 3, 10 });

		// String advice = "";

		PdfPCell table9cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.breast_examination), PdfPCell.CELL));

		PdfPCell table9cell2 = new PdfPCell(getCell(breastExamination, PdfPCell.CELL));

		table9cell1.setPadding(5);
		table9cell2.setPadding(5);
		table9cell1.setBorder(Rectangle.BOX);
		table9cell2.setBorder(Rectangle.BOX);

		table9.addCell(table9cell1);
		table9.addCell(table9cell2);
		doc.add(table9);

		PdfPCell table1cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.involution_of_uterus), PdfPCell.CELL));

		PdfPCell table1cell2 = new PdfPCell(getCell(involution, PdfPCell.CELL));

		table1cell1.setPadding(5);
		table1cell2.setPadding(5);
		table1cell1.setBorder(Rectangle.BOX);
		table1cell2.setBorder(Rectangle.BOX);

		table1.addCell(table1cell1);
		// table1.setWidths(new int[] { 5, 10 });
		table1.addCell(table1cell2);
		doc.add(table1);

		PdfPTable table11 = new PdfPTable(2);
		table11.setWidthPercentage(90);

		PdfPCell table11cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.lochia), PdfPCell.CELL));

		PdfPCell table11cell2 = new PdfPCell(getCell(lochia, PdfPCell.CELL));

		table11cell1.setPadding(5);
		table11cell2.setPadding(5);
		table11cell1.setBorder(Rectangle.BOX);
		table11cell2.setBorder(Rectangle.BOX);

		table11.setWidths(new int[] { 3, 10 });
		table11.addCell(table11cell1);
		table11.addCell(table11cell2);
		doc.add(table11);

		PdfPTable table2 = new PdfPTable(2);

		table2.setWidthPercentage(90);

		PdfPCell table2cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.perineal_care), PdfPCell.CELL));

		PdfPCell table2cell2 = new PdfPCell(getCell(perinealCare, PdfPCell.CELL));

		table2cell1.setPadding(5);
		table2cell2.setPadding(5);
		table2cell1.setBorder(Rectangle.BOX);
		table2cell2.setBorder(Rectangle.BOX);

		table2.addCell(table2cell1);
		table2.setWidths(new int[] { 3, 10 });
		table2.addCell(table2cell2);
		doc.add(table2);

		PdfPTable table12 = new PdfPTable(2);

		table12.setWidthPercentage(90);

		PdfPCell table12cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.advice), PdfPCell.CELL));

		PdfPCell table12cell2 = new PdfPCell(getCell(postadvice, PdfPCell.CELL));

		table12cell1.setPadding(5);
		table12cell2.setPadding(5);
		table12cell1.setBorder(Rectangle.BOX);
		table12cell2.setBorder(Rectangle.BOX);

		table12.setWidths(new int[] { 3, 10 });

		table12.addCell(table12cell1);
		table12.addCell(table12cell2);
		doc.add(table12);

		PdfPTable table8 = new PdfPTable(2);

		table8.setWidthPercentage(90);

		table8.setWidths(new int[] { 3, 10 });

		PdfPTable signatureofHC = new PdfPTable(1);
		signatureofHC.setWidthPercentage(90);
		signatureofHC.setSpacingBefore(50);
		signatureofHC.addCell(
				getCellHeading(context.getResources().getString(R.string.name_sign_trainer), PdfPCell.ALIGN_RIGHT));

		doc.add(signatureofHC);

	}

	// 29Aug2017 Arpitha - method to export pdfs
	public static void exportPDFs(final Context context, final Women_Profile_Pojo woman) throws Exception {

		final Dialog dialog = new Dialog(context);

		dialog.setTitle(context.getResources().getString(R.string.export_pdf));
		dialog.setContentView(R.layout.dialog_allpdfs);
		dialog.show();

		chklatent = (CheckBox) dialog.findViewById(R.id.chklatent);
		chkpartograph = (CheckBox) dialog.findViewById(R.id.chkpartograph);
		chkreferral = (CheckBox) dialog.findViewById(R.id.chkreferral);
		chkpostpartum = (CheckBox) dialog.findViewById(R.id.chkpostpartum);
		chkdischarge = (CheckBox) dialog.findViewById(R.id.chkdischarge);
		Button btnpdfok = (Button) dialog.findViewById(R.id.btnpdfok);
		chkall = (CheckBox) dialog.findViewById(R.id.chkall);
		
		
		
		

		chkall.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(!Otheroption)
				{
				if (chkall.isChecked()) {
					chklatent.setChecked(true);
					chkpartograph.setChecked(true);
					chkreferral.setChecked(true);
					chkpostpartum.setChecked(true);
					chkdischarge.setChecked(true);
				} else {
					chklatent.setChecked(false);
					chkpartograph.setChecked(false);
					chkreferral.setChecked(false);
					chkpostpartum.setChecked(false);
					chkdischarge.setChecked(false);
				}
				}else
					Otheroption = false;

			}
		});

		chklatent.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (!(chklatent.isChecked()) && chkall.isChecked())
				{
					Otheroption = true;
					chkall.setChecked(false);
				}else
					Otheroption = false;
					

			}
		});

		chkpartograph.setOnCheckedChangeListener(new OnCheckedChangeListener() {

	@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (!(chkpartograph.isChecked()) && chkall.isChecked())
				{
					Otheroption = true;
					chkall.setChecked(false);
					}else
						Otheroption = false;

			}
		});

		chkreferral.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (!(chkreferral.isChecked()) && chkall.isChecked())
				{
					Otheroption = true;
					chkall.setChecked(false);
				}else
					Otheroption = false;

			}
		});

		chkpostpartum.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (!(chkpostpartum.isChecked()) && chkall.isChecked())
				{
					Otheroption = true;
					chkall.setChecked(false);
				}else
					Otheroption = false;
			}
		});

		chkdischarge.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (!(chkdischarge.isChecked()) && chkall.isChecked())
				{
					Otheroption = true;
					chkall.setChecked(false);
				}else
					Otheroption = false;

			}
		});
		
		Otheroption = false;

		btnpdfok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				dbh = Partograph_DB.getInstance(context);
				// pdfAlertDialog(context, woman, "all", null, null);
				try {

					// generateAllPdfFiles1(context, woman);
					if (chkpostpartum.isChecked() || chklatent.isChecked() || chkpartograph.isChecked()
							|| chkreferral.isChecked() || chkdischarge.isChecked()) {
						// generateAllPdfFiles(context, woman);
						pdfAlertDialog(context, woman, "all", null, null);
						dialog.cancel();
					} else
						Toast.makeText(context, context.getResources().getString(R.string.please_select_one_option),
								Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});

	}

	private static void generateAllPdfFiles(Context context, Women_Profile_Pojo woman) throws Exception {
		// TODO Auto-generated method stub
		// generatePostPartumCareDatainpdfFormat(context, woman);
		// // if (chklatent.isChecked())
		// generateLatentPhaseinpdfFormat(context, woman);

		/*
		 * if (chkpostpartum.isChecked())
		 * generatePostPartumCareDatainpdfFormat(context, woman); if
		 * (chklatent.isChecked()) generateLatentPhaseinpdfFormat(context,
		 * woman); if (chkpartograph.isChecked()) createPdfDocument(woman); if
		 * (chkreferral.isChecked()) generateReferralPdf(context, woman); if
		 * (chkdischarge.isChecked()) generateDischargeSlipinpdfFormat(context,
		 * woman); // generateReferralPdf(context, // woman);
		 */
		// if(chkpostpartum.isChecked() || chklatent.isChecked() || )
		// Toast.makeText(context, "Pdf File saved is saved",
		// Toast.LENGTH_LONG).show();

	}

	static void generateAllPdfFiles1a(Context context, Women_Profile_Pojo woman) throws Exception {

		// File a = new File(AppContext.mainDir +
		// "/PDF/PartographSheet/testin1.txt");
		// File b = new File(AppContext.mainDir +
		// "/PDF/PartographSheet/testin2.txt");
		// File c = new File(AppContext.mainDir +
		// "/PDF/PartographSheet/testout.txt");

		// FileOutputStream outputStream = new FileOutputStream(new
		// File(a.getAbsolutePath().toString()), true); // true
		// will
		// be
		// String content = "This is the text content"; // same
		// String content1 = "This is 2nd text content"; // same
		// byte[] contentInBytes = content.getBytes(); // as
		// byte[] contentInBytes1 = content1.getBytes(); // as
		//
		// outputStream.write(contentInBytes);
		// outputStream.close();
		//
		// FileOutputStream outputStream1 = new FileOutputStream(new
		// File(b.getAbsolutePath().toString()), true); // true
		// // will
		// // be
		// // same
		// // as
		// // Context.MODE_APPEND
		//
		// outputStream1.write(contentInBytes1);
		// outputStream1.close();

		createPdfDocument(woman);
		generateLatentPhaseinpdfFormat(context, woman);
		/*
		 * FileInputStream fin1 = new FileInputStream(partofile);
		 * FileInputStream fin2 = new FileInputStream(latentfile);
		 * FileOutputStream fout = new FileOutputStream(AppContext.mainDir +
		 * "/PDF/PartographSheet/testout1.pdf"); SequenceInputStream sis = new
		 * SequenceInputStream(fin1, fin2); int i; while ((i = sis.read()) !=
		 * -1) { fout.write(i); } sis.close(); fout.close(); fin1.close();
		 * fin2.close();
		 */

		File a = partofile;
		// a.createNewFile();

		// FileOutputStream outputStream = new FileOutputStream(new
		// File(a.getAbsolutePath().toString()), true); // true
		//
		// String content = "This is the text content"; // same
		// String content1 = "This is 2nd text content"; // same
		// byte[] contentInBytes = content.getBytes(); // as
		// byte[] contentInBytes1 = content1.getBytes(); // as
		//
		// outputStream.write(contentInBytes);
		// outputStream.close();

		File b = latentfile;
		// b.createNewFile();
		//
		// FileOutputStream outputStream1 = new FileOutputStream(new
		// File(b.getAbsolutePath().toString()), true); // true
		//
		// outputStream1.write(contentInBytes1);
		// outputStream1.close();

		InputStream is1 = new FileInputStream(partofile);
		InputStream is2 = new FileInputStream(latentfile);

		// InputStream is3 = new FileInputStream("c:/source/3.pdf");

		List<InputStream> list = new ArrayList();
		list.add(is1);
		list.add(is2);
		System.out.println("the number of streams are " + list.size());
		// list.add(is3);
		Merger merger = new Merger();
		merger.merg(list);

		is1.close();
		is2.close();

	}

	static void generateAllPdfFiles1(Context context, Women_Profile_Pojo woman) throws Exception {

		File a = new File(AppContext.mainDir + "/PDF/PartographSheet/testin1.txt");
		File b = new File(AppContext.mainDir + "/PDF/PartographSheet/testin2.txt");
		File c = new File(AppContext.mainDir + "/PDF/PartographSheet/testout.txt");

		FileOutputStream outputStream = new FileOutputStream(new File(a.getAbsolutePath().toString()), true); // true

		String content = "This is the text contentc";
		;
		for (int i = 0; i <= 1000; i++) {// be
			content = content + "This is the text contentc"; // same
		}
		String content1 = "This is 2nd text content"; // same
		byte[] contentInBytes = content.getBytes(); // as
		byte[] contentInBytes1 = content1.getBytes(); // as

		outputStream.write(contentInBytes);
		outputStream.close();

		FileOutputStream outputStream1 = new FileOutputStream(new File(b.getAbsolutePath().toString()), true);

		outputStream1.write(contentInBytes1);
		outputStream1.close();

		FileInputStream fin1 = new FileInputStream(AppContext.mainDir + "/PDF/PartographSheet/testin1.pdf");
		FileInputStream fin2 = new FileInputStream(AppContext.mainDir + "/PDF/PartographSheet/testin2.pdf");
		FileOutputStream fout = new FileOutputStream(AppContext.mainDir + "/PDF/PartographSheet/testout.pdf");
		SequenceInputStream sis = new SequenceInputStream(fin1, fin2);
		int i;
		while ((i = sis.read()) != -1) {
			fout.write(i);
		}
		sis.close();
		fout.close();
		fin1.close();
		fin2.close();

	}

	static void basic(Women_Profile_Pojo woman, Context context, File file, String Pdfheading) throws Exception {
		doc = new Document(PageSize.A4);

		// try {

		Partograph_DB dbh = Partograph_DB.getInstance(context);

		postpartumfile = file;
		FileOutputStream fOut = new FileOutputStream(postpartumfile);

		// PdfWriter.getInstance(doc, fOut);

		writer = PdfWriter.getInstance(doc, fOut);

		// Partograph_CommonClass event = new Partograph_CommonClass();

		// writer.setPageEvent(event);

		doc.setMarginMirroring(false);

		doc.setMargins(0, 10, 15, 0);

		doc.open();

		Drawable d = context.getResources().getDrawable(R.drawable.sentiagraph);

		BitmapDrawable bitDw = ((BitmapDrawable) d);

		Bitmap bmp = bitDw.getBitmap();

		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);

		Image image = Image.getInstance(stream.toByteArray());

		// image.setIndentationLeft(10);

		String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
				Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";

		PdfPTable ghea = new PdfPTable(1);

		PdfPCell cellcounsiousness = new PdfPCell();
		/*
		 * cellcounsiousness.setPhrase(new
		 * Phrase(context.getResources().getString(R.string.partograph) +
		 * "                                                                                "
		 * + strdatetime, small));
		 */
		cellcounsiousness
				.setPhrase(new Phrase(Pdfheading + "                                 " + strdatetime, heading));

		cellcounsiousness.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcounsiousness.setBorder(0);
		// cellcounsiousness.setPaddingTop(4);
		// cellcounsiousness.setPaddingRight(10);
		cellcounsiousness.setPaddingLeft(120);
		// cellcounsiousness.setPaddingBottom(4);
		PdfPCell cell = new PdfPCell(image, false);
		// cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setBorder(0);
		// cell.setPaddingTop(4);

		ghea.addCell(cellcounsiousness);
		// ghea.addCell(cell);

		ghea.completeRow();

		// Phrase headerText = new Phrase(cell);
		// HeaderFooter pdfHeader = new HeaderFooter(cell, false);
		// doc.setHeader(pdfHeader);

		doc.add(ghea);
		doc.add(image);

		/*
		 * Paragraph p_heading = new Paragraph(Pdfheading, heading);
		 * 
		 * p_heading.setAlignment(Paragraph.ALIGN_CENTER);
		 * 
		 * doc.add(p_heading);
		 * 
		 * Drawable d =
		 * context.getResources().getDrawable(R.drawable.sentiagraph);
		 * 
		 * BitmapDrawable bitDw = ((BitmapDrawable) d);
		 * 
		 * Bitmap bmp = bitDw.getBitmap();
		 * 
		 * ByteArrayOutputStream stream = new ByteArrayOutputStream();
		 * 
		 * bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);
		 * 
		 * Image image = Image.getInstance(stream.toByteArray());
		 * 
		 * image.setIndentationLeft(10); doc.add(image); String strdatetime =
		 * Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.
		 * getTodaysDate(), Partograph_CommonClass.defdateformat) + " " +
		 * Partograph_CommonClass.getCurrentTime() + " "; Paragraph p1 = new
		 * Paragraph(strdatetime, small);
		 * p1.setAlignment(Paragraph.ALIGN_RIGHT); doc.add(p1);
		 */

		womanBasicInfo(woman);

		/*
		 * Paragraph basicInfo = new Paragraph("Woman Basic Information",
		 * heading); basicInfo.setAlignment(Paragraph.ALIGN_LEFT);
		 * basicInfo.setIndentationLeft(30); doc.add(basicInfo);
		 */
		doc.add(womanbasics);
	}

	static void close() throws DocumentException {
		// PdfContentByte canvas = writer.getDirectContent();
		Rectangle rect = new Rectangle(10, 10, 585, 830);// (left,bottom,right,top)
		// Rectangle rect = doc.getPageSize();
		rect.enableBorderSide(1);
		rect.enableBorderSide(2);
		rect.enableBorderSide(4);
		rect.enableBorderSide(8);
		rect.setBorder(2);
		rect.setBorder(Rectangle.BOX);
		rect.setBorderWidth(1);
		rect.setUseVariableBorders(true);
		// canvas.rectangle(doc);

		// PdfContentByte canvas = writer.getDirectContent();
		// Rectangle rect = doc.getPageSize();
		//// rect.setBorder(Rectangle.BOX); // left, right, top, bottom border
		//// rect.setBorderWidth(5); // a width of 5 user units
		//// // rect.setBorderColor(harmony.java.awt.Color); // a red border
		//// rect.setUseVariableBorders(true); // the full width will be visible
		// canvas.rectangle(rect);
		// for (int i = 0; i < 3; i++) {

		Phrase footerText = new Phrase("www.sentiacare.com");
		HeaderFooter pdfFooter = new HeaderFooter(footerText, false);
		doc.setFooter(pdfFooter);

		PdfContentByte cb = writer.getDirectContent();

		Font fontfootere = new Font(Font.HELVETICA, 10, Font.NORMAL);
		//
		Phrase footer = new Phrase("www.sentiacare.com", fontfootere);

		ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footer, doc.right() - 40, doc.bottom() + 20, 0);
		doc.add(rect);

		// }

		doc.close();
	}

	static void discahrfgedata(Women_Profile_Pojo woman, Context context) throws NotFoundException, Exception {
		/*
		 * PdfPTable womanAddress = new PdfPTable(1);
		 * womanAddress.setWidthPercentage(80);
		 * womanAddress.setSpacingBefore(1); womanAddress.setSpacingAfter(1);
		 * womanAddress.addCell(getCellHeading(
		 * context.getResources().getString(R.string.address) + ": " +
		 * woman.getAddress(), PdfPCell.ALIGN_LEFT)); //
		 * womanAddress.addCell(getCell(dvalues.get(0).getDateTimeOfDischarge(),
		 * // PdfPCell.ALIGN_CENTER));
		 */
		ArrayList<DiscgargePojo> dvalues = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());

		// doc.add(womanAddress);

		if (woman.getDel_type() != 0) {
			Paragraph deliveryInfo = new Paragraph("Woman Delivery Information", heading);
			deliveryInfo.setAlignment(Paragraph.ALIGN_LEFT);
			deliveryInfo.setIndentationLeft(30);
			doc.add(deliveryInfo);
			DeliveryInformation(woman);

			childInformation(dvalues);

			doc.add(womandelInfo);

			Paragraph childInformation = new Paragraph("Child Information", heading);
			childInformation.setAlignment(Paragraph.ALIGN_LEFT);
			childInformation.setIndentationLeft(30);
			doc.add(childInformation);

			doc.add(childInfo);

			if (woman.getNo_of_child() == 2) {
				child2Information(dvalues);
				Paragraph child2Information = new Paragraph("Second Child Information", heading);
				child2Information.setAlignment(Paragraph.ALIGN_LEFT);
				child2Information.setIndentationLeft(30);
				doc.add(child2Information);
				doc.add(childInfo2);
			}

		}

		Paragraph dischargedetials = new Paragraph("Discharge Details", heading);
		dischargedetials.setAlignment(Paragraph.ALIGN_LEFT);
		dischargedetials.setIndentationLeft(30);
		doc.add(dischargedetials);

		// doc.add(dischargeInfo);
		//
		// doc.add(dischargeInfo2);

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(90);
		table.setSpacingBefore(10);
		// table.addCell(getCellHeading(getResources().getString(R.string.date_of_discharge),
		// PdfPCell.ALIGN_LEFT));
		// table.addCell(getCell(dvalues.get(0).getDateTimeOfDischarge(),
		// PdfPCell.ALIGN_CENTER));

		String date = "";
		if (dvalues.size() > 0) {
			date = Partograph_CommonClass.getConvertedDateFormat(dvalues.get(0).getDateTimeOfDischarge().split(" ")[0],
					"dd-MM-yyyy") + "/" + dvalues.get(0).getDateTimeOfDischarge().split(" ")[1];
		}

		PdfPCell cellTwo = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.date_of_discharge), PdfPCell.ALIGN_LEFT));

		PdfPCell cellOne = new PdfPCell(getCell(date, PdfPCell.ALIGN_CENTER));

		cellOne.setPadding(5);
		cellTwo.setPadding(5);
		cellTwo.setBorder(Rectangle.BOX);
		cellOne.setBorder(Rectangle.BOX);

		table.addCell(cellTwo);
		table.addCell(cellOne);

		doc.add(table);

		// reason for discharge
		PdfPTable reasonfordisc = new PdfPTable(2);
		reasonfordisc.setWidthPercentage(90);
		// reasonfordisc.setSpacingBefore(10);
		// table.addCell(getCellHeading(getResources().getString(R.string.date_of_discharge),
		// PdfPCell.ALIGN_LEFT));
		// table.addCell(getCell(dvalues.get(0).getDateTimeOfDischarge(),
		// PdfPCell.ALIGN_CENTER));

		//// String date = "";
		// if (dvalues.size() > 0) {
		// date = Partograph_CommonClass
		// .getConvertedDateFormat(dvalues.get(0).getDateTimeOfDischarge().split("
		//// ")[0], "dd-MM-yyyy")
		// + "/" + dvalues.get(0).getDateTimeOfDischarge().split(" ")[1];
		// }

		String discreason = "";
		if (dvalues.size() > 0)
			discreason = dvalues.get(0).getDischargeReasons();

		PdfPCell reasonfordisc1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.reason_for_discharge), PdfPCell.ALIGN_LEFT));

		PdfPCell reasonfordisc2 = new PdfPCell(getCell(discreason, PdfPCell.ALIGN_CENTER));

		reasonfordisc1.setPadding(5);
		reasonfordisc2.setPadding(5);
		reasonfordisc2.setBorder(Rectangle.BOX);
		reasonfordisc1.setBorder(Rectangle.BOX);

		reasonfordisc.addCell(reasonfordisc1);
		reasonfordisc.addCell(reasonfordisc2);

		doc.add(reasonfordisc);

		PdfPTable womanref = new PdfPTable(2);
		// womanInvestigation.setSpacingBefore(5);
		womanref.setWidthPercentage(90);
		// womanInvestigation.addCell(
		// getCellHeading(getResources().getString(R.string.woman_investigations),
		// PdfPCell.ALIGN_LEFT));
		// womanInvestigation.addCell(getCell(dvalues.get(0).getWomanInvestigation(),
		// PdfPCell.ALIGN_CENTER));

		String referred = "";

		if (dvalues.size() > 0) {

			if (dvalues.get(0).getReferred() == 1) {
				referred = "Yes";
			} else if (dvalues.get(0).getReferred() == 2)
				referred = "No";
			else
				referred = " ";

		}

		PdfPCell cellref1 = new PdfPCell(getCellHeading("Referred to Other Facility", PdfPCell.ALIGN_LEFT));

		PdfPCell cellref2 = new PdfPCell(getCell(referred, PdfPCell.ALIGN_CENTER));

		cellref1.setPadding(5);
		cellref2.setPadding(5);
		cellref1.setBorder(Rectangle.BOX);
		cellref2.setBorder(Rectangle.BOX);

		womanref.addCell(cellref1);
		womanref.addCell(cellref2);

		doc.add(womanref);

		PdfPTable womanInvestigation = new PdfPTable(2);
		// womanInvestigation.setSpacingBefore(5);
		womanInvestigation.setWidthPercentage(90);

		String investigation = "";

		if (dvalues.size() > 0) {

			investigation = dvalues.get(0).getWomanInvestigation();

		}

		PdfPCell cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.woman_investigations), PdfPCell.ALIGN_LEFT));

		PdfPCell cell2 = new PdfPCell(getCell(investigation, PdfPCell.ALIGN_CENTER));

		cell1.setPadding(5);
		cell2.setPadding(5);
		cell1.setBorder(Rectangle.BOX);
		cell2.setBorder(Rectangle.BOX);

		womanInvestigation.addCell(cell1);
		womanInvestigation.addCell(cell2);

		doc.add(womanInvestigation);

		/*
		 * PdfPTable childvestigation = new PdfPTable(2);
		 * childvestigation.setSpacingBefore(5);
		 * childvestigation.setWidthPercentage(90); childvestigation.addCell(
		 * getCellHeading(getResources().getString(R.string.
		 * child_investigation), PdfPCell.ALIGN_LEFT));
		 * childvestigation.addCell(getCell(dvalues.get(0).
		 * getChildInvestigation(), PdfPCell.ALIGN_CENTER));
		 * 
		 * doc.add(childvestigation);
		 */

		PdfPTable womanCondition = new PdfPTable(2);
		// womanCondition.setSpacingBefore(5);
		womanCondition.setWidthPercentage(90);
		// womanCondition.addCell(
		// getCellHeading(getResources().getString(R.string.condition_of_mother),
		// PdfPCell.ALIGN_LEFT));
		// womanCondition.addCell(getCell(dvalues.get(0).getConditionOfWoman(),
		// PdfPCell.ALIGN_CENTER));

		String womancondition = "";

		if (dvalues.size() > 0) {

			womancondition = dvalues.get(0).getConditionOfWoman();

		}

		PdfPCell womanConditioncell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.condition_of_mother), PdfPCell.ALIGN_LEFT));

		PdfPCell womanConditioncell2 = new PdfPCell(getCell(womancondition, PdfPCell.ALIGN_CENTER));

		womanConditioncell1.setPadding(5);
		womanConditioncell2.setPadding(5);
		womanConditioncell1.setBorder(Rectangle.BOX);
		womanConditioncell2.setBorder(Rectangle.BOX);

		womanCondition.addCell(womanConditioncell1);
		womanCondition.addCell(womanConditioncell2);

		doc.add(womanCondition);

		/*
		 * PdfPTable childCondition = new PdfPTable(2);
		 * childCondition.setSpacingBefore(5);
		 * childCondition.setWidthPercentage(90); childCondition.addCell(
		 * getCellHeading(getResources().getString(R.string.
		 * condition_of_child), PdfPCell.ALIGN_LEFT));
		 * childCondition.addCell(getCell(dvalues.get(0).getConditionOfChild (),
		 * PdfPCell.ALIGN_CENTER));
		 * 
		 * doc.add(childCondition);
		 */

		PdfPTable table10 = new PdfPTable(2);
		table10.setWidthPercentage(90);
		// table10.setSpacingBefore(5);
		// table10.addCell(
		// getCellHeading(getResources().getString(R.string.place_of_next_folow_up),
		// PdfPCell.ALIGN_LEFT));
		// table10.addCell(getCell(dvalues.get(0).getPlaceOfNextFOllowUp(),
		// PdfPCell.ALIGN_CENTER));
		String place = "";
		if (dvalues.size() > 0) {
			place = dvalues.get(0).getPlaceOfNextFOllowUp();

		}
		PdfPCell table10cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.place_of_next_folow_up), PdfPCell.ALIGN_LEFT));

		PdfPCell table10cell2 = new PdfPCell(getCell(place, PdfPCell.ALIGN_CENTER));

		table10cell1.setPadding(5);
		table10cell2.setPadding(5);
		table10cell1.setBorder(Rectangle.BOX);
		table10cell2.setBorder(Rectangle.BOX);

		table10.addCell(table10cell1);
		table10.addCell(table10cell2);
		doc.add(table10);

		PdfPTable table7 = new PdfPTable(2);
		table7.setWidthPercentage(90);
		// table7.setSpacingBefore(5);
		// table7.addCell(
		// getCellHeading(getResources().getString(R.string.date_of_next_folow_up),
		// PdfPCell.ALIGN_LEFT));
		// table7.addCell(getCell(dvalues.get(0).getDateOfNextFollowUp(),
		// PdfPCell.ALIGN_CENTER));
		String strdate = "";
		if (dvalues.size() > 0 && dvalues.get(0).getDateOfNextFollowUp() != null
				&& dvalues.get(0).getDateOfNextFollowUp().trim().length() > 0) {
			strdate = Partograph_CommonClass.getConvertedDateFormat(dvalues.get(0).getDateOfNextFollowUp(),
					"dd-MM-yyyy");
		}
		PdfPCell table7cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.date_of_next_folow_up), PdfPCell.ALIGN_LEFT));

		PdfPCell table7cell2 = new PdfPCell(getCell(strdate, PdfPCell.ALIGN_CENTER));

		table7cell1.setPadding(5);
		table7cell2.setPadding(5);
		table7cell1.setBorder(Rectangle.BOX);
		table7cell2.setBorder(Rectangle.BOX);

		table7.addCell(table7cell1);
		table7.addCell(table7cell2);
		doc.add(table7);

		PdfPTable table1 = new PdfPTable(2);
		table1.setWidthPercentage(90);
		// table1.setSpacingBefore(5);

		PdfPTable table9 = new PdfPTable(2);
		table9.setWidthPercentage(90);
		// table9.setSpacingBefore(5);
		// table9.addCell(getCellHeading(getResources().getString(R.string.advice_given),
		// PdfPCell.ALIGN_LEFT));
		// table9.addCell(getCell(dvalues.get(0).getAdviceGiven(),
		// PdfPCell.ALIGN_CENTER));
		String advice = "";

		PdfPCell table9cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.advice_given), PdfPCell.ALIGN_LEFT));

		if (dvalues.size() > 0) {
			advice = dvalues.get(0).getAdviceGiven();

		}
		PdfPCell table9cell2 = new PdfPCell(getCell(advice, PdfPCell.ALIGN_CENTER));

		table9cell1.setPadding(5);
		table9cell2.setPadding(5);
		table9cell1.setBorder(Rectangle.BOX);
		table9cell2.setBorder(Rectangle.BOX);

		table9.addCell(table9cell1);
		table9.addCell(table9cell2);
		doc.add(table9);

		// table1.addCell(getCellHeading(getResources().getString(R.string.procedures),
		// PdfPCell.ALIGN_LEFT));
		// table1.addCell(getCell(dvalues.get(0).getProcedures(),
		// PdfPCell.ALIGN_CENTER));
		String procdures = "";
		if (dvalues.size() > 0) {
			procdures = dvalues.get(0).getProcedures();

		}
		PdfPCell table1cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.procedures), PdfPCell.ALIGN_LEFT));

		PdfPCell table1cell2 = new PdfPCell(getCell(procdures, PdfPCell.ALIGN_CENTER));

		table1cell1.setPadding(5);
		table1cell2.setPadding(5);
		table1cell1.setBorder(Rectangle.BOX);
		table1cell2.setBorder(Rectangle.BOX);

		table1.addCell(table1cell1);
		table1.addCell(table1cell2);
		doc.add(table1);

		PdfPTable table11 = new PdfPTable(2);
		table11.setWidthPercentage(90);
		// table11.setSpacingBefore(5);
		// table11.addCell(
		// getCellHeading(getResources().getString(R.string.discharge_diagnosis),
		// PdfPCell.ALIGN_LEFT));
		// table11.addCell(getCell(dvalues.get(0).getDischargeDiagnosis(),
		// PdfPCell.ALIGN_CENTER));

		String diagnosis = "";
		if (dvalues.size() > 0) {
			diagnosis = dvalues.get(0).getDischargeDiagnosis();

		}

		PdfPCell table11cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.discharge_diagnosis), PdfPCell.ALIGN_LEFT));

		PdfPCell table11cell2 = new PdfPCell(getCell(diagnosis, PdfPCell.ALIGN_CENTER));

		table11cell1.setPadding(5);
		table11cell2.setPadding(5);
		table11cell1.setBorder(Rectangle.BOX);
		table11cell2.setBorder(Rectangle.BOX);

		table11.addCell(table11cell1);
		table11.addCell(table11cell2);
		doc.add(table11);

		PdfPTable table2 = new PdfPTable(2);
		// table2.setSpacingBefore(5);
		table2.setWidthPercentage(90);
		// table2.addCell(
		// getCellHeading(getResources().getString(R.string.complications_observed),
		// PdfPCell.ALIGN_LEFT));
		// table2.addCell(getCell(dvalues.get(0).getComplicationObserved(),
		// PdfPCell.ALIGN_CENTER));

		String compl = "";
		if (dvalues.size() > 0) {
			compl = dvalues.get(0).getComplicationObserved();
		}

		PdfPCell table2cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.complications_observed), PdfPCell.ALIGN_LEFT));

		PdfPCell table2cell2 = new PdfPCell(getCell(compl, PdfPCell.ALIGN_CENTER));

		table2cell1.setPadding(5);
		table2cell2.setPadding(5);
		table2cell1.setBorder(Rectangle.BOX);
		table2cell2.setBorder(Rectangle.BOX);

		table2.addCell(table2cell1);
		table2.addCell(table2cell2);
		doc.add(table2);

		PdfPTable table12 = new PdfPTable(2);
		// table12.setSpacingBefore(5);
		table12.setWidthPercentage(90);
		// table12.addCell(getCellHeading(getResources().getString(R.string.finding_history),
		// PdfPCell.ALIGN_LEFT));
		// table12.addCell(getCell(dvalues.get(0).getFindingHistorys(),
		// PdfPCell.ALIGN_CENTER));

		String history = "";
		if (dvalues.size() > 0) {
			history = dvalues.get(0).getFindingHistorys();

			// }
		}
		PdfPCell table12cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.finding_history), PdfPCell.ALIGN_LEFT));

		PdfPCell table12cell2 = new PdfPCell(getCell(history, PdfPCell.ALIGN_CENTER));

		table12cell1.setPadding(5);
		table12cell2.setPadding(5);
		table12cell1.setBorder(Rectangle.BOX);
		table12cell2.setBorder(Rectangle.BOX);

		table12.addCell(table12cell1);
		table12.addCell(table12cell2);
		doc.add(table12);

		PdfPTable table8 = new PdfPTable(2);
		// table8.setSpacingBefore(5);
		table8.setWidthPercentage(90);
		// table8.addCell(getCellHeading(getResources().getString(R.string.patient_disposition),
		// PdfPCell.ALIGN_LEFT));
		// table8.addCell(getCell(dvalues.get(0).getPatientDisposition(),
		// PdfPCell.ALIGN_CENTER));
		String dsposition = "";
		if (dvalues.size() > 0) {
			dsposition = dvalues.get(0).getPatientDisposition();
			// }
		}
		PdfPCell table8cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.patient_disposition), PdfPCell.ALIGN_LEFT));

		PdfPCell table8cell2 = new PdfPCell(getCell(dsposition, PdfPCell.ALIGN_CENTER));

		table8cell1.setPadding(5);
		table8cell2.setPadding(5);
		table8cell1.setBorder(Rectangle.BOX);
		table8cell2.setBorder(Rectangle.BOX);

		table8.addCell(table8cell1);
		table8.addCell(table8cell2);
		doc.add(table8);

		PdfPTable table13 = new PdfPTable(2);
		// table13.setSpacingBefore(5);
		table13.setWidthPercentage(90);
		// table13.addCell(getCellHeading(getResources().getString(R.string.admisiion_reasons),
		// PdfPCell.ALIGN_LEFT));
		// table13.addCell(getCell(dvalues.get(0).getAdmissionReasons(),
		// PdfPCell.ALIGN_CENTER));
		String reason = " ";
		if (dvalues.size() > 0) {
			reason = dvalues.get(0).getAdmissionReasons();
		}
		PdfPCell table13cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.admisiion_reasons), PdfPCell.ALIGN_LEFT));

		PdfPCell table13cell2 = new PdfPCell(getCell(reason, PdfPCell.ALIGN_CENTER));

		table13cell1.setPadding(5);
		table13cell2.setPadding(5);
		table13cell1.setBorder(Rectangle.BOX);
		table13cell2.setBorder(Rectangle.BOX);

		table13.addCell(table13cell1);
		table13.addCell(table13cell2);
		doc.add(table13);

		PdfPTable table3 = new PdfPTable(2);
		table3.setWidthPercentage(90);
		// table3.setSpacingBefore(5);
		// table3.addCell(getCellHeading(getResources().getString(R.string.overall_remarks),
		// PdfPCell.ALIGN_LEFT));
		// table3.addCell(getCell(dvalues.get(0).getRemarks(),
		// PdfPCell.ALIGN_CENTER));
		String remarks = " ";
		if (dvalues.size() > 0) {
			remarks = dvalues.get(0).getRemarks();

		}
		PdfPCell table3cell1 = new PdfPCell(
				getCellHeading(context.getResources().getString(R.string.overall_remarks), PdfPCell.ALIGN_LEFT));

		PdfPCell table3cell2 = new PdfPCell(getCell(remarks, PdfPCell.ALIGN_CENTER));

		table3cell1.setPadding(5);
		table3cell2.setPadding(5);
		table3cell1.setBorder(Rectangle.BOX);
		table3cell2.setBorder(Rectangle.BOX);

		table3.addCell(table3cell1);
		table3.addCell(table3cell2);
		doc.add(table3);

		PdfPTable nameofHC = new PdfPTable(2);
		nameofHC.setWidthPercentage(90);
		// nameofHC.setSpacingBefore(5);
		// nameofHC.addCell(getCellHeading(getResources().getString(R.string.name_of_healthcare_provider),
		// PdfPCell.ALIGN_LEFT));
		// nameofHC.addCell(getCell(dvalues.get(0).getHCProviderName(),
		// PdfPCell.ALIGN_CENTER));
		String name = " ";
		if (dvalues.size() > 0) {
			name = dvalues.get(0).getHCProviderName();

		}
		PdfPCell nameofHCcell1 = new PdfPCell(getCellHeading(
				context.getResources().getString(R.string.name_of_healthcare_provider), PdfPCell.ALIGN_LEFT));

		PdfPCell nameofHCcell2 = new PdfPCell(getCell(name, PdfPCell.ALIGN_CENTER));

		nameofHCcell1.setPadding(5);
		nameofHCcell2.setPadding(5);
		nameofHCcell1.setBorder(Rectangle.BOX);
		nameofHCcell2.setBorder(Rectangle.BOX);

		nameofHC.addCell(nameofHCcell1);
		nameofHC.addCell(nameofHCcell2);
		doc.add(nameofHC);

		PdfPTable designationofHC = new PdfPTable(2);
		designationofHC.setWidthPercentage(90);

		// designationofHC.setSpacingBefore(5);
		// designationofHC.addCell(getCellHeading(
		// getResources().getString(R.string.designation_of_healthcare_provider),
		// PdfPCell.ALIGN_LEFT));
		// designationofHC.addCell(getCell(dvalues.get(0).getDesignation(),
		// PdfPCell.ALIGN_CENTER));

		String desig = " ";
		if (dvalues.size() > 0) {
			desig = dvalues.get(0).getDesignation();

		}
		PdfPCell designationofHCcell1 = new PdfPCell(getCellHeading(
				context.getResources().getString(R.string.designation_of_healthcare_provider), PdfPCell.ALIGN_LEFT));

		PdfPCell designationofHCcell2 = new PdfPCell(getCell(desig, PdfPCell.ALIGN_CENTER));

		designationofHCcell1.setPadding(5);
		designationofHCcell2.setPadding(5);
		designationofHCcell1.setBorder(Rectangle.BOX);
		designationofHCcell2.setBorder(Rectangle.BOX);

		designationofHC.addCell(designationofHCcell1);
		designationofHC.addCell(designationofHCcell2);
		doc.add(designationofHC);

		PdfPTable signatureofHC = new PdfPTable(1);
		signatureofHC.setWidthPercentage(90);
		signatureofHC.setSpacingBefore(45);
		signatureofHC.addCell(getCellHeading(
				context.getResources().getString(R.string.signature_of_health_care_provider), PdfPCell.ALIGN_RIGHT));

		doc.add(signatureofHC);
	}

	static void generatepdf(Women_Profile_Pojo woman, Context context) throws NotFoundException, Exception {

		String path = AppContext.mainDir + "/PDF";

		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();

		Log.d("PDFCreator", "PDF Path: " + path);

		allfile = new File(dir, woman.getWomen_name() + ".pdf");

		basic(woman, context, allfile, context.getResources().getString(R.string.sentiagraph));

		if (chklatent.isChecked()) {
			Paragraph p = new Paragraph("Latent Phase");
			doc.add(p);
			dispalyLatentPhase(context, woman);
		}

		if (chkpartograph.isChecked()) {
			Paragraph p = new Paragraph("Partograph");
			doc.newPage();
			doc.add(p);
			partograpgdata(woman, context);
		}

		if (chkreferral.isChecked()) {
			Paragraph p = new Paragraph("Referral");
			doc.newPage();
			doc.add(p);
			referraldata(woman, context);
		}

		// postpartumdata(woman, context);

		if (chkpostpartum.isChecked()) {
			Paragraph p = new Paragraph("PostPartum Care");
			doc.newPage();
			doc.add(p);
			getPostPartumData(woman, context);
		}

		if (chkdischarge.isChecked()) {
			Paragraph p = new Paragraph("Discharge");
			doc.newPage();
			doc.add(p);
			discahrfgedata(woman, context);
		}

		PdfContentByte cb = writer.getDirectContent();

		Font fontfootere = new Font(Font.HELVETICA, 10, Font.NORMAL);
		//
		Phrase footer = new Phrase("www.sentiacare.com", fontfootere);

		ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footer, doc.right() - 40, doc.bottom() + 20, 0);

		doc.close();

	}

	static void getPostPartumData(Women_Profile_Pojo woman, Context context) throws NotFoundException, Exception {

		// TODO Auto-generated method stub

		/*
		 * PdfPTable womanAddress = new PdfPTable(1);
		 * womanAddress.setWidthPercentage(80);
		 * womanAddress.setSpacingBefore(1);
		 * womanAddress.addCell(getCellHeading(
		 * context.getResources().getString(R.string.address) + ": " +
		 * woman.getAddress(), PdfPCell.ALIGN_LEFT));
		 * 
		 * doc.add(womanAddress);
		 */

		ArrayList<PostPartum_Pojo> pvalues = new ArrayList<PostPartum_Pojo>();
		Cursor cur = dbh.getPostPartumData(woman.getUserId(), woman.getWomenId());
		// Cursor cur = PostPartumCare_Activity.cur;
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				PostPartum_Pojo postpojo = new PostPartum_Pojo();
				postpojo.setUserId(cur.getString(0));
				postpojo.setWomanId(cur.getString(1));
				postpojo.setWomanName(cur.getString(2));
				postpojo.setComplaints(cur.getString(3));
				postpojo.setOther(cur.getString(4));
				postpojo.setExamination(cur.getString(5));
				postpojo.setPallor(cur.getString(6));
				postpojo.setPulse(cur.getInt(7));
				postpojo.setBp(cur.getString(8));
				postpojo.setBreastExamination(cur.getString(9));
				postpojo.setInvolution(cur.getString(10));
				postpojo.setLochia(cur.getString(11));
				postpojo.setPerineal(cur.getString(12));
				postpojo.setAdvice(cur.getString(13));
				postpojo.setDateTimeOfEntry(cur.getString(15));
				pvalues.add(postpojo);

			} while (cur.moveToNext());

		}

		if (woman.getDel_type() != 0) {
			Paragraph deliveryInfo = new Paragraph("Woman Delivery Information", heading);
			deliveryInfo.setAlignment(Paragraph.ALIGN_LEFT);
			deliveryInfo.setIndentationLeft(30);
			doc.add(deliveryInfo);
			DeliveryInformation(woman);

			doc.add(womandelInfo);

		}

		ArrayList<String> postpartumheading = new ArrayList<String>();
		postpartumheading.add(context.getResources().getString(R.string.date) + " & "
				+ context.getResources().getString(R.string.time));
		postpartumheading.add(context.getResources().getString(R.string.presenting_complaints));
		postpartumheading.add(context.getResources().getString(R.string.examination));
		postpartumheading.add(context.getResources().getString(R.string.pallor));
		postpartumheading.add(context.getResources().getString(R.string.pulse));
		postpartumheading.add(context.getResources().getString(R.string.txtbpvalue));
		postpartumheading.add(context.getResources().getString(R.string.breast_examination));
		postpartumheading.add(context.getResources().getString(R.string.involution_of_uterus));
		postpartumheading.add(context.getResources().getString(R.string.lochia));
		postpartumheading.add(context.getResources().getString(R.string.perineal_care));
		postpartumheading.add(context.getResources().getString(R.string.advice));

		PdfPTable postpartumtable = new PdfPTable(11);
		postpartumtable.setSpacingBefore(20);
		postpartumtable.setWidthPercentage(90);
		postpartumtable.setWidths(new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });
		for (int k = 0; k < 11; k++) {
			String header = " ";
			if (k < postpartumheading.size()) {
				header = postpartumheading.get(k);
			}
			PdfPCell cell = new PdfPCell();
			// cell.setGrayFill(0.9f);
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPadding(8);
			postpartumtable.addCell(cell);
		}

		postpartumtable.completeRow();

		for (int i = 0; i < pvalues.size(); i++)

		{
			ArrayList<String> val = new ArrayList<String>();
			// if(pvalues.size()<i
			if (pvalues.size() > 0 && i < pvalues.size()) {
				// String bppost, pulsepost, lochia, involution,

				String date = Partograph_CommonClass
						.getConvertedDateFormat(pvalues.get(i).getDateTimeOfEntry().split(" ")[0], "dd-MM-yyyy") + "/"
						+ pvalues.get(i).getDateTimeOfEntry().split(" ")[1];
				val.add(date);

				String admResDisplay = "";
				if (pvalues.get(i).getComplaints() != null) {
					String[] admres = pvalues.get(i).getComplaints().split(",");
					String[] admResArr = null;

					admResArr = context.getResources().getStringArray(R.array.presenting_complaints);

					if (admResArr != null) {
						if (admres != null && admres.length > 0) {
							for (String str : admres) {
								if (str != null && str.length() > 0)
									admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + ",";
							}
						}
					}
				}

				if (pvalues.get(i).getComplaints() != null && pvalues.get(i).getComplaints().trim().length() > 0) {
					String other = "";
					other = pvalues.get(i).getOther();
					complaints = admResDisplay + " " + other;
					val.add(complaints);

				} else
					val.add("");

				if (pvalues.get(i).getExamination() != null && pvalues.get(i).getExamination().trim().length() > 0) {
					examination = pvalues.get(i).getExamination();
					val.add(examination);

				} else
					val.add("");

				if (pvalues.get(i).getPallor() != null && pvalues.get(i).getPallor().trim().length() > 0) {
					pallor = pvalues.get(i).getPallor();
					val.add(pallor);

				} else
					val.add("");

				if (pvalues.get(i).getPulse() > 0) {
					postpulse = "" + pvalues.get(i).getPulse();
					//
					val.add(postpulse);
				} else
					val.add("");

				if (pvalues.get(i).getBp() != null && pvalues.get(i).getBp().trim().length() > 0) {
					postbp = pvalues.get(i).getBp();
					val.add(postbp);
				} else
					val.add("");

				if (pvalues.get(i).getBreastExamination() != null
						&& pvalues.get(i).getBreastExamination().trim().length() > 0) {
					breastExamination = pvalues.get(i).getBreastExamination();
					val.add(breastExamination);
				} else
					val.add("");

				if (pvalues.get(i).getInvolution() != null && pvalues.get(i).getInvolution().trim().length() > 0) {
					involution = pvalues.get(i).getInvolution();
					val.add(involution);
				} else
					val.add("");

				if (pvalues.get(i).getLochia() != null && pvalues.get(i).getLochia().trim().length() > 0) {
					lochia = pvalues.get(i).getLochia();
					val.add(lochia);
				} else
					val.add("");

				if (pvalues.get(i).getPerineal() != null && pvalues.get(i).getPerineal().trim().length() > 0) {
					perinealCare = pvalues.get(i).getPerineal();
					val.add(perinealCare);

				} else
					val.add("");

				if (pvalues.get(i).getAdvice() != null && pvalues.get(i).getAdvice().trim().length() > 0) {
					postadvice = pvalues.get(i).getAdvice();
					val.add(postadvice);

				} else
					val.add("");
			}

			for (int x = 0; x < 11; x++) {
				String strVal = " ";

				PdfPCell cell_moulding = new PdfPCell();
				if (val.size() > 0) {
					strVal = val.get(x);
					cell_moulding.setPadding(5);
				} else
					cell_moulding.setPadding(20);

				cell_moulding.setPhrase(new Phrase(strVal, new Font(Font.HELVETICA, 10, Font.NORMAL)));
				cell_moulding.setHorizontalAlignment(Element.ALIGN_CENTER);

				postpartumtable.addCell(cell_moulding);
			}

			postpartumtable.setKeepTogether(true);

			postpartumtable.completeRow();
		}

		if (pvalues.size() <= 0) {
			for (int i = 0; i < 5; i++) {
				for (int x = 0; x < 11; x++) {
					String strVal = " ";

					PdfPCell cell_moulding = new PdfPCell();
					/*
					 * if (val.size() > 0) { strVal = val.get(x);
					 * cell_moulding.setPadding(5); } else
					 */
					cell_moulding.setPadding(20);

					cell_moulding.setPhrase(new Phrase(strVal, new Font(Font.HELVETICA, 10, Font.NORMAL)));
					cell_moulding.setHorizontalAlignment(Element.ALIGN_CENTER);

					postpartumtable.addCell(cell_moulding);
				}
			}
		}

		doc.add(postpartumtable);

	}

}

package com.bc.partograph.common;

import java.util.ArrayList;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

public class SendSMS{

	private int mMessageSentParts;
	private int mMessageSentTotalParts;

	String SENT = "SMS_SENT";
	String DELIVERED = "SMS_DELIVERED";
	private static MessageLogPojo msgObj = null;
	private Partograph_DB dbh = null;
	
	BroadcastReceiver sendingBroadcastReceiver;
	public static Context context = AppContext.context;
	public SendSMS() throws Exception {
		dbh = new Partograph_DB(context);
		checkAndSendMsg();
	}

	/** This method get first record from the MessageLog table which are not sent 
	 * @throws Exception */
	public void checkAndSendMsg() throws Exception{
		dbh = new Partograph_DB(context);
		msgObj = dbh.getFailedSmsOrNotSent();
		if(msgObj != null){
			initializeBroadCastReceivers(KillAllActivitesAndGoToLogin.context);
			KillAllActivitesAndGoToLogin.context.registerReceiver(sendingBroadcastReceiver, new IntentFilter(SENT));
			sendSMS();
		}
	}

	/** This method initiates the SMS to the particular Record */
	private void sendSMS() {
		
		final SmsManager sms = SmsManager.getDefault();
		
		/** This below line will divide the Message body to small parts */
		ArrayList<String> parts = sms.divideMessage(msgObj.getMsgBody());
		mMessageSentTotalParts = parts.size();

		/** Create the ArrayList of sent and delivery Intents */
		ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();

		/** Create the Pending Intent for sent and delivery */
		PendingIntent sentPI = PendingIntent.getBroadcast(KillAllActivitesAndGoToLogin.context, 0, new Intent(SENT), 0);
		PendingIntent deliveredPI = PendingIntent.getBroadcast(KillAllActivitesAndGoToLogin.context, 0, new Intent(DELIVERED), 0);

		/** Loop and Populate the arraylist of sent and delivery intents */
		for (int j = 0; j < mMessageSentTotalParts; j++) {
			sentIntents.add(sentPI);
			deliveryIntents.add(deliveredPI);
		}

		/** For each Record set this variable to 0 */
		mMessageSentParts = 0;
		
		
		/** Below code will check the Network is UP or not and send the SMS */
		if(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY){
			sms.sendMultipartTextMessage(msgObj.getPhoneNo(), null, parts, sentIntents, deliveryIntents);
		}
		
	}

	
	/** This method will Register the new BroadCastReceiver */
	private void initializeBroadCastReceivers(final Context context){

		sendingBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				try{
					
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						updateRecords(context, msgObj, true, "SMS sent");
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						updateRecords(context, msgObj, false, "Generic failure");
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						updateRecords(context, msgObj, false, "No service");
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						updateRecords(context, msgObj, false, "Null PDU");
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						updateRecords(context, msgObj, false, "Radio off");
						break;
					}		
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		};

		

	}
	
	
	/** This method will Check wheather all parts of the Message body of the Record is Sent or Not 
	 *  And Update the record as Sent or not */
	private void updateRecords(Context context, MessageLogPojo mlp, boolean isSent, String errorMsg) throws Exception{
		mMessageSentParts++;
		if ( mMessageSentParts == mMessageSentTotalParts ) {
//			Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
			dbh.updateMessageLog(msgObj, isSent);
			/** Below line will UnRegister the Current BroadCastReceiver 
			 *  If Not Unregistered, Records will not update properly */
			context.unregisterReceiver(sendingBroadcastReceiver);
			
			if(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)
				checkAndSendMsg();
		}
	}
	
}


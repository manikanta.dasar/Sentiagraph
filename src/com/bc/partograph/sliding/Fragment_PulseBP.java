package com.bc.partograph.sliding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_PulseBP extends Fragment implements OnClickListener {
	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	/*
	 * ArrayList<Women_Profile_Pojo> rowItems; int pos = -1;
	 */
	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	GraphicalView mChartView = null;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	ArrayList<Add_value_pojo> arrComments;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	// Changed on 27Mar2015
	String displaydateformat;
	Add_value_pojo valpojo;
	int bpsysval = 0, bpdiaval = 0, pulval = 0;
	String womenid;
	String userid;
	String lastdate;
	String lasttime;
	// update on 7july2016 by Arpitha
	public static boolean isnotify_pulse = false;

	String lastentry;
	// 04Oct2016 Arpitha
	TextView txtdialogtitle;
	Women_Profile_Pojo woman;// 03Nov2016

	String PulseRed, PulseAmber, PulseGreen, SysRed, SysAmber, SysGreen, DiaRed, DiaAmber, DiaGreen;// 12May2017
																									// Arpitha-
																									// v2.5

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_pulseandbp, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			womenid = woman.getWomenId();
			userid = woman.getUserId();
			initializeScreen(aq.id(R.id.rlpulseandbp).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

			setInputFiltersForEdittext();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);

		aq.id(R.id.etpulseandbptime).text(strcurrTime);
		aq.id(R.id.etpulseandbpdate).text(displaydateformat);

		aq.id(R.id.etpulseandbpdate).enabled(false);
		aq.id(R.id.etpulseandbptime).enabled(false);

		aq.id(R.id.rlpulseandbpchart).gone();

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluespulseandbp);
		tbl.setVisibility(View.GONE);

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha
		// Disable btnsave if the woman is delivered
		disableAddValues();

		obj_Type = getResources().getString(R.string.object7);
		obj_Id = dbh.getObjectID(obj_Type);

		// 12May2017 Arpitha - v2.6
		String[] prop_id = new String[] { "7.1", "7.2", "7.3" };
		for (int i = 0; i < prop_id.length; i++) {
			Cursor cur = dbh.getColorCodedValuesFromTable(prop_id[i]);
			if (cur != null && cur.getCount() > 0) {
				cur.moveToFirst();
				if (cur.getString(2).equalsIgnoreCase("7.1")) {
					PulseRed = cur.getString(4);
					PulseAmber = cur.getString(5);
					PulseGreen = cur.getString(6);
				} else if (cur.getString(2).equalsIgnoreCase("7.2")) {
					SysRed = cur.getString(4);
					SysAmber = cur.getString(5);
					SysGreen = cur.getString(6);
				} else {

					DiaRed = cur.getString(4);
					DiaAmber = cur.getString(5);
					DiaGreen = cur.getString(6);

				}
			}
		} // 12May2017 Arpitha - v_2.6

		loadListDataToDisplayValues();
		displayValues(tbl);
		displayComments();

		updatetblComments(strwuserId, strWomenid, obj_Id);
		aq.id(R.id.imgbtnnotification).invisible();
		lastdate = dbh.lastvaldate(strWomenid, obj_Id);
		lasttime = dbh.lastvaltime(strWomenid, obj_Id, lastdate);
		//08Oct2017 Arpitha - 
		if (SlidingActivity.ispulsetimevalid && !(SlidingActivity.arrVal.size()>0)) {
			aq.id(R.id.imgbtnnotification).visible();
		} else
			aq.id(R.id.imgbtnnotification).invisible();

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha

//		15Aug2017 Arpitha
		if(SlidingActivity.arrVal.size()>0)
		aq.id(R.id.txtnodata)
				.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));
		
		// strCurrentTime = todaysDate + "_"+
		// aq.id(R.id.etpulseandbptime).getText().toString();

		/*
		 * ArrayList<String> womenid = dbh.getwomenids(userid);
		 * 
		 * if(womenid.contains(strWomenid)) {
		 * 
		 * lastdate = dbh.lastvaldate(strWomenid,obj_Id); lasttime =
		 * dbh.lastvaltime(strWomenid, obj_Id, lastdate); // lasttime =
		 * lasttime+"00:05"; if(lastdate!=null && lasttime!=null) { strLastTime
		 * = lastdate+"_"+lasttime; isValidTime =
		 * Partograph_CommonClass.getisValidTime(strLastTime,strCurrentTime,30);
		 * if(isValidTime) { isnotify_pulse =true;
		 * aq.id(R.id.imgbtnnotification).visible(); }else {
		 * aq.id(R.id.imgbtnnotification).invisible(); } } }
		 */
		// if(AlaramReceiver.pulse>0)
		/*
		 * if(LoginActivity.wid_pulse!=null) {
		 * if((LoginActivity.wid_pulse.contains(womenid)) &&
		 * AlaramReceiver.pulse>0) { aq.id(R.id.imgbtnnotification).visible(); }
		 * else aq.id(R.id.imgbtnnotification).invisible(); }
		 */
	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.pbpcmt_cnt = 0;

		SlidingActivity.isMsgPBP = false; // 15Sep2016
	}

	// Display Comments
	private void displayComments() throws Exception {
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());

		}
		if (arrComments.size() > 0) {

			Parcelable state = aq.id(R.id.listCommentspbp).getListView().onSaveInstanceState();
			aq.id(R.id.listCommentspbp).visible();
			aq.id(R.id.txtCommentspbp).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_pulseandbp, arrComments);
			aq.id(R.id.listCommentspbp).adapter(adapter);

			// 18dec2015
			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentspbp).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentspbp).getListView());

		} else {
			aq.id(R.id.listCommentspbp).gone();
			aq.id(R.id.txtCommentspbp).gone();
		}
	}

	// add values layout visibility gone
	private void disableAddValues() throws Exception {
		// 23Sep2016 Arpitha - make invisible when woman is referred
		// if (rowItems.get(pos).getDel_type() != 0 ||
		// SlidingActivity.isReferred) {
		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
//		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
//		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2 || SlidingActivity.arrVal.size()>0) {
			aq.id(R.id.llpulseandbpaddval).gone();
			aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

		} else {

			aq.id(R.id.llpulseandbpaddval).visible();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	// OnClick
	@Override
	public void onClick(View v) {

		try {
			switch (v.getId()) {
			case R.id.btnsavepulseandbp:

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

					Date lastregdate = null;
					String strlastinserteddate = dbh.getlastmaxdate(strwuserId,
							getResources().getString(R.string.partograph));
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (strlastinserteddate != null) {
						lastregdate = format.parse(strlastinserteddate);
					}
					Date currentdatetime = new Date();
					if (lastregdate != null && currentdatetime.before(lastregdate) && (!LoginActivity.istesting)) {
						Partograph_CommonClass.showSettingsAlert(getActivity(),
								getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
					} else {// 27Sep2016 Arpitha

						// 12jan2016
						strCurrentTime = todaysDate + "_" + aq.id(R.id.etpulseandbptime).getText().toString();

						isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

						// 26jan2016
						if (listValuesItems.size() <= 0) {
							isValidTime = true;
						}

						if (isValidTime) {
							if (validatePulseandBP()) {

								valpojo = new Add_value_pojo();
								valpojo.setStrPulseval(aq.id(R.id.etpulseval).getText().toString());
								valpojo.setStrBPsystolicval(aq.id(R.id.etbpsystolic).getText().toString());
								valpojo.setStrBPdiastolicval(aq.id(R.id.etbpdiastolic).getText().toString());
								valpojo.setPulsecomments(aq.id(R.id.etpulsecomments).getText().toString());
								valpojo.setStrTime(aq.id(R.id.etpulseandbptime).getText().toString());
								valpojo.setStrdate(todaysDate);
								valpojo.setObj_Id(obj_Id);
								valpojo.setObj_Type(obj_Type);

								/* int bpsysval = 0,bpdiaval = 0,pulval = 0; */

								if (aq.id(R.id.etpulseval).getText().toString().length() > 0) {
									pulval = Integer.parseInt(aq.id(R.id.etpulseval).getText().toString());
								}

								if (aq.id(R.id.etbpsystolic).getText().toString().length() > 0
										&& aq.id(R.id.etbpdiastolic).getText().toString().length() > 0) {
									bpsysval = Integer.parseInt(aq.id(R.id.etbpsystolic).getText().toString());
									bpdiaval = Integer.parseInt(aq.id(R.id.etbpdiastolic).getText().toString());
								}

								// change made on 30-03-2015

								if (pulval > 0 && (pulval > 120 || pulval < 50)) {
									valpojo.setIsDanger(1);
								} else
									valpojo.setIsDanger(0);

								if (bpsysval > 0) {
									if (bpsysval < 80 || bpsysval > 160) {
										valpojo.setIsDangerval2(1);
									}
								} else
									valpojo.setIsDangerval2(0);

								// 12Jun2017 Arpitha
								if (bpdiaval > 0) {

									if (bpdiaval < 50 || bpdiaval > 110) {
										valpojo.setIsDangerval3(1);
									}

								} else
									valpojo.setIsDangerval3(0);// 12Jun2017
																// Arpitha

								// if((validate()))
								if (!((pulval > 120 || pulval < 50) || (bpsysval < 80 || bpsysval > 160)
										|| (bpdiaval < 50 || bpdiaval > 110))) {
									dbh.db.beginTransaction();
									int transId = dbh.iCreateNewTrans(strwuserId);

									boolean isAdded = dbh.insertPulseBPVal(valpojo, strWomenid, strwuserId, transId);

									if (isAdded) {

										dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
										dbh.updatenotification(strWomenid, obj_Id);// 18Oct2016
																					// Arpitha
										loadListDataToDisplayValues();
										displayValues(tbl);
										commitTrans();
									} else {
										rollbackTrans();
									}

									aq.id(R.id.etpulseval).text("");
									aq.id(R.id.etbpsystolic).text("");
									aq.id(R.id.etbpdiastolic).text("");

									aq.id(R.id.etpulsecomments).text("");

									aq.id(R.id.etpulseval).getEditText().requestFocus();
								} else {
									displayConfirmationAlert("", "confirmation");
								}
							}

						} else {
							String newTime = Partograph_CommonClass.getnewtime(lastentry, 30);
							String newtime_12hrs = Partograph_CommonClass.gettimein12hrformat(newTime);
							aq.id(R.id.etpulseval).text("");
							aq.id(R.id.etbpsystolic).text("");
							aq.id(R.id.etbpdiastolic).text("");

							aq.id(R.id.etpulsecomments).text("");
							// Toast.makeText(getActivity(),
							// getResources().getString(R.string.pbpcontractvalidtime),
							// Toast.LENGTH_LONG).show();
							displayAlertDialog(getResources().getString(R.string.fhr_duration) + " " + newtime_12hrs
									+ " " + getResources().getString(R.string.on));
						}
					}
				}
				break;

			case R.id.btnviewgraphpulseandbp: {

				loadListDataToDisplayValues();
				if (listValuesItems.size() > 0)
					showGraphDialog();
				else
					displayAlertDialog(getResources().getString(R.string.nodata));
				/*
				 * Toast.makeText(getActivity(),
				 * getResources().getString(R.string.nodata), Toast.LENGTH_LONG)
				 * .show();
				 */

			}
				break;

			case R.id.pulseinfo: {
				displayPulseBPinfo();
			}
				break;

			case R.id.imgbtnnotification: {
				notification(womenid);
			}
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void showGraphDialog() {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(getActivity());
			dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			dialog.setContentView(R.layout.dialog_pulseandbp);

			dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			dialog.getWindow().setBackgroundDrawableResource(R.color.white);

			TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setText(getResources().getString(R.string.pulse_bp));

			ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						dialog.cancel();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			createChartPulseandBP(dialog);
			TableLayout tbl = (TableLayout) dialog.findViewById(R.id.tblvaluespulsebpdialog);
			displayValues(tbl);

			// Dialog is dismissed
			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
				}
			});

			// displayChartValues(tbl);
			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Display Chart vlaues
	private void displayChartValues(TableLayout tbl) {
		if (listValuesItems.size() > 0) {

			for (int i = 0; i < listValuesItems.size(); i++) {

				View tableRow = LayoutInflater.from(getActivity()).inflate(R.layout.table_item, null, false);

				TextView txtTime = (TextView) tableRow.findViewById(R.id.txttime);
				TextView txtDate = (TextView) tableRow.findViewById(R.id.txtdate);
				TextView txtPulse = (TextView) tableRow.findViewById(R.id.txtPulse);
				TextView txtColorPulse = (TextView) tableRow.findViewById(R.id.txtcolorPulse);
				TextView txtBp = (TextView) tableRow.findViewById(R.id.txtBP);
				TextView txtColorBP = (TextView) tableRow.findViewById(R.id.txtcolorBP);

				txtTime.setText(listValuesItems.get(i).getStrTime());
				txtDate.setText(listValuesItems.get(i).getStrdate());
				txtPulse.setText(listValuesItems.get(i).getStrPulseval());

				if (listValuesItems.get(i).getStrPulseval() != null
						&& !listValuesItems.get(i).getStrPulseval().isEmpty()) {
					int value = Integer.parseInt(listValuesItems.get(i).getStrPulseval());
					if (value >= 50 && value <= 60 || (value >= 100 && value <= 110)) {
						txtColorPulse.setBackgroundColor(getResources().getColor(R.color.amber));
					} else if (value > 110 || value < 50) {
						txtColorPulse.setBackgroundColor(getResources().getColor(R.color.red));
					} else {
						txtColorPulse.setBackgroundColor(getResources().getColor(R.color.green));
					}

				}

				String str1 = listValuesItems.get(i).getStrBPsystolicval() == null ? " "
						: listValuesItems.get(i).getStrBPsystolicval();
				String str2 = listValuesItems.get(i).getStrBPdiastolicval() == null ? " "
						: listValuesItems.get(i).getStrBPdiastolicval();

				String val2 = str1 + "/" + str2;

				txtBp.setText(val2);

				if (listValuesItems.get(i).getStrBPsystolicval() != null
						&& listValuesItems.get(i).getStrBPdiastolicval() != null) {
					int valueSystolic = Integer.parseInt(listValuesItems.get(i).getStrBPsystolicval());
					int valueDiastolic = Integer.parseInt(listValuesItems.get(i).getStrBPdiastolicval());

					if (valueSystolic <= 120 && valueDiastolic <= 80) {
						txtColorBP.setBackgroundColor(getResources().getColor(R.color.green));
					}
					if (valueSystolic >= 140 || valueSystolic <= 159 || valueDiastolic >= 90 || valueDiastolic <= 99) {
						txtColorBP.setBackgroundColor(getResources().getColor(R.color.amber));
					}

					if (valueSystolic >= 160 || valueDiastolic >= 100) {
						txtColorBP.setBackgroundColor(getResources().getColor(R.color.red));
					}

				}

				tbl.addView(tableRow);

			}
		}
	}

	private void createChartPulseandBP(Dialog dialog) throws Exception {
		// loadListDataToDisplayValues();
		listValuesItems = dbh.getPulseBPData(obj_Id, strWomenid, strwuserId, true);
		if (listValuesItems.size() > 0) {
			// Creating an XYSeries for values
			XYSeries pulseseries = new XYSeries(getResources().getString(R.string.pulse));
			XYSeries bpsystolicseries = new XYSeries(getResources().getString(R.string.systolic));
			XYSeries bpdiastolicseries = new XYSeries(getResources().getString(R.string.diastolic));

			for (int i = 0; i < listValuesItems.size(); i++) {

				if (listValuesItems.get(i).getStrPulseval().length() > 0
						&& !listValuesItems.get(i).getStrPulseval().isEmpty()) {
					pulseseries.add((i + 1), Double.parseDouble(listValuesItems.get(i).getStrPulseval()));
				} else {
					pulseseries.add((i + 1), 0);
				}

				if (listValuesItems.get(i).getStrBPsystolicval().length() > 0
						&& listValuesItems.get(i).getStrBPdiastolicval().length() > 0) {
					bpsystolicseries.add((i + 1), Double.parseDouble(listValuesItems.get(i).getStrBPsystolicval()));
					bpdiastolicseries.add((i + 1), Double.parseDouble(listValuesItems.get(i).getStrBPdiastolicval()));
				} else {
					bpsystolicseries.add((i + 1), 0);
					bpdiastolicseries.add((i + 1), 0);
				}

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset

			dataset.addSeries(bpsystolicseries);
			dataset.addSeries(bpdiastolicseries);
			dataset.addSeries(pulseseries);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer bpsystolicRenderer = new XYSeriesRenderer();
			bpsystolicRenderer.setColor(getResources().getColor(R.color.blue));
			bpsystolicRenderer.setPointStyle(PointStyle.CIRCLE);
			bpsystolicRenderer.setFillPoints(true);
			bpsystolicRenderer.setLineWidth(5);
			bpsystolicRenderer.setDisplayChartValues(true);
			bpsystolicRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer bpdiastolicRenderer = new XYSeriesRenderer();
			bpdiastolicRenderer.setColor(getResources().getColor(R.color.yellow));
			bpdiastolicRenderer.setPointStyle(PointStyle.CIRCLE);
			bpdiastolicRenderer.setFillPoints(true);
			bpdiastolicRenderer.setLineWidth(5);
			bpdiastolicRenderer.setDisplayChartValues(true);
			bpdiastolicRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize pulseSeries
			XYSeriesRenderer pulseRenderer = new XYSeriesRenderer();
			pulseRenderer.setColor(getResources().getColor(R.color.green));
			pulseRenderer.setPointStyle(PointStyle.CIRCLE);
			pulseRenderer.setFillPoints(true);
			pulseRenderer.setLineWidth(3);
			pulseRenderer.setDisplayChartValues(true);
			pulseRenderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle(getResources().getString(R.string.partograph));
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(getResources().getString(R.string.pulse_bp));
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 60, 80, 60, 60 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(25);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			multiRenderer.setPanEnabled(true, true);
			multiRenderer.setYAxisMax(200);
			multiRenderer.setYAxisMin(40);
			multiRenderer.setYLabels(15);
			// multiRenderer.setYAxisMin(40);
			multiRenderer.setBarSpacing(10);
			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);
			multiRenderer.setXLabelsAngle(45); // 18jan2016

			multiRenderer.setZoomButtonsVisible(true);
			for (int i = 0; i < listValuesItems.size(); i++) {
				multiRenderer.addXTextLabel((i + 1), listValuesItems.get(i).getStrTime());
				// 05jan2016
				multiRenderer.setYAxisMax(300);
				multiRenderer.setYLabels(15);

				// multiRenderer.addXTextLabel(i,
				// (i+1)+"("+listValuesItems.get(i)
				// .getStrTime() + ")");
			}

			multiRenderer.addSeriesRenderer(bpsystolicRenderer);
			multiRenderer.addSeriesRenderer(bpdiastolicRenderer);
			multiRenderer.addSeriesRenderer(pulseRenderer);

			mChartView = null;

			String[] types = new String[] { BarChart.TYPE, BarChart.TYPE, LineChart.TYPE };

			mChartView = ChartFactory.getCombinedXYChartView(getActivity(), dataset, multiRenderer, types);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 400);
			mChartView.setLayoutParams(params);

			RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.rlpulseandbpchart);
			layout.addView(mChartView);

			if (mChartView != null) {
				mChartView.repaint();
			}
		}
	}

	/** Method to rollback the transaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// dbh.db.close();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());

		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
		aq.id(R.id.imgbtnnotification).gone();
		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	public void doWork() throws Exception {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.etpulseandbptime).text(curTime);
						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	private void loadListDataToDisplayValues() throws Exception {
		listValuesItems = new ArrayList<Add_value_pojo>();

		listValuesItems = dbh.getPulseBPData(obj_Id, strWomenid, strwuserId, false);
		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 28Sep2016 Arpitha
		} else {
			displayValues(tbl);
			tbl.setVisibility(View.VISIBLE);
		}
	}

	private void displayValues(TableLayout tbl) throws Exception {

		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			// TableRow trlabel = (TableRow) findViewById(R.id.tableRow1);
			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval1lbl);

			TextView textColorPulse = new TextView(getActivity());
			textColorPulse.setTextSize(18);
			textColorPulse.setPadding(10, 10, 10, 10);
			textColorPulse.setTextColor(getResources().getColor(R.color.black));
			textColorPulse.setTextSize(18);
			textColorPulse.setGravity(Gravity.CENTER);
			trlabel.addView(textColorPulse);

			TextView textval2lbl = new TextView(getActivity());
			textval2lbl.setTextSize(18);
			textval2lbl.setPadding(10, 10, 10, 10);
			textval2lbl.setTextColor(getResources().getColor(R.color.black));
			textval2lbl.setTextSize(18);
			textval2lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval2lbl);

			TextView textColorBP = new TextView(getActivity());
			textColorBP.setTextSize(18);
			textColorBP.setPadding(10, 10, 10, 10);
			textColorBP.setTextColor(getResources().getColor(R.color.black));
			textColorBP.setTextSize(18);
			textColorBP.setGravity(Gravity.CENTER);
			trlabel.addView(textColorBP);

			TextView textcomment = new TextView(getActivity());
			textcomment.setTextSize(18);
			textcomment.setText(getResources().getString(R.string.comments));
			textcomment.setPadding(10, 10, 10, 10);
			textcomment.setTextColor(getResources().getColor(R.color.black));
			textcomment.setTextSize(18);
			textcomment.setGravity(Gravity.LEFT);
			trlabel.addView(textcomment);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));
			textColorBP.setText(getResources().getString(R.string.color));
			textColorPulse.setText(getResources().getString(R.string.color));

			textColorBP.setVisibility(View.GONE);
			textColorPulse.setVisibility(View.GONE);

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);

				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				// txtdate.setText(listValuesItems.get(i).getStrdate());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
					lastentry = listValuesItems.get(i).getStrTime();
				}
				String val1 = "", val2 = "", val3 = "", val4 = "";

				val1 = listValuesItems.get(i).getStrPulseval();
				String str1 = listValuesItems.get(i).getStrBPsystolicval() == null ? " "
						: listValuesItems.get(i).getStrBPsystolicval();
				String str2 = listValuesItems.get(i).getStrBPdiastolicval() == null ? " "
						: listValuesItems.get(i).getStrBPdiastolicval();

				if (str1.length() > 0 && str2.length() > 0)
					val2 = str1 + "/" + str2;

				val4 = listValuesItems.get(i).getPulsecomments();

				textval1lbl.setText(getResources().getString(R.string.txtpulsevalue));
				textval2lbl.setText(getResources().getString(R.string.txtbpvalue));

				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);
				tr.addView(txtvalue1);

				textColorBP.setVisibility(View.VISIBLE);
				textColorPulse.setVisibility(View.VISIBLE);

				TextView txtColorPulse = new TextView(getActivity());
				txtColorPulse.setWidth(10);
				txtColorPulse.setHeight(10);
				txtColorPulse.setTextSize(16);
				txtColorPulse.setGravity(Gravity.CENTER_HORIZONTAL);
				txtColorPulse.setText("");

				if (listValuesItems.get(i).getStrPulseval().length() > 0
						&& !listValuesItems.get(i).getStrPulseval().isEmpty()) {
					int value = Integer.parseInt(listValuesItems.get(i).getStrPulseval());
					if (value >= 50 && value <= 60 || (value >= 100 && value <= 120)) {
						txtColorPulse.setBackgroundColor(getResources().getColor(R.color.amber));
						// btnPulse.setBackgroundColor(getResources().getColor(R.color.amber));
					} else if (value > 120 || value < 50) {
						txtColorPulse.setBackgroundColor(getResources().getColor(R.color.red));
						// btnPulse.setBackgroundColor(getResources().getColor(R.color.red));
					} else {
						txtColorPulse.setBackgroundColor(getResources().getColor(R.color.green));
						// btnPulse.setBackgroundColor(getResources().getColor(R.color.green));
					}

					// tr.addView(btnPulse);
				} else {
					txtColorPulse.setWidth(10);
					txtColorPulse.setHeight(10);
					txtColorPulse.setTextSize(16);
					txtColorPulse.setGravity(Gravity.CENTER_HORIZONTAL);
					txtColorPulse.setText("");
				}

				tr.addView(txtColorPulse);

				TextView txtvalue2 = new TextView(getActivity());
				txtvalue2.setText(val2);
				txtvalue2.setTextSize(18);
				txtvalue2.setPadding(10, 10, 10, 10);
				txtvalue2.setTextColor(getResources().getColor(R.color.black));
				txtvalue2.setTextSize(18);
				txtvalue2.setGravity(Gravity.CENTER);
				tr.addView(txtvalue2);

				TextView txtColorBP = new TextView(getActivity());
				txtColorBP.setWidth(10);
				txtColorBP.setHeight(10);
				txtColorBP.setTextSize(16);
				txtColorBP.setGravity(Gravity.CENTER_HORIZONTAL);
				txtColorBP.setText("");

				if (listValuesItems.get(i).getStrBPsystolicval().length() > 0
						&& listValuesItems.get(i).getStrBPdiastolicval().length() > 0) {
					int valueSystolic = Integer.parseInt(listValuesItems.get(i).getStrBPsystolicval());
					int valueDiastolic = Integer.parseInt(listValuesItems.get(i).getStrBPdiastolicval());

					// change made on 4 nov
					if ((valueSystolic < 80 || valueSystolic > 160) || (valueDiastolic < 50 || valueDiastolic >= 110)) {
						txtColorBP.setBackgroundColor(getResources().getColor(R.color.red));
					} else if (((valueSystolic >= 80 && valueSystolic < 100)
							|| (valueSystolic >= 140 && valueSystolic <= 160))
							|| ((valueDiastolic >= 50 && valueDiastolic < 60)
									|| (valueDiastolic > 100 && valueDiastolic < 110))) {
						txtColorBP.setBackgroundColor(getResources().getColor(R.color.amber));
					} else if ((valueSystolic >= 90 && valueSystolic <= 140)
							|| (valueDiastolic >= 50 && valueDiastolic <= 90)) {
						txtColorBP.setBackgroundColor(getResources().getColor(R.color.green));
					}

				} else {
					txtColorBP.setWidth(10);
					txtColorBP.setHeight(10);
					txtColorBP.setTextSize(16);
					txtColorBP.setGravity(Gravity.CENTER_HORIZONTAL);
					txtColorBP.setText("");
				}

				tr.addView(txtColorBP);

				TextView txtcommentval = new TextView(getActivity());
				txtcommentval.setText(val4);
				txtcommentval.setTextSize(18);
				txtcommentval.setPadding(10, 10, 10, 10);
				txtcommentval.setTextColor(getResources().getColor(R.color.black));
				txtcommentval.setTextSize(18);
				txtcommentval.setGravity(Gravity.CENTER);
				tr.addView(txtcommentval);

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.BLACK);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}

	}

	/**
	 * validate Pulse and BP - 12jan2016
	 * 
	 * @return
	 */
	private boolean validatePulseandBP() throws Exception {
		if ((aq.id(R.id.etpulseval).getEditText().getText().toString().trim().length() <= 0)
				&& (aq.id(R.id.etbpsystolic).getEditText().getText().toString().trim().length() <= 0
						&& aq.id(R.id.etbpdiastolic).getEditText().getText().toString().trim().length() <= 0)) {
			// Toast.makeText(getActivity(),
			// getResources().getString(R.string.enter_val),
			// Toast.LENGTH_LONG).show();
			displayAlertDialog(getResources().getString(R.string.enter_pulse_bpsys_bpdia_val));// 12April2017
																								// Arpitha
			aq.id(R.id.etpulseval).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etbpdiastolic).getEditText().getText().toString().trim().length() > 0) {
			if (aq.id(R.id.etbpsystolic).getEditText().getText().toString().trim().length() <= 0) {
				// Toast.makeText(getActivity(),
				// getResources().getString(R.string.enterbpsystolicval),
				// Toast.LENGTH_LONG).show();
				displayAlertDialog(getResources().getString(R.string.enterbpsystolicval));
				aq.id(R.id.etbpsystolic).getEditText().requestFocus();
				return false;
			}
		}

		if (aq.id(R.id.etbpsystolic).getEditText().getText().toString().trim().length() > 0) {
			if (aq.id(R.id.etbpdiastolic).getEditText().getText().toString().trim().length() <= 0) {
				// Toast.makeText(getActivity(),
				// getResources().getString(R.string.enterbpdiastolicval),
				// Toast.LENGTH_LONG).show();
				displayAlertDialog(getResources().getString(R.string.enterbpdiastolicval));
				aq.id(R.id.etbpdiastolic).getEditText().requestFocus();
				return false;
			}
		}

		if (aq.id(R.id.etpulseval).getEditText().getText().toString().trim().length() <= 0) {
			// Toast.makeText(getActivity(), "Enter Pulse Value and Try
			// Again!!", Toast.LENGTH_LONG).show();
			displayAlertDialog(getResources().getString(R.string.entr_pulse_val));
			aq.id(R.id.etpulseval).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etbpsystolic).getEditText().getText().toString().trim().length() <= 0) {
			// Toast.makeText(getActivity(), "Enter Systolic Value and Try
			// Again!!", Toast.LENGTH_LONG).show();
			displayAlertDialog(getResources().getString(R.string.entr_systolic));
			aq.id(R.id.etbpsystolic).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etbpdiastolic).getEditText().getText().toString().trim().length() <= 0) {
			// Toast.makeText(getActivity(), "Enter Diastolic Value and Try
			// Again!!", Toast.LENGTH_LONG).show();
			displayAlertDialog(getResources().getString(R.string.entr_diastolic));
			aq.id(R.id.etbpdiastolic).getEditText().requestFocus();
			return false;
		}

		/*
		 * if (aq.id(R.id.etbpdiastolic).getEditText().getText().toString()
		 * .trim().length() > 0) { if
		 * (aq.id(R.id.etbpsystolic).getEditText().getText().toString()
		 * .trim().length() <= 0) { Toast.makeText(getActivity(),
		 * getResources().getString(R.string.enterbpsystolicval),
		 * Toast.LENGTH_LONG).show();
		 * aq.id(R.id.etbpsystolic).getEditText().requestFocus(); return false;
		 * } }
		 * 
		 * if (aq.id(R.id.etbpsystolic).getEditText().getText().toString()
		 * .trim().length() > 0) { if
		 * (aq.id(R.id.etbpdiastolic).getEditText().getText().toString()
		 * .trim().length() <= 0) { Toast.makeText(getActivity(),
		 * getResources().getString(R.string.enterbpdiastolicval),
		 * Toast.LENGTH_LONG).show();
		 * aq.id(R.id.etbpdiastolic).getEditText().requestFocus(); return false;
		 * } }
		 */
		int pulse = 0;
		int bp_sys = 0;
		int bp_dia = 0;

		if (aq.id(R.id.etpulseval).getText().length() > 0) {
			pulse = Integer.parseInt(aq.id(R.id.etpulseval).getEditText().getText().toString());
		}
		if (aq.id(R.id.etbpsystolic).getText().length() > 0) {
			bp_sys = Integer.parseInt(aq.id(R.id.etbpsystolic).getEditText().getText().toString());
		}
		if (aq.id(R.id.etbpdiastolic).getText().length() > 0) {
			bp_dia = Integer.parseInt(aq.id(R.id.etbpdiastolic).getEditText().getText().toString());
		}
		if (pulse == 0 || bp_sys == 0 || bp_dia == 0) {
			// Toast.makeText(getActivity(), "Invalid Value",
			// Toast.LENGTH_LONG).show();
			displayAlertDialog(getResources().getString(R.string.invalidval_val));
			return false;
		}

		return true;
	}

	public void displayPulseBPinfo() {
		final Dialog dialog = new Dialog(getActivity());
		// 9Sep2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.pulsebp_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.pulse_bp));

		TextView txtredvalpulse = (TextView) dialog.findViewById(R.id.greenval2);// 11may2017
																					// Arpitha
																					// -
																					// v2.6
		txtredvalpulse.setText(PulseRed);// 11may2017 Arpitha- v2.6
		TextView txtgreenvalpulse = (TextView) dialog.findViewById(R.id.greenval4);// 11may2017
		// Arpitha-
		// v2.6
		txtgreenvalpulse.setText(PulseGreen);// 11may2017 Arpitha- v2.6 TextView
		TextView txtambervalpulse = (TextView) dialog.findViewById(R.id.amberval9);// 11may2017
		// Arpitha-
		// v2.6
		txtambervalpulse.setText(PulseAmber.split("or")[0] + "\n" + PulseAmber.split("or")[1]);// 11may2017
		// Arpitha-
		// v2.6

		TextView txtredvalbpsys = (TextView) dialog.findViewById(R.id.bpsysred);// 11may2017
																				// Arpitha
																				// -
																				// v2.6
		txtredvalbpsys.setText(SysRed);// 11may2017 Arpitha- v2.6 TextView
		TextView txtgreenvalbpsys = (TextView) dialog.findViewById(R.id.bpsysgreen);// 11may2017
		// Arpitha-
		// v2.6
		txtgreenvalbpsys.setText(SysGreen);// 11may2017 Arpitha- v2.6 TextView
		TextView txtambervalbpsys = (TextView) dialog.findViewById(R.id.bpsysamber);// 11may2017
		// Arpitha-
		// v2.6
		txtambervalbpsys.setText(SysAmber.split("or")[0] + "\n" + SysAmber.split("or")[1]);// 11may2017
																							// Arpitha-
																							// v2.6

		TextView txtredvalbpdia = (TextView) dialog.findViewById(R.id.bpdiared);// 11may2017
																				// Arpitha
																				// -
																				// v2.6
		txtredvalbpdia.setText(DiaRed);// 11may2017 Arpitha- v2.6 TextView
		TextView txtgreenvalbpdia = (TextView) dialog.findViewById(R.id.bydiagreen);// 11may2017
		// Arpitha-
		// v2.6
		txtgreenvalbpdia.setText(DiaGreen);// 11may2017 Arpitha- v2.6 TextView
		TextView txtambervalbpdia = (TextView) dialog.findViewById(R.id.bpdiaamber);// 11may2017
		// Arpitha-
		// v2.6
		txtambervalbpdia.setText(DiaAmber.split("or")[0] + "\n" + DiaAmber.split("or")[1]);// 11may2017
																							// Arpitha-
																							// v2.6

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		dialog.show();

		/*
		 * Dialog dialog = new Dialog(getActivity());
		 * dialog.setContentView(R.layout.pulsebp_info); dialog.setTitle(
		 * "Pulse and BP "); dialog.show();
		 * 
		 */}

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		/*
		 * final TextView txtdialog = (TextView)
		 * dialog.findViewById(R.id.txtpulse); Button imgbtnyes = (Button)
		 * dialog.findViewById(R.id.imgbtnyes); Button imgbtnno = (Button)
		 * dialog.findViewById(R.id.imgbtnno); // Arpitha 27may16 final TextView
		 * txtdialog1 = (TextView) dialog .findViewById(R.id.txtpulseval); final
		 * TextView txtdialog2 = (TextView) dialog .findViewById(R.id.txtsys);
		 * 
		 * final TextView txtdialog3 = (TextView) dialog
		 * .findViewById(R.id.tstsysval);
		 * 
		 * final TextView txtdialog4 = (TextView) dialog
		 * .findViewById(R.id.txtdia); final TextView txtdialog5 = (TextView)
		 * dialog .findViewById(R.id.txtdiaval); final TextView txtdialog6 =
		 * (TextView) dialog .findViewById(R.id.txt);
		 * 
		 * TextView txt7 = (TextView) dialog.findViewById(R.id.txt7); TextView
		 * txt8 = (TextView) dialog.findViewById(R.id.txt8);
		 * txt7.setVisibility(View.GONE); txt8.setVisibility(View.GONE);
		 * 
		 * 
		 * TextView txt9 = (TextView) dialog.findViewById(R.id.txt9); TextView
		 * txt10 = (TextView) dialog.findViewById(R.id.txt10);
		 * txt9.setVisibility(View.GONE); txt10.setVisibility(View.GONE);
		 * 
		 * 
		 * 
		 * txtdialog.setVisibility(View.GONE);
		 * txtdialog1.setVisibility(View.GONE);
		 * txtdialog2.setVisibility(View.GONE);
		 * txtdialog3.setVisibility(View.GONE);
		 * txtdialog4.setVisibility(View.GONE);
		 * txtdialog5.setVisibility(View.GONE);
		 * txtdialog6.setVisibility(View.GONE);
		 * 
		 * if(classname=="confirmation") { imgbtnyes.setText("Save");
		 * imgbtnno.setText("Re-Check");
		 * 
		 * if(pulval>120 || pulval<50) { txtdialog.setVisibility(View.VISIBLE);
		 * txtdialog1.setVisibility(View.VISIBLE);
		 * txtdialog.setText(getResources().getString(R.string.pul_val));
		 * txtdialog1.setText(""+pulval); } if(bpsysval<80 || bpsysval>160) {
		 * txtdialog2.setVisibility(View.VISIBLE);
		 * txtdialog3.setVisibility(View.VISIBLE);
		 * txtdialog2.setText(getResources().getString(R.string.bp_sys));
		 * txtdialog3.setText(""+bpsysval); } if(bpdiaval<50 || bpdiaval>=110) {
		 * txtdialog4.setVisibility(View.VISIBLE);
		 * txtdialog5.setVisibility(View.VISIBLE);
		 * txtdialog4.setText(getResources().getString(R.string.bp_dia));
		 * txtdialog5.setText(""+bpdiaval); }
		 * //txtdialog6.setVisibility(View.VISIBLE);
		 * txtdialog6.setText(getResources().getString(R.string.bp_val)); String
		 * mess = exit_msg;
		 * 
		 * String[] a = mess.split("\\,"); txtdialog.setText(a[0]);
		 * txtdialog1.setText(a[1]); txtdialog2.setText(a[2]);
		 * txtdialog3.setText(a[3]); txtdialog4.setText(a[4]);
		 * txtdialog5.setText(a[5]); txtdialog6.setText(a[6]);
		 * 
		 * 
		 * }else { txtdialog.setText(exit_msg);
		 * txtdialog1.setVisibility(View.GONE);
		 * txtdialog2.setVisibility(View.GONE); }
		 */

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		// 04Oct2016 ARpitha
		String alertmsg = "";
		String values = "";

		imgbtnyes.setText(getResources().getString(R.string.save));
		imgbtnno.setText(getResources().getString(R.string.recheck));

		if (pulval > 120 || pulval < 50) {
			alertmsg = alertmsg + getResources().getString(R.string.pul_val) + "\n";
			values = values + "" + pulval + "\n";

		}
		if (bpsysval < 80 || bpsysval > 160) {
			alertmsg = alertmsg + getResources().getString(R.string.bp_sys) + "\n";
			values = values + "" + bpsysval + "\n";

		}
		if (bpdiaval < 50 || bpdiaval >= 110) {
			alertmsg = alertmsg + getResources().getString(R.string.bp_dia) + "\n";
			values = values + "" + bpdiaval + "\n";

		}
		txtdialog6.setText(getResources().getString(R.string.bp_val));

		txtdialog.setText(alertmsg);
		txtdialog1.setText(values);
		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("confirmation")) {
					try {

						dbh.db.beginTransaction();
						int transId = dbh.iCreateNewTrans(strwuserId);

						boolean isAdded = dbh.insertPulseBPVal(valpojo, strWomenid, strwuserId, transId);

						if (isAdded) {

							dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
							dbh.updatenotification(strWomenid, obj_Id);// 18Oct2016
																		// Arpitha
							loadListDataToDisplayValues();
							displayValues(tbl);
							commitTrans();
						} else {
							rollbackTrans();
						}

						aq.id(R.id.etpulseval).text("");
						aq.id(R.id.etbpsystolic).text("");
						aq.id(R.id.etbpdiastolic).text("");

						aq.id(R.id.etpulsecomments).text("");

						aq.id(R.id.etpulseval).getEditText().requestFocus();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	/*
	 * public boolean validate() throws Exception { if((pulval>120 || pulval<50)
	 * && ((bpsysval<80 || bpsysval>160) && (bpdiaval<50 || bpdiaval>=110))) {
	 * displayConfirmationAlert("Pulse Value is:,"+pulval+
	 * ",BP(Systolic) Value is:,"+bpsysval+",BP(Diastolic) value is:,"+bpdiaval+
	 * ",You want to Re-Check/Save??", "confirmation");
	 * //displayConfirmationAlert(
	 * "Pulse, BP(Systolic) and BP(Diastolic) values are :"+pulval+" "+bpsysval+
	 * " "+bpdiaval+" ,You want to Re-Check/Save??", "confirmation");
	 * aq.id(R.id.etpulse).getEditText().requestFocus(); return false; } else
	 * if((pulval>120 || pulval<50) && (bpsysval<80 || bpsysval>160 )) {
	 * displayConfirmationAlert("Pulse, and BP(Systolic)  values are :"+pulval+
	 * " and "+bpsysval+" ,You want to Re-Check/Save??", "confirmation");
	 * displayConfirmationAlert("Pulse Value is:,"+pulval+
	 * ",BP(Systolic) Value is:,"+bpsysval+",You want to Re-Check/Save??",
	 * "confirmation"); aq.id(R.id.etpulse).getEditText().requestFocus(); return
	 * false; }
	 * 
	 * else if((pulval>120 || pulval<50) && (bpdiaval<50 || bpdiaval>=110 )) {
	 * //displayConfirmationAlert("Pulse, and BP(Diastolic)  values are :"
	 * +pulval+" and "+bpdiaval+" ,You want to Re-Check/Save??",
	 * "confirmation"); displayConfirmationAlert("Pulse Value is:,"+pulval+
	 * ",BP(Diastolic) Value is:,"+bpdiaval+",You want to Re-Check/Save??",
	 * "confirmation"); aq.id(R.id.etpulse).getEditText().requestFocus(); return
	 * false; }
	 * 
	 * else if((bpsysval<80 || bpsysval>160) && (bpdiaval<50 || bpdiaval>=110))
	 * { //displayConfirmationAlert(
	 * "BP(Systolic), and BP(Diastolic)  values are :"+bpsysval+" and "
	 * +bpdiaval+" ,You want to Re-Check/Save??", "confirmation");
	 * displayConfirmationAlert("BP(SYstolic) Value is:,"+bpsysval+
	 * ",BP(Diastolic) Value is:,"+bpdiaval+",You want to Re-Check/Save??",
	 * "confirmation"); aq.id(R.id.etbpsystolic).getEditText().requestFocus();
	 * return false; }
	 * 
	 * else if(pulval>120 || pulval<50) { displayConfirmationAlert(
	 * "Pulse value is: ,"+ pulval+",You want to Re-Check/Save??",
	 * "confirmation"); aq.id(R.id.etpulse).getEditText().requestFocus(); return
	 * false; } else if(bpsysval<80 || bpsysval>160) { displayConfirmationAlert(
	 * "BP(Systolic) value is: ,"+ bpsysval+",You want to Re-Check/Save??",
	 * "confirmation"); aq.id(R.id.etbpsystolic).getEditText().requestFocus();
	 * return false; } else if(bpdiaval<50 || bpdiaval>=110) {
	 * displayConfirmationAlert("BP(diastolic) value is: ,"+ bpdiaval+
	 * ",You want to Re-Check/Save??", "confirmation");
	 * aq.id(R.id.etbpdiastolic).getEditText().requestFocus(); return false; }
	 * return true; }
	 */

	public void notification(String womenid) throws Exception

	{

		final Dialog dialog = new Dialog(getActivity());
		// 9Sep2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.notification_dialog);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.pulse_bp));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		TextView txt = (TextView) dialog.findViewById(R.id.textView1);
		String convertedlastdate = Partograph_CommonClass.getConvertedDateFormat(lastdate, "dd-MM-yyyy");
		txt.setText(getResources().getString(R.string.enter_pulsebp_last_entry_at) + convertedlastdate + " " + lasttime
				+ " " + getResources().getString(R.string.was_at));
		// aq.id(R.id.imgbtnnotification).invisible();
		// AlaramReceiver.pulse--;
		// LoginActivity.wid_pulse.remove(womenid);
		// AlaramReceiver.pulse_notification = false;
		dialog.show();
		// initialView();

	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		aq.id(R.id.etpulsecomments).getEditText().setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(200) });
		/*
		 * aq.id(R.id.etcomments).getEditText().setFilters(new InputFilter[] {
		 * filter }); aq.id(R.id.etattendant).getEditText().setFilters(new
		 * InputFilter[] { filter });
		 * aq.id(R.id.etdocname).getEditText().setFilters(new InputFilter[] {
		 * filter }); aq.id(R.id.etnursename).getEditText().setFilters(new
		 * InputFilter[] { filter });
		 */
	}

}

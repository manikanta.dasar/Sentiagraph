package com.bc.partograph.sliding;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Dialog;
import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_Urinetest extends Fragment implements OnClickListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	ArrayList<Add_value_pojo> arrComments;
	// Changed on 27Mar2015
	String displaydateformat;

	// updated on 17Nov2015
	String proteinval, acetoneval;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	Women_Profile_Pojo woman;// 03Nov2016

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_urinetest, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			initializeScreen(aq.id(R.id.rlurinetest).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			switch (v.getId()) {
			case R.id.btnsaveurinetest:

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {
					if (Partograph_CommonClass.CheckAutomaticDateTime(strwuserId,
							getResources().getString(R.string.partograph), getActivity())) {
						strCurrentTime = todaysDate + "_" + aq.id(R.id.eturinetesttime).getText().toString();

						isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

						if (isValidTime) {

							setUrineTestData();
						} else {
							displayConfirmationAlert(getResources().getString(R.string.before_half_an_hour),
									getResources().getString(R.string.duration));
						}
					}
				}

				break;

			default:
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());
		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
	}

	// Sync
	private void calSyncMtd() {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.eturinetesttime).text(strcurrTime);
		aq.id(R.id.eturinetestdate).text(displaydateformat);

		aq.id(R.id.eturinetestdate).enabled(false);
		aq.id(R.id.eturinetesttime).enabled(false);

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluesurinetest);
		tbl.setVisibility(View.GONE);

		obj_Type = getResources().getString(R.string.object9);
		obj_Id = dbh.getObjectID(obj_Type);

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha

		loadListDataToDisplayValues();

		// Disable btnsave if the woman is delivered
		// 23Sep2016 Arpitha - make invisible when woman is referred
		// if (rowItems.get(pos).getDel_type() != 0 ||
		// SlidingActivity.isReferred) {
		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
//		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
//		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2 || SlidingActivity.arrVal.size()>0) {
			aq.id(R.id.llurinetestaddval).gone();
			aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

		} else {
			aq.id(R.id.llurinetestaddval).visible();
		}

		displayComments();
		updatetblComments(strwuserId, strWomenid, obj_Id);

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha
		
//		15Aug2017 Arpitha
		if(SlidingActivity.arrVal.size()>0)
		aq.id(R.id.txtnodata)
				.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));
	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.utcmt_cnt = 0;

		SlidingActivity.isMsgUT = false; // 15Sep2016
	}

	// Display Comments
	private void displayComments() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());
		}
		if (arrComments.size() > 0) {

			Parcelable state = aq.id(R.id.listCommentsurinetest).getListView().onSaveInstanceState();
			aq.id(R.id.listCommentsurinetest).visible();
			aq.id(R.id.txtCommentsut).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_urinetest, arrComments);
			aq.id(R.id.listCommentsurinetest).adapter(adapter);

			// 18dec2015
			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentsurinetest).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentsurinetest).getListView());

		} else {
			aq.id(R.id.listCommentsurinetest).gone();
			aq.id(R.id.txtCommentsut).gone();
		}
	}

	public void doWork() throws Exception {

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.eturinetesttime).text(curTime);
						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	// Load data to display
	private void loadListDataToDisplayValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		listValuesItems = new ArrayList<Add_value_pojo>();

		listValuesItems = dbh.getUrineTestData(obj_Id, strWomenid, strwuserId);
		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 28Sep2016 Arpitha
		} else {
			tbl.setVisibility(View.VISIBLE);
			displayValues();
		}
	}

	// Display values
	private void displayValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval1lbl);

			TextView textval2lbl = new TextView(getActivity());
			textval2lbl.setTextSize(18);
			textval2lbl.setPadding(10, 10, 10, 10);
			textval2lbl.setTextColor(getResources().getColor(R.color.black));
			textval2lbl.setTextSize(18);
			textval2lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval2lbl);

			TextView textval3lbl = new TextView(getActivity());
			textval3lbl.setTextSize(18);
			textval3lbl.setPadding(10, 10, 10, 10);
			textval3lbl.setTextColor(getResources().getColor(R.color.black));
			textval3lbl.setTextSize(18);
			textval3lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval3lbl);

			TextView textcomments = new TextView(getActivity());
			textcomments.setTextSize(18);
			textcomments.setText(getResources().getString(R.string.comments));
			textcomments.setPadding(10, 10, 10, 10);
			textcomments.setTextColor(getResources().getColor(R.color.black));
			textcomments.setTextSize(18);
			textcomments.setGravity(Gravity.LEFT);
			trlabel.addView(textcomments);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
				}

				String val1 = "", val2 = "", val3 = "", val4 = "";

				val1 = listValuesItems.get(i).getStrProteinval();
				val2 = listValuesItems.get(i).getStrAcetoneval();
				val3 = listValuesItems.get(i).getStrVolumeval();
				val4 = listValuesItems.get(i).getUrinecomments();

				// updated on 17Nov2015
				if (val1.equalsIgnoreCase("0"))
					val1 = getResources().getString(R.string.nil);
				else if (val1.equalsIgnoreCase("1"))
					val1 = getResources().getString(R.string.oneplus);
				else if (val1.equalsIgnoreCase("2"))
					val1 = getResources().getString(R.string.twoplus);
				else if (val1.equalsIgnoreCase("3"))
					val1 = getResources().getString(R.string.threeplus);
				else if (val1.equalsIgnoreCase("4"))
					val1 = getResources().getString(R.string.fourplus);

				if (val2.equalsIgnoreCase("0"))
					val2 = getResources().getString(R.string.nil);
				else if (val2.equalsIgnoreCase("1"))
					val2 = getResources().getString(R.string.oneplus);
				else if (val2.equalsIgnoreCase("2"))
					val2 = getResources().getString(R.string.twoplus);
				else if (val2.equalsIgnoreCase("3"))
					val2 = getResources().getString(R.string.threeplus);
				else if (val2.equalsIgnoreCase("4"))
					val2 = getResources().getString(R.string.fourplus);

				textval1lbl.setText(getResources().getString(R.string.protein));
				textval2lbl.setText(getResources().getString(R.string.acetone));
				textval3lbl.setText(getResources().getString(R.string.volume));

				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);
				tr.addView(txtvalue1);

				TextView txtvalue2 = new TextView(getActivity());
				txtvalue2.setText(val2);
				txtvalue2.setTextSize(18);
				txtvalue2.setPadding(10, 10, 10, 10);
				txtvalue2.setTextColor(getResources().getColor(R.color.black));
				txtvalue2.setTextSize(18);
				txtvalue2.setGravity(Gravity.CENTER);
				tr.addView(txtvalue2);

				TextView txtvalue3 = new TextView(getActivity());
				txtvalue3.setText(val3);
				txtvalue3.setTextSize(18);
				txtvalue3.setPadding(10, 10, 10, 10);
				txtvalue3.setTextColor(getResources().getColor(R.color.black));
				txtvalue3.setTextSize(18);
				txtvalue3.setGravity(Gravity.CENTER);
				tr.addView(txtvalue3);

				TextView txtcommentval = new TextView(getActivity());
				txtcommentval.setText(val4);
				txtcommentval.setTextSize(18);
				txtcommentval.setPadding(10, 10, 10, 10);
				txtcommentval.setTextColor(getResources().getColor(R.color.black));
				txtcommentval.setTextSize(18);
				txtcommentval.setGravity(Gravity.CENTER);
				tr.addView(txtcommentval);

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}
	}

	/**
	 * This method invokes when Item clicked in Spinner
	 * 
	 * @throws Exception
	 */
	public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (adapter.getId()) {
			// facility
			case R.id.spnprotein:
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				proteinval = "" + adapter.getSelectedItemPosition();

				break;

			case R.id.spnacetone:
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				acetoneval = "" + adapter.getSelectedItemPosition();

				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);
		// updated on 19/6/16 by
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				try {
					setUrineTestData();
				} catch (Exception e) {
					e.printStackTrace();
				}
				// }

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	void setUrineTestData() throws Exception {
		Add_value_pojo valpojo = new Add_value_pojo();

		valpojo.setStrProteinval(proteinval);
		valpojo.setStrAcetoneval(acetoneval);
		valpojo.setStrVolumeval(aq.id(R.id.etvolumeval).getText().toString());
		valpojo.setUrinecomments(aq.id(R.id.eturinecomments).getText().toString());
		valpojo.setStrTime(aq.id(R.id.eturinetesttime).getText().toString());
		valpojo.setStrdate(todaysDate);
		valpojo.setObj_Id(obj_Id);
		valpojo.setObj_Type(obj_Type);

		dbh.db.beginTransaction();
		int transId = dbh.iCreateNewTrans(strwuserId);

		boolean isAdded = dbh.insertUrinetestData(valpojo, strWomenid, strwuserId, transId);

		if (isAdded) {

			dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
			loadListDataToDisplayValues();
			commitTrans();
		} else {
			rollbackTrans();
		}
		aq.id(R.id.spnprotein).setSelection(0); // 17Nov2015
		aq.id(R.id.spnacetone).setSelection(0); // 17Nov2015
		aq.id(R.id.etvolumeval).text("");

		aq.id(R.id.eturinecomments).text("");
	}
}

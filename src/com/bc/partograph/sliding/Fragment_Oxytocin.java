package com.bc.partograph.sliding;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.Property_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Dialog;
import android.app.Fragment;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_Oxytocin extends Fragment implements OnClickListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	ArrayList<Add_value_pojo> arrComments;
	// Changed on 27Mar2015
	String displaydateformat;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	Women_Profile_Pojo woman;// 03Nov2016

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_oxytocin, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			initializeScreen(aq.id(R.id.rloxytocin).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

			// setInputFiltersForEdittext();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			switch (v.getId()) {
			case R.id.btnsaveoxytocin:

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

					if (Partograph_CommonClass.CheckAutomaticDateTime(strwuserId,
							getResources().getString(R.string.partograph), getActivity())) {
						if (aq.id(R.id.etoxytocinval).getEditText().getText().toString().trim().length() > 0) {

							strCurrentTime = todaysDate + "_" + aq.id(R.id.etoxytocintime).getText().toString();

							isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

							if (isValidTime) {

								setOxytocin();

							} else {
								displayConfirmationAlert(getResources().getString(R.string.before_half_an_hour),
										"duration");
							}

						} else {
							aq.id(R.id.etoxytocinval).getEditText().requestFocus(); // 12jan2016

							Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_val),
									getActivity());
						}
					}
				}
				break;

			default:
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());
		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.etoxytocintime).text(strcurrTime);
		aq.id(R.id.etoxytocindate).text(displaydateformat);

		aq.id(R.id.etoxytocindate).enabled(false);
		aq.id(R.id.etoxytocintime).enabled(false);

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluesoxytocin);
		tbl.setVisibility(View.GONE);

		loadListDataToDisplayValues();

		// Disable btnsave if the woman is delivered
		// 23Sep2016 Arpitha - make invisible when woman is referred
		// if (rowItems.get(pos).getDel_type() != 0 ||
		// SlidingActivity.isReferred) {
		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
//		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
//		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2 || SlidingActivity.arrVal.size()>0) {
			aq.id(R.id.lloxytocinaddval).gone();
			aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

		} else {

			aq.id(R.id.lloxytocinaddval).visible();
		}

		obj_Type = getResources().getString(R.string.object5);
		obj_Id = dbh.getObjectID(obj_Type);

		loadListDataToDisplayValues();
		displayValues();
		displayComments();
		updatetblComments(strwuserId, strWomenid, obj_Id);

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha
		
//		15Aug2017 Arpitha
		if(SlidingActivity.arrVal.size()>0)
		aq.id(R.id.txtnodata)
				.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));
	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.oxycmt_cnt = 0;

		SlidingActivity.isMsgOxy = false; // 15Sep2016
	}

	// Display Comments
	private void displayComments() throws Exception {
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());

		}
		if (arrComments.size() > 0) {

			Parcelable state = aq.id(R.id.listCommentsoxytocin).getListView().onSaveInstanceState();
			aq.id(R.id.listCommentsoxytocin).visible();
			aq.id(R.id.txtCommentsoxytocin).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_oxytocin, arrComments);
			aq.id(R.id.listCommentsoxytocin).adapter(adapter);

			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentsoxytocin).getListView().onRestoreInstanceState(state);

			// 18dec2015
			Helper.getListViewSize(aq.id(R.id.listCommentsoxytocin).getListView());
		} else {
			aq.id(R.id.listCommentsoxytocin).gone();
			aq.id(R.id.txtCommentsoxytocin).gone();

		}
	}

	public void doWork() throws Exception {

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.etoxytocintime).text(curTime);
						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	private void loadListDataToDisplayValues() throws Exception {

		listValuesItems = dbh.getOxytocinData(obj_Id, strWomenid, strwuserId);
		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 28Sep2016 Arpitha

		} else {
			tbl.setVisibility(View.VISIBLE);
			displayValues();
			aq.id(R.id.txtnodata).gone();// 06oct2016 Arpitha
		}
	}

	// Display values
	private void displayValues() throws Exception {
		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval1lbl);

			// Add feb04
			TextView textval2lbl = new TextView(getActivity());
			textval2lbl.setTextSize(18);
			textval2lbl.setPadding(10, 10, 10, 10);
			textval2lbl.setTextColor(getResources().getColor(R.color.black));
			textval2lbl.setTextSize(18);
			textval2lbl.setGravity(Gravity.CENTER);
			textval2lbl.setText(getResources().getString(R.string.units));
			trlabel.addView(textval2lbl);

			// updated on 28feb16 by Arpitha
			TextView textcomments = new TextView(getActivity());
			textcomments.setTextSize(18);
			textcomments.setText(getResources().getString(R.string.comments));
			textcomments.setPadding(10, 10, 10, 10);
			textcomments.setTextColor(getResources().getColor(R.color.black));
			textcomments.setTextSize(18);
			textcomments.setGravity(Gravity.LEFT);
			// textcomments.setText(getResources().getString(R.string.units));
			trlabel.addView(textcomments);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				// txtdate.setText(listValuesItems.get(i).getStrdate());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
				}
				String val1 = "", val2 = "", val3 = "";

				val1 = listValuesItems.get(i).getOxytocindrops();
				val2 = listValuesItems.get(i).getOxytocinunits();

				textval1lbl.setText(getResources().getString(R.string.oxytocin));

				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);
				tr.addView(txtvalue1);

				TextView txtvalue2 = new TextView(getActivity());
				txtvalue2.setText(val2);
				txtvalue2.setTextSize(18);
				txtvalue2.setPadding(10, 10, 10, 10);
				txtvalue2.setTextColor(getResources().getColor(R.color.black));
				txtvalue2.setTextSize(18);
				txtvalue2.setGravity(Gravity.CENTER);
				tr.addView(txtvalue2);

				TextView txtcommentsvalue = new TextView(getActivity());
				txtcommentsvalue.setText(listValuesItems.get(i).getOxytocincomments());
				txtcommentsvalue.setTextSize(18);
				txtcommentsvalue.setPadding(10, 10, 10, 10);
				txtcommentsvalue.setTextColor(getResources().getColor(R.color.black));
				txtcommentsvalue.setTextSize(18);
				txtcommentsvalue.setGravity(Gravity.CENTER);
				tr.addView(txtcommentsvalue);

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}
	}

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);

		// updated on 19/6/16 by
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("duration")) {
					try {

						setOxytocin();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	public void setOxytocin() throws NotFoundException, Exception {

		dbh.db.beginTransaction();
		int transId = dbh.iCreateNewTrans(strwuserId);

		ArrayList<String> adata = new ArrayList<String>();
		adata.add(aq.id(R.id.etoxytocinval).getText().toString());
		adata.add(aq.id(R.id.etoxytocinunits).getText().toString());
		adata.add(aq.id(R.id.etoxytocincomments).getText().toString());
		ArrayList<String> prop_id = new ArrayList<String>();
		ArrayList<String> prop_name = new ArrayList<String>();
		Property_pojo pdata = new Property_pojo();
		Cursor cursor = dbh.getPropertyIds(obj_Id);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {

				prop_id.add(cursor.getString(0));
				prop_name.add(cursor.getString(1));

			} while (cursor.moveToNext());

			pdata.setObj_Id(obj_Id);
			pdata.setProp_id(prop_id);
			pdata.setProp_name(prop_name);
			pdata.setProp_value(adata);
			pdata.setStrdate(todaysDate);
			pdata.setStrTime(aq.id(R.id.etoxytocintime).getText().toString());
			int oxytocinval = Integer.parseInt(aq.id(R.id.etoxytocinval).getEditText().getText().toString());
			if (oxytocinval == 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.invalidval_val),
						getActivity());
			} else {

				boolean isAdded = dbh.insertData(pdata, strWomenid, strwuserId, transId);

				if (isAdded) {

					dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
					loadListDataToDisplayValues();
					displayValues();
					commitTrans();
				} else {
					rollbackTrans();
				}

				aq.id(R.id.etoxytocinval).text("");
				aq.id(R.id.etoxytocinunits).text("");
				aq.id(R.id.etoxytocincomments).text("");

				aq.id(R.id.etoxytocinval).getEditText().requestFocus();
			}
		}

	}

}

package com.bc.partograph.sliding;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Dialog;
import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_Drugs extends Fragment implements OnClickListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	ArrayList<Add_value_pojo> arrComments;
	// Changed on 27Mar2015
	String displaydateformat;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	Women_Profile_Pojo woman;// 03Nov2016

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_drugsgiven, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			initializeScreen(aq.id(R.id.rldrugsgiven).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();
			// setInputFiltersForEdittext();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			switch (v.getId()) {
			case R.id.btnsavedrugsgiven:

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

					if (Partograph_CommonClass.CheckAutomaticDateTime(strwuserId,
							getResources().getString(R.string.partograph), getActivity())) {
						if (aq.id(R.id.etdrugsgivenval).getEditText().getText().toString().trim().length() > 0) {

							strCurrentTime = todaysDate + "_" + aq.id(R.id.etdrugsgiventime).getText().toString();

							isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

							if (isValidTime) {

								setDrugsData();
							} else {
								displayConfirmationAlert(getResources().getString(R.string.before_half_an_hour),
										"duration");
							}
						} else {
							Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_val),
									getActivity());
						}
					}
				}
				break;

			default:
				break;
			}
		} catch (

		Exception e)

		{
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.db.endTransaction();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());

		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);

		aq.id(R.id.etdrugsgiventime).text(strcurrTime);
		aq.id(R.id.etdrugsgivendate).text(displaydateformat);

		aq.id(R.id.etdrugsgivendate).enabled(false);
		aq.id(R.id.etdrugsgiventime).enabled(false);

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluesdrugsgiven);
		tbl.setVisibility(View.GONE);

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha

		// Disable btnsave if the woman is delivered
		// 23Sep2016 Arpitha - make invisible when woman is referred
		// if (rowItems.get(pos).getDel_type() != 0 ||
		// SlidingActivity.isReferred) {
		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
//		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
//		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2 || SlidingActivity.arrVal.size()>0) {
			aq.id(R.id.lldrugsgivenaddval).gone();
			aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

		} else {
			aq.id(R.id.lldrugsgivenaddval).visible();
		}

		obj_Type = getResources().getString(R.string.object6);
		obj_Id = dbh.getObjectID(obj_Type);

		loadListDataToDisplayValues();
		displayValues();
		displayComments();

		updatetblComments(strwuserId, strWomenid, obj_Id);

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha
		
//		15Aug2017 Arpitha
		if(SlidingActivity.arrVal.size()>0)
		aq.id(R.id.txtnodata)
				.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));
	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.drugscmt_cnt = 0;

		SlidingActivity.isMsgDrugs = false; // 15Sep2016
	}

	// Display Comments
	private void displayComments() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());

		}
		if (arrComments.size() > 0) {
			Parcelable state = aq.id(R.id.listCommentsdrugs).getListView().onSaveInstanceState();
			aq.id(R.id.listCommentsdrugs).visible();
			aq.id(R.id.txtCommentsdrugs).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_drugsgiven, arrComments);
			aq.id(R.id.listCommentsdrugs).adapter(adapter);

			// 18dec2015
			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentsdrugs).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentsdrugs).getListView());

		} else {
			aq.id(R.id.txtCommentsdrugs).gone();
			aq.id(R.id.listCommentsdrugs).gone();
		}
	}

	public void doWork() throws Exception {

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.etdrugsgiventime).text(curTime);
						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	// Load data to display
	private void loadListDataToDisplayValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// updated on 28feb16 by Arpitha
		listValuesItems = dbh.getDrugdata(obj_Id, strWomenid, strwuserId);

		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 28Sep2016 Arpitha
		} else {
			tbl.setVisibility(View.VISIBLE);
			displayValues();
		}

	}

	// Display values
	private void displayValues() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval1lbl);
			// updated on 28feb16 by arpitha

			TextView textcomments = new TextView(getActivity());
			textcomments.setTextSize(18);
			textcomments.setText(getResources().getString(R.string.comments));
			textcomments.setPadding(10, 10, 10, 10);
			textcomments.setTextColor(getResources().getColor(R.color.black));
			textcomments.setTextSize(18);
			textcomments.setGravity(Gravity.LEFT);
			trlabel.addView(textcomments);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
				}

				String val1 = "", val2 = "", val3 = "", val4 = "";

				val1 = listValuesItems.get(i).getDrug();

				val4 = listValuesItems.get(i).getDrugcomments();

				textval1lbl.setText(getResources().getString(R.string.drugs_given));

				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);
				tr.addView(txtvalue1);

				TextView txtcommentval = new TextView(getActivity());
				txtcommentval.setText(val4);
				txtcommentval.setTextSize(18);
				txtcommentval.setPadding(10, 10, 10, 10);
				txtcommentval.setTextColor(getResources().getColor(R.color.black));
				txtcommentval.setTextSize(18);
				txtcommentval.setGravity(Gravity.CENTER);
				tr.addView(txtcommentval);

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}
	}

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);

		// updated on 19/6/16 by
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("duration")) {
					try {
						setDrugsData();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	void setDrugsData() throws Exception {

		Add_value_pojo valpojo = new Add_value_pojo();
		valpojo.setDrug(aq.id(R.id.etdrugsgivenval).getText().toString());
		valpojo.setDrugcomments(aq.id(R.id.etdrugcomments).getText().toString());
		valpojo.setStrTime(aq.id(R.id.etdrugsgiventime).getText().toString());
		valpojo.setStrdate(todaysDate);

		valpojo.setObj_Id(obj_Id);
		valpojo.setObj_Type(obj_Type);

		dbh.db.beginTransaction();
		int transId = dbh.iCreateNewTrans(strwuserId);

		boolean isAdded = dbh.insertpropertyvalues(valpojo, Partograph_DB.TBL_PROPERTYVALUES, strWomenid, strwuserId,
				transId);

		if (isAdded) {
			dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
			loadListDataToDisplayValues();
			displayValues();
			commitTrans();
		} else
			rollbackTrans();

		aq.id(R.id.etdrugsgivenval).text("");
		aq.id(R.id.etdrugcomments).text("");
		aq.id(R.id.etdrugsgivenval).getEditText().requestFocus();

	}

}
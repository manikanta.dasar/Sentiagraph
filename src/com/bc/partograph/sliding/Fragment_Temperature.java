package com.bc.partograph.sliding;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Dialog;
import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_Temperature extends Fragment implements OnClickListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	ArrayList<Add_value_pojo> arrComments;
	// Changed on 27Mar2015
	String displaydateformat;
	Add_value_pojo valpojo;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	// 04Oct201 Arpitha
	TextView txtdialogtitle;
	Women_Profile_Pojo woman;// 03Nov2016
	double tempval;
	String Red, Amber, Green;// 05May2017 Arpitha - v_2.6

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_temp, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			initializeScreen(aq.id(R.id.rltemp).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

			setInputFiltersForEdittext();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			switch (v.getId()) {
			case R.id.btnsavetemp:

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

				/*	Date lastregdate = null;
					String strlastinserteddate = dbh.getlastmaxdate(strwuserId,
							getResources().getString(R.string.partograph));
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault());
					if (strlastinserteddate != null) {
						lastregdate = format.parse(strlastinserteddate);
					}
					Date currentdatetime = new Date();
					if (lastregdate != null && currentdatetime.before(lastregdate) && (!LoginActivity.istesting)) {
						Partograph_CommonClass.showSettingsAlert(getActivity(),
								getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
					} else {// 27Sep2016 Arpitha
*/
					if(Partograph_CommonClass.CheckAutomaticDateTime(strwuserId,
							getResources().getString(R.string.partograph), getActivity()))
					{
						if (aq.id(R.id.ettempval).getEditText().getText().toString().trim().length() > 0) {

							strCurrentTime = todaysDate + "_" + aq.id(R.id.ettemptime).getText().toString();

							isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

							if (isValidTime) {
								// 15May2017 Arpitha - v2.6
								setData();

							} else {
								displayConfirmationAlert(getResources().getString(R.string.before_half_an_hour),
										"duration");
							}
						} else {
							Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_temp_val),getActivity());

						}
					}
				}
				break;
			// Arpitha on 27May16
			case R.id.fhrinfo: {
				displayTempinfo();
			}
				break;

			default:
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();

		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());

		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.ettemptime).text(strcurrTime);
		aq.id(R.id.ettempdate).text(displaydateformat);

		aq.id(R.id.ettempdate).enabled(false);
		aq.id(R.id.ettemptime).enabled(false);

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluestemp);
		tbl.setVisibility(View.GONE);

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha
		// Disable btnsave if the woman is delivered
		// 23Sep2016 Arpitha - make invisible when woman is referred
		// if (rowItems.get(pos).getDel_type() != 0 ||
		// SlidingActivity.isReferred) {
		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
//		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
//		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2 || SlidingActivity.arrVal.size()>0) {
			aq.id(R.id.lltempaddval).gone();
			aq.id(R.id.ttxheading).visible();// 30Sep2016 Arpitha

		} else {

			aq.id(R.id.lltempaddval).visible();
		}

		obj_Type = getResources().getString(R.string.object8);
		obj_Id = dbh.getObjectID(obj_Type);

		// 05May2017 Arpitha - v_2.6
		Cursor cur = dbh.getColorCodedValuesFromTable("8.1");
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			Red = cur.getString(4);
			Amber = cur.getString(5);
			Green = cur.getString(6);
		} // 05May2017 Arpitha - v_2.6

		loadListDataToDisplayValues();
		displayValues();
		displayComments();

		updatetblComments(strwuserId, strWomenid, obj_Id);

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
		} // 19Nov2016 Arpitha
//		15Aug2017 Arpitha
		if(SlidingActivity.arrVal.size()>0)
		aq.id(R.id.txtnodata)
				.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));
	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.tempcmt_cnt = 0;

		SlidingActivity.isMsgTemp = false; // 15Sep2016
	}

	// Display Comments
	private void displayComments() throws Exception {
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());
		}
		if (arrComments.size() > 0) {

			Parcelable state = aq.id(R.id.listCommentstemp).getListView().onSaveInstanceState();
			aq.id(R.id.txtCommentstemp).visible();
			aq.id(R.id.listCommentstemp).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_temp, arrComments);
			aq.id(R.id.listCommentstemp).adapter(adapter);

			// 18dec2015
			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentstemp).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentstemp).getListView());

		} else {
			aq.id(R.id.listCommentstemp).gone();
			aq.id(R.id.txtCommentstemp).gone();
		}
	}

	public void doWork() throws Exception {

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.ettemptime).text(curTime);
						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	// Load data to display
	private void loadListDataToDisplayValues() throws Exception {

		listValuesItems = dbh.getTempdata(obj_Id, strWomenid, strwuserId);

		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 28Sep2016 Arpitha

		} else {
			displayValues();
			tbl.setVisibility(View.VISIBLE);

		}
	}

	// Display values
	private void displayValues() throws Exception {
		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			textval1lbl.setText(getResources().getString(R.string.temperature));
			trlabel.addView(textval1lbl);

			TextView textcomment = new TextView(getActivity());
			textcomment.setPadding(10, 10, 10, 10);
			textcomment.setTextColor(getResources().getColor(R.color.black));
			textcomment.setTextSize(18);
			textcomment.setGravity(Gravity.LEFT);
			textcomment.setText(getResources().getString(R.string.comments));
			trlabel.addView(textcomment);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
				}

				String val1 = "";

				val1 = listValuesItems.get(i).getStrVal();

				TextView txtvalue1 = new TextView(getActivity());
				double val = Double.parseDouble(val1);
				// 05MAy2017 Arpitha - v2.6
				try {
					Double tempRed1 = Double.parseDouble(Red.split("(or)|(and)")[0].split("(<)|(>)")[1].trim());
					Double tempRed2 = Double.parseDouble(Red.split("(or)|(and)")[1].split("(<)|(>)")[1].trim());

					String a = Amber.split("(or)|(and)")[0];
					String aa = a.split("(>=)|(<=)|(<)|(>)")[0];
					String b = a.split("(>=)|(<=)|(<)|(>)")[1];
					Double tempAmber1 = Double.parseDouble(b);
					Double tempAmber2 = Double.parseDouble(Amber.split("(or)|(and)")[1].split("(>=)|(<=)")[1].trim());

					if (val >= tempAmber1 && val <= tempAmber2)
						txtvalue1.setTextColor(getResources().getColor(R.color.amber));
					else if (val > tempRed1 || val < tempRed2)
						txtvalue1.setTextColor(getResources().getColor(R.color.red));
					else if (val >= 97 && val < 99)
						txtvalue1.setTextColor(getResources().getColor(R.color.green));
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				// 05MAy2017
				// Arpitha
				// -
				// v2.6

				txtvalue1.setText(val1);
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);
				tr.addView(txtvalue1);

				TextView txtcommentval = new TextView(getActivity());
				// txtdate.setText(listValuesItems.get(i).getStrdate());
				txtcommentval.setText(listValuesItems.get(i).getTempcomments());
				txtcommentval.setTextSize(18);
				txtcommentval.setPadding(10, 10, 10, 10);
				txtcommentval.setTextColor(getResources().getColor(R.color.black));
				txtcommentval.setTextSize(18);
				txtcommentval.setGravity(Gravity.CENTER);
				tr.addView(txtcommentval);

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}
	}

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha on 27May16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);

		if (classname == "confirmation") {
			imgbtnyes.setText(getResources().getString(R.string.save));
			imgbtnno.setText(getResources().getString(R.string.recheck));
		//	String mess = exit_msg;

			txtdialog1.setText("" + tempval);
			txtdialog.setText(getResources().getString(R.string.temp_val_is));

			txtdialog2.setText(getResources().getString(R.string.bp_val));
		} else {
			txtdialog.setText(exit_msg);
			txtdialog1.setVisibility(View.GONE);
			txtdialog2.setVisibility(View.GONE);
		}

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("confirmation")) {
					dialog.cancel();
					try {

						saveTempertaureData();

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}
				if (classname.equalsIgnoreCase("duration")) {
					try {

						setData();
						// 15May2017 Arpitha
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	public void displayTempinfo() {
		final Dialog dialog = new Dialog(getActivity());
		// 9Sep2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.fhr_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.temperature));
		// 11may2017 Arpitha - v2.6
		TextView txtredval = (TextView) dialog.findViewById(R.id.greenval2);
		txtredval.setText(Red);
		TextView txtgreenval = (TextView) dialog.findViewById(R.id.greenval4);
		txtgreenval.setText(Green);
		TextView txtamberval = (TextView) dialog.findViewById(R.id.amberval9);
		txtamberval.setText(Amber);// 11may2017 Arpitha- v2.6

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		dialog.show();

	}

//	// Alert dialog
//	private void displayAlertDialog(String message) throws Exception {
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//
//		// set dialog message
//		alertDialogBuilder.setMessage(message)
//
//				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int id) {
//						// if this button is clicked, just close
//						// the dialog box and do nothing
//						dialog.cancel();
//					}
//				});
//
//		// create alert dialog
//		AlertDialog alertDialog = alertDialogBuilder.create();
//		// show it
//		alertDialog.show();
//	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		aq.id(R.id.ettempcomments).getEditText().setFilters(new InputFilter[] { filter , new InputFilter.LengthFilter(200)});
	}

	// 15May2017 Arpitha - v2.6
	void saveTempertaureData() throws Exception {
		dbh.db.beginTransaction();
		int transId = dbh.iCreateNewTrans(strwuserId);

		boolean isAdded = dbh.insertpropertyvalues(valpojo, Partograph_DB.TBL_PROPERTYVALUES, strWomenid, strwuserId,
				transId);

		if (isAdded) {
			dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
			loadListDataToDisplayValues();
			displayValues();
			commitTrans();
		} else
			rollbackTrans();

		aq.id(R.id.ettempval).text("");
		aq.id(R.id.ettempcomments).text("");
		aq.id(R.id.ettempval).getEditText().requestFocus();

	}

	// 15May2017 Arpitha - v2.6
	void setData() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		
		

		try {
			valpojo = new Add_value_pojo();
			valpojo.setStrVal(aq.id(R.id.ettempval).getText().toString());
			valpojo.setTempcomments(aq.id(R.id.ettempcomments).getText().toString());
			valpojo.setStrTime(aq.id(R.id.ettemptime).getText().toString());
			valpojo.setStrdate(todaysDate);

			valpojo.setObj_Id(obj_Id);
			valpojo.setObj_Type(obj_Type);

			tempval = Double.parseDouble(aq.id(R.id.ettempval).getText().toString());

			if (tempval > 101)
				valpojo.setTempDanger(1);//12Jun2017 Arpitha
			//	valpojo.setIsDanger(1);
			else
				valpojo.setTempDanger(0);//12Jun2017 Arpitha
				valpojo.setIsDanger(0);

			// 5may16 Arpitha
			if (tempval > 101 || tempval < 97) {
				displayConfirmationAlert(getResources().getString(R.string.temp_val_is) + tempval + ","
						+ getResources().getString(R.string.bp_val), "confirmation");

			} else {
				if (tempval == 0) {
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.invalidval_val),getActivity());
				} else {
					saveTempertaureData();
				}
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

}

/*package com.bc.partograph.comprehensiveview;

import java.util.ArrayList;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Comprehensive_View extends Activity {

	ArrayList<Women_Profile_Pojo> rowItems;
	int pos = -1;
	Partograph_DB dbh;
	String strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItemsFHR;
	ArrayList<Add_value_pojo> listValuesItemsDilatation;
	ArrayList<Add_value_pojo> listValuesItemsContractions;
	ArrayList<Add_value_pojo> listValuesItemsPBP;
	GraphicalView mChartViewDilatation = null;
	GraphicalView mChartViewFHR = null;
	GraphicalView mChartViewContraction = null;
	GraphicalView mChartViewPBP = null;
	public static int comptabpos = 0;
	public static boolean isComprehensive;
	RelativeLayout layoutfhr, layoutdil, layoutcontract, layoutpbp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comprehensive_view);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			rowItems = (ArrayList<Women_Profile_Pojo>) getIntent().getSerializableExtra("rowitems");
			pos = getIntent().getIntExtra("position", 0);
			dbh = Partograph_DB.getInstance(getApplicationContext());
			comptabpos = Partograph_CommonClass.curr_tabpos;
			isComprehensive = true;

			strWomenid = rowItems.get(pos).getWomenId();
			strwuserId = rowItems.get(pos).getUserId();

			TextView txtdata = (TextView) findViewById(R.id.txtdata);
			layoutfhr = (RelativeLayout) findViewById(R.id.rlfetalheartratechart);
			layoutdil = (RelativeLayout) findViewById(R.id.rldilatationchart);
			layoutcontract = (RelativeLayout) findViewById(R.id.rlcontractionchart);
			layoutpbp = (RelativeLayout) findViewById(R.id.rlpulseandbpchart);

			loadListDataDilatation();
			loadListDataContraction();
			loadListDataFHR();
			loadListDataPulseBP();

			if (!(listValuesItemsContractions.size() > 0 || listValuesItemsDilatation.size() > 0
					|| listValuesItemsFHR.size() > 0 || listValuesItemsPBP.size() > 0)) {
				txtdata.setText(getResources().getString(R.string.nodata));
				txtdata.setVisibility(View.VISIBLE);
			} else {
				txtdata.setText("");
				txtdata.setVisibility(View.GONE);
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		KillAllActivitesAndGoToLogin.activity_stack.add(this);
	}

	// Load Pulse BP Data
	private void loadListDataPulseBP() throws Exception {
		listValuesItemsPBP = dbh.getPulseBPData(7, strWomenid, strwuserId, true);
		createChartPulseBP();

	}

	// Pulse rate and BP Chart
	private void createChartPulseBP() throws Exception {
		if (listValuesItemsPBP.size() > 0) {
			layoutpbp.setVisibility(View.VISIBLE);
			// Creating an XYSeries for values
			XYSeries pulseseries = new XYSeries(getResources().getString(R.string.pulse));
			XYSeries bpsystolicseries = new XYSeries(getResources().getString(R.string.systolic));
			XYSeries bpdiastolicseries = new XYSeries(getResources().getString(R.string.diastolic));

			
			 * // Threshold for pulse XYSeries redSeriesP50 = new XYSeries("");
			 * XYSeries redSeriesP110 = new XYSeries("");//pulse rate < 50 and >
			 * 110 XYSeries amberSeriesP60 = new XYSeries(""); // pulse rate <
			 * 60 and > 100 XYSeries amberSeriesP100 = new XYSeries("");
			 * 
			 * // Threshold for BP XYSeries redSeriesBP90 = new XYSeries("");
			 * XYSeries redSeriesBP160 = new XYSeries(""); // bp < 90 and > 160
			 * XYSeries amberSeriesBP140 = new XYSeries(""); // bp 140 to 160
			 * XYSeries amberSeriesBP160 = new XYSeries("");
			 * 
			 * 
			 * 
			 * for(int j=0; j<10;j++) { redSeriesP50.add(j, 50);
			 * redSeriesP110.add(j, 110); amberSeriesP60.add(j, 60);
			 * amberSeriesP100.add(j, 100);
			 * 
			 * redSeriesBP90.add(j, 90); redSeriesBP160.add(j, 160);
			 * amberSeriesBP140.add(j, 140); amberSeriesBP160.add(j, 160); }
			 

			for (int i = 0; i < listValuesItemsPBP.size(); i++) {

				if (listValuesItemsPBP.get(i).getStrPulseval().length() > 0
						&& !listValuesItemsPBP.get(i).getStrPulseval().isEmpty()) {
					pulseseries.add((i + 1), Double.parseDouble(listValuesItemsPBP.get(i).getStrPulseval()));
				} else {
					pulseseries.add((i + 1), 0);
				}

				if (listValuesItemsPBP.get(i).getStrBPsystolicval().length() > 0
						&& listValuesItemsPBP.get(i).getStrBPdiastolicval().length() > 0) {
					bpsystolicseries.add((i + 1), Double.parseDouble(listValuesItemsPBP.get(i).getStrBPsystolicval()));
					bpdiastolicseries.add((i + 1),
							Double.parseDouble(listValuesItemsPBP.get(i).getStrBPdiastolicval()));
				} else {
					bpsystolicseries.add((i + 1), 0);
					bpdiastolicseries.add((i + 1), 0);
				}

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(pulseseries);
			dataset.addSeries(bpsystolicseries);
			dataset.addSeries(bpdiastolicseries);
			
			 * dataset.addSeries(redSeriesP50);
			 * dataset.addSeries(redSeriesP110);
			 * dataset.addSeries(redSeriesBP90);
			 * dataset.addSeries(redSeriesBP160);
			 * 
			 * dataset.addSeries(amberSeriesP60);
			 * dataset.addSeries(amberSeriesP100);
			 * dataset.addSeries(amberSeriesBP140);
			 * dataset.addSeries(amberSeriesBP160);
			 

			
			 * // Creating XYSeriesRenderer to customize redSeries
			 * XYSeriesRenderer redRendererP50 = new XYSeriesRenderer();
			 * redRendererP50 .setColor(getResources().getColor(R.color.red));
			 * redRendererP50.setLineWidth(4);
			 * 
			 * XYSeriesRenderer redRendererP110 = new XYSeriesRenderer();
			 * redRendererP110 .setColor(getResources().getColor(R.color.red));
			 * redRendererP110.setLineWidth(4);
			 * 
			 * XYSeriesRenderer redRendererBP90 = new XYSeriesRenderer();
			 * redRendererBP90 .setColor(getResources().getColor(R.color.red));
			 * redRendererBP90.setLineWidth(4);
			 * 
			 * XYSeriesRenderer redRendererBP160 = new XYSeriesRenderer();
			 * redRendererBP160 .setColor(getResources().getColor(R.color.red));
			 * redRendererBP160.setLineWidth(4);
			 * 
			 * // Creating XYSeriesRenderer to customize amberSeries
			 * XYSeriesRenderer amberRendererP60 = new XYSeriesRenderer();
			 * amberRendererP60
			 * .setColor(getResources().getColor(R.color.amber));
			 * amberRendererP60.setLineWidth(4);
			 * 
			 * XYSeriesRenderer amberRendererP100 = new XYSeriesRenderer();
			 * amberRendererP100
			 * .setColor(getResources().getColor(R.color.amber));
			 * amberRendererP100.setLineWidth(4);
			 * 
			 * XYSeriesRenderer amberRendererBP140 = new XYSeriesRenderer();
			 * amberRendererBP140
			 * .setColor(getResources().getColor(R.color.amber));
			 * amberRendererBP140.setLineWidth(4);
			 * 
			 * XYSeriesRenderer amberRendererBP160 = new XYSeriesRenderer();
			 * amberRendererBP160
			 * .setColor(getResources().getColor(R.color.amber));
			 * amberRendererBP160.setLineWidth(4);
			 

			// Creating XYSeriesRenderer to customize pulseSeries
			XYSeriesRenderer pulseRenderer = new XYSeriesRenderer();
			pulseRenderer.setColor(getResources().getColor(R.color.green));
			pulseRenderer.setPointStyle(PointStyle.CIRCLE);
			pulseRenderer.setFillPoints(true);
			pulseRenderer.setLineWidth(3);
			pulseRenderer.setDisplayChartValues(true);
			pulseRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer bpsystolicRenderer = new XYSeriesRenderer();
			bpsystolicRenderer.setColor(getResources().getColor(R.color.blue));
			bpsystolicRenderer.setPointStyle(PointStyle.CIRCLE);
			bpsystolicRenderer.setFillPoints(true);
			bpsystolicRenderer.setLineWidth(5);
			bpsystolicRenderer.setDisplayChartValues(true);
			bpsystolicRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer bpdiastolicRenderer = new XYSeriesRenderer();
			bpdiastolicRenderer.setColor(getResources().getColor(R.color.seablue));
			bpdiastolicRenderer.setPointStyle(PointStyle.CIRCLE);
			bpdiastolicRenderer.setFillPoints(true);
			bpdiastolicRenderer.setLineWidth(5);
			bpdiastolicRenderer.setDisplayChartValues(true);
			bpdiastolicRenderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle(getResources().getString(R.string.pulse_bp));
			multiRenderer.setChartTitleTextSize(20);
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(getResources().getString(R.string.pulse_bp));
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(15);
			multiRenderer.setLegendTextSize(15);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 60, 80, 60, 30 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(25);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			multiRenderer.setPanEnabled(true, true);

			multiRenderer.setYAxisMax(200);
			multiRenderer.setYAxisMin(60);
			multiRenderer.setYLabels(15);
			// multiRenderer.setYAxisMin(40);
			multiRenderer.setBarSpacing(10);
			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);

			multiRenderer.setZoomButtonsVisible(true);
			for (int i = 0; i < listValuesItemsPBP.size(); i++) {
				multiRenderer.addXTextLabel((i + 1), listValuesItemsPBP.get(i).getStrTime());
				// 05jan2016
				multiRenderer.setYAxisMax(300);
				multiRenderer.setYLabels(15);
				// multiRenderer.addXTextLabel(i,
				// (i+1)+"("+listValuesItems.get(i)
				// .getStrTime() + ")");
			}

			multiRenderer.addSeriesRenderer(pulseRenderer);
			multiRenderer.addSeriesRenderer(bpsystolicRenderer);
			multiRenderer.addSeriesRenderer(bpdiastolicRenderer);

			mChartViewPBP = null;

			String[] types = new String[] { LineChart.TYPE, BarChart.TYPE, BarChart.TYPE, };

			mChartViewPBP = ChartFactory.getCombinedXYChartView(getApplicationContext(), dataset, multiRenderer, types);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 400);
			mChartViewPBP.setLayoutParams(params);

			layoutpbp.addView(mChartViewPBP);

			if (mChartViewPBP != null) {
				mChartViewPBP.repaint();
			}
		} else
			layoutpbp.setVisibility(View.GONE);
	}

	// Load contraction data
	private void loadListDataContraction() throws Exception {
		listValuesItemsContractions = dbh.getContractionData(4, strWomenid, strwuserId, true);

		createChartContraction();
	}

	// Contraction chart
	private void createChartContraction() throws Exception {
		if (listValuesItemsContractions.size() > 0) {
			layoutcontract.setVisibility(View.VISIBLE);
			// Creating an XYSeries for values
			XYSeries cont_L20sec = new XYSeries("Less than 20 sec");
			XYSeries cont_B20_40sec = new XYSeries("Between 20 and 40 sec");
			XYSeries cont_M40sec = new XYSeries("More than 40 sec");
			XYSeries cont_M60sec = new XYSeries("More than 60 sec");

			for (int i = 0; i < listValuesItemsContractions.size(); i++) {
				if (listValuesItemsContractions.get(i).getDuration() < 20)
					cont_L20sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));
				if (listValuesItemsContractions.get(i).getDuration() >= 20
						&& listValuesItemsContractions.get(i).getDuration() <= 40) {
					cont_B20_40sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));
				}

				if (listValuesItemsContractions.get(i).getDuration() > 40
						&& listValuesItemsContractions.get(i).getDuration() <= 60)
					cont_M40sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));

				if (listValuesItemsContractions.get(i).getDuration() > 60) {
					cont_M60sec.add(i + 1, Double.parseDouble(listValuesItemsContractions.get(i).getContractions()));
				}

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(cont_L20sec);
			dataset.addSeries(cont_B20_40sec);
			dataset.addSeries(cont_M40sec);
			dataset.addSeries(cont_M60sec);

			// Creating XYSeriesRenderer to customize contractions < 20 sec
			XYSeriesRenderer contraction_L20Renderer = new XYSeriesRenderer();
			contraction_L20Renderer.setColor(getResources().getColor(R.color.vividorange));
			contraction_L20Renderer.setPointStyle(PointStyle.DIAMOND);
			contraction_L20Renderer.setFillPoints(true);
			contraction_L20Renderer.setLineWidth((float) 5.5d);
			contraction_L20Renderer.setDisplayChartValues(true);
			contraction_L20Renderer.setChartValuesTextSize(20);

			// contraction_L20Renderer.setFillBelowLine(true);
			// contraction_L20Renderer.setFillBelowLineColor(Color.CYAN);

			// Creating XYSeriesRenderer to customize contractions bet 20 and 40
			XYSeriesRenderer contraction_B20_40Renderer = new XYSeriesRenderer();
			contraction_B20_40Renderer.setColor(getResources().getColor(R.color.darkgreen));
			contraction_B20_40Renderer.setPointStyle(PointStyle.CIRCLE);
			contraction_B20_40Renderer.setFillPoints(true);
			contraction_B20_40Renderer.setLineWidth((float) 10.5d);
			contraction_B20_40Renderer.setDisplayChartValues(true);
			contraction_B20_40Renderer.setChartValuesTextSize(20);
			// contraction_B20_40Renderer.setFillBelowLine(true);
			// contraction_B20_40Renderer.setFillBelowLineColor(Color.MAGENTA);

			// Creating XYSeriesRenderer to customize contractions > 40
			XYSeriesRenderer contraction_M40Renderer = new XYSeriesRenderer();
			contraction_M40Renderer.setColor(getResources().getColor(R.color.darkred));
			contraction_M40Renderer.setPointStyle(PointStyle.CIRCLE);
			contraction_M40Renderer.setFillPoints(true);
			contraction_M40Renderer.setLineWidth((float) 10.5d);
			contraction_M40Renderer.setDisplayChartValues(true);
			contraction_M40Renderer.setChartValuesTextSize(20);
			// contraction_M40Renderer.setFillBelowLine(true);
			// contraction_M40Renderer.setFillBelowLineColor(Color.GRAY);

			// Creating XYSeriesRenderer to customize contractions > 40
			XYSeriesRenderer contraction_M60Renderer = new XYSeriesRenderer();
			contraction_M60Renderer.setColor(getResources().getColor(R.color.red));
			contraction_M60Renderer.setPointStyle(PointStyle.CIRCLE);
			contraction_M60Renderer.setFillPoints(true);
			contraction_M60Renderer.setLineWidth((float) 10.5d);
			contraction_M60Renderer.setDisplayChartValues(true);
			contraction_M60Renderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle(getResources().getString(R.string.contraction));
			multiRenderer.setChartTitleTextSize(30);
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(getResources().getString(R.string.contraction));
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 30, 60, 60, 30 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(25);
			multiRenderer.setPointSize(10);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(true, true);
			multiRenderer.setYAxisMax(12);
			multiRenderer.setYAxisMin(1);
			multiRenderer.setGridColor(getResources().getColor(R.color.lightgray));
			multiRenderer.setYLabels(10);
			multiRenderer.setFitLegend(true);
			multiRenderer.setShowGrid(true);
			// multiRenderer.setAntialiasing(true);
			// multiRenderer.setInScroll(true);
			multiRenderer.setShowGridX(true);
			multiRenderer.setShowGridY(true);

			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);
			multiRenderer.setZoomButtonsVisible(true);
			// multiRenderer.setBarSpacing(.5);
			multiRenderer.setBarWidth(15);

			for (int i = 0; i < listValuesItemsContractions.size(); i++) {
				
				 * multiRenderer.addXTextLabel(i + 1, "" +
				 * listValuesItemsContractions.get(i).getStrTime() + "(" +
				 * listValuesItemsContractions.get(i).getDuration() + ")");
				 

				multiRenderer.addXTextLabel(i + 1, listValuesItemsContractions.get(i).getStrTime() + "\n"
						+ listValuesItemsContractions.get(i).getDuration());
				multiRenderer.setYAxisMax(10);
				multiRenderer.setYLabels(10);
			}

			multiRenderer.addSeriesRenderer(contraction_L20Renderer);
			multiRenderer.addSeriesRenderer(contraction_B20_40Renderer);
			multiRenderer.addSeriesRenderer(contraction_M40Renderer);
			multiRenderer.addSeriesRenderer(contraction_M60Renderer);

			mChartViewContraction = null;
			// mChartView = ChartFactory.getLineChartView(getActivity(),
			// dataset,
			// multiRenderer);

			String[] types = new String[] { BarChart.TYPE, BarChart.TYPE, BarChart.TYPE };

			// mChartView = ChartFactory.getCombinedXYChartView(getActivity(),
			// dataset, multiRenderer, types);
			mChartViewContraction = ChartFactory.getBarChartView(getApplicationContext(), dataset, multiRenderer,
					Type.DEFAULT);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 600);
			mChartViewContraction.setLayoutParams(params);

			layoutcontract.addView(mChartViewContraction);

			if (mChartViewContraction != null) {
				mChartViewContraction.repaint();
			}
		} else
			layoutcontract.setVisibility(View.GONE);
	}

	private void loadListDataDilatation() throws Exception {
		listValuesItemsDilatation = dbh.getDilatationData(3, strWomenid, strwuserId, true);

		createChartDilatation();

	}

	private void createChartDilatation() throws Exception {
		if (listValuesItemsDilatation.size() > 0) {
			layoutdil.setVisibility(View.VISIBLE);
			// Creating an XYSeries for values
			XYSeries cervixseries = new XYSeries(getResources().getString(R.string.cervix));
			XYSeries desc_headseries = new XYSeries(getResources().getString(R.string.desc_head));

			XYSeries alertline = new XYSeries(getResources().getString(R.string.alertline));
			XYSeries actionline = new XYSeries(getResources().getString(R.string.actionline));

			int[] x = { 0, 1, 2, 3, 4, 5, 6 };
			int[] alertval = { 4, 5, 6, 7, 8, 9, 10 };

			int[] xactionline = { 4, 5, 6, 7, 8, 9, 10 };
			int[] actionval = { 4, 5, 6, 7, 8, 9, 10 };

			double descofhead = 0;

			for (int i = 0; i < listValuesItemsDilatation.size(); i++) {
				cervixseries.add(i, Double.parseDouble(listValuesItemsDilatation.get(i).getCervix()));

				if (listValuesItemsDilatation.get(i).getDesc_of_head() != null
						&& !listValuesItemsDilatation.get(i).getDesc_of_head().isEmpty()) {
					descofhead = Double.parseDouble(listValuesItemsDilatation.get(i).getDesc_of_head());
					desc_headseries.add(i, descofhead);
				}

			}

			for (int i = 0; i < x.length; i++) {
				alertline.add(x[i], alertval[i]);
			}

			for (int i = 0; i < xactionline.length; i++) {
				actionline.add(xactionline[i], actionval[i]);
			}
			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(cervixseries);
			dataset.addSeries(desc_headseries);
			dataset.addSeries(alertline);
			dataset.addSeries(actionline);

			// Creating XYSeriesRenderer to customize alertlineSeries
			XYSeriesRenderer alertRenderer = new XYSeriesRenderer();
			alertRenderer.setColor(getResources().getColor(R.color.amber));
			alertRenderer.setPointStyle(PointStyle.POINT);
			alertRenderer.setFillPoints(true);
			alertRenderer.setLineWidth(5);
			alertRenderer.setDisplayChartValues(false);

			// Creating XYSeriesRenderer to customize actionlineSeries
			XYSeriesRenderer actionRenderer = new XYSeriesRenderer();
			actionRenderer.setColor(getResources().getColor(R.color.red));
			actionRenderer.setPointStyle(PointStyle.POINT);
			actionRenderer.setFillPoints(true);
			actionRenderer.setLineWidth(5);
			actionRenderer.setDisplayChartValues(false);

			// Creating XYSeriesRenderer to customize pulseSeries
			XYSeriesRenderer cervixRenderer = new XYSeriesRenderer();
			cervixRenderer.setColor(getResources().getColor(R.color.springgreen));
			cervixRenderer.setPointStyle(PointStyle.TRIANGLE);
			cervixRenderer.setFillPoints(true);
			cervixRenderer.setLineWidth(3);
			cervixRenderer.setDisplayChartValues(true);
			cervixRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer descheadRenderer = new XYSeriesRenderer();
			descheadRenderer.setColor(getResources().getColor(R.color.steel_blue));
			descheadRenderer.setPointStyle(PointStyle.CIRCLE);
			descheadRenderer.setFillPoints(true);
			descheadRenderer.setLineWidth(3);
			descheadRenderer.setDisplayChartValues(true);
			descheadRenderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle(getResources().getString(R.string.dilatation));
			multiRenderer.setChartTitleTextSize(30);
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(getResources().getString(R.string.cervixanddescentofhead));
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 30, 60, 60, 30 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(25);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(true, false);
			multiRenderer.setYAxisMax(10);
			multiRenderer.setYAxisMin(0);
			multiRenderer.setYLabels(11);
			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);

			// multiRenderer.setZoomButtonsVisible(true);
			// for (int i = 0; i < listValuesItems.size(); i++) {
			// multiRenderer.addXTextLabel(i, (i+1)+"("+listValuesItems.get(i)
			// .getStrTime() + ")");
			// }

			// changes made on 04-02-2015
			for (int i = 0; i < 12; i++) {
				multiRenderer.addXTextLabel(i, "" + (i + 1));
			} // 04feb

			multiRenderer.addSeriesRenderer(cervixRenderer);
			multiRenderer.addSeriesRenderer(descheadRenderer);
			multiRenderer.addSeriesRenderer(alertRenderer);
			multiRenderer.addSeriesRenderer(actionRenderer);

			mChartViewDilatation = null;
			mChartViewDilatation = ChartFactory.getLineChartView(getApplicationContext(), dataset, multiRenderer);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 600);
			mChartViewDilatation.setLayoutParams(params);

			layoutdil.addView(mChartViewDilatation);

			if (mChartViewDilatation != null) {
				mChartViewDilatation.repaint();
			}
		} else
			layoutdil.setVisibility(View.GONE);
	}

	private void loadListDataFHR() throws Exception {

		listValuesItemsFHR = dbh.getFHRGraphData(1, strWomenid, strwuserId);

		createChart();
	}

	// Create the graph
	protected void createChart() throws Exception {
		if (listValuesItemsFHR.size() > 0) {
			layoutfhr.setVisibility(View.VISIBLE);
			String yLbl = getResources().getString(R.string.fhr);

			// Creating an XYSeries for values
			XYSeries valueseries = new XYSeries("Values");

			for (int i = 0; i < listValuesItemsFHR.size(); i++) {
				valueseries.add(i + 1, Double.parseDouble(listValuesItemsFHR.get(i).getStrVal()));

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(valueseries);

			// Creating XYSeriesRenderer to customize valueSeries
			XYSeriesRenderer valueRenderer = new XYSeriesRenderer();
			valueRenderer.setColor(Color.BLACK);
			valueRenderer.setPointStyle(PointStyle.CIRCLE);
			valueRenderer.setFillPoints(true);
			valueRenderer.setLineWidth(4);
			valueRenderer.setDisplayChartValues(true);
			valueRenderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle(getResources().getString(R.string.fetal_heart_rate));
			multiRenderer.setChartTitleTextSize(30);
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(yLbl);
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 60, 80, 60, 60 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(25);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(true, true);
			// multiRenderer.setYAxisMax(maxVal);
			// multiRenderer.setYAxisMin(0);

			multiRenderer.setYAxisMax(200);

			// updated on 17Nov2015 - YAxisMin from 80 to 40
			multiRenderer.setYAxisMin(40);
			multiRenderer.setZoomButtonsVisible(true);

			
			 * multiRenderer.setGridColor(getResources().getColor(
			 * R.color.lightgray));
			 
			multiRenderer.setYLabels(13);
			multiRenderer.setFitLegend(true);
			multiRenderer.setShowGrid(true);
			multiRenderer.setAntialiasing(true);
			multiRenderer.setInScroll(true);
			multiRenderer.setShowGridX(true);
			multiRenderer.setShowGridY(true);

			multiRenderer.setBarSpacing(.5);
			multiRenderer.setXLabelsAngle(45f);

			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);

			for (int i = 0; i < listValuesItemsFHR.size(); i++) {
				multiRenderer.addXTextLabel(i + 1, listValuesItemsFHR.get(i).getStrTime());
				multiRenderer.setYAxisMax(300);
				multiRenderer.setYLabels(15);
			}

			multiRenderer.addSeriesRenderer(valueRenderer);

			mChartViewFHR = null;
			mChartViewFHR = ChartFactory.getLineChartView(getApplicationContext(), dataset, multiRenderer);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT, 400);
			mChartViewFHR.setLayoutParams(params);

			layoutfhr.setBackground(getResources().getDrawable(R.drawable.edit_text_style_blue));
			layoutfhr.addView(mChartViewFHR);

			if (mChartViewFHR != null) {
				mChartViewFHR.repaint();
			}
		} else
			layoutfhr.setVisibility(View.GONE);
	}

	*//** This method Calls the idle timeout *//*
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

}
*/
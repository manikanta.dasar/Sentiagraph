package com.bc.partograph.sync;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bluecrimson.partograph.UserPojo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.provider.Settings.Secure;


@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
public class PrepareUserAndSettingSyncXML {
	
	String xmlString = null;

	// private final SQLiteDatabase db;
	private SyncXmlBuilder xmlBuilder;
	
	SQLiteDatabase db;
	
	Context context;//18April2017 Arpitha

	public PrepareUserAndSettingSyncXML( SQLiteDatabase db, Context cx) {
		this.db = db;
		context = cx;//18April2017 Arpitha
	}

	// Prepares XML for PendingStatus
	public String  WriteResponseXML(UserPojo user, boolean fromLogin) throws Exception  {
		String SQLUser, SQLSettings;
		String userLMD = null, settingsLMD = null;
		
		if( ! fromLogin){
			if(user.getEmailid() != null && user.getEmailid().length() > 0){
				SQLUser = "SELECT max(LastUserModifiedDate) FROM tbl_User WHERE userid = '" +user.getEmailid()+ "' " ;
				Cursor c1 = db.rawQuery(SQLUser, null);
				if(c1.moveToFirst() && c1.getCount() > 0)
					userLMD = c1.getString(0);
		
				SQLSettings = "SELECT max(LastSettingsModifiedDate) FROM tbl_Settings WHERE userid = '" +user.getEmailid()+ "' " ;
				Cursor c2 = db.rawQuery(SQLSettings, null);
				if(c2.moveToFirst() && c2.getCount() > 0)
					settingsLMD = c2.getString(0);
			}
		}

		prepareXMLforCurrentServerData(userLMD, settingsLMD, user, fromLogin);
		xmlString = xmlBuilder.end();
		return xmlString;

	}
	
	
	private void prepareXMLforCurrentServerData(String userLMD, String settingsLMD, UserPojo user, boolean fromLogin) throws IOException {
		xmlBuilder = new SyncXmlBuilder();
		xmlBuilder.openRow("Request");
		
		xmlBuilder.addColumn("masterdb", Partograph_CommonClass.properties.getProperty("serverdbmaster"));
		xmlBuilder.addColumn("emailid", user.getEmailid());
		xmlBuilder.addColumn("password", user.getPassword());
		
		String androidId = getAndroidId(context);// 18April2017 Arpitha
		
		xmlBuilder.addColumn("deviceId", androidId);// 18April2017 Arpitha
		
		if(fromLogin){
			xmlBuilder.addColumn("isLoginCheck", "true");
		}else{
			if(userLMD != null && userLMD.length() > 0)
				xmlBuilder.addColumn("userUpdatedDate", userLMD);
			
			if(settingsLMD != null && settingsLMD.length() > 0)
				xmlBuilder.addColumn("settingsUpdatedDate", settingsLMD);
		}
		
		xmlBuilder.openRow("/Request");
		xmlString = xmlBuilder.end();
	}


	/**
	 * XmlBuilder is used to write XML tags (open and close, and a few attributes)
	 * to a StringBuilder. Here we have nothing to do with IO or SQL, just a fancy StringBuilder. 
	 * 
	 * @author ccollins
	 *
	 */
	@SuppressWarnings("unused")
	static class SyncXmlBuilder {

		//private static final String SOAP_ENVELOPE = " <soap :Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">";

		private static final String OPEN_XML_STANZA = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		private static final String CLOSE_WITH_TICK = ">";
		private static final String DB_OPEN = "<database name='";
		private static final String DB_CLOSE = "</database>";
		private static final String TABLE_OPEN = "<table name='";
		private static final String TABLE_CLOSE = "</table>";
		private static final String ROW_OPEN = "<row>";
		private static final String ROW_CLOSE = "</row>";
		private static final String COL_OPEN = "<col name='";
		private static final String COL_CLOSE = "</col>";

		private static final String OPEN_WITH_TICK = "<";

		private final StringBuilder sb;

		public SyncXmlBuilder() throws IOException {
			sb = new StringBuilder();
		}

		public String end() throws IOException {
			return sb.toString();
		}

		void openRow() {
			sb.append(SyncXmlBuilder.ROW_OPEN);
		}
		void openRow(String xVal) {
			sb.append(SyncXmlBuilder.OPEN_WITH_TICK + xVal + SyncXmlBuilder.CLOSE_WITH_TICK);
		}

		void closeRow() {
			sb.append(SyncXmlBuilder.ROW_CLOSE);
		}

		void addColumn(final String name, final String val) throws IOException {
			// sb.append(XmlBuilder.COL_OPEN + name + XmlBuilder.CLOSE_WITH_TICK + val + XmlBuilder.COL_CLOSE);

			if (val == null) {
				// sb.append(SyncXmlBuilder.OPEN_WITH_TICK + name + " xsi:nil=\"true\"" + " />") ;	    		  
				// sb.append("<" + name +  "/>") ;
			}
			else {
				sb.append(SyncXmlBuilder.OPEN_WITH_TICK + name + SyncXmlBuilder.CLOSE_WITH_TICK + val + SyncXmlBuilder.OPEN_WITH_TICK + "/" + name + SyncXmlBuilder.CLOSE_WITH_TICK);	    		  
			}


		}
	}


	/*public int getLastRequestNumber(String anmId) throws Exception {
		int LastRequestNumber = 0;
//		Cursor c = EjppApp.getDb().rawQuery("Select LastRequestNumber from tblusers where UserId = "+ anmId, null);
		if (c != null) {
			if (c.moveToFirst()) {
				LastRequestNumber = c.getInt(0);
			}
		}
		c.close();
		return LastRequestNumber;
	}	*/

	public void writeTxtFile(String sg) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		//if (Environment.MEDIA_MOUNTED.equals(state)) 
		//{
			//SDcard is available
			File f=new File(AppContext.mainDir + "/testDS.xml");
			try {
				f.createNewFile();
				OutputStream out=new FileOutputStream(f);
				byte buf[]=new byte[1024];
				int len;
				while((len=inputStream.read(buf))>0)
					out.write(buf,0,len);
				out.close();
				inputStream.close();
				System.out.println("\nFile is created...................................");

			} catch (IOException e) {
				e.printStackTrace();
			}
		//}
	}
	
//	18April2017 Arpitha
	public String getAndroidId(Context context) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		String android_id = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return android_id;
	}
}
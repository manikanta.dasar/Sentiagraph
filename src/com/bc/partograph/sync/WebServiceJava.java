//28Sep2016 Arpitha
package com.bc.partograph.sync;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;

public class WebServiceJava {
	// Namespace of the Webservice - It is http://tempuri.org for .NET
	// webservice
	private static String NAMESPACE = "http://tempuri.org/";
	// Webservice URL - It is asmx file location hosted in the server in case of
	// .Net
	// Change the IP address to your machine IP address
	// private static String URL =
	// "http://192.168.1.13/iASHAWebSrvcWOValidation/iAshaWebService.asmx";

	private static final Logger LOGGER = Logger.getLogger(WebService.class.getName());
	private static final String USER_AGENT = "Chrome/41.0.2272.96 Mobile Safari/537.36";

	static Partograph_DB dbh = null;
	
	static String ipaddress = "52.66.168.113";

	public static String invokeHelloWorldWS(String xMessage, String webMethName, String URL, boolean isDangerSync)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		ipaddress = "52.66.168.113";
		URL = ipaddress;
		String xDir = AppContext.mainDir;
		@SuppressWarnings("unused")
		File f = new File(xDir + "/Request-17.xsd");
		String resTxt, restxt = null;

		URL = "http://" + URL;

		restxt = invokeService(xMessage, webMethName, URL, isDangerSync);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		return restxt;
	}

	private static String invokeService(String xMessage, String webMethName, String uRL, boolean isDangerSync)
			throws Exception {

		StringBuffer response = null;
		HttpResponse httpResponse = null;
		byte[] responseBody = null;
		String res = null;

		Context context = Partograph_CommonClass.context;
		if (webMethName.equals("forward")) {
			String content = xMessage;

			// write the xml to a file to zip it and send to web service.
			writeTheContentsToAFile(content, Partograph_CommonClass.requestId);

			// http://192.168.1.14:8008/WebServiceEx/pmessages/getData
			// String url =
			// "http://"+DietAppCommon.properties.getProperty("serverIP")+"/BCWebS26Aug/pmessages/forward";
			// String url =
			// "http://"+Partograph_CommonClass.properties.getProperty("serverIP")+Partograph_CommonClass.properties.getProperty("serverURL1")+"forward";
			// 06Sep2016 Arpitha
			/*String url = "http://" + Partograph_CommonClass.sPojo.getIpAddress() + ":" + 8080
					+ "/BCWebS26Aug/pmessages/forward";*/
			String url = "http://" + ipaddress + ":" + 8080
					+ "/BCWebS/pmessages/forward";
			System.out.println("Sent Time : " + DateFormat.getDateTimeInstance().format(new Date()));

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setConnectTimeout(30000);

			String zipToSync = "/storage/sdcard0/Partograph/Images_Sync/";
			File folder = new File(zipToSync);
			File[] listOfFiles = folder.listFiles();

			List<String> fileStringListToSyncFromDevice = new ArrayList<String>();

			String sourceuri = "/storage/sdcard0/Partograph/Images_Sync/";
			int numberOfFiles = listOfFiles.length;
			int i;

			


			for (i = 0; i < numberOfFiles; i++) {
				File file1 = listOfFiles[i];

				// TODO adds only 1 .xml files to zip
				if (file1.getAbsoluteFile().getName().endsWith(".xml") ) {
					fileStringListToSyncFromDevice.add(sourceuri + file1.getName());
					zipFiles(fileStringListToSyncFromDevice, Partograph_CommonClass.requestId);
					
				}

			}

			
			int listSize = fileStringListToSyncFromDevice.size();

			for (i = 0; i < listSize; i++) {
				File fileAll = new File(fileStringListToSyncFromDevice.get(i));
				fileAll.delete();
			}

			try {

				String sourceZipUri = "/storage/sdcard0/Partograph/Images_Sync/" + Partograph_CommonClass.requestId
						+ ".zip";
				File zipFile = new File(sourceZipUri);
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				HttpPost httppost = new HttpPost(url);

				MultipartEntity mpEntity = new MultipartEntity();

				FileBody cbZipFile = new FileBody(zipFile);

				mpEntity.addPart("file", cbZipFile);
				httppost.setEntity(mpEntity);
				httpResponse = httpclient.execute(httppost);
				System.out.println(mpEntity);

				ByteArrayOutputStream outstream = new ByteArrayOutputStream();
				httpResponse.getEntity().writeTo(outstream);
				responseBody = outstream.toByteArray();
				res = new String(responseBody);

				response = new StringBuffer();
				response.append(res);
				con.disconnect();

				System.out.println("Response = " + response.toString());

				dbh = Partograph_DB.getInstance(Partograph_CommonClass.context);
				/*
				 * //if (isDangerSync)
				 * //dbh.UpdateDSTransStatus(Partograph_CommonClass.requestId,
				 * "S"); //else
				 */ // commented on 02Sep2016 Arpitha
				dbh.UpdateTransStatus(Partograph_CommonClass.requestId, "S");

				// IS THE CHECK CORRECT TO DELETE? I.E, TO DELETE AFTER
				// THIS 'ELSE'
				// fileListToSync.getFile().delete();
				//

			} catch (ClientProtocolException e) {
				e.printStackTrace();
				return "Error";
			} catch (IOException e) {
				e.printStackTrace();
				return "Error";
			} catch (Exception e) {
				e.printStackTrace();
				return "Error";
			}

		}

		if (webMethName.equals("getData")) {
			// http://192.168.1.14:8008/WebServiceEx/pmessages/getData

			// String url =
			// "http://192.168.1.119:8080/BCWebS26Aug/pmessages/getData";
			// String url =
			// "http://"+Partograph_CommonClass.properties.getProperty("serverIP")+Partograph_CommonClass.properties.getProperty("serverURL1")+"getData";

			// String url = uRL+webMethName;

			// 06Sep2016 Arpitha
			/*
			 * String url = "http://" +
			 * Partograph_CommonClass.sPojo.getIpAddress() +
			 * "http://IPAddress:PortNumber/BCWebS26Aug/pmessages/getData" +
			 * "getData";
			 */
			/*String url = "http://" + Partograph_CommonClass.sPojo.getIpAddress() + ":" + 8080
					+ "/BCWebS26Aug/pmessages/getData";*/
			String url = "http://" + ipaddress + ":" + 8080
					+ "/BCWebS/pmessages/getData";
			try {
				URL obj = new URL(url);
				// java.net.URL obj = new URL(null, url,new
				// sun.net.www.protocol.https.Handler());
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();

				con.setConnectTimeout(300000);

				// add reuqest header
				con.setRequestMethod("POST");
				con.setRequestProperty("User-Agent", USER_AGENT);
				con.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
				con.setRequestProperty("Expect", "100-continue");
				// con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

				String str = xMessage;
				byte[] outputInBytes = str.getBytes("UTF-8");
				OutputStream os = con.getOutputStream();
				os.write(outputInBytes);
				os.close();

				LOGGER.info("\nSending 'POST' request to URL ");

				int responseCode = con.getResponseCode();
				LOGGER.info("\nSending 'POST' request to URL : " + url);
				// System.out.println("Post parameters : " + urlParameters);
				LOGGER.info("Response Code : " + responseCode);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				con.disconnect();

				// print result
				LOGGER.info("response is " + response.toString());
				System.out.println("response is " + response.toString());
			} catch (MalformedURLException e) {
				e.printStackTrace();

				return response.replace(0, response.length(), "Error").toString();

			} catch (IOException e) {
				e.printStackTrace();
				return response.replace(0, response.length(), "Error").toString();

			}
		}

		System.out.println("OUT Time : " + DateFormat.getDateTimeInstance().format(new Date()));

		return response.toString();

	}

	public static String invokeLoginOrSettings(String groupid, String userid, String pswd, String webMethName,
			String URL, boolean isLogin, String serverDb, String lastModifiedDate) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		String xDir = AppContext.mainDir;
		@SuppressWarnings("unused")
		File f = new File(xDir + "/Request-17.xsd");

		URL = "http://" + URL;

		String resTxt = null;
		// Create request
		SoapObject request = new SoapObject(NAMESPACE, webMethName);
		// Property which holds input parameters
		PropertyInfo groupidProp = new PropertyInfo();
		// Set Name
		groupidProp.setName("GroupId");
		// Set Value
		groupidProp.setValue(groupid);
		// Set dataType
		groupidProp.setType(String.class);
		// Add the property to request object
		request.addProperty(groupidProp);

		PropertyInfo useridProp = new PropertyInfo();
		useridProp.setName("UserId");
		useridProp.setValue(userid);
		useridProp.setType(String.class);
		request.addProperty(useridProp);

		if (isLogin) {
			PropertyInfo pswdProp = new PropertyInfo();
			pswdProp.setName("Pswd");
			pswdProp.setValue(pswd);
			pswdProp.setType(String.class);
			request.addProperty(pswdProp);
		} else {
			PropertyInfo pswdProp = new PropertyInfo();
			pswdProp.setName("lastUpdatedTime");
			pswdProp.setValue(lastModifiedDate);
			pswdProp.setType(String.class);
			request.addProperty(pswdProp);
		}

		PropertyInfo dbname = new PropertyInfo();
		dbname.setName("dbname");
		dbname.setValue(serverDb);
		dbname.setType(String.class);
		request.addProperty(dbname);

		// resTxt = invokeServiceCall(request, webMethName, URL, false);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		return resTxt;
	}

	private static void writeTheContentsToAFile(String content, String xmlName) {

		FileOutputStream fop = null;
		File file;
		try {
			//changed directory - 13Sep2016 Arpitha 
			file = new File("/storage/sdcard0/Partograph/Images_Sync/" + xmlName + ".xml");
			fop = new FileOutputStream(file);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			// get the content in bytes
			byte[] contentInBytes = content.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

			System.out.println("Done");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void zipFiles(List<String> files, String zipFileName) {

		FileOutputStream fos = null;
		ZipOutputStream zipOut = null;
		FileInputStream fis = null;
		try {
			fos = new FileOutputStream("/storage/sdcard0/Partograph/Images_Sync/" + zipFileName + ".zip");
			zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
			for (String filePath : files) {
				File input = new File(filePath);
				fis = new FileInputStream(input);
				ZipEntry ze = new ZipEntry(input.getName());
				System.out.println("Zipping the file: " + input.getName());
				zipOut.putNextEntry(ze);
				byte[] tmp = new byte[1024];
				int size = 0;
				while ((size = fis.read(tmp)) > 0) {
					zipOut.write(tmp, 0, size);
				}
				zipOut.flush();
				fis.close();
			}
			zipOut.close();
			System.out.println("Done... Zipped the files...");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null)
					fos.close();
			} catch (Exception ex) {

			}
		}

	}

	public static String invokeHelloWorldWS(String userid, String pswd, String webMethName, String URL, Context context,
			String lastadddate) {
		{

			String resTxt = null;
			StringBuffer response = null;
			URL = ipaddress;
			URL = "http://" + URL;

			if (webMethName.equals("getvalidUserandGetDataForUser")) {
				/*String url = "http://" + Partograph_CommonClass.properties.getProperty("ipAddress") + ":" + 8080
						+ Partograph_CommonClass.properties.getProperty("serverURL1")
						+ "getvalidUserandGetDataForUser?UserId=" + userid + "&Pswd=" + pswd + "&dbname="
						+ Partograph_CommonClass.properties.getProperty("serverdbmaster") + "&lastadddate="
						+ lastadddate;*/
				
				String url = "http://" + ipaddress + ":" + 8080
						+ Partograph_CommonClass.properties.getProperty("serverURL1")
						+ "getvalidUserandGetDataForUser?UserId=" + userid + "&Pswd=" + pswd + "&dbname="
						+ Partograph_CommonClass.properties.getProperty("serverdbmaster") + "&lastadddate="
						+ lastadddate;

				try {
					URL obj = new URL(url);
					HttpURLConnection con = (HttpURLConnection) obj.openConnection();

					con.setConnectTimeout(300000);

					// add reuqest header
					con.setRequestMethod("GET");
					con.setRequestProperty("User-Agent", USER_AGENT);
					con.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
					con.setRequestProperty("Expect", "100-continue");

					LOGGER.info("\nSending 'GET' request to URL ");

					int responseCode = con.getResponseCode();
					LOGGER.info("\nSending 'GET' request to URL : " + url);
					LOGGER.info("Response Code : " + responseCode);
					
					
					if(responseCode==200)
					{

					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
					String inputLine;
					response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					// print result
					LOGGER.info("response is " + response.toString());
					System.out.println("response is " + response.toString());

					}else
					{
						// print responseCode
						response = new StringBuffer();
						LOGGER.info("Error" + responseCode);
						System.out.println("response is " + responseCode);

					}
					
					con.disconnect();
					
				} catch (MalformedURLException e) {
					e.printStackTrace();

					return response.replace(0, response.length(), "Error").toString();

				} catch (Exception e) {
					e.printStackTrace();
					return response.replace(0, response.length(), "Error").toString();

				}

			}

			if (webMethName.equals("getCommentsData")) {

				/*String url = "http://" + Partograph_CommonClass.properties.getProperty("ipAddress") + ":" + 8080
						+ Partograph_CommonClass.properties.getProperty("serverURL1") + "getCommentsData?UserId="
						+ userid + "&dbname=" + Partograph_CommonClass.sPojo.getServerDb() + "&lastadddate="
						+ lastadddate;*/
				
				String url = "http://" + ipaddress + ":" + 8080
						+ Partograph_CommonClass.properties.getProperty("serverURL1") + "getCommentsData?UserId="
						+ userid + "&dbname=" + Partograph_CommonClass.sPojo.getServerDb() + "&lastadddate="
						+ lastadddate;

				// url =
				// "http://192.168.0.109:8081/WebServices/pmessages/getCommentsData?UserId=user703&dbname=partograph_kt&lastadddate=2016-09-25%2017:50";

				try {
					URL obj = new URL(url);
					HttpURLConnection con = (HttpURLConnection) obj.openConnection();

					con.setConnectTimeout(300000);

					// add reuqest header
					con.setRequestMethod("GET");
					con.setRequestProperty("User-Agent", USER_AGENT);
					con.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
					con.setRequestProperty("Expect", "100-continue");

					LOGGER.info("\nSending 'GET' request to URL ");

					int responseCode = con.getResponseCode();
					LOGGER.info("\nSending 'GET' request to URL : " + url);
					LOGGER.info("Response Code : " + responseCode);

					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
					String inputLine;
					response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					con.disconnect();
					con = null;
					// print result
					LOGGER.info("response is " + response.toString());
					System.out.println("response is " + response.toString());

				} catch (MalformedURLException e) {
					e.printStackTrace();

					return response.replace(0, response.length(), "Error").toString();

				} catch (IOException e) {
					e.printStackTrace();
					return response.replace(0, response.length(), "Error").toString();

				}

			}

			if (webMethName.equals("getCommentsData222")) {
				/*
				 * String url = "http://" +
				 * Partograph_CommonClass.properties.getProperty("ipAddress") +
				 * ":" + 8081 +
				 * Partograph_CommonClass.properties.getProperty("serverURL1") +
				 * "GetCommentsData?UserId=" + userid + "&Pswd=" + pswd +
				 * "&dbname=" + Partograph_CommonClass.properties.getProperty(
				 * "serverdbmaster");
				 */

				// String url =
				// "http://IPAddress:PortNumber/BCWebS/pmessages/getCommentsData?UserId=userid&dbname=userid&lastadddate=lastadddate";

				/*String url = "http://" + Partograph_CommonClass.properties.getProperty("ipAddress") + ":" + 8080
						+ Partograph_CommonClass.properties.getProperty("serverURL1") + "getCommentsData?UserId="
						+ userid + "&dbname=" + Partograph_CommonClass.sPojo.getServerDb() + "&lastadddate="
						+ lastadddate;*/
				
				String url = "http://" + ipaddress + ":" + 8080
						+ Partograph_CommonClass.properties.getProperty("serverURL1") + "getCommentsData?UserId="
						+ userid + "&dbname=" + Partograph_CommonClass.sPojo.getServerDb() + "&lastadddate="
						+ lastadddate;
				try {
					URL obj = new URL(url);
					HttpURLConnection con = (HttpURLConnection) obj.openConnection();

					con.setConnectTimeout(300000);

					// add reuqest header
					con.setRequestMethod("GET");
					con.setRequestProperty("User-Agent", USER_AGENT);
					con.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
					con.setRequestProperty("Expect", "100-continue");

					LOGGER.info("\nSending 'GET' request to URL ");

					int responseCode = con.getResponseCode();
					LOGGER.info("\nSending 'GET' request to URL : " + url);
					LOGGER.info("Response Code : " + responseCode);

					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
					String inputLine;
					response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
					con.disconnect();
					// print result
					LOGGER.info("response is " + response.toString());
					System.out.println("response is " + response.toString());

				} catch (MalformedURLException e) {
					e.printStackTrace();

					return response.replace(0, response.length(), "Error").toString();

				} catch (IOException e) {
					e.printStackTrace();
					return response.replace(0, response.length(), "Error").toString();

				}

			}

			// resTxt = invokeServiceCall(request, webMethName, URL, false);

			return response.toString();
		}

		// return response.toString();

	}

	public static String invokeGetUserandSettingdetails(String wUserId, String dbName, String string,
			String lastModifieddate, String ipAdd, Context cx) {

		StringBuffer response = null;

		/*String url = "http://" + Partograph_CommonClass.properties.getProperty("ipAddress") + ":" + 8080
				+ Partograph_CommonClass.properties.getProperty("serverURL1") + "getUserandSettingdetails?UserId="
				+ wUserId + "&dbname=" + dbName + "&lastadddate=" + lastModifieddate;*/
		
		String url = "http://" + ipaddress + ":" + 8080
				+ Partograph_CommonClass.properties.getProperty("serverURL1") + "getUserandSettingdetails?UserId="
				+ wUserId + "&dbname=" + dbName + "&lastadddate=" + lastModifieddate;

		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setConnectTimeout(300000);

			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
			con.setRequestProperty("Expect", "100-continue");

			LOGGER.info("\nSending 'GET' request to URL ");

			int responseCode = con.getResponseCode();
			LOGGER.info("\nSending 'GET' request to URL : " + url);
			LOGGER.info("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			con.disconnect();
			// print result
			LOGGER.info("response is " + response.toString());
			System.out.println("response is " + response.toString());

		} catch (MalformedURLException e) {
			e.printStackTrace();

			return response.replace(0, response.length(), "Error").toString();

		} catch (IOException e) {
			e.printStackTrace();
			return response.replace(0, response.length(), "Error").toString();

		}

		return response.toString();

	}

	/*// 12Sep2016 Arpitha - for images
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public static void encryptCameraPhoto(String partFilename)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException {
		String getkey = "sdmkey";
		byte[] key = (getkey).getBytes("UTF-8");
		MessageDigest md = MessageDigest.getInstance("MD5");
		key = md.digest(key);
		key = Arrays.copyOf(key, 16); // use only first 128 bit

		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES");

		EncryptingImage.encrypt(
				Environment.getExternalStorageDirectory().toString() + "//" + "Partograph" + "//" + "Images_Sync" + "//"
						+ partFilename,
				Environment.getExternalStorageDirectory().toString() + "//" + "Partograph" + "//" + "Images_Sync" + "//"
						+ partFilename,
				cipher, secretKeySpec);
	}*/

	/*public static void storeCameraPhotoInSDCard(Bitmap bp, String partFilename)
			throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException {
		File outputFile = new File(
				Environment.getExternalStorageDirectory().toString() + "//" + "Partograph" + "//" + "Images_Sync",
				partFilename);
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
			bp.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
			fileOutputStream.flush();
			fileOutputStream.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/

}
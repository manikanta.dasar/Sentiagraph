package com.bc.partograph.regwomenlist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.MessageLogPojo;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.common.SendSMS;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RecentCustomListAdapterSearch extends ArrayAdapter<Women_Profile_Pojo> implements AnimationListener {
	Context context;
	ArrayList<Women_Profile_Pojo> data;
	Partograph_DB dbh;
	public static LinkedHashMap<String, Integer> riskoptionsMap;
	EditText etmesss;
	String risk_observed;
	int option = 2;
	ArrayList<String> values;
	String p_no = "";
	// updated 09Oct2016 -Arpitha
	boolean isMessageLogsaved = false;
	Cursor curref;
	// 20Oct2016 Arpitha
	ArrayList<WomenReferral_pojo> rval;
	WomenReferral_pojo wpojo;
	// 21Oct2016 Arpitha
	ImageView img1, img2, img3, img4, img5, img6, img7;// 01nov2016 Arpitha img7
	ArrayList<Women_Profile_Pojo> mStringFilterListDel;
	// ValueFilter valueFilter;
	// 1Nov2016 Arpitha
	String wname;
	int age;
	String regdate;
	int gest;
	String gravida;
	String risk;
	String delstatus = "";
	String deldeldate = "";
	int pos;
	private static byte[] image1 = null;// 08Nov2016
	ArrayList<WomenReferral_pojo> arrval = new ArrayList<WomenReferral_pojo>();// 15Nov2016
																				// Arpitha
	boolean isRefrred = false;// 15Nov2016 Arpitha

	ValueFilter valueFilter;

	public RecentCustomListAdapterSearch(Context context, int textViewResourceId, ArrayList<Women_Profile_Pojo> data,
			Partograph_DB dbh) {
		super(context, textViewResourceId, data);
		this.context = context;
		this.dbh = dbh;
		this.data = data;
		mStringFilterListDel = data;

	}

	private class WomenListItem {
		TextView txtWomennamedel;
		TextView txtstatusdel;
		TextView txtdate_of_admdel;
		ImageView imgwphotodel;
		ImageView imgdangerSigndel;
		ImageView imghighriskdel;
		TextView txtreferreddel;
		TextView txtreferredplacedel;
		TextView txtreferreddatedel;
		TextView txtadddatedel;
		ImageView imgmsgdel;
		TextView txtcommentcountdel;
		TextView txtpostdel;// 16Oct2016 Arpitha
		ImageView imgsms;// 09Oct2016 Arpitha
		TextView txtbreech;// 07May2017 Arpitha - v2.6
		TextView txtwomanstatus;// 06Jun2017 Arpitha

	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// 21oct2016 Arpitha
	@Override
	public int getCount() {
		return (data != null && data.size() > 0) ? data.size() : 0;
	}

	@Override
	public Women_Profile_Pojo getItem(int position) {
		return data.get(position);
	}

	// Get main view
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.dip24_adapter, null);

				holder = new WomenListItem();
				holder.txtWomennamedel = (TextView) convertView.findViewById(R.id.txtwnamedel);
				holder.txtstatusdel = (TextView) convertView.findViewById(R.id.txtstatusdel);
				holder.txtdate_of_admdel = (TextView) convertView.findViewById(R.id.txtDateOfAdmdel);
				holder.imgwphotodel = (ImageView) convertView.findViewById(R.id.imgwphotodel);
				holder.imgdangerSigndel = (ImageView) convertView.findViewById(R.id.imgdangersigndel);
				holder.imghighriskdel = (ImageView) convertView.findViewById(R.id.imgriskdel);
				holder.txtreferreddel = (TextView) convertView.findViewById(R.id.txtreferreddel);
				holder.txtreferredplacedel = (TextView) convertView.findViewById(R.id.txtplaceofreferraldel);
				holder.txtreferreddatedel = (TextView) convertView.findViewById(R.id.txtdateofreferraldel);
				holder.txtadddatedel = (TextView) convertView.findViewById(R.id.txtadddatedel);
				holder.txtcommentcountdel = (TextView) convertView.findViewById(R.id.txtcommentcountdel);
				holder.imgmsgdel = (ImageView) convertView.findViewById(R.id.imgmsgdel);
				holder.txtpostdel = (TextView) convertView.findViewById(R.id.txtpostdel);// 16Oct2016
																							// Arpitha
				holder.imgsms = (ImageView) convertView.findViewById(R.id.imgsmsdel);// 09Oct2016
				// Arpitha
				holder.txtbreech = (TextView) convertView.findViewById(R.id.txtbreechdel);// 07May2017
																							// Arpitha
																							// -
																							// v2.6
				// 06Jun2017 Arpitha
				holder.txtwomanstatus = (TextView) convertView.findViewById(R.id.txtwomanstatus);

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			if (rowItem.getregtype() == 2) {// 15Nov2016 Arpitha
				holder.txtpostdel.setVisibility(View.VISIBLE);// 16Oct2016
																// Arpitha
			} // 16Oct2016 Arpitha

			final String womenid = rowItem.getWomenId();

			holder.txtWomennamedel.setText(rowItem.getWomen_name() == null ? " " : rowItem.getWomen_name());

			// holder.txtstatusdel.setText(
			// rowItem.getDel_type() == 0 ? deltype[rowItem.getDel_type() - 1] :
			// "" + rowItem.getGestationage());
			// deltype[rowItem.getDel_type() - 1]

			if (rowItem.getDel_type() == 0) {
				if (rowItem.getGestationage() != 0) {
					holder.txtstatusdel.setText(
							rowItem.getGestationage() + " " + context.getResources().getString(R.string.weeks));

				} else {
					holder.txtstatusdel.setText(context.getResources().getString(R.string.gest_nt_known));

				}
				holder.txtadddatedel.setVisibility(View.GONE);

			} else {
				String[] deltype = context.getResources().getStringArray(R.array.del_type);
				holder.txtstatusdel.setText(deltype[rowItem.getDel_type() - 1]);
				holder.txtadddatedel.setText(rowItem.getDel_Date() == null ? " "
						: context.getResources().getString(R.string.actualdd) + " : " + Partograph_CommonClass
								.getConvertedDateFormat(rowItem.getDel_Date(), Partograph_CommonClass.defdateformat)
								+ " / " + rowItem.getDel_Time());

			}

			if (rowItem.getregtype() != 2) {
				holder.txtdate_of_admdel
						.setText(rowItem.getDate_of_admission() == null ? " "
								: context.getResources().getString(R.string.regdate) + " : "
										+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
												Partograph_CommonClass.defdateformat)
										+ " / " + rowItem.getTime_of_admission());
			} else {
				holder.txtdate_of_admdel
						.setText(
								rowItem.getDate_of_reg_entry() == null ? " "
										: context.getResources().getString(R.string.doe) + " : "
												+ Partograph_CommonClass.getConvertedDateFormat(
														rowItem.getDate_of_reg_entry(),
														Partograph_CommonClass.defdateformat));
			}

			// 08Nov2016
			image1 = rowItem.getWomen_Image();
			if (image1 != null)
				new DownloadImageTask().execute(holder);

			int comment_count = dbh.getComentsCount(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());
			if (comment_count > 0) {
				holder.imgmsgdel.setVisibility(View.VISIBLE);
				holder.txtcommentcountdel.setVisibility(View.VISIBLE);
				holder.txtcommentcountdel.setText("" + comment_count);
			}

			if (rowItem.getRisk_category() == 1) {
				holder.imghighriskdel.setImageResource(R.drawable.ic_hr);

				holder.imghighriskdel.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						try {

							if (MotionEvent.ACTION_UP == event.getAction()) {
								risk_observed = rowItem.getComments();

								if (rowItem.getRiskoptions().length() > 0
										|| (rowItem.getComments() != null && rowItem.getComments().length() > 0)) {
									Partograph_CommonClass.displayHighRiskReasons(rowItem.getRiskoptions(),
											risk_observed, context);
								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return true;
					}
				});

			}
			// else
			// holder.imghighriskdel.setImageResource(0);

			if (rowItem.isDanger()) {
				holder.imgdangerSigndel.setVisibility(View.VISIBLE);
				holder.imgdangerSigndel.setImageResource(R.drawable.ic_compl);

				holder.imgdangerSigndel.setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						try {

							ArrayList<String> arr;

							arr = dbh.getobjectid_danger(rowItem.getWomenId());

							if (MotionEvent.ACTION_UP == event.getAction()) {

								Partograph_CommonClass.displayDangervalues(arr, context);

							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}

						return true;
					}
				});
			}

			if (Activity_WomenView.referredWomen.contains(rowItem.getWomenId()))
				isRefrred = true;
			else
				isRefrred = false;

			if (isRefrred)// 15Nov2016 Arpitha
			{

				rval = new ArrayList<WomenReferral_pojo>();// 20Oct2016 Arpitha

				curref = dbh.getReferralDetails(Partograph_CommonClass.user.getUserId(), rowItem.getWomenId());
				if (curref != null && curref.getCount() > 0) {
					curref.moveToFirst();
					do {
						// isRefrred = true;
						holder.txtreferreddel.setText(context.getResources().getString(R.string.referred));

						// 18Aug2016-bindu - ref date in def date format
						holder.txtreferreddatedel
								.setText(curref.getString(13) == null ? " "
										: context.getResources().getString(R.string.refdt) + " : "
												+ Partograph_CommonClass.getConvertedDateFormat(curref.getString(13),
														Partograph_CommonClass.defdateformat)
												+ " / " + curref.getString(14));

						holder.txtreferredplacedel
								.setText(context.getResources().getString(R.string.refto) + curref.getString(6));

						holder.txtreferredplacedel.setVisibility(View.VISIBLE);
						holder.txtreferreddatedel.setVisibility(View.VISIBLE);
						holder.txtreferreddel.setVisibility(View.VISIBLE);

						wpojo = new WomenReferral_pojo();// 20Oct2016 Arpitha
						wpojo.setWomenid(curref.getString(1));
						wpojo.setWomenname(curref.getString(2));
						wpojo.setReasonforreferral(curref.getString(7));
						wpojo.setDescriptionofreferral(curref.getString(8));
						// arrpos = 0;
						// arrpos++;
						arrval.add(wpojo);

						rval.add(wpojo);
					} while (curref.moveToNext());

				}
				// else// 15Nov2016 Arpitha
				// isRefrred = false;// 15Nov2016 Arpitha

				// //18Oct2016 ARpitha

				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					holder.imgsms.setVisibility(View.VISIBLE);// 09Oct2016
																// Arpitha

					holder.imgsms.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							try {

								if (MotionEvent.ACTION_UP == event.getAction()) {
									displayConfirmationAlert(context.getResources().getString(R.string.send_mess),
											womenid, position);

								}
							} catch (NotFoundException e) {
								AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName(), e);
								e.printStackTrace();
							} catch (Exception e) {
								AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName(), e);
								e.printStackTrace();
							}
							return true;
						}
					});
				}
			}

			// 21oct2016 Arpitha
			holder.imgwphotodel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						pos = position;// 01Nov2016 Arpitha
						Partograph_CommonClass.displaySummaryDialog(data, pos, context);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			// 07May2017 Arpitha - v2.6
			if (rowItem.getNormaltype() == 2)
				holder.txtbreech.setVisibility(View.VISIBLE);
			else
				holder.txtbreech.setVisibility(View.GONE);// 07May2017 Arpitha -
			// 06Jun2017 Arpitha // v2.6
			int hours;
			if (rowItem.getregtype() != 2)
				hours = Partograph_CommonClass.getHoursBetDates(
						Partograph_CommonClass.getTodaysDate() + "_" + Partograph_CommonClass.getCurrentTime(),
						rowItem.getDate_of_admission() + "_" + rowItem.getTime_of_admission());
			else
				hours = Partograph_CommonClass.getHoursBetDates(
						Partograph_CommonClass.getTodaysDate() + "_" + Partograph_CommonClass.getCurrentTime(),
						rowItem.getDate_of_reg_entry() + "_" + rowItem.getDate_of_reg_entry().split("/")[1]);

			boolean isPartographDataEntered = dbh.getPartoData(womenid, rowItem.getUserId());
			if (isPartographDataEntered && !isRefrred && rowItem.getDel_type() == 0)
				holder.txtwomanstatus.setText(context.getResources().getString(R.string.dip));
			else if (rowItem.getDel_type() != 0)
				holder.txtwomanstatus.setText(context.getResources().getString(R.string.delivered));
//			else if (!isPartographDataEntered && !isRefrred && rowItem.getDel_type() == 0 && hours>24)
//				holder.txtwomanstatus.setText(context.getResources().getString(R.string.reg24));
			else
				holder.txtwomanstatus.setText(context.getResources().getString(R.string.status_registered));

		}

		catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public void onAnimationEnd(Animation animation) {

	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}

	@Override
	public void onAnimationStart(Animation animation) {

	}

	// Set options for risk options if high risk - 25dec2015
	protected void setRiskOptions() throws Exception {
		int i = 0;
		riskoptionsMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;

		reasonStrArr = Arrays.asList(context.getResources().getStringArray(R.array.riskoptions));

		if (reasonStrArr != null) {
			for (String str : reasonStrArr) {
				riskoptionsMap.put(str, i);
				i++;
			}
		}
	}

	// 18Oct2016 Arpitha
	// Display Confirmation to exit the screen - 09Oct2016 Arpitha
	private boolean displayConfirmationAlert(String exit_msg, final String classname, final int position)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(context);
		dialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
				+ context.getResources().getString(R.string.sms_alert) + "</font>"));
		dialog.setContentView(R.layout.alertdialog);

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		TextView txt1 = (TextView) dialog.findViewById(R.id.txtval);
		TextView txt2 = (TextView) dialog.findViewById(R.id.txtval1);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);
		txt1.setVisibility(View.GONE);
		txt2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				try {
					display_messagedialog(classname, position);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {

					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		return true;
	}

	// 18Oct2016 Arpitha
	public void display_messagedialog(final String womenid, final int position) throws Exception {
		try {
			final Dialog sms_dialog = new Dialog(context);
			sms_dialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
					+ context.getResources().getString(R.string.send_sms) + "</font>"));// 08Feb2017
																						// Arpitha

			sms_dialog.setContentView(R.layout.alertdialog_sms);

			// Dialog d = alertDialog.show();
			int dividerId = sms_dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = sms_dialog.findViewById(dividerId);
			divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

			sms_dialog.show();

			// final TextView txtdialog = (TextView)
			// sms_dialog.findViewById(R.id.txtdialog);
			TextView txt1 = (TextView) sms_dialog.findViewById(R.id.txtval);
			TextView txt2 = (TextView) sms_dialog.findViewById(R.id.txtval1);
			// Button imgbtnyes = (Button)
			// sms_dialog.findViewById(R.id.imgbtnyes);
			final EditText etphno = (EditText) sms_dialog.findViewById(R.id.etphnno);

			etmesss = (EditText) sms_dialog.findViewById(R.id.etmess);
			EditText etopt = (EditText) sms_dialog.findViewById(R.id.etreasonoptions);
			TextView txtreason = (TextView) sms_dialog.findViewById(R.id.txtval6);
			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);

			// String RefResDisplay = "";
			String womanname = "";
			String desc = "";
			String reasonforref = null;
			final WomenReferral_pojo wpojo_sms;

			p_no = "";
			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(option);
			for (int i = 0; i < values.size(); i++) {

				// String s = values.get(i);
				p_no = p_no + values.get(i) + ",";
			}

			etphno.setText(p_no);

			Cursor c = dbh.getReferralDetails(Partograph_CommonClass.user.getUserId(), womenid);

			if (c != null && c.getCount() > 0) {
				c.moveToFirst();

				womanname = c.getString(2);
				desc = c.getString(8);
				reasonforref = c.getString(7);
			}

			txt1.setText(womanname);
			txt2.setText(desc);
			String selectedriskoption = reasonforref;// 20oct2016
			// Arpitha

			ArrayList<String> op = new ArrayList<String>();
			String admResDisplay = "";

			String[] admres = selectedriskoption.split(",");
			String[] admResArr = null;

			admResArr = context.getResources().getStringArray(R.array.reasonforreferralvalues);

			if (admResArr != null && admres != null) {// 09Nov2016 Arpitha -
				// checking null for
				// admres
				if (admres != null && admres.length > 0) {
					for (String str : admres) {
						if (str != null && str.length() > 0 && (!str.equalsIgnoreCase("null")))// 09Nov2016
							// Arpitha
							// -
							// checking
							// null
							// for
							// str
							admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + "\n";

					}
				}
			}

			if (admResDisplay.length() > 0) {
				etopt.setText(admResDisplay);
			} else {
				etopt.setVisibility(View.GONE);
				txtreason.setVisibility(View.GONE);
			}

			wpojo_sms = new WomenReferral_pojo();
			wpojo_sms.setWomenname(womanname);
			wpojo_sms.setDescriptionofreferral(desc);
			wpojo_sms.setReasonforreferral(reasonforref);

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					try {

						if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {

							if (values != null && values.size() > 0) {
								InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(), womenid,
										wpojo_sms);
								if (isMessageLogsaved) {
									Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
											Toast.LENGTH_LONG).show();
									sendSMSFunction();
									sms_dialog.cancel();
								}
							} else {
								if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() >= 10) {
									InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(),
											womenid, wpojo_sms);
									if (isMessageLogsaved) {
										Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
												Toast.LENGTH_LONG).show();
										sendSMSFunction();
									}
									sms_dialog.cancel();

								} else if (etphno.getText().toString().length() <= 0) {
									Toast.makeText(context, context.getResources().getString(R.string.entr_phno),
											Toast.LENGTH_LONG).show();
								} else if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() < 10) {
									Toast.makeText(context,
											context.getResources().getString(R.string.enter_valid_phn_no),
											Toast.LENGTH_LONG).show();
								}
							}

						} else
							Toast.makeText(context, context.getResources().getString(R.string.no_sim),
									Toast.LENGTH_LONG).show();

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgcancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sms_dialog.cancel();
				}
			});
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Insert to record to Message Log
	private void InserttblMessageLog(String phoneno, String comments, String womanid, WomenReferral_pojo data)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String message = "";

		if (data != null) {
			// c.moveToFirst();
			String RefResDisplay = "";
			String womanname = data.getWomenname();
			String desc = data.getDescriptionofreferral();
			String reasonforref = data.getReasonforreferral();
			if (reasonforref != null && reasonforref.length() > 0) {

				String[] admres = reasonforref.split(",");
				String[] admResArr = null;

				admResArr = context.getResources().getStringArray(R.array.reasonforreferralvaluessms);

				if (admResArr != null) {
					if (admres != null && admres.length > 0) {
						for (String str : admres) {
							if (str != null && str.length() > 0)
								RefResDisplay = RefResDisplay + admResArr[Integer.parseInt(str)] + ",";

						}
					}
				}
			}

			if (comments.length() > 0)
				comments = " Cmnts: " + comments; // 11Sep2016 - bindu chk comm
													// length

			message = "ePartograph Ref :- " + womanname + ", " + " Reason: " + RefResDisplay + " Desc: " + desc
					+ comments;

			ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

			if (phoneno.length() > 0) {
				String[] phn = phoneno.split("\\,");
				for (int i = 0; i < phn.length; i++) {
					String num = phn[i];
					if (num != null && num.length() > 0) {
						MessageLogPojo mlp = new MessageLogPojo(Partograph_CommonClass.user.getUserId(), womanid, num,
								desc, RefResDisplay, message, 1, 0);
						mlpArr.add(mlp);
					}
				}
			}

			if (mlpArr != null) {
				for (MessageLogPojo mlpp : mlpArr) {
					dbh.db.beginTransaction();
					int transId = dbh.iCreateNewTrans(Partograph_CommonClass.user.getUserId());// 18Oct2016
					// Arpitha
					boolean isinserted = dbh.insertToMessageLog(mlpp, transId);// 18Oct2016
																				// Arpitha
					if (isinserted) {
						isMessageLogsaved = true;
						dbh.iNewRecordTrans(Partograph_CommonClass.user.getUserId(), transId,
								Partograph_DB.TBL_MESSAGELOG);// 18Oct2016
						// Arpitha
						commitTrans();
					} else
						throw new Exception("InserttblMessageLog(): Failed to Insert Message to tblMessageLog ");
				}
			}
		}
	}

	/**
	 * This method invokes after successfull save of record Sends SMS to the
	 * Specified Number
	 */
	private void sendSMSFunction() throws Exception {
		new SendSMS();
	}

	private void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		calSyncMtd();
		Toast.makeText(context, context.getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
	}

	// Sync - 18Oct2016 Arpitha
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	// 08Nov2016
	public class DownloadImageTask extends AsyncTask<WomenListItem, Void, Bitmap> {

		ImageView imageView = null;

		protected Bitmap doInBackground(WomenListItem... item) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.imageView = (ImageView) item[0].imgwphotodel;
			return getBitmapDownloaded();
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null)
				imageView.setImageBitmap(result);
		}

		/** This function downloads the image and returns the Bitmap **/
		private Bitmap getBitmapDownloaded() {
			Bitmap btmp = null;
			if (image1 != null) {
				btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
			}
			return btmp;
		}
	}

	private class ValueFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				ArrayList<Women_Profile_Pojo> filterList = new ArrayList<Women_Profile_Pojo>();
				for (int i = 0; i < mStringFilterListDel.size(); i++) {
					if (mStringFilterListDel.get(i).getWomen_name().toUpperCase()
							.contains(constraint.toString().toUpperCase())
							|| mStringFilterListDel.get(i).getPhone_No()
									.contains(constraint.toString().toUpperCase())) {

						filterList.add(mStringFilterListDel.get(i));
					}

				}
				results.count = filterList.size();
				results.values = filterList;
			} else {
				results.count = mStringFilterListDel.size();
				results.values = mStringFilterListDel;
			}
			return results;

		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			data = (ArrayList<Women_Profile_Pojo>) results.values;
			notifyDataSetChanged();
		}

	}

	@Override
	public Filter getFilter() {
		if (valueFilter == null) {
			valueFilter = new ValueFilter();
		}
		return valueFilter;
	}

}

/*
 * package com.bc.partograph.regwomenlist;
 * 
 * import java.util.ArrayList; import java.util.Arrays; import
 * java.util.HashMap; import java.util.LinkedHashMap; import java.util.List;
 * 
 * import com.bc.partograph.common.AppContext; import
 * com.bc.partograph.common.MultiSelectionSpinner; import
 * com.bc.partograph.common.Partograph_CommonClass; import
 * com.bc.partograph.common.Partograph_DB; import com.bluecrimson.partograph.R;
 * import com.bluecrimson.partograph.WomenReferral_pojo; import
 * com.bluecrimson.partograph.Women_Profile_Pojo;
 * 
 * import android.app.Activity; import android.app.AlertDialog; import
 * android.app.Dialog; import android.content.Context; import
 * android.content.DialogInterface; import android.graphics.Bitmap; import
 * android.graphics.BitmapFactory; import android.os.AsyncTask; import
 * android.text.Html; import android.view.LayoutInflater; import
 * android.view.MotionEvent; import android.view.View; import
 * android.view.View.OnClickListener; import android.view.View.OnTouchListener;
 * import android.view.ViewGroup; import android.view.Window; import
 * android.view.animation.Animation; import
 * android.view.animation.Animation.AnimationListener; import
 * android.widget.ArrayAdapter; import android.widget.EditText; import
 * android.widget.Filter; import android.widget.ImageButton; import
 * android.widget.ImageView; import android.widget.ListView; import
 * android.widget.TextView;
 * 
 * public class RecentCustomListAdapterREG24 extends
 * ArrayAdapter<Women_Profile_Pojo> implements AnimationListener { Context
 * context; ArrayList<Women_Profile_Pojo> data; Partograph_DB dbh; String
 * todaysDate; String currTime; // Animation Animation animBlink;
 * ArrayList<String> referral_women; public static LinkedHashMap<String,
 * Integer> riskoptionsMap; EditText etmesss; ArrayList<WomenReferral_pojo>
 * rpojo; int mSelectedRow; MultiSelectionSpinner mspnreasonforreferral; String
 * admResDisplay = ""; String strCurrentTime; String user_id; String lastdate;
 * String lasttime; boolean ispulsetimevalid = false; String currenttime; String
 * islasttime; boolean isfhrtimevalid = false; boolean isdilatationtimevalid =
 * false; boolean iscontractiontimevalid = false; String risk_observed; String
 * ref_phno; int option = 2; ArrayList<String> values; String reg_phno; String
 * p_no = ""; ArrayList<Women_Profile_Pojo> mStringFilterList; ValueFilter
 * valueFilter; boolean isfourthstageoflabor = false;
 * 
 * // 20Oct2016 Arpitha boolean ispartoentered = false; ImageView imgparto;
 * boolean isapgarentered = false; boolean isstagesoflabor = false; ImageView
 * img1; ImageView img2; ImageView img3; ImageView img4; ImageView img5;
 * ImageView img6; ImageView img7;// 01nov2016 Arpitha // 1Nov2016 Arpitha
 * String wname; int age; String regdate; int gest; String gravida; String risk;
 * String delstatus; String deldeldate; int pos;
 * 
 * private static byte[] image1 = null;// 08Nov2016
 * 
 * ArrayList<String> val;// 17Jan2017 Arpitha String womenId;// 17JAn2017
 * Arpitha
 * 
 * String message;
 * 
 * HashMap<String, ArrayList<String>> partoDataDIP24 = new HashMap<String,
 * ArrayList<String>>();// 01Feb2017 // Arpitha
 * 
 * // constructor public RecentCustomListAdapterREG24(Context context, int
 * textViewResourceId, ArrayList<Women_Profile_Pojo> data, Partograph_DB dbh) {
 * super(context, textViewResourceId, data); this.context = context; this.dbh =
 * dbh; this.data = data; this.mStringFilterList = data;
 * 
 * }
 * 
 * private class WomenListItem { TextView txtWomennamedip24; TextView
 * txtstatusdip24; TextView txtdate_of_admdip24; ImageView imgwphotodip24;
 * ImageView imgdangerSigndip24; ImageView imghighriskdip24; ImageView
 * imgmsgdip24; TextView txtcommentcountdip24; TextView
 * txtdelstatusnotupdateddip24; ImageView imgnotificationdip24;
 * 
 * TextView txtwomanstatusdip24;// 21March2017 Arpitha TextView txtbreech;//
 * 07May2017 Arpitha - v2.6 }
 * 
 * @Override public int getViewTypeCount() { return getCount(); }
 * 
 * @Override public int getItemViewType(int position) { return position; }
 * 
 * @Override public int getCount() { return (data != null && data.size() > 0) ?
 * data.size() : 0; }
 * 
 * @Override public Women_Profile_Pojo getItem(int position) { return
 * data.get(position); }
 * 
 * // Get main view
 * 
 * @Override public View getView(final int position, View convertView, ViewGroup
 * parent) { AppContext.addToTrace( new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName());
 * 
 * try { WomenListItem holder = null; final Women_Profile_Pojo rowItem =
 * getItem(position);
 * 
 * LayoutInflater mInflater = (LayoutInflater)
 * context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
 * 
 * if (convertView == null) { convertView =
 * mInflater.inflate(R.layout.dip24_adapter, null);
 * 
 * holder = new WomenListItem(); holder.txtWomennamedip24 = (TextView)
 * convertView.findViewById(R.id.txtwnamedip24); holder.txtstatusdip24 =
 * (TextView) convertView.findViewById(R.id.txtstatusdip24);
 * holder.txtdate_of_admdip24 = (TextView)
 * convertView.findViewById(R.id.txtDateOfAdmdip24); holder.imgwphotodip24 =
 * (ImageView) convertView.findViewById(R.id.imgwphotodip24);
 * holder.imgdangerSigndip24 = (ImageView)
 * convertView.findViewById(R.id.imgdangersigndip24); holder.imghighriskdip24 =
 * (ImageView) convertView.findViewById(R.id.imgriskdip24); holder.imgmsgdip24 =
 * (ImageView) convertView.findViewById(R.id.imgmsgdip24);
 * holder.txtcommentcountdip24 = (TextView)
 * convertView.findViewById(R.id.txtcommentcountdip24);
 * holder.txtdelstatusnotupdateddip24 = (TextView) convertView
 * .findViewById(R.id.txtdelstatusnotupdateddip24); holder.imgnotificationdip24
 * = (ImageView) convertView.findViewById(R.id.imgnotificationdip24);
 * 
 * holder.txtwomanstatusdip24 = (TextView)
 * convertView.findViewById(R.id.txtregdip24);// 21March2017 // Arpitha
 * holder.txtbreech = (TextView)
 * convertView.findViewById(R.id.txtbreechdip24);// 07May2017 // Arpitha // - //
 * v2.6 convertView.setTag(holder); } else holder = (WomenListItem)
 * convertView.getTag();
 * 
 * final String womenId = rowItem.getWomenId();
 * 
 * user_id = rowItem.getUserId();// 27March2017 Arpitha
 * 
 * // holder.imgmsgdip24.setVisibility(View.GONE);
 * 
 * holder.txtWomennamedip24.setText(rowItem.getWomen_name() == null ? " " :
 * rowItem.getWomen_name());
 * 
 * if (rowItem.getGestationage() == 0) { holder.txtstatusdip24.setText(
 * rowItem.getDel_type() == 0 ?
 * context.getResources().getString(R.string.gest_nt_known) : "");
 * 
 * } else {
 * 
 * holder.txtstatusdip24.setText(rowItem.getDel_type() == 0 ?
 * rowItem.getGestationage() + " " +
 * context.getResources().getString(R.string.weeks) : "");
 * 
 * }
 * 
 * holder.txtdate_of_admdip24 .setText(rowItem.getDate_of_admission() == null ?
 * " " : context.getResources().getString(R.string.regdate) + " : " +
 * Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
 * Partograph_CommonClass.defdateformat) + "/" +
 * rowItem.getTime_of_admission());
 * 
 * // 08Nov2016 image1 = rowItem.getWomen_Image();
 * 
 * try { if (image1 != null)
 * 
 * new DownloadImageTask().execute(holder); } catch (Exception e) {
 * e.printStackTrace(); }
 * 
 * int risk_cat = rowItem.getRisk_category();
 * 
 * int comment_count = dbh.getComentsCount(womenId,
 * Partograph_CommonClass.user.getUserId());// 17Jan2017 // Arpitha // - //
 * changed // rowItem.getwomenid // to // womenId if (comment_count > 0) {
 * holder.imgmsgdip24.setVisibility(View.VISIBLE);
 * holder.txtcommentcountdip24.setVisibility(View.VISIBLE);
 * holder.txtcommentcountdip24.setText("" + comment_count); } else {
 * holder.imgmsgdip24.setVisibility(View.INVISIBLE);
 * holder.txtcommentcountdip24.setVisibility(View.INVISIBLE);
 * holder.txtcommentcountdip24.setText(""); }
 * 
 * 
 * holder.txtdelstatusnotupdateddip24.setText(context.getResources().getString(R
 * .string.delstatusnotupdated));
 * 
 * if (risk_cat == 1) {
 * holder.imghighriskdip24.setImageResource(R.drawable.ic_hr);
 * 
 * // updated on 14july2016 by Arpitha risk_observed = rowItem.getComments();
 * 
 * holder.imghighriskdip24.setOnTouchListener(new OnTouchListener() {
 * 
 * @Override public boolean onTouch(View v, MotionEvent event) { try {
 * 
 * if (MotionEvent.ACTION_UP == event.getAction()) { // risk_observed =
 * rowItem.getComments();
 * 
 * if (rowItem.getRiskoptions().length() > 0 || (rowItem.getComments() != null
 * && rowItem.getComments().length() > 0)) {
 * displayConfirmationAlert_sms(rowItem.getRiskoptions()); } } } catch
 * (Exception e) { AppContext.addLog(new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName(), e); e.printStackTrace(); } return true; }
 * }); }
 * 
 * // else // holder.imghighriskdip24.setImageResource(0);
 * 
 * if (rowItem.isDanger()) {
 * holder.imgdangerSigndip24.setVisibility(View.VISIBLE);
 * holder.imgdangerSigndip24.setImageResource(R.drawable.ic_compl);
 * 
 * holder.imgdangerSigndip24.setOnTouchListener(new View.OnTouchListener() {
 * 
 * @Override public boolean onTouch(View v, MotionEvent event) {
 * 
 * try {
 * 
 * ArrayList<String> arr;
 * 
 * arr = dbh.getobjectid_danger(womenId);// 17Jan2017 // Arpitha - // chnaged //
 * rowItem.getwomenId // to // womenId
 * 
 * if (MotionEvent.ACTION_UP == event.getAction()) {
 * 
 * displayConfirmationAlert_dangersign(arr, "");
 * 
 * } } catch (Exception e) { AppContext.addLog(new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName(), e); e.printStackTrace(); }
 * 
 * return true; } }); } // else //
 * holder.imgdangerSigndip24.setImageResource(0);
 * 
 * // 17Jan2017 Arpitha val = new ArrayList<String>(); val =
 * dbh.getNotificationData(womenId, Partograph_CommonClass.user.getUserId());
 * 
 * partoDataDIP24.put(womenId, val);// 01Feb2017 Arpitha
 * 
 * if (val.size() > 0 && (val.contains("1") || val.contains("2"))) {
 * holder.imgnotificationdip24.setVisibility(View.VISIBLE);
 * 
 * holder.imgnotificationdip24.setOnTouchListener(new OnTouchListener() {
 * 
 * @Override public boolean onTouch(View v, MotionEvent event) { if
 * (MotionEvent.ACTION_UP == event.getAction()) {
 * 
 * try {
 * 
 * if (partoDataDIP24 != null && partoDataDIP24.size() > 0) { message = "";
 * 
 * String s = womenId;
 * 
 * for (int i = 0; i < partoDataDIP24.get(womenId).size(); i++) {
 * 
 * if (partoDataDIP24.get(womenId).get(i).equalsIgnoreCase("2") ||
 * partoDataDIP24.get(womenId).get(i).equalsIgnoreCase("1")) {
 * 
 * if (i == 0) { message = message + "\n" +
 * context.getResources().getString(R.string.fhr); } else if (i == 1) { message
 * = message + "\n" + context.getResources().getString(R.string.dilatation); }
 * else if (i == 2) { message = message + "\n" +
 * context.getResources().getString(R.string.contraction); } else { message =
 * message + "\n" + context.getResources().getString(R.string.pulse_bp); }
 * 
 * }
 * 
 * } }
 * 
 * displayAlertDialog(
 * context.getResources().getString(R.string.notify_alert_mess) + "\n" +
 * message);
 * 
 * } catch (Exception e) { e.printStackTrace(); }
 * 
 * } return true; } });
 * 
 * }
 * 
 * // 20oct2016 Arpitha holder.imgwphotodip24.setOnClickListener(new
 * OnClickListener() {
 * 
 * @Override public void onClick(View v) { // TODO Auto-generated method stub
 * 
 * try { pos = position;// 01Nov2016 Arpitha
 * displayConfirmationAlert_summary("", ""); } catch (Exception e) { // TODO
 * Auto-generated catch block e.printStackTrace(); }
 * 
 * } }); // bindu
 * 
 * // 21March2017 Arpitha boolean isPartoDildataAvailable =
 * dbh.getPartoDilData(womenId, user_id); if (isPartoDildataAvailable)
 * 
 * holder.txtwomanstatusdip24.setText(context.getResources().getString(R.string.
 * status_dip)); else
 * 
 * holder.txtwomanstatusdip24.setText(context.getResources().getString(R.string.
 * status_registered));// 21March2017 // Arpitha
 * 
 * // 07May2017 Arpitha - v2.6 if (rowItem.getNormaltype() == 2)
 * holder.txtbreech.setVisibility(View.VISIBLE); else
 * holder.txtbreech.setVisibility(View.GONE);// 07May2017 Arpitha - // v2.6
 * 
 * }
 * 
 * catch (Exception e) { AppContext.addLog( new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName(), e); e.printStackTrace(); }
 * 
 * return convertView; }
 * 
 * @Override public void onAnimationEnd(Animation animation) {
 * 
 * }
 * 
 * @Override public void onAnimationRepeat(Animation animation) {
 * 
 * }
 * 
 * @Override public void onAnimationStart(Animation animation) {
 * 
 * }
 * 
 * // Set options for risk options if high risk - 25dec2015 protected void
 * setRiskOptions() throws Exception { int i = 0; riskoptionsMap = new
 * LinkedHashMap<String, Integer>(); List<String> reasonStrArr = null;
 * 
 * reasonStrArr =
 * Arrays.asList(context.getResources().getStringArray(R.array.riskoptions));
 * 
 * if (reasonStrArr != null) { for (String str : reasonStrArr) {
 * riskoptionsMap.put(str, i); i++; } } }
 * 
 * // Display Confirmation to exit the screen private boolean
 * displayConfirmationAlert_sms(String exit_msg) throws Exception {
 * AppContext.addToTrace( new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName()); try {
 * 
 * // 8july2016 by Arpitha ArrayList<String> op = new ArrayList<String>();
 * String[] riskoptions = exit_msg.split(","); String[] riskoptArr = null;
 * 
 * riskoptArr =
 * context.getResources().getStringArray(R.array.riskoptionsvalues);
 * 
 * if (riskoptArr != null) { if (riskoptions != null && riskoptions.length > 0)
 * { for (String str : riskoptions) { if (str != null && str.length() > 0)
 * op.add(riskoptArr[Integer.parseInt(str)]); } } } if (risk_observed != null &&
 * risk_observed.length() > 0) {
 * op.add(context.getResources().getString(R.string.other_risk_observed) + " " +
 * risk_observed); }
 * 
 * AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
 * 
 * alertDialog.setTitle(Html.fromHtml("<font color='" +
 * context.getResources().getColor(R.color.appcolor) + "'>" +
 * context.getResources().getString(R.string.highriskreasons) + "</font>"));//
 * 02Feb2017
 * 
 * View convertView =
 * LayoutInflater.from(context).inflate(R.layout.alertdialog_danger, null);
 * ListView lv = (ListView) convertView.findViewById(R.id.listoptions);
 * ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
 * android.R.layout.simple_list_item_1, op);
 * alertDialog.setView(convertView).setCancelable(true) //
 * .setTitle(context.getResources().getString(R.string.highriskreasons))
 * .setPositiveButton(context.getResources().getString(R.string.ok), new
 * DialogInterface.OnClickListener() {
 * 
 * @Override public void onClick(DialogInterface dialog, int which) {
 * dialog.cancel(); } }); lv.setAdapter(adapter); Dialog d = alertDialog.show();
 * int dividerId =
 * d.getContext().getResources().getIdentifier("android:id/titleDivider", null,
 * null); View divider = d.findViewById(dividerId);
 * divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor))
 * ;// 08Feb2017 d.show();
 * 
 * } catch (Exception e) { AppContext.addLog( new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName(), e); e.printStackTrace(); }
 * 
 * return true; }
 * 
 * // Display Confirmation to exit the screen private boolean
 * displayConfirmationAlert_dangersign(ArrayList<String> exit_msg, final String
 * classname) throws Exception { AppContext.addToTrace( new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName());
 * 
 * ArrayList<String> op = new ArrayList<String>(); ArrayList<String> riskoptions
 * = exit_msg; String[] riskoptArr = null;
 * 
 * riskoptArr = context.getResources().getStringArray(R.array.parameter);
 * 
 * String riskoptionDisplay = ""; if (riskoptArr != null) { if (riskoptions !=
 * null && riskoptions.size() > 0) { for (String str : riskoptions) { if (str !=
 * null && str.length() > 0) op.add(riskoptArr[Integer.parseInt(str)]); } } }
 * 
 * AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
 * 
 * alertDialog.setTitle(Html.fromHtml("<font color='" +
 * context.getResources().getColor(R.color.appcolor) + "'>" +
 * context.getResources().getString(R.string.partographparametersindanger) +
 * "</font>"));// 02Feb2017
 * 
 * View convertView =
 * LayoutInflater.from(context).inflate(R.layout.alertdialog_danger, null);
 * ListView lv = (ListView) convertView.findViewById(R.id.listoptions);
 * ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
 * android.R.layout.simple_list_item_1, op);
 * alertDialog.setView(convertView).setCancelable(true) //
 * .setTitle(context.getResources().getString(R.string.
 * partographparametersindanger))
 * .setPositiveButton(context.getResources().getString(R.string.ok), new
 * DialogInterface.OnClickListener() {
 * 
 * @Override public void onClick(DialogInterface dialog, int which) {
 * dialog.cancel(); } }); lv.setAdapter(adapter);
 * 
 * Dialog d = alertDialog.show(); int dividerId =
 * d.getContext().getResources().getIdentifier("android:id/titleDivider", null,
 * null); View divider = d.findViewById(dividerId);
 * divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor))
 * ;// 08Feb2017 // Arpitha
 * 
 * d.show();
 * 
 * return true; }
 * 
 * private class ValueFilter extends Filter {
 * 
 * @Override protected FilterResults performFiltering(CharSequence constraint) {
 * FilterResults results = new FilterResults();
 * 
 * if (constraint != null && constraint.length() > 0) {
 * ArrayList<Women_Profile_Pojo> filterList = new
 * ArrayList<Women_Profile_Pojo>(); for (int i = 0; i <
 * mStringFilterList.size(); i++) { if
 * (mStringFilterList.get(i).getWomen_name().toUpperCase()
 * .contains(constraint.toString().toUpperCase())) {
 * 
 * filterList.add(mStringFilterList.get(i)); }
 * 
 * } results.count = filterList.size(); results.values = filterList; } else {
 * results.count = mStringFilterList.size(); results.values = mStringFilterList;
 * } return results;
 * 
 * }
 * 
 * @Override protected void publishResults(CharSequence constraint,
 * FilterResults results) { data = (ArrayList<Women_Profile_Pojo>)
 * results.values; // notifyDataSetChanged(); }
 * 
 * }
 * 
 * @Override public Filter getFilter() { if (valueFilter == null) { valueFilter
 * = new ValueFilter(); } return valueFilter; }
 * 
 * // 20Oct2016 Arpitha private boolean displayConfirmationAlert_summary(String
 * exit_msg, final String classname) throws Exception { AppContext.addToTrace(
 * new RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName()); final Dialog dialog = new Dialog(context);
 * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
 * dialog.setContentView(R.layout.activity_summary);
 * 
 * dialog.show();
 * 
 * img1 = (ImageView) dialog.findViewById(R.id.img1); img2 = (ImageView)
 * dialog.findViewById(R.id.img2); img3 = (ImageView)
 * dialog.findViewById(R.id.img3); img4 = (ImageView)
 * dialog.findViewById(R.id.img4); img5 = (ImageView)
 * dialog.findViewById(R.id.img5); img6 = (ImageView)
 * dialog.findViewById(R.id.img6); img7 = (ImageView)
 * dialog.findViewById(R.id.img7);// 01nov2016 Arpitha
 * 
 * // 01Nov2016 Arpitha TextView txtname = (TextView)
 * dialog.findViewById(R.id.wname); TextView txtage = (TextView)
 * dialog.findViewById(R.id.wage); TextView txtregdate = (TextView)
 * dialog.findViewById(R.id.wdoa); TextView txtgest = (TextView)
 * dialog.findViewById(R.id.wgest); TextView txtgravida = (TextView)
 * dialog.findViewById(R.id.wgravida); TextView txtrisk = (TextView)
 * dialog.findViewById(R.id.wtrisk); TextView txtdelstatus = (TextView)
 * dialog.findViewById(R.id.wdelstatus); TextView txtdeldate = (TextView)
 * dialog.findViewById(R.id.wdeldate); txtdeldate.setVisibility(View.GONE);
 * txtdelstatus.setVisibility(View.GONE); wname = data.get(pos).getWomen_name()
 * == null ? " " : data.get(pos).getWomen_name(); age = data.get(pos).getAge();
 * regdate = Partograph_CommonClass.getConvertedDateFormat(data.get(pos).
 * getDate_of_admission(), Partograph_CommonClass.defdateformat) + "/" +
 * data.get(pos).getTime_of_admission(); gravida =
 * context.getResources().getString(R.string.gravida_short_label) + ":" +
 * data.get(pos).getGravida() + "," +
 * context.getResources().getString(R.string.para_short_label) + ":" +
 * data.get(pos).getPara(); if (data.get(pos).getRisk_category() == 0) risk =
 * context.getResources().getString(R.string.low); else risk =
 * context.getResources().getString(R.string.high); gest =
 * data.get(pos).getGestationage(); txtname.setText(wname); txtage.setText("" +
 * age + context.getResources().getString(R.string.yrs));
 * txtgravida.setText(gravida);
 * txtregdate.setText(context.getResources().getString(R.string.reg) + ":" +
 * regdate); if (gest == 0) {
 * txtgest.setText(context.getResources().getString(R.string.gest_age) + ":" +
 * context.getResources().getString(R.string.notknown)); } else// 18Nov2016
 * Arpitha { if (data.get(pos).getGest_age_days() == 0) {
 * txtgest.setText(context.getResources().getString(R.string.gest_age) + ":" +
 * "" + gest); } else
 * txtgest.setText(context.getResources().getString(R.string.gest_age) + ":" +
 * "" + gest + " " + context.getResources().getString(R.string.wks) +
 * data.get(pos).getGest_age_days() +
 * context.getResources().getString(R.string.days)); } // 18Nov2016 Arpitha
 * txtrisk.setText(context.getResources().getString(R.string.risk_short_label) +
 * ":" + risk); // 01Nov2016 Arpitha
 * 
 * ImageButton imgbtncanceldialog = (ImageButton)
 * dialog.findViewById(R.id.imgbtncanceldialog);
 * 
 * // String del, ref, apgar, thirdstage, fourthstage, parto = null;
 * 
 * ArrayList<String> values = dbh.getentereddata(data.get(pos).getWomenId());
 * 
 * if (values.get(0).equalsIgnoreCase("yes")) {
 * img1.setImageResource(R.drawable.ic_ok); } else
 * img1.setImageResource(R.drawable.ic_cancel);
 * 
 * if (values.get(4).equalsIgnoreCase("yes")) {
 * img2.setImageResource(R.drawable.ic_ok); } else
 * img2.setImageResource(R.drawable.ic_cancel);
 * 
 * if (values.get(5).equalsIgnoreCase("yes")) {
 * img3.setImageResource(R.drawable.ic_ok); } else
 * img3.setImageResource(R.drawable.ic_cancel); if
 * (values.get(2).equalsIgnoreCase("yes")) {
 * img4.setImageResource(R.drawable.ic_ok); } else
 * img4.setImageResource(R.drawable.ic_cancel); if
 * (values.get(1).equalsIgnoreCase("yes")) {
 * img5.setImageResource(R.drawable.ic_ok); } else
 * img5.setImageResource(R.drawable.ic_cancel); if
 * (values.get(3).equalsIgnoreCase("yes")) {
 * img6.setImageResource(R.drawable.ic_ok); } else
 * img6.setImageResource(R.drawable.ic_cancel); if
 * (values.get(6).equalsIgnoreCase("yes")) {
 * img7.setImageResource(R.drawable.ic_ok); } else
 * img7.setImageResource(R.drawable.ic_cancel);// 01Nov2016 Arpitha
 * 
 * imgbtncanceldialog.setOnClickListener(new OnClickListener() {
 * 
 * @Override public void onClick(View v) { // TODO Auto-generated method stub
 * dialog.cancel();
 * 
 * } });
 * 
 * return true; }
 * 
 * // 08Nov2016 public class DownloadImageTask extends AsyncTask<WomenListItem,
 * Void, Bitmap> {
 * 
 * ImageView imageView = null;
 * 
 * protected Bitmap doInBackground(WomenListItem... item) { try {
 * Thread.sleep(100); } catch (InterruptedException e) { // TODO Auto-generated
 * catch block e.printStackTrace(); } try { this.imageView = (ImageView)
 * item[0].imgwphotodip24;
 * 
 * } catch (Exception e) { e.printStackTrace(); } return getBitmapDownloaded();
 * }
 * 
 * protected void onPostExecute(Bitmap result) { try { if (result != null)
 * imageView.setImageBitmap(result); } catch (Exception e) {
 * e.printStackTrace(); } }
 * 
 *//** This function downloads the image and returns the Bitmap **//*
																	 * private
																	 * Bitmap
																	 * getBitmapDownloaded
																	 * () {
																	 * Bitmap
																	 * btmp =
																	 * null;
																	 * 
																	 * try {
																	 * Thread.
																	 * sleep(100
																	 * ); }
																	 * catch
																	 * (InterruptedException
																	 * e) { //
																	 * TODO
																	 * Auto-
																	 * generated
																	 * catch
																	 * block e.
																	 * printStackTrace
																	 * (); }
																	 * 
																	 * try {
																	 * 
																	 * if
																	 * (image1
																	 * != null)
																	 * { btmp =
																	 * Bitmap.
																	 * createScaledBitmap
																	 * (
																	 * BitmapFactory
																	 * .
																	 * decodeByteArray
																	 * (image1,
																	 * 0,
																	 * image1.
																	 * length),
																	 * 64, 64,
																	 * false); }
																	 * } catch
																	 * (Exception
																	 * e) { e.
																	 * printStackTrace
																	 * (); }
																	 * return
																	 * btmp; }
																	 * 
																	 * }
																	 * 
																	 * //
																	 * 17JAn2017
																	 * Arpitha
																	 * // Alert
																	 * dialog
																	 * private
																	 * void
																	 * displayAlertDialog
																	 * (String
																	 * message)
																	 * throws
																	 * Exception
																	 * {
																	 * AlertDialog
																	 * .Builder
																	 * alertDialogBuilder
																	 * = new
																	 * AlertDialog
																	 * .Builder(
																	 * context);
																	 * alertDialogBuilder
																	 * .setTitle
																	 * (context.
																	 * getResources
																	 * ().
																	 * getString
																	 * (R.string
																	 * .alert));
																	 * 
																	 * // set
																	 * dialog
																	 * message
																	 * alertDialogBuilder
																	 * .
																	 * setMessage
																	 * (message)
																	 * 
																	 * .setPositiveButton
																	 * (context.
																	 * getResources
																	 * ().
																	 * getString
																	 * (R.string
																	 * .ok), new
																	 * DialogInterface
																	 * .
																	 * OnClickListener
																	 * () {
																	 * public
																	 * void
																	 * onClick(
																	 * DialogInterface
																	 * dialog,
																	 * int id) {
																	 * // if
																	 * this
																	 * button is
																	 * clicked,
																	 * just
																	 * close //
																	 * the
																	 * dialog
																	 * box and
																	 * do
																	 * nothing
																	 * dialog.
																	 * cancel();
																	 * } });
																	 * 
																	 * // create
																	 * alert
																	 * dialog
																	 * AlertDialog
																	 * alertDialog
																	 * =
																	 * alertDialogBuilder
																	 * .create()
																	 * ; // show
																	 * it
																	 * alertDialog
																	 * .show();
																	 * }
																	 * 
																	 * 
																	 * 
																	 * 
																	 * }
																	 */
package com.bc.partograph.regwomenlist;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.MessageLogPojo;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.common.SendSMS;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class RecentCustomListAdapterReferred extends ArrayAdapter<Women_Profile_Pojo> implements AnimationListener {
	Context context;
	ArrayList<Women_Profile_Pojo> data;
	Partograph_DB dbh;
	public static LinkedHashMap<String, Integer> riskoptionsMap;
	EditText etmesss;
	ArrayList<WomenReferral_pojo> rpojo = new ArrayList<WomenReferral_pojo>();
	int mSelectedRow;
	String user_id;
	String risk_observed;
	String ref_phno;
	int option = 2;
	ArrayList<String> values;
	String reg_phno;
	String p_no = "";
	// 14Aug2016 - bindu
	Cursor curref;
	// updated 25Aug2016 - bindu
	boolean isMessageLogsaved = false;
	// 21Oct2016 Arpitha
	ArrayList<Women_Profile_Pojo> mStringFilterListreferred;
	ValueFilter valueFilter;
	ImageView img1, img2, img3, img4, img5, img6, img7;// 01nov2016 Arpitha img7
	// 1Nov2016 Arpitha
	String wname;
	int age;
	String regdate;
	int gest;
	String gravida;
	String risk;
	String delstatus = "";
	String deldeldate = "";
	int pos;
	private static byte[] image1 = null;// 08Nov2016

	// constructor
	public RecentCustomListAdapterReferred(Context context, int textViewResourceId, ArrayList<Women_Profile_Pojo> data,
			Partograph_DB dbh) {
		super(context, textViewResourceId, data);
		this.context = context;
		this.dbh = dbh;
		this.data = data;
		this.mStringFilterListreferred = data;

	}

	private class WomenListItem {
		TextView txtWomenname;
		TextView txtstatus;
		TextView txtdate_of_adm;
		ImageView imgwphoto;
		ImageView imgdangerSign;
		ImageView imghighrisk;
		ImageView imgmsg;
		TextView txtcommentcount;
		TextView txtreferredplace;
		TextView txtdateofreferal;
		ImageView imgsms;
		TextView txtpostdel;// 16Oct2016 Arpitha
		TextView txtbreech;// 07May2017 Arpitha - v2.6
		// TextView txtadddateref;//09Jun2017 Arpitha
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// 21oct2016 Arpitha
	@Override
	public int getCount() {
		return (data != null && data.size() > 0) ? data.size() : 0;
	}

	@Override
	public Women_Profile_Pojo getItem(int position) {
		return data.get(position);
	}

	// Get main view
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.referred_adapter, null);

				holder = new WomenListItem();
				holder.txtWomenname = (TextView) convertView.findViewById(R.id.txtwname);
				holder.txtstatus = (TextView) convertView.findViewById(R.id.txtstatus);
				holder.txtdate_of_adm = (TextView) convertView.findViewById(R.id.txtDateOfAdm);
				holder.imgwphoto = (ImageView) convertView.findViewById(R.id.imgwphoto);
				holder.imgdangerSign = (ImageView) convertView.findViewById(R.id.imgdangersign);
				holder.imghighrisk = (ImageView) convertView.findViewById(R.id.imgrisk);
				holder.imgmsg = (ImageView) convertView.findViewById(R.id.imgmsg);
				holder.txtcommentcount = (TextView) convertView.findViewById(R.id.txtcommentcount);
				holder.txtreferredplace = (TextView) convertView.findViewById(R.id.referredplace);
				holder.txtdateofreferal = (TextView) convertView.findViewById(R.id.txtdateofreferral);
				holder.imgsms = (ImageView) convertView.findViewById(R.id.imgsms);
				holder.txtpostdel = (TextView) convertView.findViewById(R.id.txtpostdel);// 16Oct2016
																							// Arpitha
				holder.txtbreech = (TextView) convertView.findViewById(R.id.txtbreechref);// 07May2017
																							// Arpitha
																							// -
																							// v2.6

				// holder.txtadddateref = convertView.findFo
				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();


			final String womenid = rowItem.getWomenId();
			user_id = rowItem.getUserId();

			holder.txtWomenname.setText(rowItem.getWomen_name() == null ? " " : rowItem.getWomen_name());

			/*
			 * if (rowItem.getDel_type() == 0) {// 07Jun2017 Arpitha - dispaly
			 * // delivered & referred woman in // referred tab if
			 * (rowItem.getGestationage() == 0) {
			 * holder.txtstatus.setText(context.getResources().getString(R.
			 * string.gest_nt_known));
			 * 
			 * } else {
			 * 
			 * holder.txtstatus.setText( rowItem.getGestationage() + " " +
			 * context.getResources().getString(R.string.weeks)); }
			 * 
			 * } else {
			 */
			

			if (rowItem.getregtype() == 2) {
				holder.txtpostdel.setVisibility(View.VISIBLE);// 16Oct2016
																// Arpitha
			} else
				holder.txtpostdel.setVisibility(View.GONE);//20Oct2017 Arpitha
				// 16Oct2016 Arpitha

			String[] deltype = context.getResources().getStringArray(R.array.del_type);
			if (rowItem.getDel_type() != 0)
				holder.txtstatus.setText(deltype[rowItem.getDel_type() - 1]);
			else {
				if (rowItem.getGestationage() == 0) {
					holder.txtstatus.setText(context.getResources().getString(R.string.gest_nt_known));

				} else {

					holder.txtstatus.setText(
							rowItem.getGestationage() + " " + context.getResources().getString(R.string.weeks));
				}

			}
			// }

			// }

			if (rowItem.getregtype() != 2) {
				holder.txtdate_of_adm
						.setText(rowItem.getDate_of_admission() == null ? " "
								: context.getResources().getString(R.string.regdate) + " : "
										+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
												Partograph_CommonClass.defdateformat)
										+ " / " + rowItem.getTime_of_admission());
			} else {
				holder.txtdate_of_adm
						.setText(
								rowItem.getDate_of_reg_entry() == null ? " "
										: context.getResources().getString(R.string.doe) + " : "
												+ Partograph_CommonClass.getConvertedDateFormat(
														rowItem.getDate_of_reg_entry(),
														Partograph_CommonClass.defdateformat));
			}

			// 08Nov2016
			image1 = rowItem.getWomen_Image();
			if (image1 != null)
				new DownloadImageTask().execute(holder);

			int comment_count = dbh.getComentsCount(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());
			if (comment_count > 0) {
				holder.imgmsg.setVisibility(View.VISIBLE);
				holder.txtcommentcount.setVisibility(View.VISIBLE);
				holder.txtcommentcount.setText("" + comment_count);
			}

			if (rowItem.getRisk_category() == 1) {
				holder.imghighrisk.setImageResource(R.drawable.ic_hr);

				holder.imghighrisk.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						try {

							if (MotionEvent.ACTION_UP == event.getAction()) {
								risk_observed = rowItem.getComments();

								if (rowItem.getRiskoptions().length() > 0
										|| (rowItem.getComments() != null && rowItem.getComments().length() > 0)) {
									displayConfirmationAlert_sms(rowItem.getRiskoptions());
								}
							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
						return true;
					}
				});

			} else
				holder.imghighrisk.setImageResource(0);

			if (rowItem.isDanger()) {
				holder.imgdangerSign.setImageResource(R.drawable.ic_compl);

				holder.imgdangerSign.setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						try {

							ArrayList<String> arr;

							arr = dbh.getobjectid_danger(rowItem.getWomenId());

							if (MotionEvent.ACTION_UP == event.getAction()) {

								displayConfirmationAlert_dangersign(arr, "");

							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}

						return true;
					}
				});
			} else
				holder.imgdangerSign.setImageResource(0);
			// }

			curref = dbh.getReferralDetails(Partograph_CommonClass.user.getUserId(), rowItem.getWomenId());
			if (curref != null && curref.getCount() > 0) {
				curref.moveToFirst();

				do {
					WomenReferral_pojo wrpojo = new WomenReferral_pojo();
					wrpojo.setWomenname(curref.getString(2));
					wrpojo.setReasonforreferral(curref.getString(7));
					wrpojo.setDescriptionofreferral(curref.getString(8));
					wrpojo.setPlaceofreferral(curref.getString(6));
					wrpojo.setDateofreferral(curref.getString(13) + "- " + curref.getString(14));
					rpojo.add(wrpojo);
					holder.txtreferredplace
							.setText(context.getResources().getString(R.string.refto) + curref.getString(6));

					// 18Aug2016-bindu - ref date in def date format
					holder.txtdateofreferal.setText(curref.getString(13) == null ? " "
							: context.getResources().getString(R.string.refdt) + " : " + Partograph_CommonClass
									.getConvertedDateFormat(curref.getString(13), Partograph_CommonClass.defdateformat)
									+ " / " + curref.getString(14));

				} while (curref.moveToNext());
			} else {
				holder.txtreferredplace.setText("");
				holder.txtdateofreferal.setText("");
			}

			// 29Sep2016 Arpitha
			if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
				holder.imgsms.setVisibility(View.VISIBLE);
			}

			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(option);

			holder.imgsms.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					try {
						if (MotionEvent.ACTION_UP == event.getAction()) {
							displayConfirmationAlert(context.getResources().getString(R.string.send_mess), womenid,
									position);

						}
					} catch (NotFoundException e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
					return true;
				}
			});

			// 21oct2016 Arpitha
			holder.imgwphoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						pos = position;// 01Nov2016 Arpitha
						Partograph_CommonClass.displaySummaryDialog(data, pos, context);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			// 07May2017 Arpitha - v2.6
			if (rowItem.getNormaltype() == 2)
				holder.txtbreech.setVisibility(View.VISIBLE);
			else
				holder.txtbreech.setVisibility(View.GONE);// 07May2017 Arpitha -
															// v2.6

		}

		catch (Exception e)

		{
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;

	}

	// Display Confirmation to exit the screen
	private boolean displayConfirmationAlert(String exit_msg, final String classname, final int position)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(context);
		dialog.setTitle(context.getResources().getString(R.string.sms_alert));
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		TextView txt1 = (TextView) dialog.findViewById(R.id.txtval);
		TextView txt2 = (TextView) dialog.findViewById(R.id.txtval1);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);
		txt1.setVisibility(View.GONE);
		txt2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				try {
					display_messagedialog(classname, position);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {

					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		return true;
	}

	@Override
	public void onAnimationEnd(Animation animation) {

	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}

	@Override
	public void onAnimationStart(Animation animation) {

	}

	// Display Confirmation to exit the screen
	private boolean displayConfirmationAlert_sms(String exit_msg) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		try {

			// 8july2016 by Arpitha
			ArrayList<String> op = new ArrayList<String>();
			String[] riskoptions = exit_msg.split(",");
			String[] riskoptArr = null;

			// 06jul2016 - use riskoptionsvalues instead of riskoptions array
			riskoptArr = context.getResources().getStringArray(R.array.riskoptionsvalues);

			if (riskoptArr != null) {
				if (riskoptions != null && riskoptions.length > 0) {
					for (String str : riskoptions) {
						if (str != null && str.length() > 0)
							op.add(riskoptArr[Integer.parseInt(str)]);
					}
				}
			}
			if (risk_observed != null && risk_observed.length() > 0) {
				op.add(context.getResources().getString(R.string.other_risk_observed) + " " + risk_observed);
			}

			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

			alertDialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor)
					+ "'>" + context.getResources().getString(R.string.highriskreasons) + "</font>"));// 02Feb2017

			View convertView = LayoutInflater.from(context).inflate(R.layout.alertdialog_danger, null);
			ListView lv = (ListView) convertView.findViewById(R.id.listoptions);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, op);
			alertDialog.setView(convertView).setCancelable(true).setPositiveButton(
					context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			lv.setAdapter(adapter);

			Dialog d = alertDialog.show();
			int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = d.findViewById(dividerId);
			divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

			d.show();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return true;
	}

	// Display Confirmation to exit the screen
	private boolean displayConfirmationAlert_dangersign(ArrayList<String> exit_msg, final String classname)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		ArrayList<String> op = new ArrayList<String>();
		ArrayList<String> riskoptions = exit_msg;
		String[] riskoptArr = null;

		// 06jul2016 - use riskoptionsvalues instead of riskoptions array
		riskoptArr = context.getResources().getStringArray(R.array.parameter);

		if (riskoptArr != null) {
			if (riskoptions != null && riskoptions.size() > 0) {
				for (String str : riskoptions) {
					if (str != null && str.length() > 0)
						op.add(riskoptArr[Integer.parseInt(str)]);
				}
			}
		}

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

		alertDialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
				+ context.getResources().getString(R.string.partographparametersindanger) + "</font>"));// 02Feb2017

		View convertView = LayoutInflater.from(context).inflate(R.layout.alertdialog_danger, null);
		ListView lv = (ListView) convertView.findViewById(R.id.listoptions);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, op);
		alertDialog.setView(convertView).setCancelable(true).setPositiveButton(
				context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		lv.setAdapter(adapter);

		Dialog d = alertDialog.show();
		int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = d.findViewById(dividerId);
		divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

		d.show();

		return true;
	}

	public void display_messagedialog(final String womenid, final int position) throws Exception {
		try {
			final Dialog sms_dialog = new Dialog(context);
			sms_dialog.setTitle(context.getResources().getString(R.string.sending_sms));

			sms_dialog.setContentView(R.layout.alertdialog_sms);

			sms_dialog.show();

			TextView txt1 = (TextView) sms_dialog.findViewById(R.id.txtval);
			TextView txt2 = (TextView) sms_dialog.findViewById(R.id.txtval1);

			final EditText etphno = (EditText) sms_dialog.findViewById(R.id.etphnno);

			etmesss = (EditText) sms_dialog.findViewById(R.id.etmess);
			EditText etopt = (EditText) sms_dialog.findViewById(R.id.etreasonoptions);
			TextView txtreason = (TextView) sms_dialog.findViewById(R.id.txtval6);
			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);

			p_no = "";
			for (int i = 0; i < values.size(); i++) {

				// String s = values.get(i);
				p_no = p_no + values.get(i) + ",";
			}

			etphno.setText(p_no);

			if (rpojo != null && rpojo.size() > 0) {
				String s = rpojo.get(position).getWomenname();

				txt1.setText(s);

				// changed on 3August2106 by Arpitha
				txt2.setText(rpojo.get(position).getDescriptionofreferral());
				// changed on 3August2106 by Arpitha
				String selectedriskoption = rpojo.get(position).getReasonforreferral();

				// ArrayList<String> op = new ArrayList<String>();
				String admResDisplay = "";

				String[] admres = selectedriskoption.split(",");
				String[] admResArr = null;

				admResArr = context.getResources().getStringArray(R.array.reasonforreferralvalues);

				if (admResArr != null) {
					if (admres != null && admres.length > 0) {
						for (String str : admres) {
							if (str != null && str.length() > 0 && (!str.equalsIgnoreCase("null")))// 09nov2016
																									// checking
																									// for
																									// str
																									// null
								admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + "\n";

						}
					}
				}

				if (admResDisplay.length() > 0) {
					etopt.setText(admResDisplay);
				} else {
					etopt.setVisibility(View.GONE);
					txtreason.setVisibility(View.GONE);
				}
			}

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					try {

						if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {

							if (values != null && values.size() > 0) {
								sms_dialog.cancel();
								InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(), womenid);
								if (isMessageLogsaved) {
									Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
											Toast.LENGTH_LONG).show();
									sendSMSFunction();
								}
							} else {
								if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() >= 10) {
									InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(),
											womenid);
									sms_dialog.cancel();// 09nov2016 Arpitha
									if (isMessageLogsaved) {
										Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
												Toast.LENGTH_LONG).show();
										sendSMSFunction();
									}

								} else if (etphno.getText().toString().length() <= 0) {
									Toast.makeText(context, context.getResources().getString(R.string.entr_phno),
											Toast.LENGTH_LONG).show();
								} else if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() < 10) {
									Toast.makeText(context,
											context.getResources().getString(R.string.enter_valid_phn_no),
											Toast.LENGTH_LONG).show();
								}
							}

						} else
							Toast.makeText(context, context.getResources().getString(R.string.no_sim),
									Toast.LENGTH_LONG).show();

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgcancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sms_dialog.cancel();
				}
			});
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Insert to record to Message Log
	private void InserttblMessageLog(String phoneno, String comments, String womanid) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String message = "";
		Cursor c = dbh.getReferralDetails(user_id, womanid);
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			String RefResDisplay = "";
			String womanname = c.getString(2);
			String desc = c.getString(8);
			String reasonforref = c.getString(7);
			if (reasonforref != null && reasonforref.length() > 0) {

				String[] admres = reasonforref.split(",");
				String[] admResArr = null;

				admResArr = context.getResources().getStringArray(R.array.reasonforreferralvaluessms);

				if (admResArr != null) {
					if (admres != null && admres.length > 0) {
						for (String str : admres) {
							if (str != null && str.length() > 0)
								RefResDisplay = RefResDisplay + admResArr[Integer.parseInt(str)] + ",";

						}
					}
				}
			}

			if (comments.length() > 0)
				comments = " Cmnts: " + comments; // 11Sep2016 - bindu chk comm
													// length

			message = "ePartograph Ref :- " + womanname + ", " + " Reason: " + RefResDisplay + " Desc: " + desc
					+ comments;

			ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

			if (phoneno.length() > 0) {
				String[] phn = phoneno.split("\\,");
				for (int i = 0; i < phn.length; i++) {
					String num = phn[i];
					if (num != null && num.length() > 0) {
						MessageLogPojo mlp = new MessageLogPojo(user_id, womanid, num, desc, RefResDisplay, message, 1,
								0);
						mlpArr.add(mlp);
					}
				}
			}

			if (mlpArr != null) {
				for (MessageLogPojo mlpp : mlpArr) {

					int transId = dbh.iCreateNewTrans(user_id);// 15Oct2016
																// Arpitha

					boolean isinserted = dbh.insertToMessageLog(mlpp, transId);// 15Oct2016
																				// Arpitha
					if (isinserted) {
						isMessageLogsaved = true;
						dbh.iNewRecordTrans(user_id, transId, Partograph_DB.TBL_MESSAGELOG);// 15Oct2016
																							// Arpitha
					} else
						throw new Exception("InserttblMessageLog(): Failed to Insert Message to tblMessageLog ");
				}
			}
		}
	}

	/**
	 * This method invokes after successfull save of record Sends SMS to the
	 * Specified Number
	 */
	private void sendSMSFunction() throws Exception {
		new SendSMS();
		Activity_WomenView.actionBar.setSelectedNavigationItem(3);
	}

	private class ValueFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				ArrayList<Women_Profile_Pojo> filterList = new ArrayList<Women_Profile_Pojo>();
				for (int i = 0; i < mStringFilterListreferred.size(); i++) {
					if (mStringFilterListreferred.get(i).getWomen_name().toUpperCase()
							.contains(constraint.toString().toUpperCase())
							|| mStringFilterListreferred.get(i).getPhone_No()
									.contains(constraint.toString().toUpperCase())) {

						filterList.add(mStringFilterListreferred.get(i));
					}

				}
				results.count = filterList.size();
				results.values = filterList;
			} else {
				results.count = mStringFilterListreferred.size();
				results.values = mStringFilterListreferred;
			}
			return results;

		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			data = (ArrayList<Women_Profile_Pojo>) results.values;
			notifyDataSetChanged();
		}

	}

	@Override
	public Filter getFilter() {
		if (valueFilter == null) {
			valueFilter = new ValueFilter();
		}
		return valueFilter;
	}

	/*
	 * // 20Oct2016 Arpitha private boolean
	 * displayConfirmationAlert_summary(String exit_msg, final String classname)
	 * throws Exception { AppContext.addToTrace( new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName()); final Dialog dialog = new
	 * Dialog(context); dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	 * dialog.setContentView(R.layout.activity_summary);
	 * 
	 * dialog.show();
	 * 
	 * img1 = (ImageView) dialog.findViewById(R.id.img1); img2 = (ImageView)
	 * dialog.findViewById(R.id.img2); img3 = (ImageView)
	 * dialog.findViewById(R.id.img3); img4 = (ImageView)
	 * dialog.findViewById(R.id.img4); img5 = (ImageView)
	 * dialog.findViewById(R.id.img5); img6 = (ImageView)
	 * dialog.findViewById(R.id.img6); img7 = (ImageView)
	 * dialog.findViewById(R.id.img7);// 01nov2016 Arpitha
	 * 
	 * // 01Nov2016 Arpitha TextView txtname = (TextView)
	 * dialog.findViewById(R.id.wname); TextView txtage = (TextView)
	 * dialog.findViewById(R.id.wage); TextView txtregdate = (TextView)
	 * dialog.findViewById(R.id.wdoa); TextView txtgest = (TextView)
	 * dialog.findViewById(R.id.wgest); TextView txtgravida = (TextView)
	 * dialog.findViewById(R.id.wgravida); TextView txtrisk = (TextView)
	 * dialog.findViewById(R.id.wtrisk); TextView txtdelstatus = (TextView)
	 * dialog.findViewById(R.id.wdelstatus); TextView txtdeldate = (TextView)
	 * dialog.findViewById(R.id.wdeldate); wname = data.get(pos).getWomen_name()
	 * == null ? " " : data.get(pos).getWomen_name(); age =
	 * data.get(pos).getAge();
	 * 
	 * gravida = context.getResources().getString(R.string.gravida_short_label)
	 * + ":" + data.get(pos).getGravida() + "," +
	 * context.getResources().getString(R.string.para_short_label) + ":" +
	 * data.get(pos).getPara(); if (data.get(pos).getRisk_category() == 0) risk
	 * = context.getResources().getString(R.string.low); else risk =
	 * context.getResources().getString(R.string.high);
	 * 
	 * gest = data.get(pos).getGestationage(); txtname.setText(wname);
	 * txtage.setText("" + age +
	 * context.getResources().getString(R.string.yrs));
	 * txtgravida.setText(gravida); if (data.get(pos).getregtype() != 2) {
	 * regdate = Partograph_CommonClass.getConvertedDateFormat(data.get(pos).
	 * getDate_of_admission(), Partograph_CommonClass.defdateformat) + "/" +
	 * data.get(pos).getTime_of_admission();
	 * txtregdate.setText(context.getResources().getString(R.string.reg) + ":" +
	 * regdate); } else txtregdate.setText(
	 * context.getResources().getString(R.string.reg) + ":" +
	 * data.get(pos).getDate_of_reg_entry()); if (gest == 0) {
	 * txtgest.setText(context.getResources().getString(R.string.gest_age) + ":"
	 * + context.getResources().getString(R.string.notknown)); } else//
	 * 18Nov2016 Arpitha { if (data.get(pos).getGest_age_days() == 0) {
	 * txtgest.setText(context.getResources().getString(R.string.gest_age) + ":"
	 * + "" + gest); } else
	 * txtgest.setText(context.getResources().getString(R.string.gest_age) + ":"
	 * + "" + gest + " " + context.getResources().getString(R.string.wks) +
	 * data.get(pos).getGest_age_days() +
	 * context.getResources().getString(R.string.days)); } // 18Nov2016 Arpitha
	 * txtrisk.setText(context.getResources().getString(R.string.
	 * risk_short_label) + ":" + risk);// 01Nov2016 // Arpitha
	 * 
	 * txtdeldate.setVisibility(View.GONE);// 18Nov2016 Arpitha
	 * txtdelstatus.setVisibility(View.GONE);// 18Nov2016 Arpitha
	 * 
	 * ImageButton imgbtncanceldialog = (ImageButton)
	 * dialog.findViewById(R.id.imgbtncanceldialog);
	 * 
	 * ArrayList<String> values =
	 * dbh.getentereddata(data.get(pos).getWomenId());
	 * 
	 * if (values.get(0).equalsIgnoreCase("yes")) {
	 * img1.setImageResource(R.drawable.ic_ok); } else
	 * img1.setImageResource(R.drawable.ic_cancel);
	 * 
	 * if (values.get(4).equalsIgnoreCase("yes")) {
	 * img2.setImageResource(R.drawable.ic_ok); } else
	 * img2.setImageResource(R.drawable.ic_cancel);
	 * 
	 * if (values.get(5).equalsIgnoreCase("yes")) {
	 * img3.setImageResource(R.drawable.ic_ok); } else
	 * img3.setImageResource(R.drawable.ic_cancel); if
	 * (values.get(2).equalsIgnoreCase("yes")) {
	 * img4.setImageResource(R.drawable.ic_ok); } else
	 * img4.setImageResource(R.drawable.ic_cancel); if
	 * (values.get(1).equalsIgnoreCase("yes")) {
	 * img5.setImageResource(R.drawable.ic_ok); } else
	 * img5.setImageResource(R.drawable.ic_cancel); if
	 * (values.get(3).equalsIgnoreCase("yes")) {
	 * img6.setImageResource(R.drawable.ic_ok); } else
	 * img6.setImageResource(R.drawable.ic_cancel); if
	 * (values.get(6).equalsIgnoreCase("yes")) {
	 * img7.setImageResource(R.drawable.ic_ok); } else
	 * img7.setImageResource(R.drawable.ic_cancel);// 01Nov2016 Arpitha
	 * 
	 * imgbtncanceldialog.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { // TODO Auto-generated method
	 * stub dialog.cancel();
	 * 
	 * } });
	 * 
	 * return true; }
	 */

	// 08Nov2016
	public class DownloadImageTask extends AsyncTask<WomenListItem, Void, Bitmap> {

		ImageView imageView = null;

		protected Bitmap doInBackground(WomenListItem... item) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.imageView = (ImageView) item[0].imgwphoto;
			return getBitmapDownloaded();
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null)
				imageView.setImageBitmap(result);
		}

		/** This function downloads the image and returns the Bitmap **/
		private Bitmap getBitmapDownloaded() {
			Bitmap btmp = null;
			if (image1 != null) {
				btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
			}
			return btmp;
		}
	}

}

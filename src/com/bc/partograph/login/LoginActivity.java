package com.bc.partograph.login;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.sync.UserAndSettingsSync;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.notification.Partograph_Services;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {

	EditText etusername, etpwd;
	ImageButton btnlogin;
	AQuery aq;
	UserPojo user;
	Partograph_DB dbh;
	String storedPassword;
	static boolean inetOn;
	String serverResult;
	Context ctx;
	CountDownTimer syncTimer = null;
	public static final int START_AFTER_SECONDS = 60000;
	TextView txtlink, txtversion;
	// boolean isuservalid;
	public static String user_id;
	// public static String womenid;
	public static ArrayList<Integer> objectid = new ArrayList<Integer>();// 02Nov2016
	public static boolean istesting = false;// 15Nov2016 Arpitha // Arpitha

	String notificationOnOFF;

	boolean isTabLost = false;// 14March2017 Arpitha

	ProgressDialog mProgressDialog;// 14March2017 Arpitha

	// 06April2017 Arpitha
	ProgressDialog pDialog;
	public static Context context;
	boolean fromLogin = false;

	public static String expiredate;
	public static String decyptedEvalEndDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// notification1();

		setContentView(R.layout.activity_login);
		startService(new Intent(LoginActivity.this, Partograph_Services.class));

		try {

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			aq = new AQuery(this);
			context = getApplicationContext();// 06April2017 Arpitha

			txtlink = (TextView) findViewById(R.id.txtlink);
			txtversion = (TextView) findViewById(R.id.txtversion);
			txtversion.setText(getResources().getString(R.string.app_version));
			Partograph_CommonClass.DATABASE_NAME = AppContext.dbDirRef + getResources().getString(R.string.dbName);
			Partograph_CommonClass.DATABASE_VERSION = Integer.parseInt(getResources().getString(R.string.dbVersion));
			dbh = Partograph_DB.getInstance(getApplicationContext());

			initializeScreen(aq.id(R.id.rllogin).getView());
			ctx = this;

			etusername = (EditText) findViewById(R.id.etusername);
			etpwd = (EditText) findViewById(R.id.etpwd);
			btnlogin = (ImageButton) findViewById(R.id.btnlogin);

			user = dbh.getUserProfile();

			etusername.setText(user.getEmailid());// 06April2017 Arpitha

			user_id = user.getUserId();

			txtlink.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Uri uriUrl = Uri.parse("http://www.bluecrimson.net/");
					Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
					startActivity(launchBrowser);

				}
			});

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		KillAllActivitesAndGoToLogin.activity_stack.add(this);// 09Nov2016
	}

	// validate whether automatic date and time valid , sim exists and network
	// available
	protected boolean validateAutomaticDateTime() throws Exception {
		// 24Sep2016 Arpitha - addtotrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		boolean isvalid = true;
		// 27dec2015
		if (!aq.id(R.id.chkAppTesting).isChecked()) {
			if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY || AppContext.isNetworkUp()) {
				if (Settings.Global.getInt(getContentResolver(), Global.AUTO_TIME) == 0) {
					Partograph_CommonClass.showSettingsAlert(LoginActivity.this, "");// 27Sep2016
																						// Arpitha
					isvalid = false;

				} else
					isvalid = true;
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.chksimnetwork),
						Toast.LENGTH_SHORT).show();
				isvalid = false;
			}
		} else {
			isvalid = true;
			istesting = true;
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return isvalid;
	}

	// Validate fields
	protected boolean validate() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (etusername.getText().toString().trim().equals("")) {// 15May2017
																// Arpitha -
																// v2.6 bug
																// fixing
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_email_id),
					Toast.LENGTH_SHORT).show();// 06April2017
			aq.id(R.id.etusername).getEditText().requestFocus();
			return false;
		} else if (etpwd.getText().toString().equals("")) {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_password),
					Toast.LENGTH_SHORT).show();
			aq.id(R.id.etpwd).getEditText().requestFocus();
			return false;
		}

		user = dbh.getUserProfile();
		storedPassword = user.getPwd();
		// to decrypt the password
		// byte[] data = Base64.decode(storedPassword, Base64.DEFAULT);//
		// 05Jan2017
		// Arpitha
		// String decyptedPassword = new String(data, "UTF-8");// 05Jan2017
		// Arpitha
		if (!etpwd.getText().toString().equals(storedPassword)) {// 11April2017
																	// Arpitha
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.validpwd), Toast.LENGTH_SHORT)
					.show();
			aq.id(R.id.etpwd).getEditText().requestFocus();
			return false;
		}

		// String t = etusername.getText().toString();
		// if (!(t.equalsIgnoreCase(user.getEmailid()))) {//06April2017 Arpitha
		// Toast.makeText(getApplicationContext(),
		// getResources().getString(R.string.enter_valid_email_id),
		// Toast.LENGTH_SHORT)
		// .show();//06April2017 Arpitha
		// aq.id(R.id.etusername).getEditText().requestFocus();
		// return false;
		// }
		// 15May2017 Arpitha - v2.6 bug fixing
		if (!isEmailValid(aq.id(R.id.etusername).getText().toString())) {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.incorrect_emailid_format),
					Toast.LENGTH_SHORT).show();
			return false;

		} // 15May2017 Arpitha

		if (!etusername.getText().toString().equals(user.getEmailid())) {// 11April2017
																			// Arpitha
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.email_id_entered_is_incoorect),
					Toast.LENGTH_SHORT).show();// 25April2017 Arpitha
			aq.id(R.id.etusername).getEditText().requestFocus();
			return false;
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return true;
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {

		try {

			switch (v.getId()) {
			case R.id.btnlogin: {

				btnLoginClick();

				break;

			}
			}
		} catch (SQLiteException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			if (e.toString().toLowerCase().contains("no such table: tbl_user")) {
				// AsyncCallUserValid validateUsr = new AsyncCallUserValid();
				// validateUsr.execute();
				// 06April2017 Arpitha
				user = new UserPojo();
				user.setEmailid(etusername.getText().toString());
				user.setPassword(etpwd.getText().toString());

				AsyncCallForUserAndSettings task = new AsyncCallForUserAndSettings(fromLogin, user, dbh);
				StartAsyncTaskInParallel(task);// 06April2017 Arpitha
			}
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.msgloginfailed),
					Toast.LENGTH_SHORT).show();
			if (e.toString().contains("no such table: tbl_user")) {
				// AsyncCallUserValid validateUsr = new AsyncCallUserValid();
				// validateUsr.execute();
				// 06April2017 Arpitha
				user = new UserPojo();
				user.setEmailid(etusername.getText().toString());
				user.setPassword(etpwd.getText().toString());

				AsyncCallForUserAndSettings task = new AsyncCallForUserAndSettings(fromLogin, user, dbh);
				StartAsyncTaskInParallel(task);// 06April2017 Arpitha
			}
		}
	}

	private class AsyncCallUserValid extends AsyncTask<String, String, String> {
		private ProgressDialog pDialog;

		@Override
		protected void onPostExecute(String result) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			pDialog.dismiss();

			if (!inetOn)
				Toast.makeText(ctx, getResources().getString(R.string.chk_internet), Toast.LENGTH_SHORT).show();
			else {
				if (SyncFunctions.blnUserisValid && !SyncFunctions.serTabLost) {
					try {
						dbh.createTablesAndData();

						if (dbh.insertUser(etusername.getText().toString(), etpwd.getText().toString())) {

							if (dbh.insertSettings(etusername.getText().toString())) {
								btnLoginClick();

								// 21Aug2015

								if (SyncFunctions.strUserupdate.size() > 0) {
									for (String str : SyncFunctions.strUserupdate) {
										AppContext.getDb().execSQL(str);
									}
								}

								if (SyncFunctions.strSettingsupdate.size() > 0) {
									for (String str : SyncFunctions.strSettingsupdate) {
										AppContext.getDb().execSQL(str);
									}
								}

								// 30Mach2017 Arpitha
								// 02Nov2016 Arpitha
								String obj = dbh.getobjectid_to_hide(user.getUserId());

								if (obj != null && obj.trim().length() > 0)// 01nov2016
																			// Arpitha
								{
									String[] obj_id = obj.split(",");
									for (int i = 1; i <= obj_id.length; i++) {
										LoginActivity.objectid.add(Integer.parseInt(obj_id[i - 1]));
									}
								}

							}

						}

					} catch (IOException e1) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e1);
						e1.printStackTrace();
					} catch (Exception e2) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e2);
						e2.printStackTrace();
					}
				}

				// 14March2017 Arpitha
				else if (!(SyncFunctions.blnUserisValid) && SyncFunctions.serTabLost) {

					new Timer().schedule(new TimerTask() {
						@Override
						public void run() {
							// This call is after the files are ready in server
							/*
							 * AsyncCallUserValid validateUsr = new
							 * AsyncCallUserValid(); validateUsr.execute();
							 */
							AsyncCallUserValid validateUsr = new AsyncCallUserValid();
							validateUsr.execute();
						}
					}, 10000);

				} else if (!SyncFunctions.serValidUser && !SyncFunctions.serTabLost) {
					// if(pDialog != null)
					// pDialog.dismiss();

					AsyncCallUserValid.this.cancel(true);

					if (SyncFunctions.readyToDownload && SyncFunctions.returnFilename.length() > 4) {
						startDownload();
						isTabLost = true;
					} else {

						if (SyncFunctions.returnedInfo != null && SyncFunctions.returnedInfo.length() > 0) {
							if (SyncFunctions.returnedInfo.contains("regist"))
								Toast.makeText(LoginActivity.this, "R", Toast.LENGTH_LONG).show();
							else if (SyncFunctions.returnedInfo.contains("activ"))
								Toast.makeText(LoginActivity.this, "A", Toast.LENGTH_LONG).show();
							else
								Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
						}

						if (!SyncFunctions.readyToDownload)
							AppContext.addLog(
									new RuntimeException().getStackTrace()[0].getMethodName() + " - "
											+ this.getClass().getSimpleName(),
									new Exception("\n\nFile is not ready to download\n\n"));
						else if (SyncFunctions.returnFilename == null || SyncFunctions.returnFilename.length() == 0)
							AppContext.addLog(
									new RuntimeException().getStackTrace()[0].getMethodName() + " - "
											+ this.getClass().getSimpleName(),
									new Exception("\n\nFile name is not returned from server\n\n"));
					}
				}

				else {
					AsyncCallUserValid.this.cancel(true);
					Toast.makeText(ctx, getResources().getString(R.string.msgserverloginfailed), Toast.LENGTH_SHORT)
							.show();
				}
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(ctx);
				// inetOn = true;
				if (inetOn)
					serverResult = new SyncFunctions(getApplicationContext()).SyncFuncGetUserandSettingDetailsatlogin(
							etusername.getText().toString(), etpwd.getText().toString(),
							Partograph_CommonClass.properties.getProperty("ipAddress"));

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				return e.getLocalizedMessage();
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			// 21March2017 Arpitha
			if (Looper.myLooper() == null) {
				Looper.prepare();
			} // 21March2017 Arpitha
			pDialog = ProgressDialog.show(ctx, "", getResources().getString(R.string.msgserverloginprogress), true,
					false);

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

	}

	public void btnLoginClick() throws NotFoundException, Exception {
		//try {

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			fromLogin = true;// 06April2017 Arpitha
			if (validateAutomaticDateTime()) {
				if (!validate())
					return;

				Partograph_CommonClass.currentdate = new SimpleDateFormat("yyyy-MM-dd")
						.parse(Partograph_CommonClass.getTodaysDate()); // 27Sep2016
																		// Arpitha

				Partograph_CommonClass.user = user;

				dbh = AppContext.createNewDbObj(dbh);

				Intent view = new Intent(getApplicationContext(), Activity_WomenView.class);
				startActivity(view);

				Partograph_CommonClass.sPojo = dbh.getSettingsDetails(user.getUserId());

				getSettingsDataFromServer();

				expiredate = Partograph_CommonClass.sPojo.getExpire_date();

				byte[] evalDate = null;

				if (expiredate != null && expiredate.length() > 0)
					evalDate = expiredate.getBytes("UTF-8");
				byte[] decy = null;

				if (evalDate != null && evalDate.length > 0)
					decy = Base64.decode(evalDate, Base64.DEFAULT);

				if (decy != null && decy.length > 0)
					decyptedEvalEndDate = new String(decy);

				if (decyptedEvalEndDate != null && decyptedEvalEndDate.trim().length() > 0)
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.app_evaluation_date) + " " + Partograph_CommonClass
									.getConvertedDateFormat(decyptedEvalEndDate, Partograph_CommonClass.defdateformat),
							Toast.LENGTH_LONG).show();

			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

	}

	// Get settings data from server
	private void getSettingsDataFromServer() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		int interval = 1;

		syncTimer = new CountDownTimer(Long.MAX_VALUE, interval * START_AFTER_SECONDS) {

			@Override
			public void onTick(long millisUntilFinished) {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				AsyncCallWS_Settings task = new AsyncCallWS_Settings();
				task.execute();
			}

			@Override
			public void onFinish() {

			}
		}.start();
	}

	// Async cls to sync
	public class AsyncCallWS_Settings extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			if (!inetOn)
				System.out.println("Internet :" + getResources().getString(R.string.chk_internet));

			else {
				if (Partograph_CommonClass.movedToInputFolder && Partograph_CommonClass.responseAckCount <= 5) {
					if (!SyncFunctions.syncUptoDate) {
						AsyncCallWS_Settings task = new AsyncCallWS_Settings();
						task.execute();
					} else {
						AsyncCallWS_Settings.this.cancel(true);

						try {
							Partograph_CommonClass.sPojo = dbh.getSettingsDetails(user.getUserId());
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				} else {
					AsyncCallWS_Settings.this.cancel(true);

				}

			}

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(getApplicationContext());
				if (inetOn)

					serverResult = new SyncFunctions(getApplicationContext()).SyncFunctionSettings(user.getUserId(),
							Partograph_CommonClass.properties.getProperty("serverdbmaster"),
							Partograph_CommonClass.properties.getProperty("ipAddress"));

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				return e.getLocalizedMessage();
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// 17Oct2016 Arpitha
	public void notification() {

		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
		notificationIntent.addCategory("android.intent.category.DEFAULT");

		PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// Date futureDate = new Date(new Date().getTime() + 86400000);

		// futureDate.setHours(17);
		// futureDate.setMinutes(50);
		// futureDate.setSeconds(0);

		Calendar cal = Calendar.getInstance();
		// cal.set(2016, 6, 16, 18, 0, 0);
		cal.add(Calendar.SECOND, 10);
		// cal.setTimeInMillis(System.currentTimeMillis());
		// cal.set(Calendar.HOUR_OF_DAY, 14);
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 0, broadcast);

	}

	// Arpitha - to exit application on click of back button
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
		System.exit(0);
	}

	// 14March2017 Arpitha
	private void startDownload() {
		String url = "http://" + Partograph_CommonClass.properties.getProperty("ipAddress")
				+ Partograph_CommonClass.properties.getProperty("lostDbPath") + UserAndSettingsSync.returnFilename;
		new DownloadFileAsync().execute(url);
	}

	class DownloadFileAsync extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(LoginActivity.this);
			mProgressDialog.setMessage("Downloading file..");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		@Override
		protected String doInBackground(String... aurl) {
			int count;

			try {

				URL url = new URL(aurl[0]);
				HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
				conexion.setRequestMethod("GET");
				conexion.setDoOutput(true);
				conexion.connect();

				int lenghtOfFile = conexion.getContentLength();
				Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);

				InputStream input = new BufferedInputStream(url.openStream());
				OutputStream output = new FileOutputStream(AppContext.mainDir + "/Database/partograph.db");// 06April2017
																											// Arpitha

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onProgressUpdate(String... progress) {
			Log.d("ANDRO_ASYNC", progress[0]);
			mProgressDialog.setProgress(Integer.parseInt(progress[0]));
		}

		@Override
		protected void onPostExecute(String unused) {
			try {

				if (mProgressDialog != null)
					mProgressDialog.dismiss();

				try {
					// dbh.createTablesAndData();

					// 17March2017 Arpitha
					SQLiteDatabase db = null;
					dbh.updateTablesandData1("partograph_lostTab_tbl_create_stmnt.sql", db);

					if (dbh.insertUser(etusername.getText().toString(), etpwd.getText().toString())) {

						if (dbh.insertSettings(etusername.getText().toString())) {
							btnLoginClick();

							// 21Aug2015

							if (UserAndSettingsSync.strUserupdate != null
									&& UserAndSettingsSync.strUserupdate.size() > 0) {
								for (String str : UserAndSettingsSync.strUserupdate) {
									AppContext.getDb().execSQL(str);
								}
							}

							if (UserAndSettingsSync.strSettingsupdate != null
									&& UserAndSettingsSync.strSettingsupdate.size() > 0) {
								for (String str : UserAndSettingsSync.strSettingsupdate) {
									AppContext.getDb().execSQL(str);
								}
							}

							// 30Mach2017 Arpitha
							// 02Nov2016 Arpitha
							String obj = dbh.getobjectid_to_hide(user.getUserId());

							if (obj != null && obj.trim().length() > 0)// 01nov2016
																		// Arpitha
							{
								String[] obj_id = obj.split(",");
								for (int i = 1; i <= obj_id.length; i++) {
									LoginActivity.objectid.add(Integer.parseInt(obj_id[i - 1]));
								}
							}

						}

						Partograph_CommonClass.user = dbh.getUserProfile();// 06April2017
																			// Arpitha

					}

				} catch (IOException e1) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e1);
					e1.printStackTrace();
				} catch (Exception e2) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e2);
					e2.printStackTrace();
				}

				// btnLoginClick();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// 06April2017 Arpitha
	public class AsyncCallForUserAndSettings extends AsyncTask<String, Void, String> {

		// public interface UserAndSettingsInterface {
		// public void fromUserAndSettingsAsync();
		// }

		public AsyncCallForUserAndSettings(boolean fromLogin, UserPojo user, Partograph_DB dbh) {
			dbh = dbh;
			user = user;
			fromLogin = fromLogin;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				if (!inetOn)
					Toast.makeText(ctx, getResources().getString(R.string.chk_internet), Toast.LENGTH_SHORT).show();
				else {
					if (UserAndSettingsSync.blnUserisValid && !UserAndSettingsSync.serTabLost) {
						try {
							dbh.createTablesAndData();

							if (dbh.insertUser(etusername.getText().toString(), etpwd.getText().toString())) {

								if (dbh.insertSettings(etusername.getText().toString())) {
									btnLoginClick();

									// 21Aug2015

									if (UserAndSettingsSync.strUserupdate.size() > 0) {
										for (String str : UserAndSettingsSync.strUserupdate) {
											AppContext.getDb().execSQL(str);
										}
									}

									if (UserAndSettingsSync.strSettingsupdate.size() > 0) {
										for (String str : UserAndSettingsSync.strSettingsupdate) {
											AppContext.getDb().execSQL(str);
										}
									}

									// 30Mach2017 Arpitha
									// 02Nov2016 Arpitha
									String obj = dbh.getobjectid_to_hide(user.getUserId());

									if (obj != null && obj.trim().length() > 0)// 01nov2016
																				// Arpitha
									{
										String[] obj_id = obj.split(",");
										for (int i = 1; i <= obj_id.length; i++) {
											LoginActivity.objectid.add(Integer.parseInt(obj_id[i - 1]));
										}
									}

								}

							}

						} catch (IOException e1) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e1);
							e1.printStackTrace();
						} catch (Exception e2) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e2);
							e2.printStackTrace();
						}
					}

					// 14March2017 Arpitha
					else if ((!UserAndSettingsSync.blnUserisValid) && UserAndSettingsSync.serTabLost) {

						new Timer().schedule(new TimerTask() {
							@Override
							public void run() {
								// This call is after the files are ready in
								// server
								/*
								 * AsyncCallUserValid validateUsr = new
								 * AsyncCallUserValid(); validateUsr.execute();
								 */
								AsyncCallForUserAndSettings validateUsr = new AsyncCallForUserAndSettings(fromLogin,
										user, dbh);
								validateUsr.execute();
							}
						}, 10000);

					} else if (!UserAndSettingsSync.serValidUser && !UserAndSettingsSync.serTabLost) {
						if (pDialog != null)
							pDialog.dismiss();

						AsyncCallForUserAndSettings.this.cancel(true);

						if (UserAndSettingsSync.readyToDownload && UserAndSettingsSync.returnFilename.length() > 4) {
							startDownload();
							isTabLost = true;
						} else {

							if (UserAndSettingsSync.returnedInfo != null
									&& UserAndSettingsSync.returnedInfo.length() > 0) {
								if (UserAndSettingsSync.returnedInfo.contains("regist"))
									Toast.makeText(LoginActivity.this,
											getResources().getString(R.string.msgloginfailed), Toast.LENGTH_LONG)
											.show();
								else if (UserAndSettingsSync.returnedInfo.contains("activ"))
									Toast.makeText(LoginActivity.this,
											getResources().getString(R.string.msgloginfailed), Toast.LENGTH_LONG)
											.show();
								else
									Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
							}

							if (!SyncFunctions.readyToDownload) {
								Toast.makeText(LoginActivity.this, getResources().getString(R.string.msgloginfailed),
										Toast.LENGTH_LONG).show();
								AppContext.addLog(
										new RuntimeException().getStackTrace()[0].getMethodName() + " - "
												+ this.getClass().getSimpleName(),
										new Exception("\n\nFile is not ready to download\n\n"));
							} else if (SyncFunctions.returnFilename == null
									|| SyncFunctions.returnFilename.length() == 0) {
								Toast.makeText(LoginActivity.this, getResources().getString(R.string.msgloginfailed),
										Toast.LENGTH_LONG).show();
								AppContext.addLog(
										new RuntimeException().getStackTrace()[0].getMethodName() + " - "
												+ this.getClass().getSimpleName(),
										new Exception("\n\nFile name is not returned from server\n\n"));
							}
						}
					}

					else {
						AsyncCallForUserAndSettings.this.cancel(true);
						Toast.makeText(ctx, getResources().getString(R.string.msgserverloginfailed), Toast.LENGTH_SHORT)
								.show();
					}
				}
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(context);
				// serverResult = null;
				String serviceURL = Partograph_CommonClass.properties.getProperty("serverURL");

				if (inetOn) {

					Partograph_CommonClass.isAsyncStarted = true;
					// EJppCmn.pbssi.onStartProgBar();

					// UserAndSettingsSync.RegisteredMain = "0";
					// UserAndSettingsSync.ValidatedMain = "0";

					serverResult = new UserAndSettingsSync(context, dbh).GetAllCurrentServerData(serviceURL, user,
							fromLogin);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return e.getLocalizedMessage();
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			if (fromLogin)
				// pDialog = ProgressDialog.show(getApplicationContext(), "",
				// "Server verification is in progress", true, false);
				// 21March2017 Arpitha
				if (Looper.myLooper() == null) {
				Looper.prepare();
				} // 21March2017 Arpitha
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Server verification is in progress..");
			// pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(false);
			pDialog.show();
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

	}

	// /** This method invokes the method - CallSettingsAndOtherDetails()
	// * @param user2 */
	// public static void CallSettingsAndOtherDetails(Partograph_DB dbh,
	// UserPojo userObj, boolean fromLogin){
	// Partograph_CommonClass.responseAckCount = 0;
	// Partograph_CommonClass.movedToInputFolder = false;
	// SyncFunctions.syncUptoDate = false;
	// AsyncCallForUserAndSettings task = new
	// AsyncCallForUserAndSettings(fromLogin, userObj, dbh);
	// StartAsyncTaskInParallel(task);
	// }

	/** To call multiple AsyncTask, call this method */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void StartAsyncTaskInParallel(AsyncTask<String, Void, String> task) {
		try {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
				task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			else
				task.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 15May2017 Arpitha - v2.6 bug fixing
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public void notification1() {
		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
		notificationIntent.addCategory("android.intent.category.DEFAULT");

		PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent,
				PendingIntent.FLAG_CANCEL_CURRENT);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, 10);
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 0, broadcast);

	}
}
